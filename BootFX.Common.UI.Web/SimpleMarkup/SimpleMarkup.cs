// BootFX - Application framework for .NET applications
// 
// File: SimpleMarkup.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BootFX.Common;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for SimpleMarkup.
	/// </summary>
	[Obsolete("Deprecated - use BootCMS instead.")]
	public class SimpleMarkup : Label
	{
		/// <summary>
		/// Raised when a link needs to be formated.
		/// </summary>
		public event FormatLinkEventHandler FormatLink;
		
		/// <summary>
		/// Raised when an image URL needs to be expanded.
		/// </summary>
		public event UrlEventHandler ExpandImageUrl;
		
		private string _tdCssStyle;

		private string _heading1CssStyle;

		private string _heading2CssStyle;
 
		private string _heading3CssStyle;
 
		private string _heading4CssStyle;
 
		private string _heading5CssStyle;

		private string _textFilePath;
 
		/// <summary>
		/// Private field to support <c>LinkCssClass</c> property.
		/// </summary>
		private string _linkCssClass;
		
		/// <summary>
		/// Private field to support <c>LinkCssClass</c> property.
		/// </summary>
		private string _imageCssClass;
		
		public SimpleMarkup()
		{
			this._heading1CssStyle = "heading1";
			this._heading2CssStyle = "heading2";
			this._heading3CssStyle = "heading3";
			this._heading4CssStyle = "heading4";
			this._heading5CssStyle = "heading5";
			this._tdCssStyle = "bodytext";
			this.LinkCssClass = "link";
			this.ImageCssClass = string.Empty;
		}
 
		private WebSimpleMarkupRenderer CreateRenderer()
		{
			WebSimpleMarkupRenderer renderer = new WebSimpleMarkupRenderer();
			renderer.Heading1CssStyle = this.Heading1CssStyle;
			renderer.Heading2CssStyle = this.Heading2CssStyle;
			renderer.Heading3CssStyle = this.Heading3CssStyle;
			renderer.Heading4CssStyle = this.Heading4CssStyle;
			renderer.Heading5CssStyle = this.Heading5CssStyle;
			renderer.TdCssStyle = this.TdCssStyle;
			renderer.LinkCssClass = this.LinkCssClass;
			renderer.ImageCssClass = this.ImageCssClass;

			// content...
			renderer.ContentLinkPattern = this.ContentLinkPattern;

			// image...
			renderer.ExpandImageUrl += new UrlEventHandler(renderer_ExpandImageUrl);
			renderer.FormatLink += new FormatLinkEventHandler(renderer_FormatLink);

			// return...
			return renderer;
		}

		/// <summary>
		/// Sets the text from the given markup.
		/// </summary>
		/// <param name="rawMarkup"></param>
		public void SetHtml(string rawMarkup)
		{
			WebSimpleMarkupRenderer renderer = this.CreateRenderer();
			if(renderer == null)
				throw new InvalidOperationException("renderer is null.");

			// set...
			this.Text = renderer.GetHtml(rawMarkup);
		}
 
		private void UpdateText()
		{
			string text1 = this.ResolvedTextFilePath;
			if ((text1 != null) && (text1.Length > 0))
			{
				WebSimpleMarkupRenderer renderer1 = this.CreateRenderer();
				this.Text = renderer1.LoadHtml(this.ResolvedTextFilePath);
			}
			else
				this.Text = string.Empty;
		}

		[Description("Sets the CSS style for table td elements."), Category("Styles"), Browsable(true)]
		public string TdCssStyle
		{
			get
			{
				return this._tdCssStyle;
			}
			set
			{
				if (value != this._tdCssStyle)
				{
					this._tdCssStyle = value;
				}
			}
		}
 
		[Description("Sets the CSS style for heading level 1."), Category("Styles"), Browsable(true)]
		public string Heading1CssStyle
		{
			get
			{
				return this._heading1CssStyle;
			}
			set
			{
				if (value != this._heading1CssStyle)
				{
					this._heading1CssStyle = value;
				}
			}
		}
 
		[Description("Sets the CSS style for heading level 2."), Category("Styles"), Browsable(true)]
		public string Heading2CssStyle
		{
			get
			{
				return this._heading2CssStyle;
			}
			set
			{
				if (value != this._heading2CssStyle)
				{
					this._heading2CssStyle = value;
				}
			}
		}
 
		[Category("Styles"), Browsable(true), Description("Sets the CSS style for heading level 3.")]
		public string Heading3CssStyle
		{
			get
			{
				return this._heading3CssStyle;
			}
			set
			{
				if (value != this._heading3CssStyle)
				{
					this._heading3CssStyle = value;
				}
			}
		}
 
		[Browsable(true), Description("Sets the CSS style for heading level 4."), Category("Styles")]
		public string Heading4CssStyle
		{
			get
			{
				return this._heading4CssStyle;
			}
			set
			{
				if (value != this._heading4CssStyle)
				{
					this._heading4CssStyle = value;
				}
			}
		}
 
		[Category("Styles"), Browsable(true), Description("Sets the CSS style for heading level 5.")]
		public string Heading5CssStyle
		{
			get
			{
				return this._heading5CssStyle;
			}
			set
			{
				if (value != this._heading5CssStyle)
				{
					this._heading5CssStyle = value;
				}
			}
		}
 
		[Browsable(false)]
		public string ResolvedTextFilePath
		{
			get
			{
				string text1;
				HttpContext context1 = HttpContext.Current;
				if (context1 == null)
				{
					throw new InvalidOperationException("context is null.");
				}
				if (context1.Server == null)
				{
					throw new InvalidOperationException("context.Server is null.");
				}
				if (context1.Request == null)
				{
					throw new InvalidOperationException("context.Request is null.");
				}
				if ((this.TextFilePath == null) || (this.TextFilePath.Length == 0))
				{
					text1 = null;
				}
				else if (this.TextFilePath.StartsWith("~"))
				{
					text1 = context1.Server.MapPath(string.Format("{0}/{1}", context1.Request.ApplicationPath, this.TextFilePath.Substring(1)));
				}
				else
				{
					text1 = this.TextFilePath;
				}
				return text1;
			}
		}
 
		[Category("Appearance"), Browsable(true)]
		public string TextFilePath
		{
			get
			{
				return this._textFilePath;
			}
			set
			{
				if (value != this._textFilePath)
				{
					this._textFilePath = value;
					this.UpdateText();
				}
			}
		}

		/// <summary>
		/// Gets or sets the contentlinkpattern
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets the pattern used for image embedding.")]
		public string ImagePattern
		{
			get
			{
				object asObject = ViewState["imagepattern"];
				if(asObject == null)
					return null;
				else
					return (string)asObject;
			}
			set
			{
				// check to see if the value has changed...
				if(value != this.ImagePattern)
				{
					ViewState["imagepattern"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the contentlinkpattern
		/// </summary>
		[Browsable(true), Category("Behavior"), Description("Gets the pattern used for content linking.")]
		public string ContentLinkPattern
		{
			get
			{
				object asObject = ViewState["contentlinkpattern"];
				if(asObject == null)
					return null;
				else
					return (string)asObject;
			}
			set
			{
				// check to see if the value has changed...
				if(value != ContentLinkPattern)
				{
					ViewState["contentlinkpattern"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the linkcssclass
		/// </summary>
		[Description("Sets the CSS style for images."), Category("Styles"), Browsable(true)]
		public string ImageCssClass
		{
			get
			{
				return _imageCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _imageCssClass)
				{
					// set the value...
					_imageCssClass = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the linkcssclass
		/// </summary>
		[Description("Sets the CSS style for links."), Category("Styles"), Browsable(true)]
		public string LinkCssClass
		{
			get
			{
				return _linkCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _linkCssClass)
				{
					// set the value...
					_linkCssClass = value;
				}
			}
		}

		private void renderer_ExpandImageUrl(object sender, UrlEventArgs e)
		{
			this.OnExpandImageUrl(e);
		}
		
		/// <summary>
		/// Raises the <c>ExpandImageUrl</c> event.
		/// </summary>
		protected virtual void OnExpandImageUrl(UrlEventArgs e)
		{
			// raise...
			if(ExpandImageUrl != null)
				ExpandImageUrl(this, e);
		}

		private void renderer_FormatLink(object sender, FormatLinkEventArgs e)
		{
			this.OnFormatLink(e);
		}

		/// <summary>
		/// Raises the <c>FormatLink</c> event.
		/// </summary>
		protected virtual void OnFormatLink(FormatLinkEventArgs e)
		{
			// raise...
			if(FormatLink != null)
				FormatLink(this, e);
		}
	}
}
