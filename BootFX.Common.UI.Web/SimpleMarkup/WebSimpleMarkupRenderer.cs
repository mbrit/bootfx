// BootFX - Application framework for .NET applications
// 
// File: WebSimpleMarkupRenderer.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for WebSimpleMarkupRenderer.
	/// </summary>
	[Obsolete("Deprecated - use BootCMS instead.")]
	public class WebSimpleMarkupRenderer : SimpleMarkupRendererBase
	{
		public WebSimpleMarkupRenderer()
		{
		}

		/// <summary>
		/// Resolves a URL.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		protected override string ResolveUrl(string url, string pattern)
		{
			if(url == null || url.Length == 0)
				return string.Empty;

			// request...
			HttpContext context = HttpContext.Current;
			if(context == null)
				throw new InvalidOperationException("context is null.");
			if(context.Request == null)
				throw new InvalidOperationException("context.Request is null.");

			// starts with?
			if(url.StartsWith("~") || url.StartsWith("#"))
			{
				// get...
				char first = url[0];

				// split...
				url = url.Substring(1);
				while(url.StartsWith("/"))
					url = url.Substring(1);

				// check the hash first...
				if(first == '#')
				{
					// check...
					if(pattern == null)
						throw new InvalidOperationException("'pattern' is null.");
					if(pattern.Length == 0)
						throw new InvalidOperationException("'pattern' is zero-length.");

					// don't return it as we have have to resolve a tilde...
					url = string.Format(pattern, url);
					if(url == null)
						throw new InvalidOperationException("'url' is null.");
					if(url.Length == 0)
						throw new InvalidOperationException("'url' is zero-length.");

					// get...
					first = url[0];

					// split...
					while(url.StartsWith("/") && !(pattern.StartsWith("/")))
						url = url.Substring(1);
				}

				// tilde?
				if(first == '~')
				{
					// mangle...
					string path = context.Request.ApplicationPath;
					if(!(path.EndsWith("/")) && !(path.EndsWith("\\")) && !(url.StartsWith("\\")))
						path += "/";
					url = path + url;
				}
			}

			// return...
			return url;
		}
	}
}
