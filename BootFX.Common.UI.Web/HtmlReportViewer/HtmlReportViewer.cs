// BootFX - Application framework for .NET applications
// 
// File: HtmlReportViewer.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for HtmlReportViewer.
	/// </summary>
	public class HtmlReportViewer : Control
	{
		/// <summary>
		/// Private field to support <c>Report</c> property.
		/// </summary>
		private HtmlReport _report;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public HtmlReportViewer()
		{
		}

		/// <summary>
		/// Gets or sets the report
		/// </summary>
		public HtmlReport Report
		{
			get
			{
				return _report;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _report)
				{
					// set the value...
					_report = value;
				}
			}
		}

		/// <summary>
		/// Gets the HTML for the report.
		/// </summary>
		/// <returns></returns>
		public string GetReportHtml()
		{
			if(Report == null)
				throw new InvalidOperationException("Report is null.");

			// create...
			this.RebuildView();

			// render...
			using(StringWriter writer = new StringWriter())
			{
				// set up html...
				using(HtmlTextWriter html = new HtmlTextWriter(writer))
				{
					// render this control...
					this.Render(html);
				}

				// return...
				return writer.GetStringBuilder().ToString();
			}
		}

		/// <summary>
		/// Rebuilds the report view.
		/// </summary>
		private void RebuildView()
		{
			this.Controls.Clear();
			if(this.Report == null)
				return;

			// create a table...
			IrTable table = new IrTable();
			this.Controls.Add(table);
			table.AddRow(this.Report.ToString());

			// data...
			Panel panel = new Panel();
			table.AddRow(null, panel);
			this.Report.RebuildData(panel);
		}
	}
}
