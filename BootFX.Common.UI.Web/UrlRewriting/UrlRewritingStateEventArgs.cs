// BootFX - Application framework for .NET applications
// 
// File: UrlRewritingStateEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	public delegate void UrlRewritingStateEventHandler(object sender, UrlRewritingStateEventArgs e);

	/// <summary>
	/// Defines an event args class that accepts a <c>UrlRewritingState</c> instance.
	/// </summary>
	[Serializable()]
	public class UrlRewritingStateEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <see cref="state"/> property.
		/// </summary>
		private UrlRewritingState _state;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="state"></param>
		internal UrlRewritingStateEventArgs(UrlRewritingState state)
		{
			if(state == null)
				throw new ArgumentNullException("state");
			_state = state;
		}

		/// <summary>
		/// Gets the state.
		/// </summary>
		public UrlRewritingState State
		{
			get
			{
				return _state;
			}
		}
	}
}
