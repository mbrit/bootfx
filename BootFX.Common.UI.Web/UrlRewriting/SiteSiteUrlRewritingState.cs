// BootFX - Application framework for .NET applications
// 
// File: SiteSiteUrlRewritingState.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;	
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>UrlRewriteState</c>.
	/// </summary>
	[Serializable()]
	public class SubSiteUrlRewritingState : UrlRewritingState
	{
		/// <summary>
		/// Private field to support <see cref="LocalScriptName"/> property.
		/// </summary>
		private string _localScriptName;
		
		/// <summary>
		/// Private field to support <see cref="QualifiedScriptName"/> property.
		/// </summary>
		private string _qualifiedScriptName;

		/// <summary>
		/// Private field to support <see cref="SubSiteName"/> property.
		/// </summary>
		private string _subSiteName;
		
		internal SubSiteUrlRewritingState(string url, string applicationRoot) : base(url, applicationRoot)
		{
			this.Initialize();
		}

		protected SubSiteUrlRewritingState(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.Initialize();
		}

		private void Initialize()
		{
			// check...
			if(Url == null)
				throw new InvalidOperationException("'Url' is null.");
			if(Url.Length == 0)
				throw new InvalidOperationException("'Url' is zero-length.");
			if(ApplicationRoot == null)
				throw new InvalidOperationException("'ApplicationRoot' is null.");
			if(ApplicationRoot.Length == 0)
				throw new InvalidOperationException("'ApplicationRoot' is zero-length.");

			// find the first slash...
			int index = this.UrlWithoutQuery.IndexOf("/");
			if(index != -1)
			{
				_subSiteName = this.UrlWithoutQuery.Substring(0, index);
				_localScriptName = this.UrlWithoutQuery.Substring(index + 1);
			}
			else
				_localScriptName = this.UrlWithoutQuery;

			// setup the script name...
			_qualifiedScriptName = this.ApplicationRoot;
			if(!(_qualifiedScriptName.EndsWith("/")))
				_qualifiedScriptName += "/";
			_qualifiedScriptName += _localScriptName;
		}

		/// <summary>
		/// Gets the SubSiteName.
		/// </summary>
		public string SubSiteName
		{
			get
			{
				return _subSiteName;
			}
		}
		
		/// <summary>
		/// Gets the scriptname.
		/// </summary>
		public string QualifiedScriptName
		{
			get
			{
				return _qualifiedScriptName;
			}
		}

		/// <summary>
		/// Gets the localscriptname.
		/// </summary>
		public string LocalScriptName
		{
			get
			{
				return _localScriptName;
			}
		}

		public bool HasSubSiteName
		{
			get
			{
				if(this.SubSiteName != null && this.SubSiteName.Length > 0)
					return true;
				else
					return false;
			}
		}
	}
}
