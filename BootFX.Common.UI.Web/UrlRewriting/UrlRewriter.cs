// BootFX - Application framework for .NET applications
// 
// File: UrlRewriter.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.Data;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines a helper class that acts as a friendly URL rewriter.
	/// </summary>
	/// <remarks>
	/// 
	/// To use this, put this handler in the <c>system.web</c> section of your <c>web.config</c>:
	/// 
	/// 	&lt;httpModules&gt;
	///			&lt;add type="BootFX.Common.UI.Web.UrlRewriter, BootFX.Common.UI.Web" name="BootFXUrlRewritePartition"&gt;&lt;/add&gt;
	///		&lt;/httpModules&gt;
	///		
	///	 ...replacing the type reference with a reference to your own type that extends UrlRewriter.
	///	 
	/// </remarks>
	public abstract class UrlRewriter : HttpContextAwareBase, IHttpModule, IDisposable
	{		
		/// <summary>
		/// Raised when a request is made.
		/// </summary>
		public event UrlRewritingStateEventHandler BeginRequest;
		
		/// <summary>
		/// Raised when the request has finished.
		/// </summary>
		public event EventHandler EndRequest;
		
		/// <summary>
		/// Raised when an error occurs.
		/// </summary>
		public event UrlRewritingErrorEventHandler Error;
		
		/// <summary>
		/// Private field to support <see cref="ParameterName"/> property.
		/// </summary>
		private string _parameterName;

		/// <summary>
		/// Private field to support <see cref="ParameterType"/> property.
		/// </summary>
		private Type _parameterType;
		
		protected UrlRewriter() : this(RequestParameterPartitionIdProvider.DefaultParameterName, RequestParameterPartitionIdProvider.DefaultParameterType)
		{
		}

		protected UrlRewriter(string parameterName, Type parameterType)
		{
			if(parameterName == null)
				throw new ArgumentNullException("parameterName");
			if(parameterName.Length == 0)
				throw new ArgumentOutOfRangeException("'parameterName' is zero-length.");
			if(parameterType == null)
				throw new ArgumentNullException("parameterType");
			
			// set...
			_parameterName = parameterName;
			_parameterType = parameterType;
		}

		~UrlRewriter()
		{
			this.Dispose(false);
		}

		public void Init(HttpApplication context)
		{
			if(context == null)
				throw new ArgumentNullException("context");
			context.BeginRequest += new EventHandler(context_BeginRequest);

			// mbr - 15-01-2008 - added end request...
			context.EndRequest += new EventHandler(context_EndRequest);

            // mbr - 2010-01-20 - added notification of initialization...
            Initialized(context);
		}

        /// <summary>
        /// Called when the rewriter has been initialized.
        /// </summary>
        /// <remarks>Gives consumers an opportunity to subscribe to other events on the context.</remarks>
        /// <param name="context"></param>
        // mbr - 2010-01-20 - added...
        protected virtual void Initialized(HttpApplication context)
        {
        }

		void IHttpModule.Dispose()
		{
			this.Dispose(true);
		}

		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		protected void Dispose(bool explicitCall)
		{
			GC.SuppressFinalize(this);
		}

		private void context_BeginRequest(object sender, EventArgs e)
		{
			HttpApplication app = null;
			string url = null;
			UrlRewritingState state = null;
			try
			{
				app = sender as HttpApplication;
				if(app != null)
				{
					url = app.Request.Url.PathAndQuery;
					if(url != null && url.Length > 0)
						this.ProcessUrl(app, url, ref state);
					else
						throw new InvalidOperationException("URL is null or zero.length.");
				}
				else
					throw new InvalidOperationException("The application is null.");
			}
			catch(ThreadAbortException)
			{
				// no-op...
			}
			catch(Exception ex)
			{
				this.OnError(new UrlRewritingErrorEventArgs(state, ex));
			}
		}

		/// <summary>
		/// Returns true if the given URL should be re-written.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		protected virtual bool ShouldRewriteUrl(UrlRewritingState state)
		{
			return true;
		}

		/// <summary>
		/// Processes the URL.
		/// </summary>
		/// <param name="app"></param>
		/// <param name="url"></param>
		protected abstract string GetRewrittenUrl(HttpApplication app, UrlRewritingState state);

		private void ProcessUrl(HttpApplication app, string url, ref UrlRewritingState state)
		{
			if(app == null)
				throw new ArgumentNullException("app");
			if(app.Request.ApplicationPath == null)
				throw new ArgumentNullException("app.Request.ApplicationPath");			
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");

			// get the state...
			state = this.UrlToState(app, url, app.Request.ApplicationPath);
			if(state == null)
				throw new InvalidOperationException("state is null.");

			// should we do it?
			if(!(this.ShouldRewriteUrl(state)))
				return;

			// get it...
			string newUrl = this.GetRewrittenUrl(app, state);
			if(newUrl != null && newUrl.Length > 0)
				app.Context.RewritePath(newUrl);

			// mbr - 15-01-2008 - event...
			this.OnBeginRequest(new UrlRewritingStateEventArgs(state));
		}

		/// <summary>
		/// Raises the <c>BeginRequest</c> event.
		/// </summary>
		protected virtual void OnBeginRequest(UrlRewritingStateEventArgs e)
		{
			// raise...
			if(BeginRequest != null)
				BeginRequest(this, e);
		}

		/// <summary>
		/// Gets the parametername.
		/// </summary>
		internal string ParameterName
		{
			get
			{
				return _parameterName;
			}
		}

		/// <summary>
		/// Gets the parametertype.
		/// </summary>
		internal Type ParameterType
		{
			get
			{
				return _parameterType;
			}
		}

		/// <summary>
		/// Gets the rewriting state for a URL.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		protected virtual UrlRewritingState UrlToState(HttpApplication app, string url, string applicationRoot)
		{
			return new UrlRewritingState(url, applicationRoot);
		}

		/// <summary>
		/// Raises the <c>Error</c> event.
		/// </summary>
		protected virtual void OnError(UrlRewritingErrorEventArgs e)
		{
			if(this.Log.IsWarnEnabled)
			{
				string originalUrl = "(None)";
				if(e.State != null)
					originalUrl = e.State.RawUrl;
				this.Log.Warn(string.Format("A URL rewriting error occurred: {0}.\r\nOriginal URL: {1}", e.Message, originalUrl), e.Exception);
			}

			// raise...
			if(Error != null)
				Error(this, e);
		}

		private void context_EndRequest(object sender, EventArgs e)
		{
			// mbr - 15-01-2008 - raise an event to say we're done...
			this.OnEndRequest();
		}

		/// <summary>
		/// Raises the <c>EndRequest</c> event.
		/// </summary>
		private void OnEndRequest()
		{
			OnEndRequest(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>EndRequest</c> event.
		/// </summary>
		protected virtual void OnEndRequest(EventArgs e)
		{
			// raise...
			if(EndRequest != null)
				EndRequest(this, e);
		}
	}
}
