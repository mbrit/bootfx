// BootFX - Application framework for .NET applications
// 
// File: PartitioningSubSiteUrlRewriter.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines a helper class that acts as both a friendly URL re-writer and a partition ID provider.
	/// </summary>
	/// <remarks>This class is useful if you want a site that implements sub-site and also want to use BootFX's built
	/// in partitioning.  This class assumes that you want to use a single Int64 to identify the users partition ID from
	/// the sub-site.</remarks>
	public abstract class PartitioningSubSiteUrlRewriter : SubSiteUrlRewriter, IPartitionIdProvider
	{		
		/// <summary>
		/// Private field to support <c>InnerIdProvider</c> property.
		/// </summary>
		private IPartitionIdProvider _innerIdProvider;
		
		/// <summary>
		/// Constructor.
		/// </summary>
        // mbr - 2010-01-19 - added flags...
        protected PartitioningSubSiteUrlRewriter(PartitioningSetupFlags flags)
		{
			this.Initialize(flags);
		}

        // mbr - 2010-01-19 - added flags...
        protected PartitioningSubSiteUrlRewriter(string parameterName, Type parameterType, PartitioningSetupFlags flags) 
            : base(parameterName, parameterType)
		{
			this.Initialize(flags);
		}

		private void Initialize(PartitioningSetupFlags flags)
		{
			// configure and set the provider...
			_innerIdProvider = new RequestParameterPartitionIdProvider(this.ParameterName, this.ParameterType);

            // mbr - 2010-01-19 - added...
            if((int)(flags & PartitioningSetupFlags.SkipDatabasePartitioningSetup) == 0)
			    PartitionIdProvider.DefaultProvider = this;
		}

		/// <summary>
		/// Gets the inneridprovider.
		/// </summary>
		private IPartitionIdProvider InnerIdProvider
		{
			get
			{
				// returns the value...
				return _innerIdProvider;
			}
		}

		// mbr - 04-09-2007 - for c7 - changed.
		//		public object[] GetPartitionIdsForReading()
		public object[] GetPartitionIdsForReading(EntityType et)
		{
			if(InnerIdProvider == null)
				throw new InvalidOperationException("InnerIdProvider is null.");
			return this.InnerIdProvider.GetPartitionIdsForReading(et);
		}

		// mbr - 04-09-2007 - for c7 - changed.
//		public object GetPartitionIdForWriting()
		public object GetPartitionIdForWriting(object entity)
		{
			if(InnerIdProvider == null)
				throw new InvalidOperationException("InnerIdProvider is null.");
			return this.InnerIdProvider.GetPartitionIdForWriting(entity);
		}
	}
}
