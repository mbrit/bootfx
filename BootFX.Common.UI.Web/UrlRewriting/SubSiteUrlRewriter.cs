// BootFX - Application framework for .NET applications
// 
// File: SubSiteUrlRewriter.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>SubSiteUrlRewriter</c>.
	/// </summary>
	public abstract class SubSiteUrlRewriter : UrlRewriter
	{
		/// <summary>
		/// Private field to support <c>IsFolder</c> property.
		/// </summary>
		private static Lookup _isRealFolder;
		
		/// <summary>
		/// Private field to support <c>SiteIdRegex</c> property.
		/// </summary>
		// &?siteid=(?<id>[^&]*)
		private Regex _subSiteIdRegex;

		protected SubSiteUrlRewriter() : base()
		{
			this.Initialize();
		}

		protected SubSiteUrlRewriter(string parameterName, Type parameterType) : base(parameterName, parameterType)
		{
			this.Initialize();
		}

		private void Initialize()
		{
			// regex...
			_subSiteIdRegex = new Regex(string.Format(@"&?{0}=(?<id>[^&]*)", this.ParameterName), RegexOptions.Singleline | RegexOptions.IgnoreCase);
		}

		static SubSiteUrlRewriter()
		{
			_isRealFolder = new Lookup();
			_isRealFolder.CreateItemValue += new CreateLookupItemEventHandler(_isRealFolder_CreateItemValue);
		}

		protected override UrlRewritingState UrlToState(HttpApplication app, string url, string applicationRoot)
		{
			if(applicationRoot == null)
				throw new ArgumentNullException("applicationRoot");
			if(applicationRoot.Length == 0)
				throw new ArgumentOutOfRangeException("'applicationRoot' is zero-length.");
			
			return new SubSiteUrlRewritingState(url, applicationRoot);
		}

		/// <summary>
		/// Gets the partition name as an object.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public abstract object SubSiteNameToObject(string name, UrlRewritingState state);

		private void SplitQueryString(string s, IDictionary values)
		{
			if(s == null)
				throw new ArgumentNullException("s");
			if(s.Length == 0)
				throw new ArgumentOutOfRangeException("'s' is zero-length.");
			if(values == null)
				throw new ArgumentNullException("values");
			
			int num1 = (s != null) ? s.Length : 0;
			for (int num2 = 0; num2 < num1; num2++)
			{
				int num3 = num2;
				int num4 = -1;
				while (num2 < num1)
				{
					char ch1 = s[num2];
					if (ch1 == '=')
					{
						if (num4 < 0)
						{
							num4 = num2;
						}
					}
					else if (ch1 == '&')
					{
						break;
					}
					num2++;
				}
				string text1 = null;
				string text2 = null;
				if (num4 >= 0)
				{
					text1 = s.Substring(num3, num4 - num3);
					text2 = s.Substring(num4 + 1, (num2 - num4) - 1);
				}
				else
				{
					text2 = s.Substring(num3, num2 - num3);
				}

				values[text1] = text2;

				if ((num2 == (num1 - 1)) && (s[num2] == '&'))
				{
//					this.Add(null, "");
				}
			}
		}

		protected override string GetRewrittenUrl(HttpApplication app, UrlRewritingState baseState)
		{
			if(app == null)
				throw new ArgumentNullException("app");
			if(baseState == null)
				throw new ArgumentNullException("baseState");
			
			// convert...
			SubSiteUrlRewritingState state = (SubSiteUrlRewritingState)baseState;

			// delta...
			string root = app.Request.ApplicationPath;
			if(root == null)
				throw new InvalidOperationException("root is null.");

			// get...
			if(!(state.HasSubSiteName))
				throw new SubSiteNameNotSpecifiedException("The sub-site name was not specified.");

			// get by name...
			object subSiteId = this.SubSiteNameToObject(state.SubSiteName, state);
			if(subSiteId == null)
				throw new InvalidOperationException("subSiteId is null.");

			// builder...
			StringBuilder builder = new StringBuilder();
			builder.Append(state.QualifiedScriptName);

			// mbr - 09-11-2005 - rejigged to load the values from the query string and patch in any changes...
			// this allows extravalues to be set to whatever should end up in the final string.
			if(state.HasExtraValues)
			{
				// create a master list...
				IDictionary values = CollectionsUtil.CreateCaseInsensitiveHashtable();

				// do the values...
				if(state.QueryString != null && state.QueryString.Length > 0)
					this.SplitQueryString(state.QueryString, values);

				// add the extra ones...
				foreach(DictionaryEntry entry in state.ExtraValues)
					values[entry.Key] = entry.Value;

				// set...
				values[this.ParameterName] = subSiteId;

				// walk...
				bool first = true;
				foreach(DictionaryEntry entry in values)
				{
					if(first)
					{
						builder.Append("?");
						first = false;
					}
					else
						builder.Append("&");
					builder.Append(ConversionHelper.ToString(entry.Key, Cultures.System));
					builder.Append("=");
					builder.Append(ConversionHelper.ToString(entry.Value, Cultures.System));
				}
			}
			else
			{
				// query...
				string query = state.QueryString;
				if(query == null || query.Length == 0)
				{
					builder.Append("?");
					builder.Append(this.ParameterName);
					builder.Append("=");
					builder.Append(subSiteId);
				}
				else
				{
					builder.Append("?");

					// do we have it already in the query string?
					if(SubSiteIdRegex == null)
						throw new InvalidOperationException("SubSiteIdRegex is null.");
					Match match = SubSiteIdRegex.Match(query);
					if(match.Success)
					{
						string before = query.Substring(0, match.Index);
						string after = query.Substring(match.Index + match.Length);
						builder.Append(before);
						if(before.Length > 0)
							builder.Append("&");
						builder.Append(this.ParameterName);
						builder.Append("=");
						builder.Append(subSiteId);
						builder.Append(after);
					}
					else
					{
						// add the query, then add our bit...
						builder.Append(query);
						builder.Append("&");
						builder.Append(this.ParameterName);
						builder.Append("=");
						builder.Append(subSiteId);
					}
				}
			}

			// rewrite...
			return builder.ToString();
		}

		/// <summary>
		/// Gets the siteidregex.
		/// </summary>
		private Regex SubSiteIdRegex
		{
			get
			{
				// returns the value...
				return _subSiteIdRegex;
			}
		}

		/// <summary>
		/// Gets the isfolder.
		/// </summary>
		private static Lookup IsRealFolder
		{
			get
			{
				// returns the value...
				return _isRealFolder;
			}
		}

		protected override bool ShouldRewriteUrl(UrlRewritingState state)
		{
			if(state == null)
				throw new ArgumentNullException("state");
			
			// mbr - 09-11-2005 - base...
			if(!(base.ShouldRewriteUrl(state)))
				return false;

			// check...
			if(!(state is SubSiteUrlRewritingState))
				throw new NotSupportedException(string.Format("The state '{0}' is not supported.", state.GetType()));

			// get...
			SubSiteUrlRewritingState rewriteState = (SubSiteUrlRewritingState)state;

			// mbr - 17-02-2008 - if we have a .axd file, we can't rewrite it...
			if(rewriteState.UrlWithoutQuery.ToLower().EndsWith(".axd"))
				return false;

			// if we don't have a subsite, give up...
			if(rewriteState.SubSiteName == null || rewriteState.RawUrl.Length == 0)
				return true;

			// is it a real folder?
			string mapped = state.ApplicationRoot;
			if(!(mapped.EndsWith("/")) && !(rewriteState.SubSiteName.StartsWith("/")))
				mapped += "/";
			mapped += rewriteState.SubSiteName;

			// real?
			bool isReal = (bool)IsRealFolder[mapped];
			if(isReal)
				return false;
			else
				return true;
		}

		private static void _isRealFolder_CreateItemValue(object sender, CreateLookupItemEventArgs e)
		{
			HttpContext context = HttpContext.Current;
			if(context == null)
				throw new InvalidOperationException("context is null.");
			if(context.Server == null)
				throw new InvalidOperationException("context.Server is null.");

			// map it...
			string mapped = (string)e.Key;
			mapped = context.Server.MapPath(mapped);
			
			// return...
			e.NewValue = Directory.Exists(mapped);
		}

		/// <summary>
		/// Expands the URL, using the request objects in the current HTTP context.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="subsiteName"></param>
		/// <returns></returns>
		public static string ExpandUrl(string url, string subsiteName)
		{
			if(HttpContext.Current == null)
				throw new InvalidOperationException("HttpContext.Current is null.");
			if(HttpContext.Current.Request == null)
				throw new InvalidOperationException("HttpContext.Current.Request is null.");

			// defer...
			return ExpandUrl(HttpContext.Current.Request, url, subsiteName);
		}

		/// <summary>
		/// Expands the URL using the given request and response objects.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="response"></param>
		/// <param name="url"></param>
		/// <param name="subsiteName"></param>
		/// <returns></returns>
		public static string ExpandUrl(HttpRequest request, string url, string subsiteName)
		{
			if(request == null)
				throw new ArgumentNullException("request");
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			if(subsiteName == null)
				throw new ArgumentNullException("subsiteName");
			if(subsiteName.Length == 0)
				throw new ArgumentOutOfRangeException("'subsiteName' is zero-length.");
		
			// starts?
			if(!(url.StartsWith("~")))
				throw new InvalidOperationException(string.Format("The URL '{0}' does not start with a tilde.", url));

			// expand...
			string newUrl = request.ApplicationPath;
			newUrl = HttpHelper.CombineUrlParts(newUrl, subsiteName);
			newUrl = HttpHelper.CombineUrlParts(newUrl, url.Substring(1));

			// return...
			return newUrl;
		}
	}
}
