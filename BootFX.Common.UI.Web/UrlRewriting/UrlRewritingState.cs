// BootFX - Application framework for .NET applications
// 
// File: UrlRewritingState.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;	
using System.Collections;
using System.Collections.Specialized;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>UrlRewriteState</c>.
	/// </summary>
	[Serializable()]
	public class UrlRewritingState : ISerializable
	{
		/// <summary>
		/// Private field to support <see cref="ExtraValues"/> property.
		/// </summary>
		private IDictionary _extraValues;
		
		/// <summary>
		/// Private field to support <see cref="RawUrl"/> property.
		/// </summary>
		private string _rawUrl;
		
		/// <summary>
		/// Private field to support <see cref="QueryString"/> property.
		/// </summary>
		private string _queryString;
		
		/// <summary>
		/// Private field to support <see cref="ApplicationRoot"/> property.
		/// </summary>
		private string _applicationRoot;
		
		private string _url;

		private string _urlWithoutQuery;
		
		protected internal UrlRewritingState(string url, string applicationRoot)
		{
			this.Initialize(url, applicationRoot);
		}

		protected UrlRewritingState(SerializationInfo info, StreamingContext context)
		{
			string url = info.GetString("_rawUrl");
			string applicationRoot = info.GetString("_applicationRoot");

			// init...
			this.Initialize(url, applicationRoot);

			// set extra values...
			_extraValues = (IDictionary)info.GetValue("_extraValues", typeof(IDictionary));
		}

		private void Initialize(string url, string applicationRoot)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			if(applicationRoot == null)
				throw new ArgumentNullException("applicationRoot");
			
			// root...
			_applicationRoot = applicationRoot;

			// set...
			_rawUrl = url.Replace("\\", "/");
			_url = _rawUrl.Substring(applicationRoot.Length);

			// remove the slash...
			while(_url.StartsWith("/"))
				_url = _url.Substring(1);

			// no query...
			_urlWithoutQuery = _url;
			int index = _urlWithoutQuery.IndexOf("?");
			if(index != -1)
			{
				_queryString = _urlWithoutQuery.Substring(index + 1);
				_urlWithoutQuery = _urlWithoutQuery.Substring(0, index);
			}
		}

		/// <summary>
		/// Gets the querystring.
		/// </summary>
		public string QueryString
		{
			get
			{
				return _queryString;
			}
		}

		/// <summary>
		/// Gets the rawurlwithoutquery.
		/// </summary>
		public string UrlWithoutQuery
		{
			get
			{
				return _urlWithoutQuery;
			}
		}
		
		/// <summary>
		/// Gets the rawurl.
		/// </summary>
		public string Url
		{
			get
			{
				return _url;
			}
		}

		/// <summary>
		/// Gets the applicationroot.
		/// </summary>
		public string ApplicationRoot
		{
			get
			{
				return _applicationRoot;
			}
		}

		/// <summary>
		/// Gets the rawurl.
		/// </summary>
		public string RawUrl
		{
			get
			{
				return _rawUrl;
			}
		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_rawUrl", _rawUrl);
			info.AddValue("_applicationRoot", _applicationRoot);

			// mbr - 09-11-2005 - added extra...
			info.AddValue("_extraValues", _extraValues);
		}

		/// <summary>
		/// Gets the extravalues.
		/// </summary>
		public IDictionary ExtraValues
		{
			get
			{
				if(_extraValues == null)
					_extraValues = CollectionsUtil.CreateCaseInsensitiveHashtable();
				return _extraValues;
			}
		}

		public bool HasExtraValues
		{
			get
			{
				if(_extraValues == null)
					return false;
				else
					return true;
			}
		}
	}
}
