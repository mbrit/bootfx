// BootFX - Application framework for .NET applications
// 
// File: UrlRewritingErrorEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Runtime.Serialization;

namespace BootFX.Common.UI.Web
{
	public delegate void UrlRewritingErrorEventHandler(object sender, UrlRewritingErrorEventArgs e);

	/// <summary>
	/// Defines an instance of <c>FriendlyUrlRewritingErrorEventArgs</c>.
	/// </summary>
	[Serializable()]
	public class UrlRewritingErrorEventArgs : EventArgs, ISerializable
	{
		/// <summary>
		/// Private field to support <see cref="ExceptionType"/> property.
		/// </summary>
		private Type _exceptionType;
		
		/// <summary>
		/// Private field to support <see cref="Message"/> property.
		/// </summary>
		private string _message;
		
		/// <summary>
		/// Private field to support <see cref="State"/> property.
		/// </summary>
		private UrlRewritingState _state;
		
		/// <summary>
		/// Private field to support <see cref="Exception"/> property.
		/// </summary>
		private Exception _exception;
		
		/// <summary>
		/// Constructor,
		/// </summary>
		/// <param name="url"></param>
		/// <param name="error"></param>
		internal UrlRewritingErrorEventArgs(UrlRewritingState state, Exception exception)
		{
			_state = state;
			_exception = exception;

			// message...
			if(_exception != null)
			{
				_exceptionType = this.Exception.GetType();
				_message = string.Format("{0} ({1})", this.Exception.Message, _exceptionType.FullName);
			}
			else
			{
				_message = "An unknown error occurred.";
				_exceptionType = null;
			}
		}

		protected UrlRewritingErrorEventArgs(SerializationInfo info, StreamingContext context)
		{
			_state = (UrlRewritingState)info.GetValue("_state", typeof(UrlRewritingState));
			_message = info.GetString("_message");
			_exceptionType = (Type)info.GetValue("_exceptionType", typeof(Type));
		}

		/// <summary>
		/// Gets the exception.
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _exception;
			}
		}

		/// <summary>
		/// Gets this object as a base-64 string.
		/// </summary>
		/// <returns></returns>
		public string ToBase64String()
		{
			return SerializationHelper.SerializeToBase64String(this);
		}

		/// <summary>
		/// Rehydrates this object back from a base-64 string.
		/// </summary>
		/// <param name="buf"></param>
		/// <returns></returns>
		public static UrlRewritingErrorEventArgs FromBase64String(string buf)
		{
			if(buf == null)
				throw new ArgumentNullException("buf");
			if(buf.Length == 0)
				throw new ArgumentOutOfRangeException("'buf' is zero-length.");
			
			// return...
			return (UrlRewritingErrorEventArgs)SerializationHelper.Deserialize(buf);
		}

		/// <summary>
		/// Gets the state.
		/// </summary>
		internal UrlRewritingState State
		{
			get
			{
				return _state;
			}
		}

		/// <summary>
		/// Gets the message.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_state", _state);
			info.AddValue("_message", _message);
			info.AddValue("_exceptionType", _exceptionType);
		}

		/// <summary>
		/// Gets the exceptiontype.
		/// </summary>
		public Type ExceptionType
		{
			get
			{
				return _exceptionType;
			}
		}
	}
}
