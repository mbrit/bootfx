// BootFX - Application framework for .NET applications
// 
// File: Level.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines heading and summary levels.
	/// </summary>
	public enum Level
	{
		Level1 = 0,
		Level2 = 1,
		Level3 = 2,
		Level4 = 3,
		Level5 = 4
	}
}
