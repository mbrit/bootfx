// BootFX - Application framework for .NET applications
// 
// File: Spacing.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines spacing sizes.
	/// </summary>
	public enum Spacing
	{
		Small = 5,
		Medium = 15,
		Large = 30
	}
}
