// BootFX - Application framework for .NET applications
// 
// File: IrTable.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for IrTable.
	/// </summary>
	public class IrTable : Table
	{
		/// <summary>
		/// Private field to support <c>HeadingCssClass</c> property.
		/// </summary>
		private string _headingCssClass;
		
		/// <summary>
		/// Private field to support <c>DefaultFloatingPointFormat</c> property.
		/// </summary>
		private string _defaultFloatingPointFormat = "n2";
		
		/// <summary>
		/// Raised when data needs to be formatted.
		/// </summary>
		public event FormatEventHandler FormatData;
		
		/// <summary>
		/// Private field to support <c>DataRowOrdinal</c> property.
		/// </summary>
		private int _dataRowOrdinal = 0;
		
		/// <summary>
		/// Private field to support <c>DefaultDateTimeFormat</c> property.
		/// </summary>
		private string _defaultDateTimeFormat;
		
		/// <summary>
		/// Private field to support <c>DataHeaderCssClass</c> property.
		/// </summary>
		private string _dataHeaderCssClass = "dataheader";

		/// <summary>
		/// Private field to support <c>DataRowCssClass</c> property.
		/// </summary>
		private string _dataRowCssClass = "datarow";

		/// <summary>
		/// Private field to support <c>AlternateDataRowCssClass</c> property.
		/// </summary>
		private string _alternateDataRowCssClass = "alternatedatarow";

		/// <summary>
		/// Private field to support <c>DataSummaryCssClass</c> property.
		/// </summary>
		private string _dataSummaryCssClass = "datasummary";
		
		/// <summary>
		/// Private field to support <c>DefaultCellCssClass</c> property.
		/// </summary>
		private string _defaultCellCssClass = "bodytext";

		/// <summary>
		/// Private field to support <c>Heading1CssClass</c> property.
		/// </summary>
		private string _heading1CssClass = "heading1";

		/// <summary>
		/// Private field to support <c>Heading2CssClass</c> property.
		/// </summary>
		private string _heading2CssClass = "heading2";

		/// <summary>
		/// Private field to support <c>Heading3CssClass</c> property.
		/// </summary>
		private string _heading3CssClass = "heading3";

		/// <summary>
		/// Private field to support <c>Heading4CssClass</c> property.
		/// </summary>
		private string _heading4CssClass = "heading4";

		/// <summary>
		/// Private field to support <c>Heading5CssClass</c> property.
		/// </summary>
		private string _heading5CssClass = "heading5";
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public IrTable()
		{
		}

		/// <summary>
		/// Adds a row.
		/// </summary>
		/// <returns></returns>
		public TableRow AddRow()
		{
			TableRow row = this.CreateRow();
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Creates a row.
		/// </summary>
		/// <returns></returns>
		public TableRow CreateRow()
		{
			return new TableRow();
		}

		/// <summary>
		/// Adds a row.
		/// </summary>
		/// <param name="cssClass"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableRow AddRow(string cssClass, object value)
		{
			return this.AddRow(cssClass, new object[] { value });
		}

		/// <summary>
		/// Adds a row.
		/// </summary>
		/// <param name="cssClass"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableRow AddRow(string cssClass, params object[] values)
		{
			TableRow row = this.CreateRow(cssClass, values);
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Adds a row with the given value.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableRow CreateRow(string cssClass, object value)
		{
			return this.CreateRow(cssClass, new object[] { value });
		}

		/// <summary>
		/// Adds a row and creates cells with the given parameters
		/// </summary>
		/// <param name="texts"></param>
		/// <returns></returns>
		public TableRow CreateRow(string cssClass, params object[] values)
		{
			return this.CreateRow(cssClass, false, values);
		}

		/// <summary>
		/// Creates a row.
		/// </summary>
		/// <param name="cssCalss"></param>
		/// <param name="forData"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		private TableRow CreateRow(string cssClass, bool forData, params object[] values)
		{
			TableRow row = this.AddRow();
			foreach(object value in values)
			{
				TableCell cell = this.CreateCell(cssClass, value, forData);
				row.Cells.Add(cell);
			}

			// return...
			return row;
		}

		/// <summary>
		/// Adds a header row.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow AddDataHeaderRow(Level level, params object[] values)
		{
			TableRow row = this.CreateDataHeaderRow(level, values);
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Adds a header row.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow AddDataSummaryRow(Level level, params object[] values)
		{
			TableRow row = this.CreateDataSummaryRow(level, values);
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Adds a header row.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow AddDataRow(params object[] values)
		{
			TableRow row = this.CreateDataRow(values);
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Gets or sets the defaultcellcssclass
		/// </summary>
		public string DefaultCellCssClass
		{
			get
			{
				return _defaultCellCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultCellCssClass)
				{
					// set the value...
					_defaultCellCssClass = value;
				}
			}
		}

		/// <summary>
		/// Gets the CSS class for the given header level.
		/// </summary>
		/// <param name="level"></param>
		/// <returns></returns>
		private string GetDataHeaderCssClass(Level level)
		{
			return this.DataHeaderCssClass;
		}

		/// <summary>
		/// Gets the CSS class for the given header level.
		/// </summary>
		/// <param name="level"></param>
		/// <returns></returns>
		private string GetDataSummaryCssClass(Level level)
		{
			return this.DataSummaryCssClass;
		}

		/// <summary>
		/// Creates a header row.
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow CreateDataHeaderRow(Level level, params object[] values)
		{
			string cssClass = this.GetDataHeaderCssClass(level);
			return this.CreateRow(cssClass, true, values);
		}

		/// <summary>
		/// Creates a header row.
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow CreateDataSummaryRow(Level level, params object[] values)
		{
			string cssClass = this.GetDataSummaryCssClass(level);
			return this.CreateRow(cssClass, true, values);
		}

		/// <summary>
		/// Creates a data row.
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		public TableRow CreateDataRow(params object[] values)
		{
			string cssClass = this.DataRowCssClass;
			if(this.DataRowOrdinal % 2 == 1)
				cssClass = this.AlternateDataRowCssClass;
			TableRow row = this.CreateRow(cssClass, true, values);
			this.DataRowOrdinal++;
			return row;
		}

		/// <summary>
		/// Gets or sets the datarowordinal
		/// </summary>
		private int DataRowOrdinal
		{
			get
			{
				return _dataRowOrdinal;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataRowOrdinal)
				{
					// set the value...
					_dataRowOrdinal = value;
				}
			}
		}

		/// <summary>
		/// Creates a data cell.
		/// </summary>
		/// <param name="cssClass"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableCell CreateDataCell(string cssClass, object value)
		{
			return this.CreateCell(cssClass, value, true);
		}

		/// <summary>
		/// Adds a cell with the given value.
		/// </summary>
		/// <param name="row"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableCell CreateCell(string cssClass, object value)
		{
			return this.CreateCell(cssClass, value, false);
		}

		/// <summary>
		/// Creates a cell.
		/// </summary>
		/// <param name="cssClass"></param>
		/// <param name="value"></param>
		/// <param name="forData"></param>
		/// <returns></returns>
		private TableCell CreateCell(string cssClass, object value, bool forData)
		{
			// basics...
			if(value is TableCell)
				return (TableCell)value;
			else if(value is Control)
				return this.CreateCell(cssClass, (Control)value);
			else if(value is NullReferenceException)
				return this.CreateCell(cssClass, string.Empty);
			else if(value is string)
				return this.CreateCell(cssClass, (string)value);
			else
			{
				// create...
				value = this.FormatCellValue(value, forData);
				return this.CreateCell(cssClass, value.ToString());
			}
		}

		/// <summary>
		/// Formats a cell value.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="forData"></param>
		/// <returns></returns>
		private string FormatCellValue(object value, bool forData)
		{
			if(value == null)
				return string.Empty;
			
			// for?
			if(forData)
			{
				FormatEventArgs e = new FormatEventArgs(value);
				this.OnFormatData(e);
				if(e.SetValueCalled)
					return e.Text;
				else
					return this.DefaultFormatCellData(value);
			}
			else
				return value.ToString();
		}

		/// <summary>
		/// Formats the given string.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		private string DefaultFormatCellData(object value)
		{
			if(value == null)
				return string.Empty;
			if(value is string)
				return (string)value;

			// else...
			switch(Type.GetTypeCode(value.GetType()))
			{
				case TypeCode.Decimal:
					return ((decimal)value).ToString(this.DefaultFloatingPointFormat);
				case TypeCode.Double:
					return ((double)value).ToString(this.DefaultFloatingPointFormat);
				case TypeCode.Single:
					return ((float)value).ToString(this.DefaultFloatingPointFormat);

				case TypeCode.DateTime:
					if(this.DefaultDateTimeFormat != null && this.DefaultDateTimeFormat.Length > 0)
						return ((DateTime)value).ToString(this.DefaultDateTimeFormat);
					else
						return value.ToString();

				default:
					return value.ToString();
			}
		}

		/// <summary>
		/// Gets or sets the defaultfloatingpointformat
		/// </summary>
		public string DefaultFloatingPointFormat
		{
			get
			{
				return _defaultFloatingPointFormat;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultFloatingPointFormat)
				{
					// set the value...
					_defaultFloatingPointFormat = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>FormatData</c> event.
		/// </summary>
		protected virtual void OnFormatData(FormatEventArgs e)
		{
			// raise...
			if(FormatData != null)
				FormatData(this, e);
		}

		/// <summary>
		/// Gets or sets the defaultdatetimeformat
		/// </summary>
		public string DefaultDateTimeFormat
		{
			get
			{
				return _defaultDateTimeFormat;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultDateTimeFormat)
				{
					// set the value...
					_defaultDateTimeFormat = value;
				}
			}
		}

		/// <summary>
		/// Creates a cell.
		/// </summary>
		/// <param name="cssClass"></param>
		/// <returns></returns>
		private TableCell CreateCell(string cssClass)
		{
			TableCell cell = new TableCell();
			if(cssClass != null && cssClass.Length > 0)
			{
				try
				{
					cell.CssClass = cssClass;
				}
				catch(Exception ex)
				{
					throw new InvalidOperationException(string.Format("Failed to set CSS class to '{0}'.", cssClass), ex);
				}
			}
			return cell;
		}

		/// <summary>
		/// Creates a cell that hosts the given control.
		/// </summary>
		/// <param name="control"></param>
		/// <returns></returns>
		public TableCell CreateCell(string cssClass, Control control)
		{
			if(control == null)
				throw new ArgumentNullException("control");
			
			// create...
			TableCell cell = this.CreateCell(cssClass);
			cell.Controls.Add(control);

			// return...
			return cell;
		}

		/// <summary>
		/// Adds a cell to a row.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public TableCell CreateCell(string cssClass, string text)
		{
			// create...
			TableCell cell = this.CreateCell(cssClass);
			cell.Text = text;

			// return...
			return cell;
		}

		/// <summary>
		/// Gets or sets the datasummarycssclass
		/// </summary>
		public string DataSummaryCssClass
		{
			get
			{
				return _dataSummaryCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataSummaryCssClass)
				{
					// set the value...
					_dataSummaryCssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the alternatedatarowcssclass
		/// </summary>
		public string AlternateDataRowCssClass
		{
			get
			{
				return _alternateDataRowCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _alternateDataRowCssClass)
				{
					// set the value...
					_alternateDataRowCssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the datarowcssclass
		/// </summary>
		public string DataRowCssClass
		{
			get
			{
				return _dataRowCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataRowCssClass)
				{
					// set the value...
					_dataRowCssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the dataheadercssclass
		/// </summary>
		public string DataHeaderCssClass
		{
			get
			{
				return _dataHeaderCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataHeaderCssClass)
				{
					// set the value...
					_dataHeaderCssClass = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the headingcssclass
		/// </summary>
		public string HeadingCssClass
		{
			get
			{
				return _headingCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _headingCssClass)
				{
					// set the value...
					_headingCssClass = value;
				}
			}
		}

		/// <summary>
		/// Creates a heading row.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableRow CreateHeadingRow(Level level, object value)
		{
			string cssClass = this.GetHeadingCssClass(level);
			return this.CreateRow(cssClass, value);
		}

		/// <summary>
		/// Adds a heading row.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TableRow AddHeadingRow(Level level, object value)
		{
			TableRow row = this.CreateHeadingRow(level, value);
			this.Rows.Add(row);
			return row;
		}

		/// <summary>
		/// Gets the CSS class for the given heading.
		/// </summary>
		/// <param name="level"></param>
		/// <returns></returns>
		private string GetHeadingCssClass(Level level)
		{
			switch(level)
			{
				case Level.Level1:
					return this.Heading1CssClass;
				case Level.Level2:
					return this.Heading2CssClass;
				case Level.Level3:
					return this.Heading3CssClass;
				case Level.Level4:
					return this.Heading4CssClass;
				case Level.Level5:
					return this.Heading5CssClass;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", level, level.GetType()));
			}
		}

		/// <summary>
		/// Gets or sets the heading5cssclass
		/// </summary>
		public string Heading5CssClass
		{
			get
			{
				return _heading5CssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _heading5CssClass)
				{
					// set the value...
					_heading5CssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the heading4cssclass
		/// </summary>
		public string Heading4CssClass
		{
			get
			{
				return _heading4CssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _heading4CssClass)
				{
					// set the value...
					_heading4CssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the heading3cssclass
		/// </summary>
		public string Heading3CssClass
		{
			get
			{
				return _heading3CssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _heading3CssClass)
				{
					// set the value...
					_heading3CssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the heading2cssclass
		/// </summary>
		public string Heading2CssClass
		{
			get
			{
				return _heading2CssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _heading2CssClass)
				{
					// set the value...
					_heading2CssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the heading1cssclass
		/// </summary>
		public string Heading1CssClass
		{
			get
			{
				return _heading1CssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _heading1CssClass)
				{
					// set the value...
					_heading1CssClass = value;
				}
			}
		}

		/// <summary>
		/// Creates a spacing cell.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableCell CreateSpacingCell(Spacing spacing)
		{
			return this.CreateSpacingCell((int)spacing);
		}

		/// <summary>
		/// Creates a spacing cell.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableCell CreateSpacingCell(int spacing)
		{
			TableCell cell = this.CreateCell(null);
			cell.Height = new Unit(spacing, UnitType.Pixel);
			return cell;
		}

		/// <summary>
		/// Creates a spacing row.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableRow CreateSpacingRow(Spacing spacing)
		{
			return this.CreateSpacingRow((int)spacing);
		}
	
		/// <summary>
		/// Creates a spacing row.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableRow CreateSpacingRow(int spacing)
		{
			return this.CreateRow(null, this.CreateSpacingCell(spacing));
		}

		/// <summary>
		/// Adds a spacing row.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableRow AddSpacingRow(Spacing spacing)
		{
			return this.AddSpacingRow((int)spacing);
		}

		/// <summary>
		/// Adds a spacing row.
		/// </summary>
		/// <param name="spacing"></param>
		/// <returns></returns>
		public TableRow AddSpacingRow(int spacing)
		{
			TableRow row = this.CreateSpacingRow(spacing);
			this.Rows.Add(row);
			return row;
		}
	}
}
