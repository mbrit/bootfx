// BootFX - Application framework for .NET applications
// 
// File: WebDatabaseUpdate.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using BootFX.Common.Data;
using BootFX.Common.Data.Schema;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>WebDatabaseUpdate</c>.
	/// </summary>
	internal class WebDatabaseUpdate : OperationItem, IDisposable
	{	
		/// <summary>
		/// Private field to support <c>CheckResults</c> property.
		/// </summary>
		private DatabaseUpdateCheckResults _checkResults;
		
		/// <summary>
		/// Private field to support <c>StartTime</c> property.
		/// </summary>
		private DateTime _startTime;
		
		/// <summary>
		/// Private field to support <c>Thread</c> property.
		/// </summary>
		private Thread _thread;
		
		/// <summary>
		/// Private field to support <see cref="State"/> property.
		/// </summary>
		private WebDatabaseUpdateState _state = WebDatabaseUpdateState.Idle;
		
		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static WebDatabaseUpdate _current = null;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private WebDatabaseUpdate()
		{
		}
		
		~WebDatabaseUpdate()
		{
			this.Dispose();
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static WebDatabaseUpdate()
		{
			_current = new WebDatabaseUpdate();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		internal static WebDatabaseUpdate Current
		{
			get
			{
				return _current;
			}
		}

		/// <summary>
		/// Gets the state.
		/// </summary>
		internal WebDatabaseUpdateState State
		{
			get
			{
				return _state;
			}
		}

		/// <summary>
		/// Checks the status.
		/// </summary>
		internal void Update()
		{
			this.AssertNotRunning();

			// start...
			_thread = new Thread(new ThreadStart(UpdateThreadEntryPoint));
			_thread.Name = "Web Database Update - Update";
			_thread.IsBackground = true;
			_thread.Start();
		}

		/// <summary>
		/// Checks the status.
		/// </summary>
		internal void CheckStatus()
		{
			this.AssertNotRunning();

			// start...
			_thread = new Thread(new ThreadStart(CheckStatusThreadEntryPoint));
			_thread.Name = "Web Database Update - Check Status";
			_thread.IsBackground = true;
			_thread.Start();
		}

		/// <summary>
		/// Asserts that we are not already running.
		/// </summary>
		private void AssertNotRunning()
		{
			if(this.Thread != null && this.Thread.IsAlive)
				throw new InvalidOperationException("Database update operation is already running.");
		}

		/// <summary>
		/// Gets the thread.
		/// </summary>
		private Thread Thread
		{
			get
			{
				// returns the value...
				return _thread;
			}
		}

		private void Start(WebDatabaseUpdateState state)
		{
			_state = state;
			_startTime = DateTime.UtcNow;
			_checkResults = null;
			this.Reset();
		}

		private void UpdateThreadEntryPoint()
		{
			try
			{
				// setup...
				this.Start(WebDatabaseUpdateState.Updating);

				// mbr - 28-09-2007 - added args...			
				DatabaseUpdate.Current.Update(this, new DatabaseUpdateArgs());

				// set...
				this.Status = "Database update completed OK.";
			}
			catch(ThreadAbortException)
			{
				// no-op...
			}
			catch(Exception ex)
			{
				const string message = "An error occurred when updating the database.";
				if(this.Log.IsErrorEnabled)
					this.Log.Error(message, ex);
				this.SetLastError(message, ex);
			}
			finally
			{
				this.Finish();
			}
		}

		/// <summary>
		/// Gets the checkresults.
		/// </summary>
		public DatabaseUpdateCheckResults CheckResults
		{
			get
			{
				// returns the value...
				return _checkResults;
			}
		}

		private void CheckStatusThreadEntryPoint()
		{
			try
			{
				// setup...
				this.Start(WebDatabaseUpdateState.CheckingStatus);

				// set...
				this.Status = "Checking status...";

				// mbr - 28-09-2007 - added args...			
				_checkResults = DatabaseUpdate.Current.Check(this, new DatabaseUpdateArgs());
				if(CheckResults == null)
					throw new InvalidOperationException("CheckResults is null.");

				// what happned?
				if(this.CheckResults.IsUpToDate)
					this.Status = "The database is up-to-date.";
				else
					this.Status = "The database is not up-to-date and changes need to be made.";
			}
			catch(ThreadAbortException)
			{
				// no-op...
			}
			catch(Exception ex)
			{
				const string message = "An error occurred when checking the status.";
				if(this.Log.IsErrorEnabled)
					this.Log.Error(message, ex);
				this.SetLastError(message, ex);

			}
			finally
			{
				this.Finish();
			}
		}

		private void Finish()
		{
			// were we cancelled?
			if(this.IsCancelled && this.LastError == null)
			{
				switch(State)
				{
					case WebDatabaseUpdateState.Updating:
						this.Status = "The update operation was cancelled.  The database may not be in a usable state.  You should run 'Database Update' again to completion in order to ensure the database is in the correct state.";
						break;

					case WebDatabaseUpdateState.CheckingStatus:
						this.Status = "The status check was cancelled.";
						break;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", State, State.GetType()));
				}
			}

			// set...
			_startTime = DateTime.UtcNow;
			_state = WebDatabaseUpdateState.Idle;
		}

		/// <summary>
		/// Gets the starttime.
		/// </summary>
		private DateTime StartTime
		{
			get
			{
				// returns the value...
				return _startTime;
			}
		}

		/// <summary>
		/// Gets the elapsed.
		/// </summary>
		internal TimeSpan Elapsed
		{
			get
			{
				if(this.StartTime != DateTime.MinValue)
					return DateTime.UtcNow - this.StartTime;
				else
					return TimeSpan.Zero;
			}
		}

		public void Dispose()
		{
			// teardown...
			if(_thread != null)
			{
				_thread.Abort();
				_thread = null;
			}

			// suppress...
			GC.SuppressFinalize(this);
		}
	}
}
