// BootFX - Application framework for .NET applications
// 
// File: UserManager.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web;
using System.Text;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using BootFX.Common.Crypto;
using BootFX.Common.Authentication;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Holds the site's user manager.
	/// </summary>
	public class UserManager : HttpContextAwareBase
	{
		/// <summary>
		/// Private field to support <c>CacheCurrentUserIdInSession</c> property.
		/// </summary>
		private bool _cacheCurrentUserIdInSession = true;
		
		/// <summary>
		/// Raised when the user manager has failed to get a user ID.
		/// </summary>
		/// <remarks>This offers a last opportunity to get the user ID from some other location, e.g. a context-bound property bag.</remarks>
		public event UserIdEventHandler UserIdNeeded;

		/// <summary>
		/// Raised when the user is logged on explicitly.
		/// </summary>
		public event EventHandler UserLoggedOn;
		
		/// <summary>
		/// Raised when the user is logged on using a cookie.
		/// </summary>
		public event EventHandler UserLoggedOnFromCookie;
		
		/// <summary>
		/// Raised when a user needs to be loaded.
		/// </summary>
		public event IAppUserEventHandler LoadUser;
		
		/// <summary>
		/// Private field to support <see cref="CurrentUser"/> property.
		/// </summary>
		private IAppUser _currentUser;
		
		/// <summary>
		/// Defines the session key that holds the user ID.
		/// </summary>
		private const string SessionKeyBase = "__bfxUser";

		/// <summary>
		/// Defines the session key that holds the user ID.
		/// </summary>
		private const string CookieKeyBase = "__bfxUser";

		private enum CookieType
		{
			UserId = 0,
			Username = 1,
			Deleted = 2
		}

		/// <summary>
		/// Private constructor.
		/// </summary>
		public UserManager()
		{
		}
	
		/// <summary>
		/// Gets the current user id.
		/// </summary>
		private object[] CurrentUserId
		{
			get
			{
				object[] userId = null;
				if(this.CacheCurrentUserIdInSession)
				{
					object userIdAsObject = this.Session[SessionKey];
					if(userIdAsObject == null)
					{
						// get it from the cookie...
						string username = null;
						userId = LoadUserIdFromCookie(ref username);
						if(userId != null && userId.Length > 0)
						{
							// set...
							Session[SessionKey] = userId;

							// if we got in here, we must have logged in from a cookie...
							this.OnUserLoggedOnFromCookie();
						}

						// mbr - 11-11-2005 - do we not have one?  provide a way to load it from the a token or context...
						if(userId == null || userId.Length == 0)
						{
							UserIdEventArgs e = new UserIdEventArgs(userId);
							this.OnUserIdNeeded(e);
							userId = e.GetUserId();
						}

						// return...
						return userId;
					}
					else
					{
						// return...
						if(userIdAsObject is object[])
							return (object[])userIdAsObject;
						else
							throw new InvalidOperationException(string.Format("'{0}' is an invalid type.", userIdAsObject));
					}
				}
				else
				{
					// mbr - 14-03-2007 - demand load it every time.					
					UserIdEventArgs e = new UserIdEventArgs(userId);
					this.OnUserIdNeeded(e);
					userId = e.GetUserId();

					// return...
					return userId;
				}
			}
		}

		/// <summary>
		/// Gets or sets the cachecurrentuseridinsession
		/// </summary>
		public bool CacheCurrentUserIdInSession
		{
			get
			{
				return _cacheCurrentUserIdInSession;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _cacheCurrentUserIdInSession)
				{
					// set the value...
					_cacheCurrentUserIdInSession = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>UserIdNeeded</c> event.
		/// </summary>
		protected virtual void OnUserIdNeeded(UserIdEventArgs e)
		{
			// raise...
			if(UserIdNeeded != null)
				UserIdNeeded(this, e);
		}

		/// <summary>
		/// Returns true if the user is authenticated.
		/// </summary>
		public bool IsAuthenticated
		{
			get
			{
				if(this.CurrentUserId == null)
					return false;
				else
					return true;
			}
		}

		/// <summary>
		/// Gets the current user ID.
		/// </summary>
		/// <returns></returns>
		public object[] GetCurrentUserId()
		{
			object[] id = this.CurrentUserId;
			if(id != null)
				return (object[])id.Clone();
			else
				return null;
		}

		// mbr - 2007-11-25 - no longer needed.
//		/// <summary>
//		/// Logs on the given user.
//		/// </summary>
//		/// <param name="user"></param>
//		/// <param name="remember"></param>
//		public void Logon(IAppUser user, bool remember)
//		{
//			Logon(user, remember, new TimeSpan(365, 0,0,0));
//		}

		/// <summary>
		/// Logs on the given user.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="remember"></param>
		// mbr - 2007-11-25 - changed.
		public void Logon(IAppUser user, RememberUserFlags flags, TimeSpan rememberPeriod)
		{
			if(user == null)
				throw new ArgumentNullException("user");
			
			// get...
			object[] id = user.GetId();
			if(id == null)
				throw new InvalidOperationException("'id' is null.");
			if(id.Length == 0)
				throw new InvalidOperationException("'id' is zero-length.");
			
			// set...
			this.Session[SessionKey] = id.Clone();

			// do we want to remember?
			string cookieString = null;
			switch(flags)
			{
					// no-op...
				case RememberUserFlags.DoNotRememberUsers:
					cookieString = this.GetCookieStringForSave(CookieType.Deleted, new object[] {});
					break;

					// username only...
				case RememberUserFlags.UsernameOnly:

					// do we have an iappuser2?
					if(!(user is IAppUser2))
					{
						throw new NotSupportedException(string.Format("A user of type '{0}' needs to support 'IAppUser2' in order to use 'remember username' functionality.", 
							user.GetType()));
					}

					// get...
					string username = ((IAppUser2)user).Username;
					if(username == null)
						throw new InvalidOperationException("'username' is null.");
					if(username.Length == 0)
						throw new InvalidOperationException("'username' is zero-length.");

					// get the string...
					cookieString = this.GetCookieStringForSave(CookieType.Username, new object[] { username });
					if(cookieString == null)
						throw new InvalidOperationException("'cookieString' is null.");
					if(cookieString.Length == 0)
						throw new InvalidOperationException("'cookieString' is zero-length.");

					// stop...
					break;

					// both...
				case RememberUserFlags.UsernameAndPassword:

					// get...
					cookieString = this.GetCookieStringForSave(CookieType.UserId, id);

					// stop...
					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", flags, flags.GetType()));
			}

			// always store a cookie.  
			if(cookieString == null)
				throw new InvalidOperationException("'cookieString' is null.");
			if(cookieString.Length == 0)
				throw new InvalidOperationException("'cookieString' is zero-length.");

			// store the cookie (or delete it if we're deleting)...
			HttpCookie cookie = new HttpCookie(CookieKey, cookieString);
			if(flags != RememberUserFlags.DoNotRememberUsers)
				cookie.Expires = DateTime.Now + rememberPeriod;
			else
				cookie.Expires = DateTime.Now.AddDays(-1);

			// send...
			this.Response.Cookies.Add(cookie);

			// logon...
			this.OnUserLoggedOn();
		}

		private string CookieKey
		{
			get
			{
				StringBuilder builder = new StringBuilder();
				builder.Append(CookieKeyBase);
				foreach(char c in Runtime.Current.Application.ProductName)
				{
					if(char.IsLetterOrDigit(c))
						builder.Append(c);
					else
						builder.Append("_");
				}

				// mbr - 22-02-2008 - add the application root as well...
				builder.Append("_");
				string path = this.Request.ApplicationPath;
				while(path.StartsWith("/"))
					path = path.Substring(1);
				if(path.Length == 0)
					path = "root";
				builder.Append(path);

                // mbr - 2009-09-2009 - added partition name...
                string pid = this.GetPartitionName();
                if (!(string.IsNullOrEmpty(pid)))
                {
                    builder.Append("_");
                    builder.Append(pid);
                }

				// return...
				string key = builder.ToString();
				return key;
			}
		}

        /// <summary>
        /// Gets the partition name
        /// </summary>
        /// <returns></returns>
        protected virtual string GetPartitionName()
        {
            // mbr - 2009-09-18 - added this method to address a problem whereby if you have one browser
            // session connecting to two partitions, the users keep logging on and off.  the result of
            // this method is combined with the session and cookie base to get a workable name...
            string pid = this.Request.Params[RequestParameterPartitionIdProvider.DefaultParameterName];
            return pid;
        }

		/// <summary>
		/// Loads the user ID from the cookie.
		/// </summary>
		/// <returns></returns>
		private object[] LoadUserIdFromCookie(ref string username)
		{
			// reset...
			username = null;

			// get it...
			HttpCookie cookie = this.Request.Cookies[CookieKey];
			if(cookie == null || cookie.Value == null || cookie.Value.Length == 0)
				return null;

			// mbr - 22-02-2008 - added try...catch...
			try
			{
				// decrypt...
				object[] id = this.DecryptUserId(cookie.Value, ref username);
				return id;
			}
			catch(Exception ex)
			{
				if(this.Log.IsWarnEnabled)
					this.Log.Warn("Failed to restore user ID from cookie.", ex);

				// nothing...
				return null;
			}
		}

		/// <summary>
		/// Decrypts the user ID.
		/// </summary>
		/// <param name="idAsString"></param>
		/// <returns></returns>
		private object[] DecryptUserId(string idAsString, ref string username)
		{
			if(idAsString == null)
				throw new ArgumentNullException("idAsString");
			if(idAsString.Length == 0)
				throw new ArgumentOutOfRangeException("'idAsString' is zero-length.");

			// mbr - 22-02-2008 - clear username.
			username = null;

			// crypto...
			SymmetricCrypto crypto = this.GetCookieCrypto();
			if(crypto == null)
				throw new InvalidOperationException("crypto is null.");

			// bytes...
			byte[] bs = Convert.FromBase64String(idAsString);
			object[] id = null;
			using(MemoryStream stream = new MemoryStream(bs))
			{
				using(CryptoStream cryptoStream = new CryptoStream(stream, crypto.CreateDecryptor(), CryptoStreamMode.Read))
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.FilterLevel = TypeFilterLevel.Low;

					// de...
					id = (object[])formatter.Deserialize(cryptoStream);
				}
			}

			// what's the id...
			if(id == null)
				throw new InvalidOperationException("'id' is null.");
			if(id.Length == 0)
				throw new InvalidOperationException("'id' is zero-length.");

			// ok, so the type is the zero'th element...
			CookieType type = (CookieType)id[0];
			switch(type)
			{
					// nothing here...
				case CookieType.Deleted:
					return null;

					// ah - id[1] is the username...
				case CookieType.Username:

					// get...
					if(id.Length != 2)
						throw new InvalidOperationException(string.Format("An ID length of '{0}' is invalid for a 'username' cookie type.", id.Length));

					// set...
					username = (string)id[1];
					return null;

					// copy the array out...
				case CookieType.UserId:

					// copy...
					object[] userId = new object[id.Length - 1];
					Array.Copy(id, 1, userId, 0, userId.Length);

					// return...
					return userId;
					
				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", type, type.GetType()));
			}
		}

		/// <summary>
		/// Encrypts the user id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		private string GetCookieStringForSave(CookieType type, object[] id)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(type != CookieType.Deleted && id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			// crypto...
			SymmetricCrypto crypto = this.GetCookieCrypto();
			if(crypto == null)
				throw new InvalidOperationException("crypto is null.");

			// rebuild the id array...
			object[] toSave = new object[id.Length + 1];
			toSave[0] = (int)type;		// save the int value to avoid serialization issues...
			Array.Copy(id, 0, toSave, 1, id.Length);

			// formatter...
			using(MemoryStream stream = new MemoryStream())
			{
				using(CryptoStream cryptoStream = new CryptoStream(stream, crypto.CreateEncryptor(), CryptoStreamMode.Write))
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.FilterLevel = TypeFilterLevel.Low;

					// serialize...
					formatter.Serialize(cryptoStream, toSave);

					// flush...
					cryptoStream.FlushFinalBlock();

					// flush...
					stream.Flush();

					// read it back...
					string idAsString = Convert.ToBase64String(stream.ToArray(), 0, (int)stream.Length);
					return idAsString;
				}
			}
		}

		/// <summary>
		/// Gets the cookie crypto.
		/// </summary>
		/// <returns></returns>
		private SymmetricCrypto GetCookieCrypto()
		{
			RijndaelCrypto crypto = new RijndaelCrypto(false);

			// mbr - 22-02-2008 - changed to use the name...			
//			crypto.SetKey(SymmetricKey.GetMD5Hash("foo"));
//			crypto.SetIV(SymmetricKey.GetMD5Hash("bar"));
			crypto.SetKey(SymmetricKey.GetMD5Hash(Runtime.Current.Application.ProductCompany));
			crypto.SetIV(SymmetricKey.GetMD5Hash(Runtime.Current.Application.ProductName));

			// return...
			return crypto;
		}

		/// <summary>
		/// Logs off the current user.
		/// </summary>
		public void Logoff()
		{
			// reset state...
			this.Session[SessionKey] = null;
			_currentUser = null;

			// reset the cookie...
			this.ResetUserCookie();
		}

		private void ResetUserCookie()
		{
			// clear it...
			HttpCookie cookie = new HttpCookie(CookieKey, string.Empty);

            // mbr - 22-02-2008 - (http://msdn2.microsoft.com/en-us/library/ms178195.aspx) - the trick is
            // to set the cookie to have *already* expired.  the browser will clear it up...
            //cookie.Expires = DateTime.Now.Add(5);
            cookie.Expires = DateTime.Now.AddDays(-1);

			// send...
			this.Response.Cookies.Add(cookie);
		}

		/// <summary>
		/// Gets the currentuser.
		/// </summary>
		public IAppUser CurrentUser
		{
			get
			{
				// demand load only if we don't have the current user...
				if(_currentUser == null)
				{
					object[] id = this.CurrentUserId;
					if(id != null)
					{
						_currentUser = DoLoadUser(id);
						if(_currentUser == null)
						{
							// user has been deleted...
							this.Session[SessionKey] = null;

							// logoff...
							this.ResetUserCookie();
						}
					}
				}
				return _currentUser;
			}
		}

		/// <summary>
		/// Loads the user with the given ID.
		/// </summary>
		/// <param name="id"></param>
		private IAppUser DoLoadUser(object[] id)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			// create it...
			IAppUserEventArgs e = new IAppUserEventArgs((object[])id.Clone());
			this.OnLoadUser(e);

			// check...
			return e.AppUser;
		}

		/// <summary>
		/// Raises the <c>LoadUser</c> event.
		/// </summary>
		protected virtual void OnLoadUser(IAppUserEventArgs e)
		{
			// raise...
			if(LoadUser != null)
				LoadUser(this, e);
		}

		/// <summary>
		/// Raises the <c>UserLoggedOnFromCookie</c> event.
		/// </summary>
		private void OnUserLoggedOnFromCookie()
		{
			OnUserLoggedOnFromCookie(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>UserLoggedOnFromCookie</c> event.
		/// </summary>
		protected virtual void OnUserLoggedOnFromCookie(EventArgs e)
		{
			// raise...
			if(UserLoggedOnFromCookie != null)
				UserLoggedOnFromCookie(this, e);
		}

		/// <summary>
		/// Raises the <c>UserLoggedOn</c> event.
		/// </summary>
		private void OnUserLoggedOn()
		{
			OnUserLoggedOn(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>UserLoggedOn</c> event.
		/// </summary>
		protected virtual void OnUserLoggedOn(EventArgs e)
		{
			// raise...
			if(UserLoggedOn != null)
				UserLoggedOn(this, e);
		}

		/// <summary>
		/// Gets the remembered username.
		/// </summary>
		public string GetRememberedUsername()
		{
			// load the cookie...
			string username = null;
			this.LoadUserIdFromCookie(ref username);

			// return...
			return username;
		}

        private string SessionKey
        {
            get
            {
                // mbr - 2009-09-18 - changed this to use the partitionid...
                StringBuilder builder = new StringBuilder();
                builder.Append(SessionKeyBase);

                // pid...
                string pid = this.GetPartitionName();
                if (!(string.IsNullOrEmpty(pid)))
                {
                    builder.Append("_");
                    builder.Append(pid);
                }

                // return...
                return builder.ToString();
            }
        }
	}
}
