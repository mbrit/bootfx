// BootFX - Application framework for .NET applications
// 
// File: BasePage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Collections.Specialized;
using System.Globalization;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using BootFX.Common.UI;
using BootFX.Common.Data;
using BootFX.Common.Authentication;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Describes a base page implementation.
	/// </summary>
	public class BasePage : Page, ILoggable
	{
		/// <summary>
		/// Raised when access to an administrative control is required.
		/// </summary>
		public event AdminAuthorizeEventHandler AuthorizeAdmin;
		
		private IAppUser _currentUser = null;

		/// <summary>
		/// Private field to support <c>SmartErrorHandling</c> property.
		/// </summary>
		private SmartErrorHandlingType _smartErrorHandling = SmartErrorHandlingType.UseInstallationSettings;
		
		/// <summary>
		/// Private field to support <c>DatabaseViewStateId</c> property.
		/// </summary>
		private Guid _databaseViewStateId = Guid.Empty;
		
		/// <summary>
		/// Private field to support <c>ViewStateMode</c> property.
		/// </summary>
		private ViewStateMode _viewStateMode = ViewStateMode.NativeAspNet;
		
		/// <summary>
		/// Raised when the user manager has failed to get a user ID.
		/// </summary>
		/// <remarks>This offers a last opportunity to get the user ID from some other location, e.g. a context-bound property bag.</remarks>
		public event UserIdEventHandler UserIdNeeded;

		/// <summary>
		/// Private field to support <c>OnLoadCalled</c> property.
		/// </summary>
		private bool _onLoadCalled;
							
		/// <summary>
		/// Private field to support <c>Logs</c> property.
		/// </summary>
		private ILogSet _logs = null;

		/// <summary>
		/// Raised when the user is logged on explicitly.
		/// </summary>
		public event EventHandler UserLoggedOn;
		
		/// <summary>
		/// Raised when the user is logged on using a cookie.
		/// </summary>
		public event EventHandler UserLoggedOnFromCookie;

		/// <summary>
		/// Private field to support <c>DisableHttpCaching</c> property.
		/// </summary>
		private bool _disableHttpCaching = false;
		
		/// <summary>
		/// Raised when the user should be loaded.
		/// </summary>
		public event IAppUserEventHandler LoadUser;
		
		/// <summary>
		/// Private field to support <c>UserManager</c> property.
		/// </summary>
		private UserManager _userManager;
		
		/// <summary>
		/// Private field to support <c>IsSecure</c> property.
		/// </summary>
		private bool _isSecure = false;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public BasePage()
		{
			_userManager = new UserManager();
			_userManager.LoadUser += new IAppUserEventHandler(_userManager_LoadUser);
			_userManager.UserLoggedOn += new EventHandler(_userManager_UserLoggedOn);
			_userManager.UserLoggedOnFromCookie += new EventHandler(_userManager_UserLoggedOnFromCookie);
			_userManager.UserIdNeeded += new UserIdEventHandler(_userManager_UserIdNeeded);
		}

		/// <summary>
		/// Gets the onloadcalled.
		/// </summary>
		private bool OnLoadCalled
		{
			get
			{
				// returns the value...
				return _onLoadCalled;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			try
			{
				// load...
				base.OnLoad (e);
			}
			finally
			{
				_onLoadCalled = true;
			}
		}


		protected override void OnInit(EventArgs e)
		{
			// cache...
			if(this.DisableHttpCaching)
				this.Response.Cache.SetCacheability(HttpCacheability.NoCache);

			// check...
			if(this.IsSecure)
			{
				if(this.EnsureIsAuthenticated() == false)
					return;
			}

			// do the standard load stuff...
			base.OnInit (e);
		}

		/// <summary>
		/// Gets or sets the issecure
		/// </summary>
		protected internal bool IsSecure
		{
			get
			{
				return _isSecure;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _isSecure)
				{
					// set the value...
					_isSecure = value;
				}
			}
		}

		/// <summary>
		/// Returns true if the user is authenticated.
		/// </summary>
		/// <returns></returns>
		protected bool IsAuthenticated
		{
			get
			{
				return UserManager.IsAuthenticated;
			}
		}

		/// <summary>
		/// Ensures that the user is authenticated.
		/// </summary>
		/// <returns>True if the user is authenticated, false if not.</returns>
		/// <remarks>The user is redirected to <c>logon.aspx</c> if not.</remarks>
		private bool EnsureIsAuthenticated()
		{
			if(this.IsAuthenticated == false)
			{
				this.GoToLogonPage();
				return false;
			}
			else
				return true;
		}

		/// <summary>
		/// Navigates to the logon page.
		/// </summary>
		public virtual void GoToLogonPage()
		{
			this.Response.Redirect(Navigator.Current.LogonPage, true);
		}

		/// <summary>
		/// Gets the currentuser.
		/// </summary>
		private object[] GetCurrentUserId()
		{
			return UserManager.GetCurrentUserId();
		}

		/// <summary>
		/// Gets the currentuser.
		/// </summary>
		public IAppUser CurrentUser
		{
			get
			{
				if(_currentUser == null)
					_currentUser = this.LoadCurrentUser();
				return _currentUser;
			}
		}



		/// <summary>
		/// Gets the current user.
		/// </summary>
		/// <returns></returns>
		protected virtual IAppUser LoadCurrentUser()
		{
			// default implementation.
			return UserManager.CurrentUser;
		}

		/// <summary>
		/// Logs on the given user.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="remember"></param>
		[Obsolete("Use the overload that takes RememberUserFlags.")]
		protected void Logon(IAppUser user, bool remember)
		{
			this.Logon(user, remember, new TimeSpan(365, 0,0,0));
		}

		/// <summary>
		/// Logs on the given user.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="remember"></param>
		[Obsolete("Use the overload that takes RememberUserFlags.")]
		protected void Logon(IAppUser user, bool remember, TimeSpan rememberPeriod)
		{
			RememberUserFlags flags = RememberUserFlags.DoNotRememberUsers;
			if(remember)
				flags = RememberUserFlags.UsernameAndPassword;

			// defer...
			this.Logon(user, flags, rememberPeriod);
		}

		/// <summary>
		/// Logs on the given user.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="remember"></param>
		public void Logon(IAppUser user, RememberUserFlags flags, TimeSpan rememberPeriod)
		{
			UserManager.Logon(user, flags, rememberPeriod);

			// mbr - 20-05-2008 - set...
			_currentUser = user;
		}

		/// <summary>
		/// Logs off the current user.
		/// </summary>
		public void Logoff()
		{
			UserManager.Logoff();
		}

		/// <summary>
		/// Flips back to the start URL.
		/// </summary>
		protected virtual void FlipBack()
		{
			Navigator.Current.FlipBack();
		}

		/// <summary>
		/// Flips back to the start URL.
		/// </summary>
		protected void FlipBack(string name, string value)
		{
			Navigator.Current.FlipBack(name, value);
		}

		/// <summary>
		/// Flips back to the start URL.
		/// </summary>
		protected void FlipBack(NameValueCollection additionalValues)
		{
			Navigator.Current.FlipBack(additionalValues);
		}

		/// <summary>
		/// Gets the usermanager.
		/// </summary>
		private UserManager UserManager
		{
			get
			{
				// returns the value...
				return _userManager;
			}
		}

		private void _userManager_LoadUser(object sender, IAppUserEventArgs e)
		{
			this.OnLoadUser(e);
		}

		/// <summary>
		/// Raises the <c>LoadUser</c> event.
		/// </summary>
		protected virtual void OnLoadUser(IAppUserEventArgs e)
		{
			// raise...
			if(LoadUser != null)
				LoadUser(this, e);
		}

		/// <summary>
		/// Gets or sets the disablehttpcaching
		/// </summary>
		protected bool DisableHttpCaching
		{
			get
			{
				return _disableHttpCaching;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _disableHttpCaching)
				{
					// set the value...
					_disableHttpCaching = value;
				}
			}
		}

		/// <summary>
		/// Sanitizes the given text so that it can be properly displayed.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		protected string SanitizeForView(string text)
		{
			if(text == null || text.Length == 0) 
				return string.Empty;

			// OK, just do a HtmlEncode...
			return this.Server.HtmlEncode(text);
		}

		/// <summary>
		/// Gets the client's browser type.
		/// </summary>
		protected BrowserType BrowserType
		{
			get
			{
				if(Request == null)
					throw new InvalidOperationException("Request is null.");
				if(Request.Browser == null)
					throw new InvalidOperationException("Request.Browser is null.");
				return ClientSideScriptHelper.Current.GetBrowserType(this.Request.Browser);
			}
		}

		/// <summary>
		/// Gets the client's browser type.
		/// </summary>
		protected Platform ClientPlatform
		{
			get
			{
				if(Request == null)
					throw new InvalidOperationException("Request is null.");
				if(Request.Browser == null)
					throw new InvalidOperationException("Request.Browser is null.");
				return ClientSideScriptHelper.Current.GetClientPlatform(this.Request.Browser);
			}
		}

		private void _userManager_UserLoggedOn(object sender, EventArgs e)
		{
			this.OnUserLoggedOn();
		}

		private void _userManager_UserLoggedOnFromCookie(object sender, EventArgs e)
		{
			this.OnUserLoggedOnFromCookie();
		}

		/// <summary>
		/// Raises the <c>UserLoggedOnFromCookie</c> event.
		/// </summary>
		private void OnUserLoggedOnFromCookie()
		{
			OnUserLoggedOnFromCookie(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>UserLoggedOnFromCookie</c> event.
		/// </summary>
		protected virtual void OnUserLoggedOnFromCookie(EventArgs e)
		{
			// raise...
			if(UserLoggedOnFromCookie != null)
				UserLoggedOnFromCookie(this, e);
		}

		/// <summary>
		/// Raises the <c>UserLoggedOn</c> event.
		/// </summary>
		private void OnUserLoggedOn()
		{
			OnUserLoggedOn(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>UserLoggedOn</c> event.
		/// </summary>
		protected virtual void OnUserLoggedOn(EventArgs e)
		{
			// raise...
			if(UserLoggedOn != null)
				UserLoggedOn(this, e);
		}

		
		/// <summary>
		/// Gets or sets the usetemplate
		/// </summary>
		protected bool UseTemplate
		{
			get
			{
                return false;
			}
			set
			{
                // no-op...
			}
		}

		ILog ILoggable.Log
		{
			get
			{
				return this.Log;
			}
		}

		/// <summary>
		/// Gets the default log.
		/// </summary>
		protected ILog Log
		{
			get
			{
				return this.Logs.DefaultLog;
			}
		}

		ILogSet ILoggable.Logs
		{
			get
			{
				return this.Logs;
			}
		}

		/// <summary>
		/// Gets the set of logs for other activities.
		/// </summary>
		protected ILogSet Logs
		{
			get
			{
				// mbr - 11-10-2005 - provided an ability to invalidate logs if the context changes...				
				if(_logs != null && _logs.ContextId != LogSet.CurrentContextId)
					_logs = null;
				if(_logs == null)
					_logs = new LogSet(this.GetType());
				return _logs;
			}
		}

		private void _userManager_UserIdNeeded(object sender, UserIdEventArgs e)
		{
			this.OnUserIdNeeded(e);
		}

		/// <summary>
		/// Raises the <c>UserIdNeeded</c> event.
		/// </summary>
		protected virtual void OnUserIdNeeded(UserIdEventArgs e)
		{
			// raise...
			if(UserIdNeeded != null)
				UserIdNeeded(this, e);
		}

		/// <summary>
		/// Gets the partition ID.
		/// </summary>
		protected virtual long PartitionId
		{
			get
			{
				// mbr - 01-02-2006 - changed to deferral...
				if(Request == null)
					throw new InvalidOperationException("Request is null.");
				return GetPartitionId(this.Request);
			}
		}

		// mbr - 01-02-2006 - added.
		public static long GetPartitionId(HttpRequest request)
		{
			if(request == null)
				throw new ArgumentNullException("request");

			string asString = request.Params[RequestParameterPartitionIdProvider.DefaultParameterName];
			if(asString != null && asString.Length > 0)
				return ConversionHelper.ToInt64(asString, Cultures.System);
			else
				return 0;
		}

		/// <summary>
		/// Constructs a path by passing in the sub-site URL.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string ResolveUrlForSubSite(string url)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");

			// get...
			string subSiteName = this.GetSubSiteName();
			if(subSiteName == null)
				throw new InvalidOperationException("'subSiteName' is null.");
			if(subSiteName.Length == 0)
				throw new InvalidOperationException("'subSiteName' is zero-length.");

			// create...
			string newUrl = this.Request.ApplicationPath;
			newUrl = this.Combine(newUrl, subSiteName);

			// add the end...
			while(url.StartsWith("~"))
				url = url.Substring(1);

			// add...
			newUrl = this.Combine(newUrl, url);

			// return...
			return newUrl;
		}

		private string Combine(string url1, string url2)
		{
			if(url1 == null)
				url1 = string.Empty;
			if(url2 == null)
				url2 = string.Empty;

			// check...
			if(!(url1.EndsWith("/")) && !(url1.EndsWith("\\")) && !(url2.StartsWith("/")) 
				&& !(url2.StartsWith("\\")))
				url1 += "/";
			return url1 + url2;
		}

		/// <summary>
		/// Gets the sub-site name for this request.
		/// </summary>
		/// <returns></returns>
		protected virtual string GetSubSiteName()
		{
			throw new NotImplementedException("'GetSubSiteName' must be configured to return the sub-site name.");
		}

		/// <summary>
		/// Gets or sets the viewstatemode
		/// </summary>
		protected ViewStateMode ViewStateMode
		{
			get
			{
				return _viewStateMode;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _viewStateMode)
				{
					// set the value...
					_viewStateMode = value;
				}
			}
		}

		// mbr - 11-04-2006 - database view state deprecated.		
//		// mbr - 02-03-2006 - added.
//		protected override void SavePageStateToPersistenceMedium(object viewState)
//		{
//			switch(ViewStateMode)
//			{
//				case ViewStateMode.NativeAspNet:
//					base.SavePageStateToPersistenceMedium(viewState);
//					break;
//
//				case ViewStateMode.Database:
//					this.SerializeViewStateToDatabase(viewState);
//					break;
//
//				default:
//					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", ViewStateMode, ViewStateMode.GetType()));
//			}
//		}
//
//		// mbr - 02-03-2006 - added.
//		protected override object LoadPageStateFromPersistenceMedium()
//		{
//			switch(ViewStateMode)
//			{
//				case ViewStateMode.NativeAspNet:
//					return base.LoadPageStateFromPersistenceMedium();
//
//				case ViewStateMode.Database:
//					return this.DeserializeViewStateFromDatabase();
//
//				default:
//					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", ViewStateMode, ViewStateMode.GetType()));
//			}
//		}
//
//		/// <summary>
//		/// Serializes the view state to the database.
//		/// </summary>
//		private object DeserializeViewStateFromDatabase()
//		{
//			_databaseViewStateId = Guid.Empty;
//
//			// get the method...
//			string methodAsString = this.Request.Form[DatabaseViewStateMethodKey];
//			if(methodAsString == null || methodAsString.Length == 0)
//				return null;
//
//			// get it...
//			ViewStateMethod method = ViewStateMethod.InPageCompressed;
//			try
//			{
//				method = (ViewStateMethod)Enum.Parse(typeof(ViewStateMethod), methodAsString, true);
//			}
//			catch(Exception ex)
//			{
//				throw new InvalidOperationException(string.Format("Failed to convert '{0}' to a valid view state method.", methodAsString), ex);
//			}
//
//			// it's compressed...
//			string asString = this.Request.Form[DatabaseViewStateKey];
//			if(asString == null)
//				throw new InvalidOperationException("'asString' is null.");
//			if(asString.Length == 0)
//				throw new InvalidOperationException("'asString' is zero-length.");
//
//			// what happened?
//			byte[] bs = null;
//			bool isCompressed = false;
//			switch(method)
//			{
//				case ViewStateMethod.InPageCompressed:
//
//					// bytes...
//					bs = Convert.FromBase64String(asString);
//					isCompressed = true;
//
//					// stop...
//					break;
//
//				case ViewStateMethod.Database:
//
//					// get...
//					try
//					{
//						_databaseViewStateId = new Guid(asString);
//					}
//					catch(Exception ex)
//					{
//						throw new InvalidOperationException(string.Format("Failed to convert '{0}' to a GUID.", asString), ex);
//					}
//
//					// get it...
//					ViewStateItem item = ViewStateItem.GetById(this.DatabaseViewStateId);
//					if(item == null)
//						return null;
//
//					// update...
//					this.UpdateViewStateExpiry(item);
//
//					// save...
//					item.SaveChanges();
//
//					// decompress...
//					bs = item.Data;
//					isCompressed = item.Compressed;
//
//					// stop...
//					break;
//
//				default:
//					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", method, method.GetType()));
//			}
//
//			// check...
//			if(bs == null)
//				throw new InvalidOperationException("'bs' is null.");
//			if(bs.Length == 0)
//				throw new InvalidOperationException("'bs' is zero-length.");
//
//			// decompress...
//			if(isCompressed)
//				bs = CompressionHelper.Decompress(bs);
//			
//			// get it...
//			LosFormatter formatter = new LosFormatter();
//			using(MemoryStream stream = new MemoryStream(bs))
//				return formatter.Deserialize(stream);
//		}
//
//		/// <summary>
//		/// Gets the databaseviewstateid.
//		/// </summary>
//		private Guid DatabaseViewStateId
//		{
//			get
//			{
//				// returns the value...
//				return _databaseViewStateId;
//			}
//		}
//
//		/// <summary>
//		/// Serializes the view state to the database.
//		/// </summary>
//		private void SerializeViewStateToDatabase(object viewState)
//		{
//			// format it first, because it might be so small we might as well just put it on the page...
//			LosFormatter formatter = new LosFormatter();
//			byte[] bs = null;
//			using(MemoryStream stream = new MemoryStream())
//			{
//				// dump it...
//				formatter.Serialize(stream, viewState);
//
//				// set it...
//				bs = stream.ToArray();
//				if(bs == null)
//					throw new InvalidOperationException("bs is null.");
//			}
//
//			// compress...
//			bool compressed = true;
//			bs = CompressionHelper.Compress(bs);
//
//			// is it small?
//			if(bs.Length < ViewStateCeiling)
//			{
//				// set...
//				this.RegisterHiddenField(DatabaseViewStateMethodKey, ViewStateMethod.InPageCompressed.ToString());
//				this.RegisterHiddenField(DatabaseViewStateKey, Convert.ToBase64String(bs));
//
//				// return...
//				return;
//			}
//
//			// set the method...
//			this.RegisterHiddenField(DatabaseViewStateMethodKey, ViewStateMode.Database.ToString());
//
//			// check...
//			ViewStateItem.EnsureTableExists();
//
//			// get the usage...
//			long usage = Interlocked.Increment(ref _databaseViewStateUsage);
//			if(usage % CleanupViewStateOnEveryNthRequest == 0)
//				CleanupDatabaseViewState();
//
//			// guid...
//			if(this.DatabaseViewStateId == Guid.Empty)
//				_databaseViewStateId = Guid.NewGuid();
//
//			// save it...
//			this.RegisterHiddenField(DatabaseViewStateKey, this.DatabaseViewStateId.ToString());
//
//			// create it...
//			ViewStateItem item = ViewStateItem.GetById(this.DatabaseViewStateId, true);
//			if(item == null)
//			{
//				item = new ViewStateItem();
//				item.ViewStateId = this.DatabaseViewStateId;
//			}
//
//			// set again...
//			item.PageName = this.GetType().BaseType.Name;
//			const int max = 64;
//			if(item.PageName.Length > max)
//				item.PageName = item.PageName.Substring(0, max);
//			item.Data = bs;
//			item.Compressed = compressed;
//			item.Size = bs.Length;
//
//			// set...
//			this.UpdateViewStateExpiry(item);
//
//			// save it...
//			item.SaveChanges();
//		}
//
//		private void UpdateViewStateExpiry(ViewStateItem item)
//		{
//			if(item == null)
//				throw new ArgumentNullException("item");
//			
//			// set the expiry to be one minute more than the session...
//			item.Expires = DateTime.UtcNow.AddMinutes(this.Session.Timeout + 1);
//		}
//
//		/// <summary>
//		/// Cleans up the database viewstate.
//		/// </summary>
//		private void CleanupDatabaseViewState()
//		{
//			try
//			{
//				// run...
//				Database.ExecuteNonQuery(new SqlStatement("delete from bfxviewstate where expires < @p0", new object[] { DateTime.UtcNow }));
//			}
//			catch(Exception ex)
//			{
//				if(this.Log.IsWarnEnabled)
//					this.Log.Warn("Failed to cleanup database viewstate.", ex);
//			}
//		}

		protected bool ResolvedSmartErrorHandling
		{
			get
			{
				switch(SmartErrorHandling)
				{
					case SmartErrorHandlingType.AlwaysOn:
						return true;

					case SmartErrorHandlingType.AlwaysOff:
						return false;

					case SmartErrorHandlingType.UseInstallationSettings:
						return Runtime.Current.InstallationSettings.UseSmartErrorHandlingOnWeb;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", SmartErrorHandling, SmartErrorHandling.GetType()));
				}
			}
		}

		protected override void OnError(EventArgs e)
		{
			if(this.ResolvedSmartErrorHandling)
			{
				// tag...
				Guid guid = WebErrorWidget.TagError();
				if(guid == Guid.Empty)
					throw new InvalidOperationException("Error tagging returned an empty GUID.");

				// defer...
				HandleError(guid);
			}
			else
				base.OnError (e);
		}

		/// <summary>
		/// Gets or sets the smarterrorhandling
		/// </summary>
		public SmartErrorHandlingType SmartErrorHandling
		{
			get
			{
				return _smartErrorHandling;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _smartErrorHandling)
				{
					// set the value...
					_smartErrorHandling = value;
				}
			}
		}

		protected virtual void HandleError(Guid guid)
		{
			this.Response.Redirect(string.Format("~/error.aspx?{0}={1}", WebErrorWidget.DefaultTagName, guid), true);
		}

		/// <summary>
		/// Gets the given parameter value.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <param name="culture"></param>
		/// <param name="onNotFound"></param>
		/// <returns></returns>
		public bool GetParamValueAsBoolean(string name, bool defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (bool)this.GetParamValue(name, typeof(bool), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given parameter value.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <param name="culture"></param>
		/// <param name="onNotFound"></param>
		/// <returns></returns>
		public int GetParamValueAsInt32(string name, int defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (int)this.GetParamValue(name, typeof(int), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given parameter value.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <param name="culture"></param>
		/// <param name="onNotFound"></param>
		/// <returns></returns>
		public string GetParamValueAsString(string name, string defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (string)this.GetParamValue(name, typeof(string), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given parameter value.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="type"></param>
		/// <param name="defaultValue"></param>
		/// <param name="culture"></param>
		/// <param name="onNotFound"></param>
		/// <returns></returns>
		public object GetParamValue(string name, Type type, object defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return this.GetValueInternal(this.Request.Params, name, type, defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <QueryString name="name"></QueryString>
		/// <QueryString name="defaultValue"></QueryString>
		/// <QueryString name="culture"></QueryString>
		/// <QueryString name="onNotFound"></QueryString>
		/// <returns></returns>
		public bool GetQueryStringValueAsBoolean(string name, bool defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (bool)this.GetQueryStringValue(name, typeof(bool), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <QueryString name="name"></QueryString>
		/// <QueryString name="defaultValue"></QueryString>
		/// <QueryString name="culture"></QueryString>
		/// <QueryString name="onNotFound"></QueryString>
		/// <returns></returns>
		public int GetQueryStringValueAsInt32(string name, int defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (int)this.GetQueryStringValue(name, typeof(int), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <QueryString name="name"></QueryString>
		/// <QueryString name="defaultValue"></QueryString>
		/// <QueryString name="culture"></QueryString>
		/// <QueryString name="onNotFound"></QueryString>
		/// <returns></returns>
		public string GetQueryStringValueAsString(string name, string defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (string)this.GetQueryStringValue(name, typeof(string), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <QueryString name="name"></QueryString>
		/// <QueryString name="type"></QueryString>
		/// <QueryString name="defaultValue"></QueryString>
		/// <QueryString name="culture"></QueryString>
		/// <QueryString name="onNotFound"></QueryString>
		/// <returns></returns>
		public object GetQueryStringValue(string name, Type type, object defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return this.GetValueInternal(this.Request.QueryString, name, type, defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <Form name="name"></Form>
		/// <Form name="defaultValue"></Form>
		/// <Form name="culture"></Form>
		/// <Form name="onNotFound"></Form>
		/// <returns></returns>
		public bool GetFormValueAsBoolean(string name, bool defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (bool)this.GetFormValue(name, typeof(bool), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <Form name="name"></Form>
		/// <Form name="defaultValue"></Form>
		/// <Form name="culture"></Form>
		/// <Form name="onNotFound"></Form>
		/// <returns></returns>
		public int GetFormValueAsInt32(string name, int defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (int)this.GetFormValue(name, typeof(int), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <Form name="name"></Form>
		/// <Form name="defaultValue"></Form>
		/// <Form name="culture"></Form>
		/// <Form name="onNotFound"></Form>
		/// <returns></returns>
		public string GetFormValueAsString(string name, string defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return (string)this.GetFormValue(name, typeof(string), defaultValue, culture, onNotFound);
		}

		/// <summary>
		/// Gets the given query string value.
		/// </summary>
		/// <Form name="name"></Form>
		/// <Form name="type"></Form>
		/// <Form name="defaultValue"></Form>
		/// <Form name="culture"></Form>
		/// <Form name="onNotFound"></Form>
		/// <returns></returns>
		public object GetFormValue(string name, Type type, object defaultValue, CultureInfo culture, OnNotFound onNotFound)
		{
			return this.GetValueInternal(this.Request.Form, name, type, defaultValue, culture, onNotFound);
		}
		
		// mbr - 06-07-2007 - added.	
		private object GetValueInternal(NameValueCollection values, string name, Type type, object defaultValue, 
			CultureInfo culture, OnNotFound onNotFound)
		{
			if(values == null)
				throw new ArgumentNullException("values");
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");

			// contains?
			object value = values[name];
			if(value != null)
			{
				// convert?
				if(type != null)
					return ConversionHelper.ChangeType(value, type, culture);
				else
					return value;
			}
			else
			{
				switch(onNotFound)
				{
					case OnNotFound.ReturnNull:
						return defaultValue;

					case OnNotFound.ThrowException:
						throw new InvalidOperationException(string.Format("A value with name '{0}' was not found.", values));

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", onNotFound, onNotFound.GetType()));
				}
			}
		}

		/// <summary>
		/// Gets the username that was last remembered with the 'remember username' flag.
		/// </summary>
		/// <returns></returns>
		public string GetRememberedUsername()
		{
			if(UserManager == null)
				throw new InvalidOperationException("UserManager is null.");
			return this.UserManager.GetRememberedUsername();
		}

		/// <summary>
		/// Authorizes the admin control.
		/// </summary>
		/// <returns></returns>
		internal bool AuthorizeAdminControl()
		{
			// args...
			AdminAuthorizeEventArgs e = new AdminAuthorizeEventArgs();
			e.IsAuthorized = false;

			// on...
			this.OnAuthorizeAdmin(e);

			// return...
			return e.IsAuthorized;
		}

		/// <summary>
		/// Raises the <c>AuthorizeAdmin</c> event.
		/// </summary>
		protected virtual void OnAuthorizeAdmin(AdminAuthorizeEventArgs e)
		{
			// raise...
			if(AuthorizeAdmin != null)
				AuthorizeAdmin(this, e);
		}
	}
}
