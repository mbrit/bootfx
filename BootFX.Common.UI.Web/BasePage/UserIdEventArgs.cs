// BootFX - Application framework for .NET applications
// 
// File: UserIdEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	public delegate void UserIdEventHandler(object sender, UserIdEventArgs e);

	/// <summary>
	/// Defines an instance of <c>UserIdEventArgs</c>.
	/// </summary>
	public class UserIdEventArgs
	{
		/// <summary>
		/// Private field to support <c>UserId</c> property.
		/// </summary>
		private object[] _userId;
		
		public UserIdEventArgs(object[] userId)
		{
			_userId = userId;
		}

		/// <summary>
		/// Gets the userid.
		/// </summary>
		private object[] UserId
		{
			get
			{
				// returns the value...
				return _userId;
			}
		}

		public void SetUserId(object[] userId)
		{
			_userId = userId;
		}

		public object[] GetUserId()
		{
			return _userId;
		}
	}
}
