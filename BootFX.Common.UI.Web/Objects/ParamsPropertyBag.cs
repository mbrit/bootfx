// BootFX - Application framework for .NET applications
// 
// File: ParamsPropertyBag.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ParamsPropertyBag.
	/// </summary>
	public class HttpRequestArgsPropertyBag
	{
		public HttpRequestArgsPropertyBag(NameValueCollection values)
		{
		}
	}
}
