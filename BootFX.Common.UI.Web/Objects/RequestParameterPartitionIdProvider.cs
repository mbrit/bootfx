// BootFX - Application framework for .NET applications
// 
// File: RequestParameterPartitionIdProvider.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>RequestParameterPartitionIdProvider</c>.
	/// </summary>
	public class RequestParameterPartitionIdProvider : HttpContextAwareBase, IPartitionIdProvider
	{
		public const string DefaultParameterName = "__bfxpid";
		public static Type DefaultParameterType = typeof(long);

		/// <summary>
		/// Private field to support <c>ParameterType</c> property.
		/// </summary>
		private Type _parameterType;
		
		/// <summary>
		/// Private field to support <see cref="ParameterName"/> property.
		/// </summary>
		private string _parameterName;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="paramName"></param>
		public RequestParameterPartitionIdProvider() : this(DefaultParameterName, DefaultParameterType)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="paramName"></param>
		public RequestParameterPartitionIdProvider(string parameterName, Type parameterType)
		{
			if(parameterName == null)
				throw new ArgumentNullException("parameterName");
			if(parameterName.Length == 0)
				throw new ArgumentOutOfRangeException("'parameterName' is zero-length.");
			if(parameterType == null)
				throw new ArgumentNullException("parameterType");
			
			_parameterName = parameterName;
			_parameterType = parameterType;
		}

		// mbr - 04-09-2007 - for c7 - changed.		
//		public virtual object[] GetPartitionIdsForReading()
		public virtual object[] GetPartitionIdsForReading(EntityType et)
		{
			object id = this.GetPartitionIdForWriting(null);
			if(id == null)
				throw new InvalidOperationException("id is null.");
			return new object[] { id };
		}

		// mbr - 04-09-2007 - for c7 - changed.		
//		public virtual object GetPartitionIdForWriting()
		public virtual object GetPartitionIdForWriting(object entity)
		{
			// check...
			if(ParameterName == null)
				throw new InvalidOperationException("'ParameterName' is null.");
			if(ParameterName.Length == 0)
				throw new InvalidOperationException("'ParameterName' is zero-length.");
			if(ParameterType == null)
				throw new InvalidOperationException("ParameterType is null.");

			// get it...
			string asString = this.Request.Params[this.ParameterName];
			if(asString == null)
				throw new InvalidOperationException(string.Format("Value for parameter '{0}' was null.", this.ParameterName));
			if(asString.Length == 0)
				throw new InvalidOperationException(string.Format("Value for parameter '{0}' was zero-length.", this.ParameterName));

			// convert...
			return ConversionHelper.ChangeType(asString, this.ParameterType, Cultures.System);
		}

		/// <summary>
		/// Gets the parametername.
		/// </summary>
		private string ParameterName
		{
			get
			{
				return _parameterName;
			}
		}

		/// <summary>
		/// Gets the parametertype.
		/// </summary>
		private Type ParameterType
		{
			get
			{
				// returns the value...
				return _parameterType;
			}
		}
	}
}
