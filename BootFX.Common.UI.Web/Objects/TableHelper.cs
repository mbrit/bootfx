// BootFX - Application framework for .NET applications
// 
// File: TableHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>TableHelper</c>.
	/// </summary>
	public sealed class TableHelper
	{
		public const int DefaultSpacerHeight = 15;

		private TableHelper()
		{
		}

		public static void AddTextRow(Table table, string text)
		{
			AddTextRow(table, new string[] { text });
		}

		public static void AddTextRow(Table table, params string[] texts)
		{
			if(table == null)
				throw new ArgumentNullException("table");
			
			// add...
			TableRow row = CreateTextRow(texts);
			if(row == null)
				throw new InvalidOperationException("row is null.");
			table.Rows.Add(row);
		}

		public static void AddTextCell(TableRow row, string text)
		{
			AddTextCell(row, new string[] { text });
		}

		public static void AddTextCell(TableRow row, params string[] texts)
		{
			if(row == null)
				throw new ArgumentNullException("row");
			
			// create...
			foreach(string text in texts)
			{
				TableCell cell = CreateTextCell(text);
				row.Cells.Add(cell);
			}
		}

		public static TableRow CreateTextRow(string text)
		{
			return CreateTextRow(new string[] { text });
		}

		public static TableRow CreateTextRow(params string[] texts)
		{
			TableRow row = new TableRow();
			AddTextCell(row, texts);

			// return...
			return row;
		}

		public static TableCell CreateTextCell(string text)
		{
			TableCell cell = new TableCell();
			cell.Text = text;

			// return...
			return cell;
		}

		public static TableCell CreateTextCell(object value)
		{
			if(value == null)
				return CreateTextCell(string.Empty);

			// convert...
			string asString = null;
			if(value is string)
				asString = (string)value;
			else
				asString = ConversionHelper.ToString(value, Cultures.User);

			// return...
			return CreateTextCell(asString);
		}

		public static void AddControlRow(Table table, Control control)
		{
			AddControlRow(table, new Control[] { control });
		}

		public static void AddControlRow(Table table, string text, params Control[] controls)
		{
			if(table == null)
				throw new ArgumentNullException("table");

			// row...
			TableRow row = CreateControlRow(text, controls);
			if(row == null)
				throw new InvalidOperationException("row is null.");
			table.Rows.Add(row);
		}

		// mbr - 20-08-2007 - added.		
		public static TableRow CreateControlRow(string text, params Control[] controls)
		{
			if(controls == null)
				throw new InvalidOperationException("controls is null.");
			
			// create...
			TableRow row = new TableRow();

			// then...
			row.Cells.Add(CreateTextCell(text));

			// then...
			foreach(Control control in controls)
			{
				TableCell cell = CreateControlCell(control);
				row.Cells.Add(cell);
			}		

			// return...
			return row;
		}

		public static void AddControlRow(Table table, params Control[] controls)
		{
			if(table == null)
				throw new ArgumentNullException("table");
			
			// add...
			TableRow row = CreateControlRow(controls);
			if(row == null)
				throw new InvalidOperationException("row is null.");
			table.Rows.Add(row);
		}

		public static void AddControlCell(TableRow row, Control control)
		{
			AddControlCell(row, new Control[] { control });
		}

		public static void AddControlCell(TableRow row, params Control[] controls)
		{
			if(row == null)
				throw new ArgumentNullException("row");
			
			// create...
			foreach(Control control in controls)
			{
				TableCell cell = CreateControlCell(control);
				row.Cells.Add(cell);
			}
		}

		public static TableRow CreateControlRow(Control control)
		{
			return CreateControlRow(new Control[] { control });
		}

		public static TableRow CreateControlRow(params Control[] controls)
		{
			TableRow row = new TableRow();
			AddControlCell(row, controls);

			// return...
			return row;
		}

		public static TableCell CreateControlCell(Control control)
		{
			TableCell cell = new TableCell();
			cell.Controls.Add(control);

			// return...
			return cell;
		}

		public static TableRow CreateSpacerRow()
		{
			return CreateSpacerRow(DefaultSpacerHeight);
		}

		public static TableRow CreateSpacerRow(int height)
		{
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Height = new Unit(height, UnitType.Pixel);
			return row;
		}

		public static Table CreateControlTable(params Control[] controls)
		{
			if(controls == null)
				throw new ArgumentNullException("controls");
			
			// create...
			Table table = new Table();
			TableRow row = new TableRow();
			table.Rows.Add(row);

			foreach(Control control in controls)
			{
				TableCell cell = new TableCell();
				row.Cells.Add(cell);
				cell.Controls.Add(control);
			}

			// return...
			return table;
		}

		public static TableRow CreateNameValueRow(string text, object value)
		{
			TableRow row = new TableRow();
			row.Cells.Add(CreateTextCell(text));
			row.Cells.Add(CreateTextCell(value));

			// return...
			return row;
		}

		public static void AddSpacerRow(Table table)
		{
			if(table == null)
				throw new ArgumentNullException("table");
			
			// add...
			table.Rows.Add(CreateSpacerRow());
		}

		public static void AddSpacerRow(Table table, int height)
		{
			if(table == null)
				throw new ArgumentNullException("table");
			
			// add...
			table.Rows.Add(CreateSpacerRow(height));
		}
	}
}
