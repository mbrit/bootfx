// BootFX - Application framework for .NET applications
// 
// File: TransmitDocumentFlags.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Flags for use with TransmitDocument.
	/// </summary>
	public enum TransmitDocumentFlags
	{
		Normal = 0,
		UseBuffering = 1,
		DontSendDisposition = 2
	}
}
