// BootFX - Application framework for .NET applications
// 
// File: WindowOpenFeatures.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>WindowOpenFeatures</c>.
	/// </summary>
	[Flags()]
	public enum WindowOpenFeatures
	{
		None = 0,

		/// <summary>
		/// This is 'location'.
		/// </summary>
		AddressBar = 1,

		MenuBar = 2,
		Resizable = 4,
		ScrollBars = 8,
		Status = 16,
	}
}
