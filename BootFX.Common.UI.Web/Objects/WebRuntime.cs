// BootFX - Application framework for .NET applications
// 
// File: WebRuntime.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Web;
using System.Text;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for WebRuntime.
	/// </summary>
	public class WebRuntime : HttpContextAwareBase
	{
		private const string TokenKey = "__bfxToken";

		public static Encoding DefaultTransmitEncoding = Encoding.UTF8;

		// mbr - 27-06-2008 - deprecated.		
//		/// <summary>
//		/// Called when a Web service is required for the current session.
//		/// </summary>
//		public event WebServiceTokenEventHandler WebServiceTokenRequired;
		
		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static WebRuntime _current = null;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private WebRuntime()
		{
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static WebRuntime()
		{
			_current = new WebRuntime();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static WebRuntime Current
		{
			get
			{
				return _current;
			}
		}

		// mbr - 27-06-2008 - deprecated.	
//		/// <summary>
//		/// Gets the given Web proxy, handling application of the authentication token and URL redirection.
//		/// </summary>
//		/// <param name="wsType"></param>
//		/// <returns></returns>
//		public WebServicesClientProtocol GetProxy(Type wsType)
//		{
//			if(wsType == null)
//				throw new ArgumentNullException("wsType");
//			
//			// create an instance of that...
//			WebServicesClientProtocol proxy = null;
//			try
//			{
//				object asObject = Activator.CreateInstance(wsType);
//				if(asObject == null)
//					throw new InvalidOperationException("asObject is null.");
//				if(!(asObject is WebServicesClientProtocol))
//					throw new InvalidOperationException(string.Format("Proxy is of type '{0}', not WebServicesClientProtocol.", asObject.GetType()));
//
//				// set...
//				proxy = (WebServicesClientProtocol)asObject;
//			}
//			catch(Exception ex)
//			{
//				throw new InvalidOperationException(string.Format("Failed to create an Web service proxy instance of '{0}'.", wsType), ex);
//			}
//
//			// set...
//			BFXToken token = this.CurrentToken;
//			if(token != null)
//				proxy.RequestSoapContext.Security.Tokens.Add(token);
//
//			// credentials...
//			proxy.Credentials = CredentialCache.DefaultCredentials;
//
//			// TODO: set the URL here...
//
//			// return...
//			return proxy;
//		}

		// mbr - 27-06-2008 - deprecated.		
//		/// <summary>
//		/// Raises the <c>WebServiceTokenRequired</c> event.
//		/// </summary>
//		protected virtual void OnWebServiceTokenRequired(WebServiceTokenEventArgs e)
//		{
//			// raise...
//			if(WebServiceTokenRequired != null)
//				WebServiceTokenRequired(this, e);
//		}
//
//		/// <summary>
//		/// Gets the current token from the session.
//		/// </summary>
//		public BFXToken CurrentToken
//		{
//			get
//			{
//				if(Session == null)
//					throw new InvalidOperationException("Session is null.");
//				BFXToken current = (BFXToken)this.Session[TokenKey];
//				if(current == null)
//				{
//					WebServiceTokenEventArgs e = new WebServiceTokenEventArgs();
//					this.OnWebServiceTokenRequired(e);
//
//					// set...?
//					current = e.Token;
//
//					// set...
//					this.Session[TokenKey] = current;
//				}
//
//				// return...
//				return current;
//			}
//		}

		/// <summary>
		/// Gets window open script.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="name">The name of the window, or a constant value stored in <c>Target</c>.</param>
		/// <param name="features"></param>
		/// <returns></returns>
		public string GetWindowOpenScriptBlock(string url, string name, WindowOpenFeatures features, int top, int left, int width, int height)
		{
			return string.Format("<script language=\"javascript\">\r\n{0};\r\n</script>", this.GetWindowOpenScript(url, name, features, top, left, width, height));
		}
		
		/// <summary>
		/// Gets window open script.
		/// </summary>
		/// <param name="url"></param>
		/// <param name="name">The name of the window, or a constant value stored in <c>Target</c>.</param>
		/// <param name="features"></param>
		/// <returns></returns>
		public string GetWindowOpenScript(string url, string name, WindowOpenFeatures features, int top, int left, int width, int height)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");			

			// http://msdn.microsoft.com/library/default.asp?url=/workshop/author/dhtml/reference/methods/open_0.asp
			StringBuilder builder = new StringBuilder();
			builder.Append("window.open('");
			builder.Append(url);
			builder.Append("', '");
			builder.Append(name);
			builder.Append("'");

			// features...
			StringBuilder featureBuilder = new StringBuilder();
			if(left != -1)
				this.AddFeature(featureBuilder, "left", left);
			if(top != -1)
				this.AddFeature(featureBuilder, "top", top);
			if(width != -1)
				this.AddFeature(featureBuilder, "width", width);
			if(height != -1)
				this.AddFeature(featureBuilder, "height", height);

			// add...
			this.CheckAndAddFeature(featureBuilder, features, WindowOpenFeatures.AddressBar);
			this.CheckAndAddFeature(featureBuilder, features, WindowOpenFeatures.MenuBar);
			this.CheckAndAddFeature(featureBuilder, features, WindowOpenFeatures.Resizable);
			this.CheckAndAddFeature(featureBuilder, features, WindowOpenFeatures.ScrollBars);
			this.CheckAndAddFeature(featureBuilder, features, WindowOpenFeatures.Status);

			// add...
			if(featureBuilder.Length > 0)
			{
				builder.Append(", '");
				builder.Append(featureBuilder);
				builder.Append("'");
			}

			// end...
			builder.Append(")");

			// return...
			string script = builder.ToString();
			return script;
		}

		private void AddFeature(StringBuilder builder, string name, object value)
		{
			if(builder == null)
				throw new ArgumentNullException("builder");
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// add...
			if(builder.Length > 0)
				builder.Append(", ");
			builder.Append(name);
			builder.Append("=");
			builder.Append(value);
		}

		private void CheckAndAddFeature(StringBuilder builder, WindowOpenFeatures selected, WindowOpenFeatures check)
		{
			if(builder == null)
				throw new ArgumentNullException("builder");
			
			// value...
			string value = null;
			if((int)(selected & check) != 0)
				value = "on";
			else
				value = "off";

			// defer...
			this.AddFeature(builder, check.ToString().ToLower(), value);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		[Obsolete("Use the overload that takes TransmitDocumentFlags.")]
		public void TransmitDocumentFromFile(string filePath, Encoding encoding, string mimeType, HttpResponse response)
		{
			this.TransmitDocumentFromFile(filePath, encoding, mimeType, response, TransmitDocumentFlags.Normal);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		public void TransmitDocumentFromFile(string filePath, Encoding encoding, string mimeType, HttpResponse response,
			TransmitDocumentFlags flags)
		{
			if(filePath == null)
				throw new ArgumentNullException("filePath");
			if(filePath.Length == 0)
				throw new ArgumentOutOfRangeException("'filePath' is zero-length.");

			// filename...
			string filename = null;
			if((int)(flags & TransmitDocumentFlags.DontSendDisposition) == 0)
				filename = Path.GetFileName(filePath);
			
			// open it...
			using(FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				this.TransmitDocument(stream, encoding, mimeType, filename, response, flags);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		[Obsolete("Use the overload that takes TransmitDocumentFlags.")]
		public void TransmitDocument(string data, Encoding encoding, string mimeType, string fileName, HttpResponse response)
		{
			this.TransmitDocument(data, encoding, mimeType, fileName, response, TransmitDocumentFlags.Normal);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		// mbr - 28-05-2008 - added.		
		public void TransmitDocument(byte[] data, Encoding encoding, string mimeType, string fileName, HttpResponse response,
			TransmitDocumentFlags flags)
		{
			if(data == null)
				throw new ArgumentNullException("data");
			
			// go...
			using(MemoryStream stream = new MemoryStream(data))
				this.TransmitDocument(stream, encoding, mimeType, fileName, response, flags);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		public void TransmitDocument(string data, Encoding encoding, string mimeType, string fileName, HttpResponse response,
			TransmitDocumentFlags flags)
		{
			if(encoding == null)
				encoding = DefaultTransmitEncoding;

			// create...
			using(MemoryStream stream = new MemoryStream())
			{
				// add...
				byte[] bs = encoding.GetBytes(data);
				stream.Write(bs, 0, bs.Length);

				// reset...
				stream.Seek(0, SeekOrigin.Begin);

				// send...
				TransmitDocument(stream, encoding, mimeType, fileName, response, flags);
			}
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		[Obsolete("Use the overload that takes TransmitDocumentFlags.")]
		public void TransmitDocument(XmlDocument doc, string mimeType, string fileName, HttpResponse response)
		{
			this.TransmitDocument(doc, mimeType, fileName, response, TransmitDocumentFlags.Normal);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		public void TransmitDocument(XmlDocument doc, string mimeType, string fileName, HttpResponse response,
			TransmitDocumentFlags flags)
		{
			if(doc == null)
				throw new ArgumentNullException("doc");

			// default...
			if(mimeType == null || mimeType.Length == 0)
				mimeType = MimeTypes.Xml;
			
			// create...
			using(MemoryStream stream = new MemoryStream())
			{
				// add...
				doc.Save(stream);
				
				// reset...
				stream.Seek(0, SeekOrigin.Begin);

				// send...
				TransmitDocument(stream, DefaultTransmitEncoding, mimeType, fileName, response, flags);
			}
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		[Obsolete("Use the overload that takes TransmitDocumentFlags.")]
		public void TransmitDocument(Stream stream, Encoding encoding, string mimeType, string fileName, 
			HttpResponse response)
		{
			this.TransmitDocument(stream, encoding, mimeType, fileName, response, TransmitDocumentFlags.Normal);
		}

		/// <summary>
		/// Transmits a document to the client.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="encoding"></param>
		/// <param name="mimeType"></param>
		/// <param name="fileName"></param>
		/// <param name="response"></param>
		public void TransmitDocument(Stream stream, Encoding encoding, string mimeType, string fileName, 
			HttpResponse response, TransmitDocumentFlags flags)
		{
			if(stream == null)
				throw new ArgumentNullException("stream");			
			if(mimeType == null)
				throw new ArgumentNullException("mimeType");
			if(mimeType.Length == 0)
				throw new ArgumentOutOfRangeException("'mimeType' is zero-length.");
			if(response == null)
				throw new ArgumentNullException("response");

			// mbr - 24-04-2007 - added this...			
			if(encoding == null)
				encoding = DefaultTransmitEncoding;

			// mbr - 06-07-2007 - added.
			if((int)(flags & TransmitDocumentFlags.UseBuffering) == 0)
				response.Buffer = false;

			// mbr - 17-07-2007 - Owen found many issues with this...			
			response.ClearHeaders();
			response.ClearContent();
			response.Clear();

			// set...
			response.ContentType = mimeType;
			long length = stream.Length;
			if(length > 0)
				response.AddHeader("Content-Length", length.ToString());
			response.ContentEncoding = encoding;

			// disposition...
			if(fileName != null && fileName.Length > 0)
				response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));

			// send...
			StreamHelper.CopyStream(stream, response.OutputStream);

			// mbr - 17-07-2007 - final Owen issue...
			response.End();
		}
	}
}
