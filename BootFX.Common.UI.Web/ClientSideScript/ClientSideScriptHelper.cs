// BootFX - Application framework for .NET applications
// 
// File: ClientSideScriptHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for <see cref="ClientSideScriptHelper"/>.
	/// </summary>
	public class ClientSideScriptHelper
	{
		/// <summary>
		/// Defines the filename of the helper script file.
		/// </summary>
		private const string ScriptFileName = "ifx.js";

		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static ClientSideScriptHelper _current = null;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private ClientSideScriptHelper()
		{
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static ClientSideScriptHelper()
		{
			_current = new ClientSideScriptHelper();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static ClientSideScriptHelper Current
		{
			get
			{
				return _current;
			}
		}

		/// <summary>
		/// Gets the script URI.
		/// </summary>
		/// <remarks>This is in the format <c>/ifxcss/a.b.c.d/ifx.js</c></remarks>
		public string ScriptUri
		{
			get
			{
				return string.Format("/ifxcss/{0}/{1}", this.GetType().Assembly.GetName().Version, ScriptFileName);
			}
		}

		/// <summary>
		/// Gets the client's platform.
		/// </summary>
		/// <param name="browser"></param>
		/// <returns></returns>
		public Platform GetClientPlatform(HttpBrowserCapabilities browser)
		{
			if(browser == null)
				throw new ArgumentNullException("browser");			
			return Platform.Windows;
		}

		/// <summary>
		/// Gets the user's browser type.
		/// </summary>
		/// <param name="browser"></param>
		/// <returns></returns>
		public BrowserType GetBrowserType(HttpBrowserCapabilities browser)
		{
			if(browser == null)
				throw new ArgumentNullException("browser");
			
			string name = browser.Browser;
			if(name != null)
				name = name.ToLower();
			switch(name)
			{
				case "ie":
					return BrowserType.Ie;
				case "netscape":
					return BrowserType.Netscape;
				case "operat":
					return BrowserType.Opera;

				default:
					return BrowserType.Unknown;
			}
		}
	}
}
