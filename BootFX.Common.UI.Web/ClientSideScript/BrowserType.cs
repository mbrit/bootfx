// BootFX - Application framework for .NET applications
// 
// File: BrowserType.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for BrowserType.
	/// </summary>
	public enum BrowserType
	{
		Unknown = 0,
		Ie = 1,
		Netscape = 2,
		Opera = 3
	}
}
