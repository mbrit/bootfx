// BootFX - Application framework for .NET applications
// 
// File: HelpIcon.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for <see cref="HelpIcon"/>.
	/// </summary>
	public class HelpIcon : Image
	{
		/// <summary>
		/// Private field to support <c>HelpText</c> property.
		/// </summary>
		private string _helpText;
		
		/// <summary>
		/// Creates a new instance of <see cref="HelpIcon"/>.
		/// </summary>
		public HelpIcon()
		{
			this.ImageUrl = "~/images/help.gif";
			this.Width = new Unit(16, UnitType.Pixel);
			this.Height = new Unit(16, UnitType.Pixel);
//			this.Style["cursor"] = "help";
		}

		/// <summary>
		/// Gets or sets the helptext
		/// </summary>
		public string HelpText
		{
			get
			{
				return _helpText;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _helpText)
				{
					// set the value...
					_helpText = value;
				}
			}
		}

		/// <summary>
		/// Gets the sanitized help text.
		/// </summary>
		private string SanitizedHelpText
		{
			get
			{
				if(this.HelpText != null && this.HelpText.Length > 0)
					return this.HelpText.Replace("\\", string.Empty).Replace("'", string.Empty);
				else
					return "(No help available.)";
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			// set...
			this.Attributes["onmouseover"] = string.Format("javascript:drc('{0}', 'Help'); return true;", this.SanitizedHelpText);
			this.Attributes["onmouseout"] = string.Format("javascript:nd(); return true;", this.SanitizedHelpText);

			// update...
			base.Render (writer);
		}
	}
}
