// BootFX - Application framework for .NET applications
// 
// File: Navigator.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Describes a page that handles navigation.
	/// </summary>
	public class Navigator : HttpContextAwareBase
	{
		private const string BfxPartitionIdRegEx = @"\b__bfxpid=(?<id>[^&]*)";
		
		private string _logonPage = "~/logon.aspx";

		/// <summary>
		/// Defines the name used for flip URLs.
		/// </summary>
		public const string FlipParamName = "__fxflip";

		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static Navigator _current = null;
		
		/// <summary>
		/// Raised when a site URL is needed.
		/// </summary>
		public event EventHandler SiteUrlNeeded;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private Navigator()
		{
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static Navigator()
		{
			_current = new Navigator();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static Navigator Current
		{
			get
			{
				return _current;
			}
		}


		/// <summary>
		/// Gets the logon page name.
		/// </summary>
		public string LogonPage
		{
			get
			{
				return _logonPage;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");		
				_logonPage = value;
			}
		}
	
		/// <summary>
		/// Gets the logon page name.
		/// </summary>
		public string HomePage
		{
			get
			{
				return "~/default.aspx";
			}
		}

		/// <summary>
		/// Goes to the site home page.
		/// </summary>
		public void GoHome()
		{
			this.Response.Redirect(Navigator.Current.HomePage);
		}

		/// <summary>
		/// Flips to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		public void FlipTo(string targetUrl)
		{
			this.FlipTo(targetUrl, true, null);
		}

		/// <summary>
		/// Flips to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		public void FlipTo(string targetUrl, string partitionName)
		{
			if(partitionName == null)
				throw new ArgumentNullException("partitionName");
			if(partitionName.Length == 0)
				throw new ArgumentOutOfRangeException("'partitionName' is zero-length.");
			
			this.FlipTo(targetUrl, true, partitionName);
		}

		/// <summary>
		/// Flips to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		private void FlipTo(string targetUrl, bool endResponse, string partitionName)
		{
			if(targetUrl == null)
				throw new ArgumentNullException("targetUrl");
			if(targetUrl.Length == 0)
				throw new ArgumentOutOfRangeException("'targetUrl' is zero-length.");
			
			// get...
			string flipUrl = this.GetFlipUrl(targetUrl, partitionName);
			if(flipUrl == null)
				throw new InvalidOperationException("'flipUrl' is null.");
			if(flipUrl.Length == 0)
				throw new InvalidOperationException("'flipUrl' is zero-length.");

			// go...
			this.Response.Redirect(flipUrl, endResponse);
		}

		/// <summary>
		/// Gets a flip URL targeted to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		/// <returns></returns>
		public string GetFlipUrl(string targetUrl, string partitionName)
		{
			if(targetUrl == null)
				throw new ArgumentNullException("targetUrl");
			if(targetUrl.Length == 0)
				throw new ArgumentOutOfRangeException("'targetUrl' is zero-length.");

			// current...
			string current = this.GetCurrentUrlAsBase64(partitionName);
			if(current == null)
				throw new InvalidOperationException("'current' is null.");
			if(current.Length == 0)
				throw new InvalidOperationException("'current' is zero-length.");

			// create...
			StringBuilder builder = new StringBuilder();
			builder.Append(targetUrl);
			if(targetUrl.IndexOf("?") == -1)
				builder.Append("?");
			else
				builder.Append("&");
			builder.Append(FlipParamName);
			builder.Append("=");
			builder.Append(current);

			// return...
			return builder.ToString();
		}

		// NMY - 21/09/2007 - Case 797: Web Application - Overreader recommendations - return to session listing
		/// <summary>
		/// Gets a flip URL targeted to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		/// <returns></returns>
		public string GetFlipUrl(string targetUrl, string partitionName, string parameters, bool preserveExistingParameters)
		{
		    if (targetUrl == null)
		        throw new ArgumentNullException("targetUrl");
		    if (targetUrl.Length == 0)
		        throw new ArgumentOutOfRangeException("'targetUrl' is zero-length.");

		    // current...
		    // Delay encoding until the parameter has been added to the return url
		    string current = this.GetCurrentUrl(partitionName);
		    if (current == null)
		        throw new InvalidOperationException("'current' is null.");
		    if (current.Length == 0)
		        throw new InvalidOperationException("'current' is zero-length.");

		    // create...
		    StringBuilder builder = new StringBuilder();
		    builder.Append(targetUrl);
		    if (targetUrl.IndexOf("?") == -1)
		        builder.Append("?");
		    else
		        builder.Append("&");
		    builder.Append(FlipParamName);
		    builder.Append("=");

		    // Preserve the existing parameters?
		    if (!(preserveExistingParameters))
		    {
		        if (current.IndexOf("?") != -1)
		        {
		            current = current.Substring(0, current.IndexOf("?"));
		        }
		    }

		    // Conditionally add the parameters
		    if (parameters != null && parameters.Length > 0)
		    {
		        // Only add one set of parameters
		        if (current.IndexOf("?") == -1)
		            current += "?" + parameters;
		        else
		            current += "&" + parameters;
		    }

		    // Now encode everything
		    builder.Append(EncodeUrl(current));

		    // return...
		    return builder.ToString();
		}

		/// <summary>
		/// Flips back to the calling page.
		/// </summary>
		public void FlipBack()
		{
			this.FlipBack(null);
		}

		/// <summary>
		/// Flips back to the calling name.
		/// </summary>
		public void FlipBack(string name, string value)
		{			
			// create...
			NameValueCollection values = this.BuildReturnUrlValues(name, value);
			if(values == null)
				throw new InvalidOperationException("values is null.");

			// defer...
			this.FlipBack(values);
		}

		private NameValueCollection BuildReturnUrlValues(string name, string value)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// create...
			NameValueCollection values = new NameValueCollection();
			values[name] = value;

			// return...
			return values;
		}

		/// <summary>
		/// Gets the return flip URL for the given page.
		/// </summary>
		/// <param name="page"></param>
		/// <returns>Returns the URL, or null if no flip URL is available.</returns>
		public string GetReturnUrl()
		{
			return this.GetReturnUrl(null);
		}

		/// <summary>
		/// Gets the return flip URL for the given page.
		/// </summary>
		/// <param name="page"></param>
		/// <returns>Returns the URL, or null if no flip URL is available.</returns>
		public string GetReturnUrl(string name, string value)
		{
			// create...
			NameValueCollection values = this.BuildReturnUrlValues(name, value);
			if(values == null)
				throw new InvalidOperationException("values is null.");

			// defer...
			return this.GetReturnUrl(values);
		}

		/// <summary>
		/// Gets the return flip URL for the given page.
		/// </summary>
		/// <param name="page"></param>
		/// <returns>Returns the URL, or null if no flip URL is available.</returns>
		public string GetReturnUrl(NameValueCollection additionalValues)
		{
			// get...
			string flip = this.Request.Params[FlipParamName];
			if(flip == null || flip.Length == 0)
				return null;
			else
			{
				// decode...
				string decodedFlip = Encoding.Unicode.GetString(Convert.FromBase64String(flip));
				if(decodedFlip == null)
					throw new InvalidOperationException("'decodedFlip' is null.");
				if(decodedFlip.Length == 0)
					throw new InvalidOperationException("'decodedFlip' is zero-length.");

				// jm - 29-06-2006 - check for partition id
				long partitionId = 0;
				Regex regex = new Regex(@"\b__bfxpid=(?<id>[^&]*)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				Match match = regex.Match(decodedFlip);
				if(match.Success)
					partitionId = ConversionHelper.ToInt64(match.Groups["id"].Value, Cultures.System);

				// add...
				if(additionalValues != null && additionalValues.Count > 0)
				{
					// get the values from the decoded flip...
					NameValueCollection existingValues = this.GetQueryValues(decodedFlip);
					if(existingValues == null)
						throw new InvalidOperationException("existingValues is null.");

					// marge...
					foreach(string key in additionalValues)
						existingValues[key] = additionalValues[key];

					// create...
					StringBuilder builder = new StringBuilder();

					// find the ?...
					int index = decodedFlip.IndexOf("?");
					if(index != -1)
						builder.Append(decodedFlip.Substring(0, index));
					else
						builder.Append(decodedFlip);
					builder.Append("?");

					// walk...
					bool first = true;
					foreach(string key in additionalValues)
					{
						if(first == true)
							first = false;
						else
							builder.Append("&");
						builder.Append(key);
						builder.Append("=");
						builder.Append(additionalValues[key]);
					}

					// partition id?
					if(partitionId > 0)
						builder.Append("&__bfxpid=" + partitionId.ToString());

					// set...
					decodedFlip = builder.ToString();
				}
				else
				{
					if(partitionId > 0)
						decodedFlip = string.Format("{0}?__bfxpid={1}", decodedFlip, partitionId.ToString());
				}

				// back..
				return decodedFlip;
			}
		}

		/// <summary>
		/// Flips back to the calling page.
		/// </summary>
		/// <param name="additionalValues"></param>
		public void FlipBack(NameValueCollection additionalValues)
		{
			// get...
			string url = this.GetReturnUrl(additionalValues);
			if(url == null || url.Length == 0)
				this.GoHome();
			else
				this.Response.Redirect(url);
		}

		/// <summary>
		/// Gets the query values.
		/// </summary>
		/// <returns></returns>
		private NameValueCollection GetQueryValues()
		{
			NameValueCollection values =new NameValueCollection();
			foreach(string name in this.Request.QueryString.Keys)
				values[name] = this.Request.QueryString[name];

			// return...
			return values;
		}

		/// <summary>
		/// Gets the values from the given raw string.
		/// </summary>
		/// <param name="rawString"></param>
		/// <returns></returns>
		/// <remarks>This method will seek out the '?' delimiter.</remarks>
		private NameValueCollection GetQueryValues(string url)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			
			// create...
			NameValueCollection results = new NameValueCollection();
			int index = url.IndexOf("?");
			if(index != -1)
			{
				// get...
				string raw = url.Substring(index + 1);

				// split...
				string[] parts = raw.Split('&');
				foreach(string part in parts)
				{
					// split again...
					index = part.IndexOf("=");

					// name...
					string name = string.Empty;
					string value = string.Empty;
					if(index != -1)
					{
						name = part.Substring(0, index);
						value = part.Substring(index + 1);
					}
					else
						name = part;

					// add...
					results[name] = value;
				}
			}

			// return...
			return results;
		}

		public string EncodeUrl(string url)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			
			// return...
			return Convert.ToBase64String(Encoding.Unicode.GetBytes(url));
		}

		/// <summary>
		/// Gets a flip URL targeted to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		/// <returns></returns>
		private string GetCurrentUrlAsBase64(string partitionName)
		{
			// get...
			string currentUrl = this.GetCurrentUrl(partitionName);
			if(currentUrl == null)
				throw new InvalidOperationException("'currentUrl' is null.");
			if(currentUrl.Length == 0)
				throw new InvalidOperationException("'currentUrl' is zero-length.");

			// defer...
			return this.EncodeUrl(currentUrl);
		}

		/// <summary>
		/// Gets a flip URL targeted to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		/// <returns></returns>
		private string GetCurrentUrl(string partitionName)
		{
			// get the qv...
			NameValueCollection values = this.GetQueryValues();
			if(values == null)
				throw new InvalidOperationException("values is null.");

			// defer...
			return this.GetCurrentUrl(values, partitionName);
		}

		/// <summary>
		/// Gets a flip URL targeted to the given URL.
		/// </summary>
		/// <param name="targetUrl"></param>
		/// <returns></returns>
		private string GetCurrentUrl(NameValueCollection values, string partitionName)
		{
			if(values == null)
				throw new InvalidOperationException("values is null.");
			
			// get...
			string scriptName = this.Request.ServerVariables["script_name"];
			int index = scriptName.LastIndexOf(@"/");
			if(partitionName != null && partitionName.Length > 0)
			{
				string pageName = scriptName.Substring(index + 1);
				string vd = scriptName.Substring(0, index);

				scriptName = string.Format("{0}/{1}/{2}", vd, partitionName, pageName);
			}

			// get...
			StringBuilder builder = new StringBuilder();
			builder.Append(scriptName);
			bool first = true;
			foreach(string key in values.Keys)
			{
				if(first)
				{
					builder.Append("?");
					first = false;
				}
				else
					builder.Append("&");
				builder.Append(key);
				builder.Append("=");
				builder.Append(values[key]);
			}

			// return it...
			return builder.ToString();
		}

		/// <summary>
		/// Raises the <c>SiteUrlNeeded</c> event.
		/// </summary>
		private void OnSiteUrlNeeded()
		{
			OnSiteUrlNeeded(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>SiteUrlNeeded</c> event.
		/// </summary>
		protected virtual void OnSiteUrlNeeded(EventArgs e)
		{
			// raise...
			if(SiteUrlNeeded != null)
				SiteUrlNeeded(this, e);
		}
	}
}
