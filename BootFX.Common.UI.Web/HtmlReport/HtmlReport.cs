// BootFX - Application framework for .NET applications
// 
// File: HtmlReport.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Base class for Web reports.
	/// </summary>
	public abstract class HtmlReport : Loggable
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		protected HtmlReport()
		{
		}

		/// <summary>
		/// Rebuilds the report.
		/// </summary>
		public abstract void RebuildData(Control control);
	}
}
