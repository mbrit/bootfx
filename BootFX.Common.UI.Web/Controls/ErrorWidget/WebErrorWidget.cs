// BootFX - Application framework for .NET applications
// 
// File: WebErrorWidget.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>WebErrorWidget</c>.
	/// </summary>
    public class WebErrorWidget : LoggableTable, IUserFeedbackWidgetHost
	{
		/// <summary>
		/// Private field to support <see cref="FeedbackWidget"/> property.
		/// </summary>
		private UserFeedbackWidget _feedbackWidget;
		
		/// <summary>
		/// Private field to support <c>RefreshViewCalled</c> property.
		/// </summary>
		private bool _refreshViewCalled = false;
		
		private const string SessionKey = "__bfxError";
		public const string DefaultTagName = "__bfxErrorTag";

		public WebErrorWidget()
		{
		}

		/// <summary>
		/// Tags the last occured error and returns a GUID.
		/// </summary>
		/// <returns></returns>
		public static Guid TagError()
		{
			HttpContext context = HttpContext.Current;
			if(context == null)
				throw new InvalidOperationException("context is null.");

			// get...
			Exception ex = context.Server.GetLastError();
			if(ex == null)
				throw new InvalidOperationException("ex is null.");
			try
			{
				// mbr - 15-06-2008 - created an overload that you can give an exception to...
				//				// set...
//				Guid guid = Guid.NewGuid();
//				context.Session[SessionKey + guid.ToString()] = new WebError(context.Request, ex);
//
//				// return...
//				return guid;

				return TagError(ex);
			}
			finally
			{
				context.Server.ClearError();
			}
		}

		/// <summary>
		/// Tags the last occured error and returns a GUID.
		/// </summary>
		/// <returns></returns>
		public static Guid TagError(Exception ex)
		{
			if(ex == null)
				throw new ArgumentNullException("ex");

			// get...
			HttpContext context = HttpContext.Current;
			if(context == null)
				throw new InvalidOperationException("context is null.");

			// set...
			Guid guid = Guid.NewGuid();
			context.Session[SessionKey + guid.ToString()] = new WebError(context.Request, ex);

			// return...
			return guid;
		}

		private void RefreshView()
		{
			_refreshViewCalled = true;
			this.BuildViewBody();
		}

		private void BuildViewBody()
		{
			this.Rows.Clear();

			// add...
			this.RenderText(this, "<H1>An error has occurred</H1>An unhandled error has occurred in this application.  Your work may be lost.  Please contact technical support if you require advice on how to proceed.", null);

			// get that error...
			WebError error = this.Error;
			if(error != null)
			{
//				// extended...
//				if(this.ShowExtendedDetails)
//				{
//					this.RenderText(this, @"<BR /><BR /><SPAN STYLE=""FONT-WEIGHT:BOLD;"">This information may be of use when diagnosing the error:</SPAN>", null);
//
//					// ex...
//					StringBuilder builder = new StringBuilder();
//					using(StringReader reader = new StringReader(error.Exception.ToString()))
//					{
//						while(true)
//						{
//							string buf = reader.ReadLine();
//							if(buf == null)
//								break;
//
//							// len...
//							if(builder.Length > 0)
//								builder.Append("<BR />");
//
//							// build...
//							for(int index = 0; index < buf.Length; index++)
//							{
//								if(char.IsWhiteSpace(buf[index]))
//									builder.Append("&nbsp;");
//								else
//								{
//									builder.Append(buf.Substring(index));
//									break;
//								}
//							}
//						}
//					}
//					this.RenderText(this, string.Format(@"<SPAN STYLE=""FONT-SIZE:8PT;"">{0}</SPAN>", builder), null);
//
//					// ex...
//					this.RenderText(this, "<BR /><BR />Technical support may ask you to copy the contents of the box below and e-mail it to them:<BR />", null);
//					TableRow row = new TableRow();
//					this.Rows.Add(row);
//					TableCell cell = new TableCell();
//					row.Cells.Add(cell);
//					TextBox box = new TextBox();
//					box.TextMode = TextBoxMode.MultiLine;
//					box.Text = error.ErrorXmlAsBase64Block;
//					box.Rows = 15;
//					box.Columns = 65;
//					box.EnableViewState = false;
//					box.ReadOnly = true;
//					cell.Controls.Add(box);
//
//					// right-click...
//					//					this.RenderText(this, @"<BR /><SPAN STYLE=""FONT-SIZE:8PT;"">To e-mail the contents of the text box, right-click on it and choose <I>Select All</I>, and then<BR />select <i>Copy</i>. Then, create a new message in your e-mail application and select <i>Edit - Paste</i>.<BR />Finally, address and send the message as normal.</SPAN>", null);
//				}
//				else if(this.AllowExtendedErrorInformation)
//				{
//					TableRow row = new TableRow();
//					this.Rows.Add(row);
//					TableCell cell = new TableCell();
//					row.Cells.Add(cell);
//					
//					// link...
//					LinkButton button = new LinkButton();
//					cell.Controls.Add(button);
//					button.Text = "Show extended error information...";
//					button.Click += new EventHandler(button_Click);
//				}

				// mbr - 15-05-2008 - changed to use the feedback widget...
				this.FeedbackWidget.SetError(error);
				TableHelper.AddControlRow(this, FeedbackWidget);
			}
			else
			{
				if(this.AllowExtendedErrorInformation)
					this.RenderText(this, "Extended error information is not available.", null);	
			}
		}

		private void RenderText(Table table, string buf, string cssClass)
		{
			if(table == null)
				throw new ArgumentNullException("table");

			TableRow row = new TableRow();
			table.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Text = buf;
			cell.EnableViewState = false;
			if(cssClass != null && cssClass.Length > 0)
				cell.CssClass = cssClass;
		}

		private void RenderField(string name, string value)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// output...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Text = name + ":&nbsp;";
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Text = value;
		}

		/// <summary>
		/// Gets or sets the errortag.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>ErrorTag</c> for storage.
		/// </remarks>
		public Guid ErrorTag
		{
			get
			{
				object value = ViewState["ErrorTag"];
				if(value == null)
					return Guid.Empty;
				else
					return (Guid)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != ErrorTag)
				{
					// set the value...
					ViewState["ErrorTag"] = value;

					// get the error out of the session and put it in this control's view state...
					string key = SessionKey + value.ToString();
					WebError error = (WebError)this.Page.Session[key];
					if(error != null)
					{
						// error...
						this.Error = error;

						// reset...
						this.Page.Session.Remove(key);
					}

					// update...
					this.RefreshView();
				}
			}
		}

		/// <summary>
		/// Gets or sets the error.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>Error</c> for storage.
		/// </remarks>
		private WebError Error
		{
			get
			{
				object value = ViewState["Error"];
				if(value == null)
					return null;
				else
					return (WebError)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != Error)
				{
					// set the value...
					ViewState["Error"] = value;
				}
			}
		}

		private void EnsureErrorInitialized()
		{
			if(this.ErrorTag != Guid.Empty)
				return;

			// try...
			HttpRequest request = HttpContext.Current.Request;
			if(request == null)
				throw new InvalidOperationException("request is null.");

			// get...
			string asString = request.Params[DefaultTagName];
			if(asString != null && asString.Length > 0)
				this.ErrorTagAsString = asString;

            // mbr - 2010-06-07 - removed...
            //if(this.Log.IsErrorEnabled)
            //{
            //    const string message = "An error was handled by the Web error widget.";
            //    if(this.Error != null)
            //    {
            //        if(this.Error.Exception != null)
            //            this.Log.Error(message, this.Error.Exception);
            //        this.Log.Error(message + "");
            //    }
            //    else
            //        this.Log.Error(message + "  No error information was available.");
            //}
		}

		/// <summary>
		/// Gets the basic exception.
		/// </summary>
		/// <returns></returns>
		public Exception GetException()
		{
			// check...
			this.EnsureErrorInitialized();

			// return...
			if(Error == null)
				throw new InvalidOperationException("Error is null.");
			return this.Error.Exception;
		}

		/// <summary>
		/// Gets the complete XML error message.
		/// </summary>
		/// <returns></returns>
		public string GetErrorXml()
		{
			// check...
			this.EnsureErrorInitialized();

			// return...
			if(Error == null)
				throw new InvalidOperationException("Error is null.");
			return this.Error.ErrorXml;
		}

		/// <summary>
		/// Gets the refreshviewcalled.
		/// </summary>
		private bool RefreshViewCalled
		{
			get
			{
				// returns the value...
				return _refreshViewCalled;
			}
		}

		protected override void EnsureChildControls()
		{
			if(!(this.RefreshViewCalled))
				base.EnsureChildControls ();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// first?
			if(!(this.Page.IsPostBack))
			{
				// tag?
				if(this.ErrorTag == Guid.Empty)
				{
					this.EnsureErrorInitialized();
				}
			}

			// update...
			if(!(this.RefreshViewCalled))
				this.RefreshView();
		}

		// mbr - 15-05-2008 - removed.
		//		/// <summary>
//		/// Gets or sets the showextendeddetails.
//		/// </summary>
//		/// <remarks>
//		/// This property uses view state item <c>ShowExtendedDetails</c> for storage.
//		/// </remarks>
//		private bool ShowExtendedDetails
//		{
//			get
//			{
//				object value = ViewState["ShowExtendedDetails"];
//				if(value == null)
//					return false;
//				else
//					return (bool)value;
//			}
//			set
//			{
//				// check to see if the value has changed...
//				if(value != ShowExtendedDetails)
//				{
//					// set the value...
//					ViewState["ShowExtendedDetails"] = value;
//
//					// update...
//					this.RefreshView();
//				}
//			}
//		}

		/// <summary>
		/// Gets the FeedbackWidget.
		/// </summary>
		private UserFeedbackWidget FeedbackWidget
		{
			get
			{
				if(_feedbackWidget == null)
					_feedbackWidget = new UserFeedbackWidget();
				return _feedbackWidget;
			}
		}

		// mbr - 15-05-2008 - removed.
//		private void button_Click(object sender, EventArgs e)
//		{
//			this.ShowExtendedDetails = true;
//		}

		public string ErrorTagAsString
		{
			get
			{
				return this.ErrorTag.ToString();
			}
			set
			{
				if(value != null && value.Length > 0)
					this.ErrorTag = new Guid(value);
				else
					this.ErrorTag = Guid.Empty;
			}
		}

		
		/// <summary>
		/// Gets or sets the allowadvanced.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>AllowAdvanced</c> for storage.
		/// </remarks>
		[Browsable(true), Category("Behavior")]
        public bool AllowExtendedErrorInformation
		{
			get
			{
				object value = ViewState["AllowExtendedErrorInformation"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != AllowExtendedErrorInformation)
				{
					// set the value...
					ViewState["AllowExtendedErrorInformation"] = value;

					// update...
					if(this.RefreshViewCalled)
						this.RefreshView();
				}
			}
		}

        // mbr - 2009-12-21 - database log is obsolete...
        ///// <summary>
        ///// Receives an error received through a "phone home" post.
        ///// </summary>
        ///// <param name="page"></param>
        //public static void ReceiveError(Page page)
        //{
        //    ReceiveError(page, null);
        //}

        ///// <summary>
        ///// Receives an error received through a "phone home" post.
        ///// </summary>
        ///// <param name="page"></param>
        //public static void ReceiveError(Page page, ConnectionSettings settings)
        //{
        //    if(page == null)
        //        throw new ArgumentNullException("page");
			
        //    // check...
        //    if(page == null)
        //        throw new InvalidOperationException("page is null.");
        //    if(page.Response == null)
        //        throw new InvalidOperationException("page.Response is null.");

        //    // create a document...
        //    XmlDocument outDoc = new XmlDocument();
        //    XmlElement outElement = outDoc.CreateElement("Result");
        //    outDoc.AppendChild(outElement);

        //    // try...
        //    try
        //    {
        //        if(page.Request == null)
        //            throw new InvalidOperationException("page.Request is null.");
        //        if(page.Request.InputStream == null)
        //            throw new InvalidOperationException("page.Request.InputStream is null.");

        //        // get...
        //        XmlDocument doc = new XmlDocument();
        //        doc.Load(page.Request.InputStream);

        //        // create a bfx log item...
        //        object result = DatabaseAppender.WritePhoneHomeEntry(doc, settings);

        //        // create...
        //        XmlHelper.AddElement(outElement, "Ok", result);
        //    }
        //    catch(Exception ex)
        //    {
        //        // create...
        //        HandleErrorReceiveException(outElement, page.Response, ex);
        //    }

        //    // send...
        //    WebRuntime.Current.TransmitDocument(outDoc, null, null, page.Response,
        //        TransmitDocumentFlags.Normal);
        //}

        //private static void HandleErrorReceiveException(XmlElement element, HttpResponse response, Exception ex)
        //{
        //    if(element == null)
        //        throw new ArgumentNullException("element");
        //    if(response == null)
        //        throw new ArgumentNullException("response");
        //    if(ex == null)
        //        throw new ArgumentNullException("ex");

        //    // log it...
        //    ILog log = LogSet.GetLog(typeof(WebErrorWidget));
        //    if(log.IsErrorEnabled)
        //        log.Error("Failed to handle receipt of exception message.");
			
        //    // create...
        //    XmlHelper.AddElement(element, "Error", ex.ToString());
        //}
	}
}
