// BootFX - Application framework for .NET applications
// 
// File: WebError.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using BootFX.Common.Xml;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>WebError</c>.
	/// </summary>
	[Serializable()]
	internal class WebError : IUserMessages
	{
		/// <summary>
		/// Private field to support <c>ErrorXml</c> property.
		/// </summary>
		private string _errorXml;
		
		/// <summary>
		/// Private field to support <see cref="Exception"/> property.
		/// </summary>
		private Exception _exception;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="exception"></param>
		internal WebError(HttpRequest request, Exception exception)
		{
			if(request == null)
				throw new ArgumentNullException("request");
			if(exception == null)
				throw new ArgumentNullException("exception");
			
			// set...
			_exception = exception;

			// create an error report if the runtime is started...
			if(Runtime.IsStarted)
			{
				// create...
				const string message = "An unhandled error has occurred in a Web application.";
				LogData data = new LogData(LogLevel.Error, new LogArgs(message, exception), message);

				// extra...
				IDictionary extra = new HybridDictionary();
				extra["Url"] = request.Url.ToString();

				// server variables...
				try
				{
					this.AddParams(request.ServerVariables, extra, "SV");
					this.AddParams(request.QueryString, extra, "QS");
					this.AddParams(request.Form, extra, "F");
				}
				catch
				{
					// mbr - 28-09-2006 - no-op... sometimes this goes wrong?  not sure why...					
				}

				// cookies...
				foreach(string name in request.Cookies)
				{
					HttpCookie cookie = request.Cookies[name];
					extra["C_" + name] = cookie.Value;
				}

				// get...
				XmlDocument doc = PhoneHomeHelper.Current.CreateErrorReport(data, extra);
				if(doc == null)
					throw new InvalidOperationException("doc is null.");
				_errorXml = XmlHelper.XmlDocumentToString(doc);
			}
		}

		private void AddParams(NameValueCollection ps, IDictionary extra, string prefix)
		{
			if(ps == null)
				throw new ArgumentNullException("ps");
			if(extra == null)
				throw new ArgumentNullException("extra");
			if(prefix == null)
				throw new ArgumentNullException("prefix");
			if(prefix.Length == 0)
				throw new ArgumentOutOfRangeException("'prefix' is zero-length.");			
			
			foreach(string name in ps.Keys)
			{
				if(name != "ALL_HTTP" && name != "ALL_RAW" && name != "QUERY_STRING" && name != "HTTP_COOKIE")
					extra[prefix + "_" + name] = ps[name];
			}
		}

		/// <summary>
		/// Gets the exception.
		/// </summary>
		internal Exception Exception
		{
			get
			{
				return _exception;
			}
		}

		/// <summary>
		/// Gets the errorxml.
		/// </summary>
		internal string ErrorXml
		{
			get
			{
				// returns the value...
				return _errorXml;
			}
		}

		public string ErrorXmlAsBase64String
		{
			get
			{
				string xml = this.ErrorXml;
				if(xml == null)
					throw new InvalidOperationException("'xml' is null.");
				if(xml.Length == 0)
					throw new InvalidOperationException("'xml' is zero-length.");

				// return...
				return Convert.ToBase64String(Encoding.Unicode.GetBytes(xml));
			}
		}

		public string ErrorXmlAsBase64Block
		{
			get
			{
				string asString = this.ErrorXmlAsBase64String;

				// walk...
				StringBuilder builder = new StringBuilder();

				// header...
				builder.Append("----------ERROR START----------");

				// walk...
				for(int index = 0; index < asString.Length; index++)
				{
					if(index % 60 == 0)
						builder.Append("\r\n");
					builder.Append(asString[index]);
				}

				// footer...
				builder.Append("\r\n----------ERROR END----------\r\n");

				// done...
				return builder.ToString();
			}
		}

		/// <summary>
		/// Gets the user messages to display.
		/// </summary>
		/// <returns></returns>
		public string[] GetUserMessages()
		{
			if(Exception == null)
				throw new InvalidOperationException("Exception is null.");

			// return...
			return ExceptionWithUserMessages.GetUserMessages(this.Exception);
		}
	}
}
