// BootFX - Application framework for .NET applications
// 
// File: WebHealthCheckWidget.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Threading;
using BootFX.Common;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Describes a control that can be used to run a health check.
	/// </summary>
	public class WebHealthCheckWidget : Label
	{		
		/// <summary>
		/// Raised when the check succeeds.
		/// </summary>
		public event HealthCheckResultEventHandler Finished;
		
		/// <summary>
		/// Runs the checks that you require.
		/// </summary>
		public event EventHandler DoCheck;

		/// <summary>
		/// Private field to support <see cref="Checker"/> property.
		/// </summary>
		private HealthCheck _checker;
		
		public WebHealthCheckWidget()
		{
			// set...
			this.SetChecker(new HealthCheck());

			// reset...
			this.Text = "Pending...";
		}

		/// <summary>
		/// Sets the checker to use.
		/// </summary>
		/// <param name="checker"></param>
		public void SetChecker(HealthCheck checker)
		{
			if(checker == null)
				throw new ArgumentNullException("checker");			

			// create and sub...
			_checker = new HealthCheck();
			_checker.DoCheck += new EventHandler(_checker_DoCheck);
			_checker.Finished += new HealthCheckResultEventHandler(_checker_Finished);
		}

		/// <summary>
		/// Gets the checker.
		/// </summary>
		private HealthCheck Checker
		{
			get
			{
				return _checker;
			}
		}

		private void _checker_DoCheck(object sender, EventArgs e)
		{
			this.OnDoCheck();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// run...
			if(Checker == null)
				throw new InvalidOperationException("Checker is null.");
			this.Checker.Check();
		}

		private void _checker_Finished(object sender, HealthCheckResultEventArgs e)
		{
			if(e == null)
				throw new ArgumentNullException("e");

			// on...
			this.OnFinished(e);

			// show...
			if(e.Result == null)
				throw new InvalidOperationException("e.Result is null.");
			this.Text = e.Result.Message;
		}

		/// <summary>
		/// Raises the <c>DoCheck</c> event.
		/// </summary>
		private void OnDoCheck()
		{
			OnDoCheck(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DoCheck</c> event.
		/// </summary>
		protected virtual void OnDoCheck(EventArgs e)
		{
			// raise...
			if(DoCheck != null)
				DoCheck(this, e);
		}

		/// <summary>
		/// Raises the <c>CheckFailed</c> event.
		/// </summary>
		protected virtual void OnFinished(HealthCheckResultEventArgs e)
		{
			// raise...
			if(Finished != null)
				Finished(this, e);
		}
	}
}
