// BootFX - Application framework for .NET applications
// 
// File: InnerDataGrid.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Data.Comparers;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
    /// <summary>
    /// Defines an overridden form of data grid that captures the output and routes it through a handle on <c>AutoDataGrid</c>.
    /// </summary>
    // mbr - 2010-03-02 - added...
    internal class InnerDataGrid : DataGrid
    {
        /// <summary>
        /// Private value to support the <see cref="Owner">Owner</see> property.
        /// </summary>
        private AutoDataGrid _owner;

        internal InnerDataGrid(AutoDataGrid owner)
        {
            if (owner == null)
                throw new ArgumentNullException("owner");
            _owner = owner;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // shim it...
            string html = null;
            using (StringWriter output = new StringWriter())
            {
                HtmlTextWriter newWriter = (HtmlTextWriter)Activator.CreateInstance(writer.GetType(), output);
                base.Render(newWriter);

                // get...
                html = output.ToString();
            }

            // get...
            if (Owner == null)
                throw new InvalidOperationException("'Owner' is null.");
            html = Owner.HandleGridRender(html);
            if (html == null)
                throw new InvalidOperationException("'html' is null.");

            // render...
            writer.Write(html);
        }

        /// <summary>
        /// Gets the Owner value.
        /// </summary>
        private AutoDataGrid Owner
        {
            get
            {
                return _owner;
            }
        }
    }
}
