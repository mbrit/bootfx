// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridDropDownList.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoDataGridDropDownList</c>.
	/// </summary>
	internal class AutoDataGridDropDownList : ConstraintDropDownList
	{
		internal AutoDataGridDropDownList()
		{
		}
	}
}
