// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridHyperLink.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ConstraintLabel</c>.
	/// </summary>
	public class AutoDataGridHyperLink : HyperLink, IConstraintControl, IAutoDataGridBindableControl
	{
		/// <summary>
		/// Private field to support <c>LinkTemplate</c> property.
		/// </summary>
		private string _linkTemplate;
		
		/// <summary>
		/// Private field to support <c>Value</c> property.
		/// </summary>
		private object _value;
		
		/// <summary>
		/// Private field to support <c>BootFX.Common.Data.SqlOperator Operator</c> property.
		/// </summary>
		private SqlOperator _operator = SqlOperator.EqualTo;
		
		/// <summary>
		/// Private field to support <c>EntityTypeId</c> property.
		/// </summary>
		private string _entityTypeId;
		
		/// <summary>
		/// Private field to support <c>FieldName</c> property.
		/// </summary>
		private string _fieldName;
		
		public AutoDataGridHyperLink()
		{
		}

		public BootFX.Common.UI.ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.SupportsDbNull;
			}
		}

		/// <summary>
		/// Gets or sets the value
		/// </summary>
		public object Value
		{
			get
			{
				return _value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _value)
				{
					// set the value...
					_value = value;

					// update...
					this.Text = ConversionHelper.ToString(this.Value, Cultures.User);
				}
			}
		}

		/// <summary>
		/// Gets or sets the entitytypeid
		/// </summary>
		public string EntityTypeId
		{
			get
			{
				return _entityTypeId;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _entityTypeId)
				{
					// set the value...
					_entityTypeId = value;
				}
			}
		}

		public bool IsValueSet
		{
			get
			{
				if(this.Value != null)
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Gets or sets the BootFX.Common.data.sqloperator operator
		/// </summary>
		public BootFX.Common.Data.SqlOperator Operator
		{
			get
			{
				return _operator;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _operator)
				{
					// set the value...
					_operator = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the fieldname
		/// </summary>
		public string FieldName
		{
			get
			{
				return _fieldName;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _fieldName)
				{
					// set the value...
					_fieldName = value;
				}
			}
		}

		public void Initialize(BootFX.Common.Data.FlexData.IFlexField field, LayoutHint hint)
		{
			// TODO:  Add ConstraintLabel.Initialize implementation
		}

		public void MangleFilter(SqlFilter filter)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		public void SerializeSqlSearchSettings(IDictionary bag)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		public string ManglerId
		{
			get
			{
				// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
				throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
			}
			set
			{
				// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
				throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
			}
		}

		public void DeserializeSqlSearchSettings(IDictionary bag)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		public void DataBound(object entity)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");

			// get...
			EntityType et = EntityType.GetEntityType(entity, OnNotFound.ThrowException);
			object[] key = et.Storage.GetKeyValues(entity);
			if(key == null)
				throw new InvalidOperationException("'key' is null.");
			if(key.Length != 1)
				throw new InvalidOperationException(string.Format("'key' is an invalid length ({0}).", key.Length));
			
			// check...
			if(this.LinkTemplate != null && this.LinkTemplate.Length > 0)
				this.NavigateUrl = string.Format(this.LinkTemplate, key[0]);
			else
				this.NavigateUrl = string.Empty;				
		}

		/// <summary>
		/// Gets or sets the linktemplate
		/// </summary>
		internal string LinkTemplate
		{
			get
			{
				return _linkTemplate;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _linkTemplate)
				{
					// set the value...
					_linkTemplate = value;
				}
			}
		}
	}
}
