// BootFX - Application framework for .NET applications
// 
// File: AutoDataGrid.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Data.Comparers;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoDataGrid</c>.
	/// </summary>
	public class AutoDataGrid : AutoControlBase, INamingContainer
	{
        private bool _sortSpecificationLoaded = false;
        private int _sortColumnIndex = -1;
        private BootFX.Common.Data.SortDirection _sortDirection = BootFX.Common.Data.SortDirection.Ascending;

        /// <summary>
        /// Private value to support the <see cref="ShowWidgetColumn">ShowWidgetColumn</see> property.
        /// </summary>
        private bool _showWidgetColumn = true;

        /// <summary>
        /// Defines the <c>BeforeBind</c> event.
        /// </summary>
        public event AutoDataGridBindEventHandler BeforeBind;

        /// <summary>
        /// Defines the <c>AfterBind</c> event.
        /// </summary>
        public event AutoDataGridBindEventHandler AfterBind;

        /// <summary>
        /// Defines the <c>ComparerNeeded</c> event.
        /// </summary>
        public event EventHandler ComparerNeeded;

        private IComparer _comparer;

        /// <summary>
        /// Private value to support the <see cref="ShowWhenNoItems">ShowWhenNoItems</see> property.
        /// </summary>
        private bool _showWhenNoItems = false;

        /// <summary>
        /// Defines the <c>GridRender</c> event.
        /// </summary>
        public event RenderEventHandler GridRender;

        private Unit _widgetColumnWidth = Unit.Empty;

        /// <summary>
        /// 		/// Private field to support <see cref="EntityType"/> property.
        /// </summary>
        private EntityType _entityType;

        /// <summary>
        /// Private field to support <see cref="Grid"/> property.
        /// </summary>
        private InnerDataGrid _grid;

        /// <summary>
		/// Raised when a command is received.
		/// </summary>
		public event DataGridCommandEventHandler ItemCommand;
        public event DataGridItemEventHandler ItemCreated;
		
		/// <summary>
		/// Raised when buttons are needed.
		/// </summary>
		public event AutoDataGridButtonRequestEventHandler ButtonsNeeded;
		
		/// <summary>
		/// Raised after an item is inserted.
		/// </summary>
		public event EntityEventHandler AfterUpdate;
		
		/// <summary>
		/// Raised after an item is inserted.
		/// </summary>
		public event EntityEventHandler AfterInsert;
		
		/// <summary>
		/// Raised after an item is inserted.
		/// </summary>
		public event EntityEventHandler AfterDelete;
		
		/// <summary>
		/// Private field to support <see cref="RefreshViewDepth"/> property.
		/// </summary>
		private int _refreshViewDepth = 0;
		
		/// <summary>
		/// Private field to support <see cref="RefreshViewCalled"/> property.
		/// </summary>
		private bool _refreshViewCalled = false;
		
		/// <summary>
		/// Private field to support <c>IgnoreDataBind</c> property.
		/// </summary>
		private bool _ignoreDataBind = false;
		
		/// <summary>
		/// Private field to support <see cref="NoItemsLabel"/> property.
		/// </summary>
		private Label _noItemsLabel;
		
		/// <summary>
		/// Private field to support <c>AddFormCaption</c> property.
		/// </summary>
		private string _addFormCaption;
		
		private object _dataSource = null;
		private string _addButtonText = "Add";

		/// <summary>
		/// Raised when a source of data is needed for the grid.
		/// </summary>
		public event EventHandler DataSourceNeeded;
		
		/// <summary>
		/// Private field to support <see cref="AddForm"/> property.
		/// </summary>
		private AutoForm _addForm;

		/// <summary>
		/// Private field to support <c>Columns</c> property.
		/// </summary>
		private AutoColumnSpecificationCollection _columns = new AutoColumnSpecificationCollection();
		
		/// <summary>
		/// Raised before an entity is updated.
		/// </summary>
		public event AutoControlApplyEventHandler BeforeDelete;
		
		/// <summary>
		/// Raised before an entity is updated.
		/// </summary>
		public event AutoControlApplyEventHandler BeforeUpdate;
		
		/// <summary>
		/// Raised before an entity is inserted.
		/// </summary>
		public event AutoControlApplyEventHandler BeforeInsert
		{
			add
			{
				if(AddForm == null)
					throw new InvalidOperationException("AddForm is null.");
				this.AddForm.BeforeSaveChanges += value;
			}
			remove
			{
				if(AddForm == null)
					throw new InvalidOperationException("AddForm is null.");
				this.AddForm.BeforeSaveChanges -= value;
			}
		}

		/// <summary>
		/// Raises the <c>AfterInsert</c> event.
		/// </summary>
		protected virtual void OnAfterInsert(EntityEventArgs e)
		{
			// raise...
			if(AfterInsert != null)
				AfterInsert(this, e);
		}
				
		/// <summary>
		/// Raises the <c>AfterUpdate</c> event.
		/// </summary>
		protected virtual void OnAfterUpdate(EntityEventArgs e)
		{
			// raise...
			if(AfterUpdate != null)
				AfterUpdate(this, e);
		}
				
		/// <summary>
		/// Raised before an entity is inserted.
		/// </summary>
		public event AutoControlApplyEventHandler ValidateNewItem
		{
			add
			{
				if(AddForm == null)
					throw new InvalidOperationException("AddForm is null.");
				this.AddForm.Validate += value;
			}
			remove
			{
				if(AddForm == null)
					throw new InvalidOperationException("AddForm is null.");
				this.AddForm.Validate -= value;
			}
		}

		public AutoDataGrid()
		{
			this.Width = new Unit(100, UnitType.Percentage);
		}

		/// <summary>
		/// Gets the refreshviewdepth.
		/// </summary>
		private int RefreshViewDepth
		{
			get
			{
				return _refreshViewDepth;
			}
		}

		/// <summary>
		/// Updates the view.
		/// </summary>
		private void RefreshView(bool reset, bool onlyIfAlreadyDone)
		{
			// stop...
			if(RefreshViewDepth > 0)
				return;

			// add...
			_refreshViewDepth++;
			try
			{
                if (reset)
                {
                    _dataSource = null;
                    _comparer = null;
                }

				// first?
				if(onlyIfAlreadyDone && !(this.RefreshViewCalled))
					return;

				// call...
				_refreshViewCalled = true;

				// update...
				this.RefreshGrid();
				this.RefreshAddForm();
			}
			finally
			{
				_refreshViewDepth--;
			}
		}

		/// <summary>
		/// Gets the refreshviewcalled.
		/// </summary>
		private bool RefreshViewCalled
		{
			get
			{
				return _refreshViewCalled;
			}
		}

		private void RefreshGrid()
		{
			this.Rows.Clear();
			
			// grid...
			TableHelper.AddControlRow(this, this.Grid);
			TableHelper.AddControlRow(this, this.FeedbackWidget);
			TableHelper.AddControlRow(this, this.NoItemsLabel);

			// mbr - 12-02-2007 - show add?
			if(this.AllowAdd)
			{
				// spacer...
				TableHelper.AddTextRow(this, "&nbsp;");

				// add form...
				if(this.AddFormCaption != null && this.AddFormCaption.Length > 0)
				{
					TableCell cell = TableHelper.CreateTextCell(this.AddFormCaption);
					cell.CssClass = "heading2";

					// add...
					TableRow row = new TableRow();
					this.Rows.Add(row);
					row.Cells.Add(cell);
				}
				TableHelper.AddControlRow(this, this.AddForm);
			}

			// expand the grid...
			this.SetupGridColumns();
			this.BindGrid();

			// do we have any items?
			if(this.DataSource is IList)
			{
				IList list = (IList)this.DataSource;

                // mbr - 2010-03-02 - changed...
//				if(list.Count == 0)
                if (list.Count == 0 && !(ShowWhenNoItems))
				{
					this.Grid.Visible = false;
					this.NoItemsLabel.Visible = true;
				}
				else
				{
					this.Grid.Visible = true;
					this.NoItemsLabel.Visible = false;
				}
			}

			// setup the add table...
			this.RefreshAddForm();
        }

		private void RefreshAddForm()
		{
			if(AddForm == null)
				throw new InvalidOperationException("AddForm is null.");
			this.AddForm.RefreshView();
		}

		public override void DataBind()
		{
			// ignore?
			if(this.IgnoreDataBind)
				return;

			// set...
			if(Grid == null)
				throw new InvalidOperationException("Grid is null.");
			this.RefreshView(false, false);
		}

		/// <summary>
		/// Gets the grid.
		/// </summary>
        // mbr - 2010-03-02 - changed...
		//private DataGrid Grid
		private InnerDataGrid Grid
		{
			get
			{
				if(_grid == null)
				{
					// basics...
                    _grid = new InnerDataGrid(this);
					_grid.ID = "grid";
					_grid.AutoGenerateColumns = false;

					// mbr - 29-01-2007 - paging...
					_grid.AllowPaging = this.AllowPaging;
					_grid.PageSize = this.PageSize;
					_grid.PagerStyle.Mode = PagerMode.NumericPages;

					// mbr - 29-01-2007 - style...
					_grid.HeaderStyle.CssClass = "gridheader";
					_grid.FooterStyle.CssClass = "gridfooter";
					_grid.ItemStyle.CssClass = "griditem";
					_grid.AlternatingItemStyle.CssClass = "gridalternateitem";
					_grid.PagerStyle.CssClass = "gridpager";
					_grid.EditItemStyle.CssClass = "gridedititem";
					_grid.SelectedItemStyle.CssClass = "gridhighlight";

					// mbr - 14-02-2007 - sorting...
					_grid.AllowSorting = this.AllowSorting;

					// events...
					_grid.ItemDataBound += new DataGridItemEventHandler(_grid_ItemDataBound);
					_grid.EditCommand += new DataGridCommandEventHandler(_grid_EditCommand);
					_grid.UpdateCommand += new DataGridCommandEventHandler(_grid_UpdateCommand);
					_grid.DeleteCommand += new DataGridCommandEventHandler(_grid_DeleteCommand);
					_grid.CancelCommand += new DataGridCommandEventHandler(_grid_CancelCommand);
					_grid.PageIndexChanged += new DataGridPageChangedEventHandler(_grid_PageIndexChanged);
					_grid.SortCommand += new DataGridSortCommandEventHandler(_grid_SortCommand);
					_grid.ItemCommand += new DataGridCommandEventHandler(_grid_ItemCommand);
                    _grid.ItemCreated += new DataGridItemEventHandler(_grid_ItemCreated);
				}
				return _grid;
			}
		}

        void _grid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            this.OnItemCreated(e);
        }

        protected virtual void OnItemCreated(DataGridItemEventArgs e)
        {
            if (this.ItemCreated != null)
                this.ItemCreated(this, e);
        }

		/// <summary>
		/// Gets or sets the allowpaging.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>AllowPaging</c> for storage.
		/// </remarks>
		[Browsable(true), Category("Behavior")]
		public bool AllowPaging
		{
			get
			{
				object value = ViewState["AllowPaging"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != AllowPaging)
				{
					// set the value...
					ViewState["AllowPaging"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the pagesize.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>PageSize</c> for storage.
		/// </remarks>
		[Browsable(true), Category("Behavior")]
		public int PageSize
		{
			get
			{
				object value = ViewState["PageSize"];
				if(value == null)
					return 20;
				else
					return (int)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != PageSize)
				{
					// set the value...
					ViewState["PageSize"] = value;
				}
			}
		}

		[Browsable(true)]
		public Unit GridWidth
		{
			get
			{
				return this.Grid.Width;
			}
			set
			{
				this.Grid.Width = value;
			}
		}

		/// <summary>
		/// Populates the given grid with columns.
		/// </summary>
		private void SetupGridColumns()
		{
			// nothing?
			if(this.Columns.Count == 0)
			{
				this.OnColumnsNeeded(EventArgs.Empty);

				// still nothing?
				if(this.Columns.Count == 0)
					this.InferColumnSpecification(false);
			}

			// setup...
			this.Grid.Columns.Clear();
			foreach(AutoColumnSpecification spec in this.Columns)
			{
				if(spec.IsOnGrid)
				{
					// create a column for that...
					DataGridColumn column = null;
					if(spec is CustomAutoColumnSpecification)
						column = new AutoDataGridCustomColumn(this, (CustomAutoColumnSpecification)spec);
					else
						column = new AutoDataGridValueColumn(this, spec);

					// sort...
					column.SortExpression = this.Grid.Columns.Count.ToString();

					// add...
					this.Grid.Columns.Add(column);
				}
			}

			// do the widths...
			int numColumns = this.Grid.Columns.Count;
			if(numColumns > 0)
			{
				int widthPer = 100 / numColumns;
				foreach(DataGridColumn column in this.Grid.Columns)
				{
					if(column.HeaderStyle.Width == Unit.Empty && column.ItemStyle.Width == Unit.Empty)
						column.HeaderStyle.Width = new Unit(widthPer, UnitType.Percentage);
				}
			}

            // mbr - 2010-05-17 - add optionally...
            if(this.ShowWidgetColumn)
			    this.Grid.Columns.Add(new AutoDataGridWidgetColumn(this));
		}

        /// <summary>
        /// Gets the ShowWidgetColumn value.
        /// </summary>
        public bool ShowWidgetColumn
        {
            get
            {
                return _showWidgetColumn;
            }
            set
            {
                _showWidgetColumn = value;
            }
        }

		/// <summary>
		/// Gets or sets the data source for the control.
		/// </summary>
		public object DataSource
		{
			get
			{
				if(_dataSource == null)
				{
					this.OnDataSourceNeeded();

					// mbr - 14-02-2007 - sort...
					if(_dataSource != null)
                    {
                        // mbr - 2010-04-05 - comparer...
                        if(this.SortColumnIndex != -1)
						    this.SortDataSource(false);
                    }
				}
				return _dataSource;
			}
			set
			{
				_entityType = null;
                _comparer = null;

                // check...
				_dataSource = value;
			}
		}

        // mbr - 2010-04-05 - added...
        private void ProcessComparer()
        {
            if (DataSource == null)
                throw new InvalidOperationException("'DataSource' is null.");
            if (Comparer == null)
                throw new InvalidOperationException("'Comparer' is null.");

            // if...
            if (this.DataSource is EntityCollection)
                ((EntityCollection)this.DataSource).Sort(this.Comparer, this.SortDirection);
            else
                throw new NotSupportedException(string.Format("Cannot handle '{0}'.", this.DataSource.GetType()));
        }

        /// <summary>
        /// Gets the Comparer value.
        /// </summary>
        public IComparer Comparer
        {
            get
            {
                return _comparer;
            }
            set
            {
                _comparer = value;
            }
        }

		/// <summary>
		/// Gets the entitytype.
		/// </summary>
		public override EntityType EntityType
		{
			get
			{
				if(_entityType == null && this.DataSource != null)
				{
					// check?
					if(!(this.DataSource is IEntityType))
						throw new NotSupportedException(string.Format("A data source of type '{0}' is not supported.", this.DataSource.GetType()));

					// set...
					_entityType = ((IEntityType)this.DataSource).EntityType;
					if(_entityType == null)
						throw new InvalidOperationException("_entityType is null.");
				}
				return _entityType;
			}
		}

		private void _grid_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			// ignore headers/footers...
			if(e.Item.DataItem == null)
				return;

			// mode...
			ExtendedFieldViewMode mode = ExtendedFieldViewMode.View;
			if(e.Item.ItemType == ListItemType.EditItem)
				mode = ExtendedFieldViewMode.Edit;

			// check...
			if(EntityType == null)
				throw new InvalidOperationException("EntityType is null.");

			// key fields...
			EntityField[] keys = this.EntityType.GetKeyFields();
			if(keys == null)
				throw new InvalidOperationException("'keys' is null.");
			if(keys.Length != 1)
				throw new InvalidOperationException(string.Format("'keys' is an invalid length ({0}).", keys.Length));

			// value...
			object entity = EntityView.Unwrap(e.Item.DataItem);

			// walk...
			foreach(TableCell cell in e.Item.Cells)
			{
				foreach(Control control in cell.Controls)
				{
					// spec...
					AutoColumnPair pair = this.GetColumnPair(control, false);
					if(pair != null)
					{
						// check...
						if(pair.Specification is CustomAutoColumnSpecification)
							((CustomAutoColumnSpecification)pair.Specification).OnItemDataBound(new CustomControlEntityEventArgs(control, mode, entity, null));
						else
						{
							// what happened?
							if(control is IConstraintControl)
							{
								IConstraintControl cc = (IConstraintControl)control;

								// member...
								EntityMember member = this.EntityType.GetMember(cc.FieldName, OnNotFound.ThrowException);
								if(member is EntityField)
								{
									EntityField field = (EntityField)member;

									// get the value...
									object value = this.EntityType.Storage.GetValue(entity, (EntityField)member);

									// are we an enum?
									if(field.HasEnumerationType)
										cc.Value = this.FormatEnumerationValue(field.EnumerationType, value);
									else
										cc.Value = value;
								}
								else if(member is ChildToParentEntityLink)
								{
									object subItem = this.EntityType.Persistence.GetParent(entity, member.Name, new string[] {});
									if(subItem != null)
									{
										switch(pair.Mode)
										{
											case ExtendedFieldViewMode.View:
												cc.Value = this.GetSubItemText(subItem, pair.Specification);
												break;

											case ExtendedFieldViewMode.Edit:
												cc.Value = this.GetSubItemValueAsString(subItem, pair.Specification);
												break;

											default:
												throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", pair.Mode, pair.Mode.GetType()));
										}								
									}
									else
										cc.Value = null;
								}
								else if(member is EntityProperty)
								{
									// get...
									cc.Value = this.EntityType.Type.InvokeMember(member.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty, 
										null, entity, null);
								}
								else
									throw new NotSupportedException(string.Format("A member of type '{0}' is not supported.", member.GetType()));
							}
						}

						// mbr - 24-01-2008 - this check might as well be outside...
//						if(control is IAutoDataGridBindableControl)
//							((IAutoDataGridBindableControl)control).DataBound(entity);
					}

					// mbr - 24-01-2008 - moved out here...
					if(control is IAutoDataGridBindableControl)
						((IAutoDataGridBindableControl)control).DataBound(entity);

					// button or link button?
					if(control is Button)
					{
						string name = ((Button)control).CommandName;
						string arg = ((Button)control).CommandArgument;
						if(name != null && name.Length > 0 && (arg == null || arg.Length == 0))
							((Button)control).CommandArgument = ConversionHelper.ToString(this.EntityType.Storage.GetValue(entity, keys[0]), Cultures.System);
					}
					else if(control is LinkButton)
					{
						string name = ((LinkButton)control).CommandName;
						string arg = ((LinkButton)control).CommandArgument;
						if(name != null && name.Length > 0 && (arg == null || arg.Length == 0))
							((LinkButton)control).CommandArgument = ConversionHelper.ToString(this.EntityType.Storage.GetValue(entity, keys[0]), Cultures.System);
					}
				}
			}
		}

		private void _grid_EditCommand(object source, DataGridCommandEventArgs e)
		{
			this.Grid.EditItemIndex = e.Item.ItemIndex;
			this.BindGrid();
		}

        /// <summary>
        /// Raises the <c>AfterBind</c> event.
        /// </summary>
        protected virtual void OnAfterBind(AutoDataGridBindEventArgs e)
        {
            // raise...
            if (AfterBind != null)
                AfterBind(this, e);
        }

        /// <summary>
        /// Raises the <c>BeforeBind</c> event.
        /// </summary>
        protected virtual void OnBeforeBind(AutoDataGridBindEventArgs e)
        {
            // raise...
            if (BeforeBind != null)
                BeforeBind(this, e);
        }

		/// <summary>
		/// Binds the grid, checking paging first.
		/// </summary>
		private void BindGrid()
		{
			if(Grid == null)
				throw new InvalidOperationException("Grid is null.");

            // mbr - 2010-05-17 - added...
            AutoDataGridBindEventArgs e = new AutoDataGridBindEventArgs();
            this.OnBeforeBind(e);
            try
            {
                // mbr - 14-02-2007 - reset the grid (issues with sorting otherwise?)			
                this.Grid.DataSource = null;
                this.Grid.DataSource = this.DataSource;

                // datasource...
                IList list = this.Grid.DataSource as IList;
                if (list != null)
                {
                    if (this.Grid.CurrentPageIndex > 0)
                    {
                        // max...
                        int topItem = this.Grid.CurrentPageIndex * this.Grid.PageSize;
                        if (topItem >= list.Count)
                            this.Grid.CurrentPageIndex = 0;
                    }
                }

                // sort?
                if (Page == null)
                    throw new InvalidOperationException("Page is null.");
                if (!(this.Page.IsPostBack) && this.SortFirstColumn && this.SortColumnIndex == -1 && this.Columns.Count > 0)
                {
                    this.SortColumnIndex = 0;
                    this.SortDirection = BootFX.Common.Data.SortDirection.Ascending;

                    // sort... 
                    SortDataSource(false);
                }

                // do it!
                this.Grid.DataBind();
            }
            finally
            {
                this.OnAfterBind(e);
            }
		}

		private void _grid_UpdateCommand(object source, DataGridCommandEventArgs e)
		{
			try
			{
				ApplyGrid();
			}
			catch(Exception ex)
			{
				this.HandleException("An error occurred when updating an item.", ex);
			}
		}

		/// <summary>
		/// Applies changes to an edit.
		/// </summary>
		/// <returns></returns>
		private bool ApplyGrid()
		{
			if(this.Grid.EditItemIndex == -1)
				throw new InvalidOperationException("The grid is not being edited.");

			// check...
			if(DataSource == null)
				throw new InvalidOperationException("DataSource is null.");
			if(EntityType == null)
				throw new InvalidOperationException("EntityType is null.");

			// what are we changing?
			DataGridItem editItem = null;
			foreach(DataGridItem item in this.Grid.Items)
			{
				if(item.ItemType == ListItemType.EditItem)
				{
					editItem = item;
					break;
				}
			}

			// check...
			if(editItem == null)
				throw new InvalidOperationException("editItem is null.");

			// what's the item...?
			if(!(this.DataSource is IList))
				throw new InvalidOperationException(string.Format("A data source of type '{0}' is not indexed.", this.DataSource.GetType()));
			
			// jm - 10-4-2007 - need the index within the datasource, not the index within the page
			//object entity = EntityView.Unwrap(((IList)this.DataSource)[editItem.ItemIndex]);
			object entity = EntityView.Unwrap(((IList)this.DataSource)[editItem.DataSetIndex]);
			if(entity == null)
				throw new InvalidOperationException("entity is null.");

			// return...
			bool result = Apply(entity, editItem, this.BeforeUpdate);
			if(result)
			{
				this.StopEditing(true);

				// mbr - 2008-09-05 - save...
				this.OnAfterUpdate(new EntityEventArgs(entity));
			}

			// return...
			return result;
		}

		private void _grid_DeleteCommand(object source, DataGridCommandEventArgs e)
		{
			// checker...
			FormChecker checker = new FormChecker(this);

			try
			{
				string asString = e.CommandArgument as string;
				if(asString == null)
					throw new InvalidOperationException("'asString' is null.");
				if(asString.Length == 0)
					throw new InvalidOperationException("'asString' is zero-length.");

				// get...
				if(EntityType == null)
					throw new InvalidOperationException("EntityType is null.");
				object entity = EntityType.Persistence.GetById(new object[] { asString }, OnNotFound.ThrowException);
				if(entity == null)
					throw new InvalidOperationException("entity is null.");

				// get...
				ArrayList ccs = new ArrayList();
				this.WalkAndFindConstraintControls(e.Item, ccs);

				// mark...
				if(entity is Entity)
				{
					// mbr - 31-07-2007 - case 279 - before...
					AutoControlApplyEventArgs args = new AutoControlApplyEventArgs(entity, ccs, checker);
					this.OnBeforeDelete(args);

					// anything?
					if(!(checker.HasErrors))
					{
						// delete it...
						((Entity)entity).MarkForDeletion();
						((Entity)entity).SaveChanges();

						// after...
						this.OnAfterDelete(new EntityEventArgs(entity));
					}
				}
				else
					throw new NotSupportedException(string.Format("Cannot handle '{0}'.", entity.GetType()));
			}
			catch(Exception ex)
			{
				this.HandleException("An error occurred when deleting an item.", ex);
				return;
			}

			// show...
			if(checker.HasErrors)
			{
				// mbr - 23-03-2008 - feedback...
//				this.ProblemLabel.Text = checker.GetAllErrorsSeparatedByBrTag();
				this.FeedbackWidget.SetError(checker);
			}

			// update...
			this.RefreshView(true, false);
		}

		private void _grid_CancelCommand(object source, DataGridCommandEventArgs e)
		{
			this.StopEditing(false);
		}

		private void StopEditing(bool reset)
		{
			this.Grid.EditItemIndex = -1;
			this.RefreshView(reset, false);
		}

		/// <summary>
		/// Raises the <c>BeforeUpdate</c> event.
		/// </summary>
		protected virtual void OnBeforeUpdate(AutoControlApplyEventArgs e)
		{
			// raise...
			if(BeforeUpdate != null)
				BeforeUpdate(this, e);
		}

		/// <summary>
		/// Raises the <c>BeforeDelete</c> event.
		/// </summary>
		protected virtual void OnBeforeDelete(AutoControlApplyEventArgs e)
		{
			// raise...
			if(BeforeDelete != null)
				BeforeDelete(this, e);
		}

		/// <summary>
		/// Raises the <c>AfterUpdate</c> event.
		/// </summary>
		protected virtual void OnAfterDelete(EntityEventArgs e)
		{
			// raise...
			if(AfterDelete != null)
				AfterDelete(this, e);
		}

		/// <summary>
		/// Infers the column specification from the data source.
		/// </summary>
		public void InferColumnSpecification(bool includeKeys)
		{
			this.Columns.Clear();
			if(this.DataSource == null)
				return;

			// check...
			if(this.EntityType == null)
				return;

			// walk...
			this.Columns.AddRange(this.InferColumnsFromEntityType(this.EntityType, includeKeys));
		}

		/// <summary>
		/// Gets a collection of AutoColumnSpecification objects.
		/// </summary>
		public AutoColumnSpecificationCollection Columns
		{
			get
			{
				return _columns;
			}
		}

		/// <summary>
		/// Gets the AddForm.
		/// </summary>
		public AutoForm AddForm
		{
			get
			{
				if(_addForm == null)
				{
					_addForm = new AutoForm(this);
					_addForm.ApplyButtonText = this.AddButtonText;
					_addForm.BeforeSaveChanges += new AutoControlApplyEventHandler(_addForm_BeforeSaveChanges);
					_addForm.AfterSaveChanges += new EntityEventHandler(_addForm_AfterSaveChanges);
				}
				return _addForm;
			}
		}

		/// <summary>
		/// Gets or sets the addbuttontext
		/// </summary>
		[Browsable(true)]
		public string AddButtonText
		{
			get
			{
				return _addButtonText;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _addButtonText)
				{
					// set the value...
					_addButtonText = value;

					// check...
					if(_addForm != null)
						_addForm.ApplyButtonText = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>DataSourceNeeded</c> event.
		/// </summary>
		private void OnDataSourceNeeded()
		{
			OnDataSourceNeeded(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DataSourceNeeded</c> event.
		/// </summary>
		protected virtual void OnDataSourceNeeded(EventArgs e)
		{
			// raise...
			if(DataSourceNeeded != null)
				DataSourceNeeded(this, e);
		}

		private void _addForm_AfterSaveChanges(object sender, EntityEventArgs e)
		{
			this.StopEditing(true);

			// mbr - 14-03-2007 - raise...
			this.OnAfterInsert(e);
		}

		/// <summary>
		/// Gets or sets the addformcaption
		/// </summary>
		[Browsable(true)]
		public string AddFormCaption
		{
			get
			{
				return _addFormCaption;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _addFormCaption)
				{
					// set the value...
					_addFormCaption = value;
				}
			}
		}

		private void _addForm_BeforeSaveChanges(object sender, AutoControlApplyEventArgs e)
		{
			this.StopEditing(true);
		}

		/// <summary>
		/// Gets the columns for the add form.
		/// </summary>
		/// <returns></returns>
		internal AutoColumnSpecificationCollection GetColumnsForAddForm()
		{
			AutoColumnSpecificationCollection results = new AutoColumnSpecificationCollection();
			foreach(AutoColumnSpecification specification in this.Columns)
			{
				if(specification.IsOnAddForm)
					results.Add(specification);
			}

			// return...
			return results;
		}

		/// <summary>
		/// Gets the noitemslabel.
		/// </summary>
		private Label NoItemsLabel
		{
			get
			{
				if(_noItemsLabel == null)
				{
					_noItemsLabel = new Label();
					_noItemsLabel.Text = "No items are available.";
				}
				return _noItemsLabel;
			}
		}

		/// <summary>
		/// Gets or sets the text displayed when no items are available in the grid.
		/// </summary>
		[Browsable(true), Category("Appearance"), Description("Gets or sets the text displayed when no items are available in the grid.")]
		public string NoItemsText 
		{
			get
			{
				return this.NoItemsLabel.Text;
			}
			set
			{
				this.NoItemsLabel.Text = value;
			}
		}

		private void _grid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
			this.Grid.CurrentPageIndex = e.NewPageIndex;
			this.BindGrid();
		}

		/// <summary>
		/// Gets or sets the ignoredatabind
		/// </summary>
		[Browsable(true), Category("Behavior")]
		public bool IgnoreDataBind
		{
			get
			{
				return _ignoreDataBind;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _ignoreDataBind)
				{
					// set the value...
					_ignoreDataBind = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the supportedchanges.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>SupportedChanges</c> for storage.
		/// </remarks>
		[Browsable(false), Category("Behavior")]
		public ChangeAction SupportedChanges
		{
			get
			{
				object value = ViewState["SupportedChanges"];
				if(value == null)
					return ChangeAction.All;
				else
					return (ChangeAction)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != SupportedChanges)
				{
					// set the value...
					ViewState["SupportedChanges"] = value;
				}
			}
		}

		/// <summary>
		/// Returns true if the given change is supported.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		private bool IsChangeSupported(ChangeAction action)
		{
			if((int)(this.SupportedChanges & action) != 0)
				return true;
			else
				return false;
		}

		private void SetChangeFlags(ChangeAction action, bool on)
		{
			this.SupportedChanges |= action;
			if(!(on))
				this.SupportedChanges ^= action;
		}

		[Browsable(false), Category("Behavior")]
		public bool AllowAdd
		{
			get
			{
				return this.IsChangeSupported(ChangeAction.Insert);
			}
			set
			{
				this.SetChangeFlags(ChangeAction.Insert, value);
				this.RefreshView(false, true);
			}
		}

		[Browsable(false), Category("Behavior")]
		public bool AllowDelete
		{
			get
			{
				return this.IsChangeSupported(ChangeAction.Delete);
			}
			set
			{
				this.SetChangeFlags(ChangeAction.Delete, value);
				this.RefreshView(false, true);
			}
		}
	
		[Browsable(false), Category("Behavior")]
		public bool AllowEdit
		{
			get
			{
				return this.IsChangeSupported(ChangeAction.Update);
			}
			set
			{
				this.SetChangeFlags(ChangeAction.Update, value);
				this.RefreshView(false, true);
			}
		}

		/// <summary>
		/// Gets or sets the allowsort.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>AllowSort</c> for storage.
		/// </remarks>
		[Browsable(false), Category("Behavior")]
		public bool AllowSorting
		{
			get
			{
				object value = ViewState["AllowSorting"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != AllowSorting)
				{
					// set the value...
					ViewState["AllowSorting"] = value;
				}
			}
		}

		private void _grid_SortCommand(object source, DataGridSortCommandEventArgs e)
		{
            // index...
            int index = ConversionHelper.ToInt32(e.SortExpression, Cultures.System);

            // update and run...
            if (this.SortColumnIndex == index)
            {
                // toggle the sort...
                if (this.SortDirection == BootFX.Common.Data.SortDirection.Ascending)
                    this.SortDirection = BootFX.Common.Data.SortDirection.Descending;
                else
                    this.SortDirection = BootFX.Common.Data.SortDirection.Ascending;
            }
            else
            {
                // change the sort and force to be ascending...
                this.SortColumnIndex = index;
                this.SortDirection = BootFX.Common.Data.SortDirection.Ascending;
            }

            // reset the page...
            if (this.AllowPaging)
                this.Grid.CurrentPageIndex = 0;

            // run...
            this.SortDataSource(true);
		}

		/// <summary>
		/// Gets or sets the sortfirstcolumn.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>SortFirstColumn</c> for storage.
		/// </remarks>
		[Browsable(false), Category("Behavior")]
		public bool SortFirstColumn
		{
			get
			{
				object value = ViewState["SortFirstColumn"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != SortFirstColumn)
				{
					// set the value...
					ViewState["SortFirstColumn"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the sortcolumnindex.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>SortColumnIndex</c> for storage.
		/// </remarks>
		public int SortColumnIndex
		{
			get
			{
                //object value = ViewState["SortColumnIndex"];
                //if(value == null)
                //    return -1;
                //else
                //    return (int)value;
                this.LoadSortSpecification();
                return _sortColumnIndex;
			}
			set
			{
				// check to see if the value has changed...
				if(value != SortColumnIndex)
				{
					// set the value...
					//ViewState["SortColumnIndex"] = value;
                    _sortColumnIndex = value;
                    this.SaveSortSpecification();
				}
			}
		}

        private void SaveSortSpecification()
        {
            string value = string.Format("{0}|{1}", this.SortColumnIndex, this.SortDirection);
            string key = this.GetSessionKey();
            if (key == null)
                throw new InvalidOperationException("'key' is null.");
            if (key.Length == 0)
                throw new InvalidOperationException("'key' is zero-length.");

            // set...
            this.Page.Session[key] = value;
        }

        private string GetSessionKey()
        {
            StringBuilder builder = new StringBuilder();
            foreach (AutoColumnSpecification column in this.Columns)
            {
                if (builder.Length > 0)
                    builder.Append("|");
                builder.Append(column.Name);
            }

            // get...
            string hash = HashHelper.GetMd5HashOfStringAsBase64(builder.ToString());

            // return...
            string key = string.Format("GridSort_{0}_{1}", this.EntityType.FullName, hash);
            return key;
        }

        private void LoadSortSpecification()
        {
            if (_sortSpecificationLoaded)
                return;

            // get...
            try
            {
                string key = this.GetSessionKey();
                if (key == null)
                    throw new InvalidOperationException("'key' is null.");
                if (key.Length == 0)
                    throw new InvalidOperationException("'key' is zero-length.");

                // get...
                string asString = (string)this.Page.Session[key];
                if (!(string.IsNullOrEmpty(asString)))
                {
                    int index = asString.IndexOf("|");
                    if (index == -1)
                        throw new InvalidOperationException(string.Format("A value of '{0}' is invalid.", asString));

                    // get...
                    _sortColumnIndex = ConversionHelper.ToInt32(asString.Substring(0, index), Cultures.System);
                    _sortDirection = (BootFX.Common.Data.SortDirection)Enum.Parse(typeof(BootFX.Common.Data.SortDirection), asString.Substring(index + 1));
                }
                else
                {
                    _sortColumnIndex = -1;
                    _sortDirection = BootFX.Common.Data.SortDirection.Ascending;
                }
            }
            finally
            {
                _sortSpecificationLoaded = true;
            }
        }

        /// <summary>
        /// Raises the <c>ComparerNeeded</c> event.
        /// </summary>
        private void OnComparerNeeded()
        {
            OnComparerNeeded(EventArgs.Empty);
        }

        /// <summary>
        /// Raises the <c>ComparerNeeded</c> event.
        /// </summary>
        protected virtual void OnComparerNeeded(EventArgs e)
        {
            // raise...
            if (ComparerNeeded != null)
                ComparerNeeded(this, e);
        }

		/// <summary>
		/// Sorts the column.
		/// </summary>
		/// <param name="column"></param>
		private void SortDataSource(bool refreshView)
		{
			if(DataSource == null)
				throw new InvalidOperationException("DataSource is null.");
			if(this.SortColumnIndex < 0)
				throw new InvalidOperationException(string.Format("A sort column index of '{0}' is invalid.", this.SortColumnIndex));

            // reset the comparer...
            this.OnComparerNeeded();
            if (this.Comparer != null)
                this.ProcessComparer();
            else
            {
                // what column is that?
                DataGridColumn column = this.Grid.Columns[this.SortColumnIndex];
                if (column == null)
                    throw new InvalidOperationException("column is null.");

                // what to do...
                if (column is AutoDataGridValueColumn)
                {
                    AutoDataGridValueColumn valueSpecification = (AutoDataGridValueColumn)column;

                    // mbr - 10-08-2007 - case 46 - don't use fields, use members...
                    EntityMember member = valueSpecification.Member;
                    if (member == null)
                        throw new InvalidOperationException("member is null.");

                    // culture...
                    CultureInfo culture = Cultures.User;

                    // mbr - 10-08-2007 - case 46 - get a comparer.
                    IComparer comparer = null;
                    if (valueSpecification.Member is EntityField)
                        comparer = ((EntityField)valueSpecification.Member).GetComparer(culture);
                    else if (valueSpecification.Member is EntityProperty)
                        comparer = ComparerBase.GetComparer(((EntityProperty)valueSpecification.Member).Type, culture);
                    else
                        comparer = new BootFX.Common.Data.Comparers.StringComparer(culture);

                    // sort...
                    if (this.DataSource is EntityCollection)
                    {
                        // sort...
                        if (comparer == null)
                            throw new InvalidOperationException("comparer is null.");
                        ((EntityCollection)this.DataSource).Sort(comparer, this.SortDirection);
                    }
                    else
                    {
                        // mbr - 14-02-2007 - haven't got a good story for this yet.					
                        //	throw new NotSupportedException(string.Format("Cannot handle '{0}'.", this.DataSource.GetType()));
                    }
                }
                else
                {
                    // mbr - 14-02-2007 - haven't got a good story for this yet.					
                    //throw new NotSupportedException(string.Format("Cannot handle '{0}'.", column.GetType()));
                }
            }

			// update...
			if(refreshView)
				this.BindGrid();
		}

		/// <summary>
		/// Gets or sets the BootFX.Common.Data.SortDirection.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>SortDirection</c> for storage.
		/// </remarks>
		public BootFX.Common.Data.SortDirection SortDirection
		{
			get
			{
                //object value = ViewState["SortDirection"];
                //if(value == null)
                //    return BootFX.Common.Data.SortDirection.Ascending;
                //else
                //    return (BootFX.Common.Data.SortDirection)value;
                this.LoadSortSpecification();
                return _sortDirection;
            }
			set
			{
				// check to see if the value has changed...
				if(value != SortDirection)
				{
					// set the value...
					//ViewState["SortDirection"] = value;
                    _sortDirection = value;
                    SaveSortSpecification();
				}
			}
		}

		public void FixupCheckboxes()
		{
			GridCheckBoxHelper helper = new GridCheckBoxHelper();
			helper.FixupGrid(this);
		}

		public void FixupCheckboxes(string checkBoxBaseID)
		{
			GridCheckBoxHelper helper = new GridCheckBoxHelper();
			helper.FixupGrid(this, checkBoxBaseID);
		}

		/// <summary>
		/// Raises the <c>ButtonsNeeded</c> event.
		/// </summary>
		protected internal virtual void OnButtonsNeeded(AutoDataGridButtonRequestEventArgs e)
		{
			// raise...
			if(ButtonsNeeded != null)
				ButtonsNeeded(this, e);
		}

		private void _grid_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			this.OnItemCommand(e);
		}

		/// <summary>
		/// Raises the <c>ItemCommand</c> event.
		/// </summary>
		protected virtual void OnItemCommand(DataGridCommandEventArgs e)
		{
			// raise...
			if(ItemCommand != null)
				ItemCommand(this, e);
		}

		public Unit WidgetColumnWidth
		{
			get
			{
				return _widgetColumnWidth;
			}
			set
			{
				_widgetColumnWidth = value;
			}
		}

        // mbr - 2010-03-02 - added...
        internal string HandleGridRender(string html)
        {
            RenderEventArgs e = new RenderEventArgs(html);
            this.OnGridRender(e);

            // return...
            return e.Html;
        }

        /// <summary>
        /// Raises the <c>GridRender</c> event.
        /// </summary>
        // mbr - 2010-03-02 - added...
        protected virtual void OnGridRender(RenderEventArgs e)
        {
            // raise...
            if (GridRender != null)
                GridRender(this, e);
        }

        /// <summary>
        /// Gets the ShowWhenNoItems value.
        /// </summary>
        // mbr - 2010-03-02 - added...
        public bool ShowWhenNoItems
        {
            get
            {
                return _showWhenNoItems;
            }
            set
            {
                _showWhenNoItems = value;
            }
        }

        public int PageButtonCount
        {
            get
            {
                return this.Grid.PagerStyle.PageButtonCount;
            }
            set
            {
                this.Grid.PagerStyle.PageButtonCount = value;
            }
        }

        public int CurrentPageIndex
        {
            get
            {
                return this.Grid.CurrentPageIndex;
            }
            set
            {
                this.Grid.CurrentPageIndex = value;
            }
        }

        public int PageCount
        {
            get
            {
                return this.Grid.PageCount;
            }
        }
    }
}
