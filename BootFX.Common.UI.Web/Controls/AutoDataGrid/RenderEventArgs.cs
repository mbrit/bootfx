// BootFX - Application framework for .NET applications
// 
// File: RenderEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Text;

namespace BootFX.Common.UI.Web
{
    public delegate void RenderEventHandler(object sender, RenderEventArgs e);

    public class RenderEventArgs : EventArgs
    {
        /// <summary>
        /// Private value to support the <see cref="Html">Html</see> property.
        /// </summary>
        private string _html;

        public RenderEventArgs(string html)
        {
            if (html == null)
                throw new ArgumentNullException("html");

            // set...
            this.Html = html;
        }

        /// <summary>
        /// Gets the Html value.
        /// </summary>
        public string Html
        {
            get
            {
                return _html;
            }
            set
            {
                _html = value;
            }
        }
    }
}
