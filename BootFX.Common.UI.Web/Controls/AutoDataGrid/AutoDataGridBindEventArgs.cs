// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridBindEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Text;

namespace BootFX.Common.UI.Web
{
    public delegate void AutoDataGridBindEventHandler(object sender, AutoDataGridBindEventArgs e);

    public class AutoDataGridBindEventArgs : EventArgs
    {
        /// <summary>
        /// Private value to support the <see cref="BindState">BindState</see> property.
        /// </summary>
        private object _bindState;

        public AutoDataGridBindEventArgs()
        {
        }

        /// <summary>
        /// Gets the BindState value.
        /// </summary>
        public object BindState
        {
            get
            {
                return _bindState;
            }
            set
            {
                _bindState = value;
            }
        }
    }
}
