// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridTemplateControlFactory.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	internal abstract class AutoDataGridTemplateControlFactory : ITemplate
	{
		/// <summary>
		/// Private field to support <see cref="column"/> property.
		/// </summary>
		private AutoColumn _column;

		private ExtendedFieldViewMode _mode = ExtendedFieldViewMode.View;
	
		protected AutoDataGridTemplateControlFactory(AutoColumn column, ExtendedFieldViewMode mode)
		{				
			if(column == null)
				throw new ArgumentNullException("column");
			_column = column;
			_mode = mode;
		}

		/// <summary>
		/// Gets the mode.
		/// </summary>
		internal ExtendedFieldViewMode Mode
		{
			get
			{
				return _mode;
			}
		}

		/// <summary>
		/// Gets the column.
		/// </summary>
		internal AutoColumn Column
		{
			get
			{
				return _column;
			}
		}

		public abstract void InstantiateIn(Control container);

		protected AutoDataGrid Grid
		{
			get
			{
				if(Column == null)
					throw new InvalidOperationException("Column is null.");
				return this.Column.Grid;
			}
		}
	}
}
