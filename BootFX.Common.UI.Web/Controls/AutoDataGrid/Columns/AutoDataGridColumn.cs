// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridColumn.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoColumn</c>.
	/// </summary>
	internal abstract class AutoColumn : TemplateColumn
	{
		private AutoDataGrid _grid = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="field"></param>
		internal AutoColumn(AutoDataGrid grid)
		{
			if(grid == null)
				throw new ArgumentNullException("grid");			
			_grid = grid;
		}

		/// <summary>
		/// Gets the grid.
		/// </summary>
		internal AutoDataGrid Grid
		{
			get
			{
				return _grid;
			}
		}
	}
}
