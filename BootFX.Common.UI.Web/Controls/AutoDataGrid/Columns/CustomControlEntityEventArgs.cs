// BootFX - Application framework for .NET applications
// 
// File: CustomControlEntityEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	public delegate void CustomControlEntityEventHandler (object sender, CustomControlEntityEventArgs e);

	/// <summary>
	/// Defines an instance of <c>CustomControlEntityEventArgs</c>.
	/// </summary>
	public class CustomControlEntityEventArgs : CustomControlEventArgs, IEntityType
	{
		/// <summary>
		/// Private field to support <see cref="Checker"/> property.
		/// </summary>
		private FormChecker _checker;
		
		/// <summary>
		/// Private field to support <see cref="Entity"/> property.
		/// </summary>
		private object _entity;
		private EntityType _entityType;
		
		internal CustomControlEntityEventArgs(Control control, ExtendedFieldViewMode mode, object entity, FormChecker checker) : base(control, mode)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");

			// set...
			_entity = entity;
			_checker = checker;
		}

		/// <summary>
		/// Gets the checker.
		/// </summary>
		public FormChecker Checker
		{
			get
			{
				return _checker;
			}
		}

		/// <summary>
		/// Gets the entity.
		/// </summary>
		public object Entity
		{
			get
			{
				return _entity;
			}
		}

		public EntityType EntityType
		{
			get
			{
				if(_entityType == null)
					_entityType = EntityType.GetEntityType(this.Entity, OnNotFound.ThrowException);
				return _entityType;
			}
		}
	}
}
