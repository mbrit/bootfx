// BootFX - Application framework for .NET applications
// 
// File: CustomControlEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	public delegate void CustomControlEventHandler(object sender, CustomControlEventArgs e);

	/// <summary>
	/// Defines an instance of <c>ControlEventArgs</c>.
	/// </summary>
	public class CustomControlEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <see cref="mode"/> property.
		/// </summary>
		private ExtendedFieldViewMode _mode;
		
		/// <summary>
		/// Private field to support <c>Control</c> property.
		/// </summary>
		private Control _control;

		/// <summary>
		/// Constructor.
		/// </summary>
		internal CustomControlEventArgs(Control control, ExtendedFieldViewMode mode)
		{
			_control = control;
			_mode = mode;
		}

		/// <summary>
		/// Gets the mode.
		/// </summary>
		public ExtendedFieldViewMode Mode
		{
			get
			{
				return _mode;
			}
		}

		/// <summary>
		/// Gets or sets the control
		/// </summary>
		public Control Control
		{
			get
			{
				return _control;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _control)
				{
					// set the value...
					_control = value;
				}
			}
		}
	}
}
