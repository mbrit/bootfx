// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridValueColumn.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoColumn</c>.
	/// </summary>
	internal class AutoDataGridValueColumn : AutoColumn
	{
		/// <summary>
		/// Private field to support <see cref="Specification"/> property.
		/// </summary>
		private AutoColumnSpecification _specification;
		
		/// <summary>
		/// Private field to support <c>FieldControl</c> property.
		/// </summary>
		private IConstraintControl _fieldControl;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="field"></param>
		internal AutoDataGridValueColumn(AutoDataGrid grid, AutoColumnSpecification specification) : base(grid)
		{
			if(specification == null)
				throw new ArgumentNullException("specification");			
			_specification = specification;

			// setup...
			this.HeaderText = specification.HeaderText;
			this.ItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.View);
			this.EditItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.Edit);
			
			// jm - 16-03-2007 - Added to support custom widths
			this.HeaderStyle.Width = specification.Width;
			this.ItemStyle.Width = specification.Width;
		}

		/// <summary>
		/// Gets the field.
		/// </summary>
		// mbr - 10-08-2007 - case 46 - added.
		internal EntityMember Member
		{
			get
			{
				if(Specification == null)
					throw new InvalidOperationException("Specification is null.");
				if(Grid == null)
					throw new InvalidOperationException("Grid is null.");
				if(Grid.EntityType == null)
					throw new InvalidOperationException("Grid.EntityType is null.");

				// get...
				return this.Grid.EntityType.GetMember(this.Specification.Name, OnNotFound.ThrowException);
			}
		}

		/// <summary>
		/// Gets the field.
		/// </summary>
		internal EntityField Field
		{
			get
			{
				if(Specification == null)
					throw new InvalidOperationException("Specification is null.");
				if(Grid == null)
					throw new InvalidOperationException("Grid is null.");
				if(Grid.EntityType == null)
					throw new InvalidOperationException("Grid.EntityType is null.");

				// get...
				return this.Grid.EntityType.Fields.GetField(this.Specification.Name, OnNotFound.ThrowException);
			}
		}

		/// <summary>
		/// Gets the specification.
		/// </summary>
		internal AutoColumnSpecification Specification
		{
			get
			{
				return _specification;
			}
		}

		/// <summary>
		/// Gets or sets the fieldcontrol
		/// </summary>
		internal IConstraintControl FieldControl
		{
			get
			{
				return _fieldControl;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _fieldControl)
				{
					// set the value...
					_fieldControl = value;
				}
			}
		}

		private class ItemTemplateControlFactory : AutoDataGridTemplateControlFactory
		{
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="column"></param>
			/// <param name="mode"></param>
			internal ItemTemplateControlFactory(AutoColumn column, ExtendedFieldViewMode mode) : base(column, mode)
			{
			}

			public override void InstantiateIn(Control container)
			{
				if(container == null)
					throw new ArgumentNullException("container");

				// get...
				AutoDataGridValueColumn column = (AutoDataGridValueColumn)this.Column;
				
				// get...
				IConstraintControl cc = this.Grid.CreateControlForField(column.Specification, this.Mode);
				if(cc == null)
					throw new InvalidOperationException("cc is null.");

				// update...
				if(cc is TextBox)
					((TextBox)cc).Width = new Unit(100,  UnitType.Percentage);

				// add...
				column.FieldControl = cc;
				container.Controls.Add((Control)column.FieldControl);
			}
		}
	}
}
