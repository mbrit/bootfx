// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridCustomColumn.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoDataGridCustomColumn</c>.
	/// </summary>
	internal class AutoDataGridCustomColumn : AutoColumn
	{
		/// <summary>
		/// Private field to support <see cref="specification"/> property.
		/// </summary>
		private CustomAutoColumnSpecification _specification;
		
		internal AutoDataGridCustomColumn(AutoDataGrid grid, CustomAutoColumnSpecification specification) : base(grid)
		{
			if(specification == null)
				throw new ArgumentNullException("specification");			
			_specification = specification;

			// setup...
			this.HeaderText = specification.HeaderText;
			this.ItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.View);
			this.EditItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.Edit);
			
			// jm - 16-03-2007 - Added to support custom widths
			this.HeaderStyle.Width = specification.Width;
			this.ItemStyle.Width = specification.Width;
		}

		private class ItemTemplateControlFactory : AutoDataGridTemplateControlFactory
		{
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="column"></param>
			/// <param name="mode"></param>
			internal ItemTemplateControlFactory(AutoColumn column, ExtendedFieldViewMode mode) : base(column, mode)
			{
			}

			public override void InstantiateIn(Control container)
			{
				if(container == null)
					throw new ArgumentNullException("container");

				// defer to the spec...
				CustomAutoColumnSpecification specification = ((AutoDataGridCustomColumn)this.Column).Specification;
				if(specification == null)
					throw new InvalidOperationException("specification is null.");

				// defer...
				CustomControlEventArgs e = new CustomControlEventArgs(null, this.Mode);
				specification.OnControlRequired(e);

				// check...
				if(e.Control == null)
					throw new InvalidOperationException("A control was not specified.  Respond to the ControlRequired event to create a control for the view.");

				// add...
				container.Controls.Add(e.Control);
				this.Grid.AddToControlMap(e.Control, specification, this.Mode);
			}
		}

		/// <summary>
		/// Gets the specification.
		/// </summary>
		internal CustomAutoColumnSpecification Specification
		{
			get
			{
				return _specification;
			}
		}
	}
}
