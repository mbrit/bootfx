// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridButtonRequestEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootFX.Common.UI.Web
{
	public delegate void AutoDataGridButtonRequestEventHandler(object sender, AutoDataGridButtonRequestEventArgs e);

	/// <summary>
	/// Summary description for AutoDataGridButtonRequestEventArgs.
	/// </summary>
	public class AutoDataGridButtonRequestEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <see cref="Mode"/> property.
		/// </summary>
		private ExtendedFieldViewMode _mode;
		
		/// <summary>
		/// Private field to support <see cref="container"/> property.
		/// </summary>
		private Control _container;
		
		/// <summary>
		/// Private field to support <see cref="Grid"/> property.
		/// </summary>
		private AutoDataGrid _grid;
		
		/// <summary>
		/// Private field to support <see cref="factory"/> property.
		/// </summary>
		private AutoDataGridWidgetColumn.ItemTemplateControlFactory _factory;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="grid"></param>
		internal AutoDataGridButtonRequestEventArgs(AutoDataGrid grid, 
			AutoDataGridWidgetColumn.ItemTemplateControlFactory factory, Control container, ExtendedFieldViewMode mode)
		{
			if(grid == null)
				throw new ArgumentNullException("grid");
			if(factory == null)
				throw new ArgumentNullException("factory");
			if(container == null)
				throw new ArgumentNullException("container");
			
			// set...
			_grid = grid;
			_factory = factory;
			_container = container;
			_mode = mode;
		}

		/// <summary>
		/// Gets the mode.
		/// </summary>
		public ExtendedFieldViewMode Mode
		{
			get
			{
				return _mode;
			}
		}

		/// <summary>
		/// Gets the factory.
		/// </summary>
		internal AutoDataGridWidgetColumn.ItemTemplateControlFactory Factory
		{
			get
			{
				return _factory;
			}
		}

		/// <summary>
		/// Gets the grid.
		/// </summary>
		public AutoDataGrid Grid
		{
			get
			{
				return _grid;
			}
		}

		/// <summary>
		/// Gets the container.
		/// </summary>
		internal Control Container
		{
			get
			{
				return _container;
			}
		}

		/// <summary>
		/// Adds a link button.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="urlTemplate"></param>
		/// <param name="confirmClick"></param>
		/// <returns></returns>
		public ConfirmLinkButton AddLinkButton(string text, string commandName)
		{
			if(text == null)
				throw new ArgumentNullException("text");
			if(text.Length == 0)
				throw new ArgumentOutOfRangeException("'text' is zero-length.");
			if(commandName == null)
				throw new ArgumentNullException("commandName");
			if(commandName.Length == 0)
				throw new ArgumentOutOfRangeException("'commandName' is zero-length.");
			
			// create...
			AutoDataGridButton button = new AutoDataGridButton();
			button.Text = text;
			button.CommandName = commandName;

			// add...
			AddControl(button);

			// return...
			return button;
		}

		/// <summary>
		/// Adds a button.
		/// </summary>
		/// <param name="button"></param>
		public void AddControl(Control button)
		{
			if(button == null)
				throw new ArgumentNullException("button");
			
			// set...
			if(Factory == null)
				throw new InvalidOperationException("Factory is null.");
			if(Container == null)
				throw new InvalidOperationException("Container is null.");
			this.Factory.AddButton(this.Container, button);
		}
	}
}
