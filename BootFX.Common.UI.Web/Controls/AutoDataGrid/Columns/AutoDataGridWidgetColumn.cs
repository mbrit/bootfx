// BootFX - Application framework for .NET applications
// 
// File: AutoDataGridWidgetColumn.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoDataGridWidgets</c>.
	/// </summary> 
	internal class AutoDataGridWidgetColumn : AutoColumn
	{
		internal AutoDataGridWidgetColumn(AutoDataGrid grid) : base(grid)
		{
			// template...
			this.ItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.View);
			this.EditItemTemplate = new ItemTemplateControlFactory(this, ExtendedFieldViewMode.Edit);
		}

		internal class ItemTemplateControlFactory : AutoDataGridTemplateControlFactory
		{
			internal ItemTemplateControlFactory(AutoColumn column, ExtendedFieldViewMode mode) : base(column, mode)
			{
			}

			public override void InstantiateIn(Control container)
			{
				if(container == null)
					throw new ArgumentNullException("container");

				// adjust...
				this.AdjustWidth(container);
				
				// mbr - 25-10-2007 - add my own buttons!				
				AutoDataGridButtonRequestEventArgs e = new AutoDataGridButtonRequestEventArgs(this.Grid, this, container, this.Mode);
				this.Grid.OnButtonsNeeded(e);

				// get...
				switch(Mode)
				{
					case ExtendedFieldViewMode.View:

						// mbr - 12-02-2007 - action?						
						if(this.Grid.AllowEdit)
						{
							// edit...
							AutoDataGridButton edit = new AutoDataGridButton();
							edit.Text = "Edit";
							edit.CommandName = "edit";
							this.AddButton(container, edit);
						}

						// mbr - 12-02-2007 - allow?
						if(this.Grid.AllowDelete)
						{
							// delete...
							AutoDataGridButton delete = new AutoDataGridButton();
							delete.Text = "Delete";
							delete.CommandName = "delete";
							delete.DoConfirmation = true;
							this.AddButton(container, delete);
						}
						
						break;

					case ExtendedFieldViewMode.Edit:

						// update...
						AutoDataGridButton update = new AutoDataGridButton();
						update.Text = "Update";
						update.CommandName = "update";
						this.AddButton(container, update);

						// cancel...
						AutoDataGridButton cancel = new AutoDataGridButton();
						cancel.Text = "Cancel";
						cancel.CommandName = "cancel";
						this.AddButton(container, cancel);

						break;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Mode, Mode.GetType()));
				}
			}

			/// <summary>
			/// Adds a button.
			/// </summary>
			/// <param name="container"></param>
			/// <param name="control"></param>
			internal void AddButton(Control container, Control control)
			{
				if(container == null)
					throw new ArgumentNullException("container");
				if(control == null)
					throw new ArgumentNullException("control");
			
				// add...
				if(container.Controls.Count > 0)
				{
					Label label = new Label();
					label.Text = "&nbsp;|&nbsp;";
					container.Controls.Add(label);
				}

				// add...
				container.Controls.Add(control);

				// adjust...
				this.AdjustWidth(container);
			}

			private void AdjustWidth(Control container)
			{
				if(container == null)
					throw new ArgumentNullException("container");

				// get...
				int numButtons = 0;
				foreach(Control check in container.Controls)
				{
					if(check is AutoDataGridButton)
						numButtons++;
				}

				// set...
				if(this.Grid.WidgetColumnWidth == Unit.Empty)
					this.Column.HeaderStyle.Width = new Unit(numButtons * 50, UnitType.Pixel);
				else
					this.Column.HeaderStyle.Width = this.Grid.WidgetColumnWidth;
			}
		}
	}
}
