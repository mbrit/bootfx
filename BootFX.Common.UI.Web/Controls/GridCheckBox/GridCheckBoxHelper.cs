// BootFX - Application framework for .NET applications
// 
// File: GridCheckBoxHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Helper class for managing grid checkboxes.
	/// </summary>
	public class GridCheckBoxHelper
	{
		public GridCheckBoxHelper()
		{
		}

		/// <summary>
		/// Fixes up the current grid.
		/// </summary>
		/// <param name="grid"></param>
		public void FixupGrid(DataGrid grid)
		{
			this.FixupGrid(grid, GridCheckBox.DefaultBaseID);
		}

		/// <summary>
		/// Fixes up the current grid.
		/// </summary>
		/// <param name="grid"></param>
		public void FixupGrid(DataGrid grid, string checkBoxBaseID)
		{
			if(grid == null)
				throw new ArgumentNullException("grid");

			// fix...
			this.FixupGridInternal(grid, checkBoxBaseID);
		}

		/// <summary>
		/// Fixes up the current grid.
		/// </summary>
		/// <param name="grid"></param>
		public void FixupGrid(AutoDataGrid grid)
		{
			this.FixupGrid(grid, GridCheckBox.DefaultBaseID);
		}

		/// <summary>
		/// Fixes up the current grid.
		/// </summary>
		/// <param name="grid"></param>
		public void FixupGrid(AutoDataGrid grid, string checkBoxBaseID)
		{
			if(grid == null)
				throw new ArgumentNullException("grid");

			// fix...
			this.FixupGridInternal(grid, checkBoxBaseID);
		}

		/// <summary>
		/// Fixes up the given control.
		/// </summary>
		/// <param name="control"></param>
		private void FixupGridInternal(Control control, string checkBoxBaseID)
		{	
			if(control == null)
				throw new ArgumentNullException("control");
			if(checkBoxBaseID == null)
				throw new ArgumentNullException("checkBoxBaseID");
			if(checkBoxBaseID.Length == 0)
				throw new ArgumentOutOfRangeException("'checkBoxBaseID' is zero-length.");

			// check...
			if(control.Page == null)
				throw new InvalidOperationException("control.Page is null.");

			// ok, so now we need to find all of the controls of the appropriate type...
			GridCheckBox[] boxes = (GridCheckBox[])FormHelper.GetControlsRecursive(control, typeof(GridCheckBox));
			if(boxes == null)
				throw new InvalidOperationException("boxes is null.");

			// get...
			string gridClientId = control.UniqueID;
			gridClientId = gridClientId.Replace("$", "\\$");

			// create a regex...
			Regex regex = new Regex(string.Format(@"{0}\$.*\${1}(?<id>.*)", gridClientId, checkBoxBaseID), 
				RegexOptions.Singleline | RegexOptions.IgnoreCase);

			// walk the form variables and find the ones that are on...
			ArrayList selectedIds = new ArrayList();
			foreach(string name in control.Page.Request.Form.AllKeys)
			{
				Match match = regex.Match(name);
				if(match.Success)
					selectedIds.Add(match.Groups["id"].Value);
			}

			// get the boxes...
			foreach(GridCheckBox box in boxes)
			{
				Match match = regex.Match(box.UniqueID);
				if(match.Success)
				{
					string boxId = match.Groups["id"].Value;

					// on?
					bool on = false;
					foreach(string selectedId in selectedIds)
					{
						if(string.Compare(selectedId, boxId, true, Cultures.System) == 0)
						{
							on = true;
							break;
						}
					}

					// set...
					box.Checked = on;
				}
			}
		}
	}
}
