// BootFX - Application framework for .NET applications
// 
// File: GridCheckBox.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Class that defines a checkbox that can be used on a grid.
	/// </summary>
	public class GridCheckBox : CheckBox
	{
		internal const string DefaultBaseID = "bfxGcb";

		/// <summary>
		/// Private field to support <c>BaseID</c> property.
		/// </summary>
		private string _baseID = DefaultBaseID;
		
		/// <summary>
		/// Raised when the <c>ObjectId</c> property has changed.
		/// </summary>
		[Browsable(true), Category("Property Changed"), Description("Raised when the ObjectId property has changed.")]
		public event EventHandler ObjectIdChanged;
		
		/// <summary>
		/// Private field to support <c>ObjectId</c> property.
		/// </summary>
		public string _objectId;

		/// <summary>
		/// Constructor.
		/// </summary>
		public GridCheckBox()
		{
		}

		/// <summary>
		/// Gets or sets the objectid
		/// </summary>
		public string ObjectId
		{
			get
			{
				return _objectId;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				// check to see if the value has changed...
				if(value != _objectId)
				{
					// set the value...
					_objectId = value;

					// set the id of the item...
					if(BaseID == null)
						throw new InvalidOperationException("'BaseID' is null.");
					if(BaseID.Length == 0)
						throw new InvalidOperationException("'BaseID' is zero-length.");
					this.ID = BaseID + _objectId;

					// raise...
					this.OnObjectIdChanged();
				}
			}
		}

		public int ObjectIdAsInt32
		{
			get
			{
				string id = this.ObjectId;
				if(id != null && id.Length > 0)
					return ConversionHelper.ToInt32(id, Cultures.System);
				else
					return 0;
			}
			set
			{
				this.ObjectId = value.ToString();
			}
		}

		/// <summary>
		/// Gets or sets the base ID of the control.  
		/// </summary>
		/// <remarks>This ID will be used to prefix the object's ID - e.g. if you have an ID of <c>27</c> and 
		/// <c>BaseID</c> is <c>bfxGcb</c>, the control's ID will be <c>bfxGcb27</c>.</remarks>
		public string BaseID
		{
			get
			{
				return _baseID;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _baseID)
				{
					// set the value...
					_baseID = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>ObjectIdChanged</c> event.
		/// </summary>
		private void OnObjectIdChanged()
		{
			OnObjectIdChanged(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>ObjectIdChanged</c> event.
		/// </summary>
		protected virtual void OnObjectIdChanged(EventArgs e)
		{
			// raise...
			if(ObjectIdChanged != null)
				ObjectIdChanged(this, e);
		}
	}
}
