// BootFX - Application framework for .NET applications
// 
// File: BaseWebControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ComponentModel;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
    /// <summary>
    /// Defines an instance of <c>BaseControl</c>.
    /// </summary>
    public class BaseWebControl : WebControl, ILoggable
    {
        /// <summary>
        /// Private field to support <see cref="Logs"/> property.
        /// </summary>
        private ILogSet _logs;

        public BaseWebControl()
        {
        }

        ILog ILoggable.Log
        {
            get
            {
                return this.Log;
            }
        }

        /// <summary>
        /// Gets the default log.
        /// </summary>
        protected ILog Log
        {
            get
            {
                if (Logs == null)
                    throw new InvalidOperationException("Logs is null.");
                return this.Logs.DefaultLog;
            }
        }

        ILogSet ILoggable.Logs
        {
            get
            {
                return this.Logs;
            }
        }

        /// <summary>
        /// Gets the logs.
        /// </summary>
        protected ILogSet Logs
        {
            get
            {
                // mbr - 11-10-2005 - provided an ability to invalidate logs if the context changes...				
                if (_logs != null && _logs.ContextId != LogSet.CurrentContextId)
                    _logs = null;
                if (_logs == null)
                    _logs = new LogSet(this.GetType());
                return _logs;
            }
        }
    }
}
