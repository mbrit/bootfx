// BootFX - Application framework for .NET applications
// 
// File: ConfirmLinkButton.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BootFX.Common;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConfirmLinkButton.
	/// </summary>
	public class ConfirmLinkButton : LinkButton, IConfirmButton
	{
        /// <summary>
        /// Private value to support the <see cref="Controller">Controller</see> property.
        /// </summary>
        private ConfirmButtonController _controller;

        public ConfirmLinkButton()
		{
		}

		/// <summary>
		/// Gets or sets the confirmtext.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>ConfirmText</c> for storage.
		/// </remarks>
		[Browsable(true), Category("Behavior")]
		public string ConfirmText
		{
			get
			{
				object value = ViewState["ConfirmText"];
				if(value == null)
					return "Are you sure?";
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != ConfirmText)
				{
					// set the value...
					ViewState["ConfirmText"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the doconfirmation.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>DoConfirmation</c> for storage.
		/// </remarks>
		public bool DoConfirmation
		{
			get
			{
				object value = ViewState["DoConfirmation"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != DoConfirmation)
				{
					// set the value...
					ViewState["DoConfirmation"] = value;
				}
			}
		}

        protected override void Render(HtmlTextWriter writer)
        {
            if (Controller == null)
                throw new InvalidOperationException("'Controller' is null.");
            this.Controller.Render(writer);
        }

        /// <summary>
        /// Gets the Controller value.
        /// </summary>
        private ConfirmButtonController Controller
        {
            get
            {
                if (_controller == null)
                    _controller = new ConfirmButtonController(this);
                return _controller;
            }
        }

        public void BaseRender(HtmlTextWriter writer)
        {
            base.Render(writer);
        }
    }
}
