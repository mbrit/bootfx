// BootFX - Application framework for .NET applications
// 
// File: ConfirmButtonController.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BootFX.Common.UI.Web
{
    internal class ConfirmButtonController
    {
        /// <summary>
        /// Private value to support the <see cref="Owner">Owner</see> property.
        /// </summary>
        private IConfirmButton _owner;

        internal ConfirmButtonController(IConfirmButton button)
        {
            if (button == null)
                throw new ArgumentNullException("button");
            _owner = button;
        }

        /// <summary>
        /// Gets the Owner value.
        /// </summary>
        private IConfirmButton Owner
        {
            get
            {
                return _owner;
            }
        }

        internal void Render(HtmlTextWriter writer)
        {
            if (!(this.Owner.DoConfirmation))
                this.Owner.BaseRender(writer);
            else
            {
                // create a dummy writer...
                string originalCode = null;
                using (StringWriter defer = new StringWriter())
                {
                    HtmlTextWriter deferWriter = new HtmlTextWriter(defer);
                    this.Owner.BaseRender(deferWriter);

                    // get...
                    deferWriter.Flush();
                    defer.Flush();
                    originalCode = defer.GetStringBuilder().ToString();
                }

                // mbr - 20-08-2007 - this fails if you set a style on the control...				
                //				Regex regex = new Regex(@"\<a\s+href=\""javascript:(?<href>[^\""]+)\""", RegexOptions.Singleline | RegexOptions.IgnoreCase);
                Regex regex = new Regex(@"\s+href=\""javascript:(?<href>[^\""]+)\""", RegexOptions.Singleline | RegexOptions.IgnoreCase);
                Match match = regex.Match(originalCode);
                if (match.Success)
                {
                    // group...
                    Group group = match.Groups["href"];
                    if (group == null)
                        throw new InvalidOperationException("group is null.");

                    // time to build...
                    writer.Write(originalCode.Substring(0, group.Index));
                    writer.Write("if(confirm('");
                    writer.Write(this.SanitizedConfirmText);
                    writer.Write("')) ");
                    writer.Write(originalCode.Substring(group.Index));
                }
                else
                {
                    // no match on regex, just write what we can...
                    writer.Write(originalCode);
                }
            }
        }

        private string Evaluator(Match match)
        {
            if (match == null)
                throw new ArgumentNullException("match");


            // return...
            return string.Format("if(confirm('{0}')) {1}", this.SanitizedConfirmText, match.Groups["href"].Value);
        }

        private string SanitizedConfirmText
        {
            get
            {
                if (this.Owner.ConfirmText != null && this.Owner.ConfirmText.Length > 0)
                    return this.Owner.ConfirmText.Replace("\"", "\\\"").Replace("'", "''");
                else
                    return this.Owner.ConfirmText;
            }
        }
    }
}
