// BootFX - Application framework for .NET applications
// 
// File: ConnectionStringBuilder.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ConnectionStringBuilder</c>.
	/// </summary>
	public class ConnectionStringBuilder : AdminControl
	{
		/// <summary>
		/// Raised when the user clicks 'Create Database'.
		/// </summary>
		public event EventHandler CreateDatabase;
		
		/// <summary>
		/// Raised when the string is built.
		/// </summary>
		public event StringEventHandler StringBuilt;
		
		/// <summary>
		/// Private field to support <c>CreateDatabaseButton</c> property.
		/// </summary>
		private Button _createDatabaseButton;
		
		/// <summary>
		/// Private field to support <see cref="BuildButton"/> property.
		/// </summary>
		private Button _buildButton;
		
		public ConnectionStringBuilder()
		{
			this.SaveButtonVisible = false;
			this.Width = new Unit(450, UnitType.Pixel);
			this.Style["border"] = "1px solid black";
		}

		/// <summary>
		/// Gets the buildbutton.
		/// </summary>
		public Button BuildButton
		{
			get
			{
				if(_buildButton == null)
				{
					_buildButton = new Button();
					_buildButton.CssClass = "field";
					_buildButton.Text = "Build Connection String";
					_buildButton.Click += new EventHandler(_buildButton_Click);
				}
				return _buildButton;
			}
		}

		private void _buildButton_Click(object sender, EventArgs e)
		{
			this.FeedbackWidget.Reset();
			try
			{
				string connectionString = null;
				if(!(this.GetConnectionString(ref connectionString)))
					return;

				// check...
				if(connectionString == null)
					throw new InvalidOperationException("'connectionString' is null.");
				if(connectionString.Length == 0)
					throw new InvalidOperationException("'connectionString' is zero-length.");

				// that's it...
				this.OnStringBuilt(new StringEventArgs(connectionString));
			}
			catch(Exception ex)
			{
				this.FeedbackWidget.SetError(ex);
			}
		}

		protected virtual bool GetConnectionString(ref string connectionString)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		/// <summary>
		/// Raises the <c>StringBuilt</c> event.
		/// </summary>
		protected virtual void OnStringBuilt(StringEventArgs e)
		{
			// raise...
			if(StringBuilt != null)
				StringBuilt(this, e);
		}

		/// <summary>
		/// Gets the createdatabasebutton.
		/// </summary>
		protected Button CreateDatabaseButton
		{
			get
			{
				// returns the value...
				if(_createDatabaseButton == null)
				{
					_createDatabaseButton = new Button();
					_createDatabaseButton.CssClass = "field";
					_createDatabaseButton.Text = "Create Database";
					_createDatabaseButton.Click += new EventHandler(_createDatabaseButton_Click);
				}
				return _createDatabaseButton;
			}
		}

		private void _createDatabaseButton_Click(object sender, EventArgs e)
		{
			this.OnCreateDatabase();
		}

		/// <summary>
		/// Raises the <c>CreateDatabase</c> event.
		/// </summary>
		private void OnCreateDatabase()
		{
			OnCreateDatabase(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>CreateDatabase</c> event.
		/// </summary>
		protected virtual void OnCreateDatabase(EventArgs e)
		{
			// raise...
			if(CreateDatabase != null)
				CreateDatabase(this, e);
		}
	}
}
