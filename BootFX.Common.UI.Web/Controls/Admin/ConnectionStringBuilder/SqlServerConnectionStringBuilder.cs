// BootFX - Application framework for .NET applications
// 
// File: SqlServerConnectionStringBuilder.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Data;	

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>SqlServerConnectionStringBuilderAdminControl</c>.
	/// </summary>
	public class SqlServerConnectionStringBuilder : ConnectionStringBuilder
	{
		/// <summary>
		/// Private field to support <see cref="IntegratedSecurityCheckBox"/> property.
		/// </summary>
		private CheckBox _integratedSecurityCheckBox;

		/// <summary>
		/// Private field to support <see cref="ServerNameTextBox"/> property.
		/// </summary>
		private TextBox _usernameTextBox;

		/// <summary>
		/// Private field to support <see cref="ServerNameTextBox"/> property.
		/// </summary>
		private TextBox _passwordTextBox;

		/// <summary>
		/// Private field to support <see cref="ServerNameTextBox"/> property.
		/// </summary>
		private TextBox _serverNameTextBox;
		
		/// <summary>
		/// Private field to support <see cref="ServerNameTextBox"/> property.
		/// </summary>
		private TextBox _databaseNameTextBox;
		
		public SqlServerConnectionStringBuilder()
		{
		}

		protected override void OnFirstLoad(EventArgs e)
		{
			base.OnFirstLoad (e);

			// set...
			this.IntegratedSecurityCheckBox.Checked = true;
			this.ServerNameTextBox.Text = "localhost";
		}

		protected override void BuildViewBody()
		{
			// table...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "smallbodytext";
			cell.Text = "Enter the details of the server and user, and click the <b>Build Connection String</b> button to populate the 'Connection string' box below";

			// row...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);

			// table...
			Table table = new Table();
			cell.Controls.Add(table);
			table.Rows.Add(TableHelper.CreateControlRow("Server name:", this.ServerNameTextBox));
			table.Rows.Add(TableHelper.CreateControlRow("Database name:", this.DatabaseNameTextBox));
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.IntegratedSecurityCheckBox));
			table.Rows.Add(TableHelper.CreateControlRow("Username:", this.UsernameTextBox));
			table.Rows.Add(TableHelper.CreateControlRow("Password:", this.PasswordTextBox));
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.BuildButton));

			// check...
			bool enabled = !(this.IntegratedSecurityCheckBox.Checked);
			this.UsernameTextBox.Enabled = enabled;
			this.PasswordTextBox.Enabled = enabled;

			// divider...
			this.AddDivider();

			// create...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "smallbodytext";
			cell.Text = "To create a database, provide the credentials of a user that can connect to the 'master' database.  Click <b>Create Database</b>, then change the credentials to be a different user with more appropriate permissions, click <b>Build Connection String</b> and then click <b>Database Update</b>.";

			// spacer...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.CreateDatabaseButton);
			cell.HorizontalAlign = HorizontalAlign.Right;
		}

		/// <summary>
		/// Gets the integratedsecuritycheckbox.
		/// </summary>
		private CheckBox IntegratedSecurityCheckBox
		{
			get
			{
				if(_integratedSecurityCheckBox == null)
				{
					_integratedSecurityCheckBox = new CheckBox();
					_integratedSecurityCheckBox.CssClass = "Field";
					_integratedSecurityCheckBox.Text = "Use integrated security";
					_integratedSecurityCheckBox.AutoPostBack = true;
					_integratedSecurityCheckBox.CheckedChanged += new EventHandler(_integratedSecurityCheckBox_CheckedChanged);
				}
				return _integratedSecurityCheckBox;
			}
		}

		/// <summary>
		/// Gets the passwordtextbox.
		/// </summary>
		private TextBox PasswordTextBox
		{
			get
			{
				if(_passwordTextBox == null)
				{
					_passwordTextBox = new TextBox();
					_passwordTextBox.MaxLength = 64;
					_passwordTextBox.CssClass = "field";
				}
				return _passwordTextBox;
			}
		}

		/// <summary>
		/// Gets the usernametextbox.
		/// </summary>
		private TextBox UsernameTextBox
		{
			get
			{
				if(_usernameTextBox == null)
				{
					_usernameTextBox = new TextBox();
					_usernameTextBox.MaxLength = 64;
					_usernameTextBox.CssClass = "field";
				}
				return _usernameTextBox;
			}
		}

		/// <summary>
		/// Gets the servernametextbox.
		/// </summary>
		private TextBox ServerNameTextBox
		{
			get
			{
				if(_serverNameTextBox == null)
				{
					_serverNameTextBox = new TextBox();
					_serverNameTextBox.MaxLength = 64;
					_serverNameTextBox.CssClass = "field";
				}
				return _serverNameTextBox;
			}
		}

		/// <summary>
		/// Gets the servernametextbox.
		/// </summary>
		private TextBox DatabaseNameTextBox
		{
			get
			{
				if(_databaseNameTextBox == null)
				{
					_databaseNameTextBox = new TextBox();
					_databaseNameTextBox.MaxLength = 64;
					_databaseNameTextBox.CssClass = "field";
				}
				return _databaseNameTextBox;
			}
		}

		protected override bool GetConnectionString(ref string connectionString)
		{
			return this.GetConnectionString(false, ref connectionString);
		}

		private bool GetConnectionString(bool master, ref string connectionString)
		{
			string serverName = this.ServerNameTextBox.Text.Trim();
			if(serverName.Length == 0)
			{
				this.FeedbackWidget.SetError("You must enter the server name.");
				return false;
			}
			string databaseName = DatabaseNameTextBox.Text.Trim();
			if(databaseName.Length == 0)
			{
				this.FeedbackWidget.SetError("You must enter the database name.");
				return false;
			}

			// master?
			if(master)	
				databaseName = "master";

			// integrated?
			string username = null;
			string password = null;
			bool integrated = this.IntegratedSecurityCheckBox.Checked;
			if(!(integrated))
			{
				username = this.UsernameTextBox.Text.Trim();
				if(username.Length == 0)
				{
					this.FeedbackWidget.SetError("You must enter the username.");
					return false;
				}
				password = this.PasswordTextBox.Text.Trim();
				if(password.Length == 0)
				{
					this.FeedbackWidget.SetError("You must enter the password.");
					return false;
				}
			}

			// create...
			if(integrated)
				connectionString = string.Format("data source={0};initial catalog={1};integrated security=sspi", serverName, databaseName);
			else
				connectionString = string.Format("data source={0};initial catalog={1};uid={2};pwd={3}", serverName, databaseName, username, password);

			// ok...
			return true;
		}

		private void _integratedSecurityCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			this.RefreshView();
		}

		protected override void OnCreateDatabase(EventArgs e)
		{
			base.OnCreateDatabase (e);

			// get...
			string master = null;
			if(!(this.GetConnectionString(true, ref master)))
				return;

			// run...
			try
			{
				using(IConnection connection = Connection.CreateConnection(typeof(SqlServerConnection), master))
					connection.ExecuteNonQuery(new SqlStatement(string.Format("CREATE DATABASE [{0}];", this.DatabaseNameTextBox.Text.Trim())));

				// set...
				this.FeedbackWidget.SetInfo(string.Format("The database '{0}' was created on '{1}'.", this.DatabaseNameTextBox.Text.Trim(), 
					this.ServerNameTextBox.Text.Trim()));
			}
			catch(Exception ex)
			{
				this.ReportException(string.Format("The database '{0}' could not be created on '{1}'.", this.DatabaseNameTextBox.Text.Trim(), 
					this.ServerNameTextBox.Text.Trim()), ex);
			}
		}
	}
}
