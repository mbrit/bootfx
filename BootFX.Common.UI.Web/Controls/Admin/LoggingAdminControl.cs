// BootFX - Application framework for .NET applications
// 
// File: LoggingAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Data;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	public class LoggingAdminControl : AdminControl
	{
        ///// <summary>
        ///// Private field to support <see cref="MaxHistoryDaysAudit"/> property.
        ///// </summary>
        //private DropDownList _maxHistoryDaysAudit;
		
        ///// <summary>
        ///// Private field to support <see cref="MaxHistoryDaysAudit"/> property.
        ///// </summary>
        //private DropDownList _maxHistoryDaysNonAudit;
		
		/// <summary>
		/// Private field to support <see cref="WebSmartCheckBox"/> property.
		/// </summary>
		private CheckBox _webSmartCheckBox;

		private CheckBox _showExceptionOnUiCheckBox;

//		/// <summary>
//		/// Private field to support <c>PhoneHomeTestButton</c> property.
//		/// </summary>
//		private Button _phoneHomeTestButton;
//		
//		/// <summary>
//		/// Private field to support <c>PhoneHomeCheckBox</c> property.
//		/// </summary>
//		private CheckBox _phoneHomeCheckBox;
//
//		/// <summary>
//		/// Private field to support <see cref="PhoneHomeUrl"/> property.
//		/// </summary>
//		private TextBox _phoneHomeUrl;
//		
//		/// <summary>
//		/// Private field to support <see cref="PhoneHomeUrl"/> property.
//		/// </summary>
//		private TextBox _phoneHomeKey;
		
        ///// <summary>
        ///// Private field to support <c>TestDatabaseButton</c> property.
        ///// </summary>
        //private Button _testDatabaseButton;
		
		/// <summary>
		/// Private field to support <see cref="TestEventLogButton"/> property.
		/// </summary>
		private Button _testEventLogButton;
		
//		/// <summary>
//		/// Private field to support <c>TestEmailButton</c> property.
//		/// </summary>
//		private Button _testEmailButton;
//		
//		/// <summary>
//		/// Private field to support <c>EmailHostNameTextBox</c> property.
//		/// </summary>
//		private TextBox _emailHostNameTextBox = new EmailSettingsTextBox(TextBoxMode.SingleLine);
//
//		/// <summary>
//		/// Private field to support <c>EmailFromTextBox</c> property.
//		/// </summary>
//		private TextBox _emailFromTextBox = new EmailSettingsTextBox(TextBoxMode.SingleLine);
//
//		/// <summary>
//		/// Private field to support <c>EmailToTextBox</c> property.
//		/// </summary>
//		private TextBox _emailToTextBox = new EmailSettingsTextBox(TextBoxMode.SingleLine);
//
//		/// <summary>
//		/// Private field to support <c>EmailUsernameTextBox</c> property.
//		/// </summary>
//		private TextBox _emailUsernameTextBox = new EmailSettingsTextBox(TextBoxMode.SingleLine);
//
//		/// <summary>
//		/// Private field to support <c>EmailPasswordTextBox</c> property.
//		/// </summary>
//		private TextBox _emailPasswordTextBox = new EmailSettingsTextBox(TextBoxMode.Password);
//		
//		/// <summary>
//		/// Private field to support <see cref="EmailConfirmTextBox"/> property.
//		/// </summary>
//		private TextBox _emailConfirmTextBox = new EmailSettingsTextBox(TextBoxMode.Password);

        ///// <summary>
        ///// Private field to support <c>databaseLoggingNoneLogLevelRadioButton</c> property.
        ///// </summary>
        //private LogLevelRadioButton _databaseLoggingNoneLogLevelRadioButton = null;

        ///// <summary>
        ///// Private field to support <c>databaseLoggingInfoLogLevelRadioButton</c> property.
        ///// </summary>
        //private LogLevelRadioButton _databaseLoggingInfoLogLevelRadioButton = null;

        ///// <summary>
        ///// Private field to support <c>databaseLoggingWarnLogLevelRadioButton</c> property.
        ///// </summary>
        //private LogLevelRadioButton _databaseLoggingWarnLogLevelRadioButton = null;

        ///// <summary>
        ///// Private field to support <c>databaseLoggingErrorLogLevelRadioButton</c> property.
        ///// </summary>
        //private LogLevelRadioButton _databaseLoggingErrorLogLevelRadioButton = null;

		/// <summary>
		/// Private field to support <c>EventLogNoneLogLevelRadioButton</c> property.
		/// </summary>
		private LogLevelRadioButton _eventLogNoneLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Debug, true, "eventlog");

		/// <summary>
		/// Private field to support <c>EventLogInfoLogLevelRadioButton</c> property.
		/// </summary>
		private LogLevelRadioButton _eventLogInfoLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Info, false, "eventlog");

		/// <summary>
		/// Private field to support <c>EventLogWarnLogLevelRadioButton</c> property.
		/// </summary>
		private LogLevelRadioButton _eventLogWarnLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Warn, false, "eventlog");

		/// <summary>
		/// Private field to support <c>EventLogErrorLogLevelRadioButton</c> property.
		/// </summary>
		private LogLevelRadioButton _eventLogErrorLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Error, false, "eventlog");

//		/// <summary>
//		/// Private field to support <c>emailNoneLogLevelRadioButton</c> property.
//		/// </summary>
//		private LogLevelRadioButton _emailNoneLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Debug, true, "email");
//
//		/// <summary>
//		/// Private field to support <c>emailInfoLogLevelRadioButton</c> property.
//		/// </summary>
//		private LogLevelRadioButton _emailInfoLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Info, false, "email");
//
//		/// <summary>
//		/// Private field to support <c>emailWarnLogLevelRadioButton</c> property.
//		/// </summary>
//		private LogLevelRadioButton _emailWarnLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Warn, false, "email");
//
//		/// <summary>
//		/// Private field to support <c>emailErrorLogLevelRadioButton</c> property.
//		/// </summary>
//		private LogLevelRadioButton _emailErrorLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Error, false, "email");
		
		public LoggingAdminControl()
		{
			// set...
			this.Width = new Unit(100, UnitType.Percentage);

            //// mbr - 20-08-2007 - moved here...
            //_databaseLoggingNoneLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Debug, true, "databaselogging");
            //_databaseLoggingNoneLogLevelRadioButton.AutoPostBack = true;;
            //_databaseLoggingNoneLogLevelRadioButton.CheckedChanged += new EventHandler(_databaseLoggingNoneLogLevelRadioButton_CheckedChanged); 

            //_databaseLoggingInfoLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Info, false, "databaselogging");
            //_databaseLoggingInfoLogLevelRadioButton.AutoPostBack = true;
            //_databaseLoggingInfoLogLevelRadioButton.CheckedChanged += new EventHandler(_databaseLoggingInfoLogLevelRadioButton_CheckedChanged);

            //_databaseLoggingWarnLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Warn, false, "databaselogging");
            //_databaseLoggingWarnLogLevelRadioButton.AutoPostBack = true;
            //_databaseLoggingWarnLogLevelRadioButton.CheckedChanged += new EventHandler(_databaseLoggingWarnLogLevelRadioButton_CheckedChanged);

            //_databaseLoggingErrorLogLevelRadioButton = new LogLevelRadioButton(LogLevel.Error, false, "databaselogging");
            //_databaseLoggingErrorLogLevelRadioButton.AutoPostBack = true;
            //_databaseLoggingErrorLogLevelRadioButton.CheckedChanged += new EventHandler(_databaseLoggingErrorLogLevelRadioButton_CheckedChanged);
		}

		protected override void OnFirstLoad(EventArgs e)
		{
			base.OnFirstLoad (e);

			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// set...
			if(this.Settings.DoEventLogReporting)
			{
				switch(Settings.EventLogLevel)
				{
					case LogLevel.Debug:
					case LogLevel.Info:
						this.EventLogInfoLogLevelRadioButton.Checked = true;
						break;

					case LogLevel.Warn:
						this.EventLogWarnLogLevelRadioButton.Checked = true;
						break;
					
					case LogLevel.Error:
					case LogLevel.Fatal:
						this.EventLogErrorLogLevelRadioButton.Checked = true;
						break;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Settings.EventLogLevel, Settings.EventLogLevel.GetType()));
				}
			}
			else
				this.EventLogNoneLogLevelRadioButton.Checked = true;

//			// set...
//			if(this.Settings.DoEmailReporting)
//			{
//				switch(Settings.EmailReportingLevel)
//				{
//					case LogLevel.Debug:
//					case LogLevel.Info:
//						this.EmailInfoLogLevelRadioButton.Checked = true;
//						break;
//
//					case LogLevel.Warn:
//						this.EmailWarnLogLevelRadioButton.Checked = true;
//						break;
//					
//					case LogLevel.Error:
//					case LogLevel.Fatal:
//						this.EmailErrorLogLevelRadioButton.Checked = true;
//						break;
//
//					default:
//						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Settings.EmailReportingLevel, Settings.EmailReportingLevel.GetType()));
//				}
//			}
//			else
//				this.EmailNoneLogLevelRadioButton.Checked = true;

            //// set...
            //if(this.Settings.DoDatabaseLogging)
            //{
            //    switch(Settings.DatabaseLoggingLevel)
            //    {
            //        case LogLevel.Debug:
            //        case LogLevel.Info:
            //            this.DatabaseLoggingInfoLogLevelRadioButton.Checked = true;
            //            break;

            //        case LogLevel.Warn:
            //            this.DatabaseLoggingWarnLogLevelRadioButton.Checked = true;
            //            break;
					
            //        case LogLevel.Error:
            //        case LogLevel.Fatal:
            //            this.DatabaseLoggingErrorLogLevelRadioButton.Checked = true;
            //            break;

            //        default:
            //            throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Settings.DatabaseLoggingLevel, Settings.DatabaseLoggingLevel.GetType()));
            //    }
            //}
            //else
            //    this.DatabaseLoggingNoneLogLevelRadioButton.Checked = true;

//			// settings...  
//			this.EmailHostNameTextBox.Text = this.Settings.EmailReportingHostName;
//			this.EmailUsernameTextBox.Text = this.Settings.EmailReportingUsername;
//			this.EmailFromTextBox.Text = this.Settings.EmailReportingFrom;
//			this.EmailToTextBox.Text = this.Settings.EmailReportingTo;

			// mbr - 15-05-2007 - removed.			
//			// mbr - 03-08-2006 - ph...
//			this.PhoneHomeCheckBox.Checked = this.Settings.DoPhoneHomeLogging;
//			this.PhoneHomeUrl.Text = this.Settings.PhoneHomeUrl;
//			this.PhoneHomeKey.Text = this.Settings.PhoneHomeKey;
//			this.RefreshPhoneHomeState();

			// mbr - 27-09-2006 - web...
			this.WebSmartCheckBox.Checked = this.Settings.UseSmartErrorHandlingOnWeb;

			// mbr - 19-05-2008 - added...
			this.ShowExceptionOnUiCheckBox.Checked = this.Settings.ShowExceptionDetailOnUi;

			// mbr - 20-08-2007 - update...
            //this.RefreshDatabaseLogEnableDisable();
		}

		/// <summary>
		/// Updates the view.
		/// </summary>
		protected override void BuildViewBody()
		{
			// check...
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// database...
			this.Rows.Add(this.CreateHeadingRow("Error Handling"));
			this.Rows.Add(TableHelper.CreateControlRow(this.WebSmartCheckBox));
			this.Rows.Add(TableHelper.CreateControlRow(this.ShowExceptionOnUiCheckBox));

			// div...
			this.AddDivider();

            // mbr - 2009-12-21 - deprecated...
            //// database...
            //this.Rows.Add(this.CreateHeadingRow("Database Logging"));

            //// radiobuttons...
            //TableRow row = new TableRow();
            //this.Rows.Add(row);
            //TableCell cell = new TableCell();
            //row.Cells.Add(cell);
            //cell.Controls.Add(this.CreateLogLevelRadioButtonTable(this.DatabaseLoggingNoneLogLevelRadioButton, this.DatabaseLoggingInfoLogLevelRadioButton, this.DatabaseLoggingWarnLogLevelRadioButton, 
            //    this.DatabaseLoggingErrorLogLevelRadioButton));

            //// where?
            //row = new TableRow();
            //this.Rows.Add(row);
            //cell = new TableCell();
            //row.Cells.Add(cell);
            //cell.CssClass = "bodytext";
            //Table table = new Table();
            //cell.Controls.Add(table);
            //ConnectionSettings logDb = this.Settings.GetConnectionSettingsForLogging();
            //if(logDb != null)
            //{
            //    table.Rows.Add(TableHelper.CreateNameValueRow("Log database connection type:", logDb.ConnectionType.FullName));
            //    table.Rows.Add(TableHelper.CreateNameValueRow("Log database connection string:", logDb.ConnectionString));
            //}
            //table.Rows.Add(TableHelper.CreateNameValueRow("Table name:", this.Settings.DatabaseLoggingTableNativeName));

            //// mbr - 20-08-2007 - changed to be editable...			
            //table.Rows.Add(this.CreateTextBoxRow("Maximum days history for audit items:", this.MaxHistoryDaysAuditList));
            //if(!(this.Page.IsPostBack))
            //    FormHelper.SelectListItemByValue(this.MaxHistoryDaysAuditList, this.Settings.MaxDatabaseAuditLogHistoryDays.ToString());
            //table.Rows.Add(this.CreateTextBoxRow("Maximum days history for non-audit items:", this.MaxHistoryDaysNonAuditList));
            //if(!(this.Page.IsPostBack))
            //    FormHelper.SelectListItemByValue(this.MaxHistoryDaysNonAuditList, this.Settings.MaxDatabaseLogHistoryDays.ToString());

            //// spacer...
            //this.Rows.Add(TableHelper.CreateSpacerRow());

            //// button...
            //TableRow row = new TableRow();
            //this.Rows.Add(row);
            //TableCell cell = new TableCell();
            //row.Cells.Add(cell);
            //cell.Controls.Add(this.TestDatabaseButton);

            //// note...
            //row = new TableRow();
            //this.Rows.Add(row);
            //cell = new TableCell();
            //row.Cells.Add(cell);
            //cell.CssClass = "smallbodytext";
            //cell.Text = "The 'Test' button will attempt to create the table in the database if it does not exist.";

			// div...
			this.AddDivider();

			// event log...
			this.Rows.Add(this.CreateHeadingRow("Windows Event Log Error Reporting"));

			// radiobuttons...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.CreateLogLevelRadioButtonTable(this.EventLogNoneLogLevelRadioButton, this.EventLogInfoLogLevelRadioButton, this.EventLogWarnLogLevelRadioButton, 
				this.EventLogErrorLogLevelRadioButton));

			// where?
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "bodytext";
			cell.Text = string.Format("Event log name:&nbsp;<b>{0}</b>", this.Settings.EventLogSourceName);

			// test...
			this.Rows.Add(TableHelper.CreateSpacerRow());
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.TestEventLogButton);

//			// e-mail...
//			this.AddDivider();
//			this.Rows.Add(this.CreateHeadingRow("E-mail Error Reporting"));
//
//			// radiobuttons...
//			row = new TableRow();
//			this.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(this.CreateLogLevelRadioButtonTable(this.EmailNoneLogLevelRadioButton, this.EmailInfoLogLevelRadioButton, this.EmailWarnLogLevelRadioButton, 
//				this.EmailErrorLogLevelRadioButton));
//
//			// add the e-mail settings table...
//			Table emailTable = new Table();
//			cell.Controls.Add(emailTable);
//			emailTable.Rows.Add(this.CreateTextBoxRow("SMTP server name:", this.EmailHostNameTextBox));
//			emailTable.Rows.Add(this.CreateTextBoxRow("Username:", this.EmailUsernameTextBox));
//			emailTable.Rows.Add(this.CreateTextBoxRow("Password:", this.EmailPasswordTextBox));
//			emailTable.Rows.Add(this.CreateTextBoxRow("Confirm password:", this.EmailConfirmTextBox));
//			emailTable.Rows.Add(this.CreateTextBoxRow("From address:", this.EmailFromTextBox));
//			emailTable.Rows.Add(this.CreateTextBoxRow("To address:", this.EmailToTextBox));
//
//			// test...
//			this.Rows.Add(TableHelper.CreateSpacerRow());
//			row = new TableRow();
//			this.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(this.TestEmailButton);
//
//			// information...
//			row = new TableRow();
//			this.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.CssClass = "smallbodytext";
//			cell.Text = "The 'Send Test E-mail' button will send to the last saved configuration.";

			// mbr - 15-05-2007 - this method of phone home removed.			
//			// ph...
//			this.AddDivider();
//			this.Rows.Add(this.CreateHeadingRow("'Phone Home' Error Reporting"));
//
//			// add...
//			table = new Table();
//			row = new TableRow();
//			this.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(table);
//
//			// add...
//			row = new TableRow();
//			table.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.ColumnSpan = 2;
//			cell.Controls.Add(this.PhoneHomeCheckBox);
//
//			// add...
//			row = new TableRow();
//			table.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.CssClass = "bodytext";
//			cell.Text = "URL:&nbsp;";
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(this.PhoneHomeUrl);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(this.PhoneHomeTestButton);
//
//			// add...
//			row = new TableRow();
//			table.Rows.Add(row);
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.CssClass = "bodytext";
//			cell.Text = "Key:&nbsp;";
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(this.PhoneHomeKey);
		}

		// mbr - 20-08-2007 - changed to accept any control.
//		private TableRow CreateTextBoxRow(string caption, TextBox textBox)
		private TableRow CreateTextBoxRow(string caption, Control control)
		{
			if(caption == null)
				throw new ArgumentNullException("caption");
			if(caption.Length == 0)
				throw new ArgumentOutOfRangeException("'caption' is zero-length.");
			if(control == null)
				throw new ArgumentNullException("control");
			
			// create...
			TableRow row = new TableRow();

			// caption...
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "bodytext";
			cell.Text = caption + "&nbsp;";

			// control...
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(control);

			// return...
			return row;
		}

		private Table CreateLogLevelRadioButtonTable(LogLevelRadioButton none, LogLevelRadioButton info, LogLevelRadioButton warn, LogLevelRadioButton error)
		{
			if(none == null)
				throw new ArgumentNullException("none");
			if(info == null)
				throw new ArgumentNullException("info");
			if(warn == null)
				throw new ArgumentNullException("warn");
			if(error == null)
				throw new ArgumentNullException("error");
			
			// add...
			Table table = new Table();
			TableRow row = new TableRow();
			table.Rows.Add(row);

			// loop...
			Control[] controls = new Control[] { none, info, warn, error };
			foreach(Control control in controls)
			{
				TableCell cell = new TableCell();
				row.Cells.Add(cell);
				cell.Controls.Add(control);
			}

			// return...
			return table;
		}

//		/// <summary>
//		/// Gets the emailerrorLogLevelRadioButton.
//		/// </summary>
//		private LogLevelRadioButton EmailErrorLogLevelRadioButton
//		{
//			get
//			{
//				// returns the value...
//				return _emailErrorLogLevelRadioButton;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailwarnLogLevelRadioButton.
//		/// </summary>
//		private LogLevelRadioButton EmailWarnLogLevelRadioButton
//		{
//			get
//			{
//				// returns the value...
//				return _emailWarnLogLevelRadioButton;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailinfoLogLevelRadioButton.
//		/// </summary>
//		private LogLevelRadioButton EmailInfoLogLevelRadioButton
//		{
//			get
//			{
//				// returns the value...
//				return _emailInfoLogLevelRadioButton;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailnoneLogLevelRadioButton.
//		/// </summary>
//		private LogLevelRadioButton EmailNoneLogLevelRadioButton
//		{
//			get
//			{
//				// returns the value...
//				return _emailNoneLogLevelRadioButton;
//			}
//		}

        ///// <summary>
        ///// Gets the DatabaseLoggingerrorLogLevelRadioButton.
        ///// </summary>
        //private LogLevelRadioButton DatabaseLoggingErrorLogLevelRadioButton
        //{
        //    get
        //    {
        //        // returns the value...
        //        return _databaseLoggingErrorLogLevelRadioButton;
        //    }
        //}
		
        ///// <summary>
        ///// Gets the DatabaseLoggingwarnLogLevelRadioButton.
        ///// </summary>
        //private LogLevelRadioButton DatabaseLoggingWarnLogLevelRadioButton
        //{
        //    get
        //    {
        //        // returns the value...
        //        return _databaseLoggingWarnLogLevelRadioButton;
        //    }
        //}
		
        ///// <summary>
        ///// Gets the DatabaseLogginginfoLogLevelRadioButton.
        ///// </summary>
        //private LogLevelRadioButton DatabaseLoggingInfoLogLevelRadioButton
        //{
        //    get
        //    {
        //        // returns the value...
        //        return _databaseLoggingInfoLogLevelRadioButton;
        //    }
        //}
		
        ///// <summary>
        ///// Gets the DatabaseLoggingnoneLogLevelRadioButton.
        ///// </summary>
        //private LogLevelRadioButton DatabaseLoggingNoneLogLevelRadioButton
        //{
        //    get
        //    {
        //        // returns the value...
        //        return _databaseLoggingNoneLogLevelRadioButton;
        //    }
        //}

		/// <summary>
		/// Gets the eventlogerrorLogLevelRadioButton.
		/// </summary>
		private LogLevelRadioButton EventLogErrorLogLevelRadioButton
		{
			get
			{
				// returns the value...
				return _eventLogErrorLogLevelRadioButton;
			}
		}
		
		/// <summary>
		/// Gets the eventlogwarnLogLevelRadioButton.
		/// </summary>
		private LogLevelRadioButton EventLogWarnLogLevelRadioButton
		{
			get
			{
				// returns the value...
				return _eventLogWarnLogLevelRadioButton;
			}
		}
		
		/// <summary>
		/// Gets the eventloginfoLogLevelRadioButton.
		/// </summary>
		private LogLevelRadioButton EventLogInfoLogLevelRadioButton
		{
			get
			{
				// returns the value...
				return _eventLogInfoLogLevelRadioButton;
			}
		}
		
		/// <summary>
		/// Gets the eventlognoneLogLevelRadioButton.
		/// </summary>
		private LogLevelRadioButton EventLogNoneLogLevelRadioButton
		{
			get
			{
				// returns the value...
				return _eventLogNoneLogLevelRadioButton;
			}
		}

		protected override bool Apply()
		{
			// check...
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// mbr - 20-08-2007 - added a checker...
			FormChecker checker = new FormChecker(this);

			// set...
			if(this.EventLogNoneLogLevelRadioButton.Checked)
				this.Settings.DoEventLogReporting = false;
			else
			{
				this.Settings.DoEventLogReporting = true;

				// level...
				if(this.EventLogInfoLogLevelRadioButton.Checked)
					this.Settings.EventLogLevel = LogLevel.Info;
				else if(this.EventLogWarnLogLevelRadioButton.Checked)
					this.Settings.EventLogLevel = LogLevel.Warn;
				else if(this.EventLogErrorLogLevelRadioButton.Checked)
					this.Settings.EventLogLevel = LogLevel.Error;

				// ensure...
//				this.Settings.EnsureEventLogSourceExists();
			}

//			// email...
//			if(this.EmailNoneLogLevelRadioButton.Checked)
//				this.Settings.DoEmailReporting = false;
//			else
//			{
//				this.Settings.DoEmailReporting = true;
//
//				// level...
//				if(this.EmailInfoLogLevelRadioButton.Checked)
//					this.Settings.EmailReportingLevel = LogLevel.Info;
//				else if(this.EmailWarnLogLevelRadioButton.Checked)
//					this.Settings.EmailReportingLevel = LogLevel.Warn;
//				else if(this.EmailErrorLogLevelRadioButton.Checked)
//					this.Settings.EmailReportingLevel = LogLevel.Error;
//
//				// set the host, etc.  (do this whether we're switching it on or not.)
//				this.Settings.EmailReportingHostName = hostName;
//				this.Settings.EmailReportingUsername = username;
//				this.Settings.EmailReportingFrom = from;
//				this.Settings.EmailReportingTo = to;
//
//				// has the password changed?
//				if(password != null && password.Length > 0)
//					this.Settings.EmailReportingPassword = password;
//			}

            //// database...
            //if(this.DatabaseLoggingNoneLogLevelRadioButton.Checked)
            //    this.Settings.DoDatabaseLogging = false;
            //else
            //{
            //    this.Settings.DoDatabaseLogging = true;

            //    // level...
            //    if(this.DatabaseLoggingInfoLogLevelRadioButton.Checked)
            //        this.Settings.DatabaseLoggingLevel = LogLevel.Info;
            //    else if(this.DatabaseLoggingWarnLogLevelRadioButton.Checked)
            //        this.Settings.DatabaseLoggingLevel = LogLevel.Warn;
            //    else if(this.DatabaseLoggingErrorLogLevelRadioButton.Checked)
            //        this.Settings.DatabaseLoggingLevel = LogLevel.Error;
            //}

            //// mbr - 20-08-2007 - update...  (save this even if the other bits aren't done.)
            //this.Settings.MaxDatabaseLogHistoryDays = checker.GetInt32Value(
            //    new ControlReference(this.MaxHistoryDaysNonAuditList, "Non-audit history days"), true);
            //this.Settings.MaxDatabaseAuditLogHistoryDays = checker.GetInt32Value(
            //    new ControlReference(this.MaxHistoryDaysAuditList, "Audit history days"), true);

			// mbr - 27-09-2006 - web...
			this.Settings.UseSmartErrorHandlingOnWeb = this.WebSmartCheckBox.Checked;

			// mbr - 19-05-2008 - added.		
			this.Settings.ShowExceptionDetailOnUi = this.ShowExceptionOnUiCheckBox.Checked;

			// ok?
			if(checker.HasErrors)
			{
				this.FeedbackWidget.SetError(checker);
				return false;
			}

			// save...
			this.Settings.Save();

			// ingo...
			this.FeedbackWidget.SetInfo("Settings have been saved.  Changes will take effect without the application requiring restarting.");

			// ok...
			return true;
		}

//		private class EmailSettingsTextBox : TextBox
//		{
//			internal EmailSettingsTextBox(TextBoxMode mode)
//			{
//				this.CssClass = "field";
//				this.TextMode = mode;
//				this.MaxLength = 48;
//				this.Width = new Unit(200, UnitType.Pixel);
//			}
//		}

		private class LogLevelRadioButton : RadioButton
		{
			internal LogLevelRadioButton(LogLevel level, bool none, string groupName)
			{
				this.GroupName = groupName;
				this.CssClass = "field";

				// text...
				if(none)
					this.Text = "None";
				else
				{
					switch(level)
					{
						case LogLevel.Info:
							this.Text = "Information";
							break;

						case LogLevel.Warn:
							this.Text = "Warning";
							break;

						case LogLevel.Error:
							this.Text = "Error";
							break;

						default:
							throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", level, level.GetType()));
					}
				}			
			}
		}

//		/// <summary>
//		/// Gets the emailpasswordtextbox.
//		/// </summary>
//		private TextBox EmailPasswordTextBox
//		{
//			get
//			{
//				// returns the value...
//				return _emailPasswordTextBox;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailusernametextbox.
//		/// </summary>
//		private TextBox EmailUsernameTextBox
//		{
//			get
//			{
//				// returns the value...
//				return _emailUsernameTextBox;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailtotextbox.
//		/// </summary>
//		private TextBox EmailToTextBox
//		{
//			get
//			{
//				// returns the value...
//				return _emailToTextBox;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailfromtextbox.
//		/// </summary>
//		private TextBox EmailFromTextBox
//		{
//			get
//			{
//				// returns the value...
//				return _emailFromTextBox;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the emailhostnametextbox.
//		/// </summary>
//		private TextBox EmailHostNameTextBox
//		{
//			get
//			{
//				// returns the value...
//				return _emailHostNameTextBox;
//			}
//		}
//
//		/// <summary>
//		/// Gets the emailconfirmtextbox.
//		/// </summary>
//		private TextBox EmailConfirmTextBox
//		{
//			get
//			{
//				return _emailConfirmTextBox;
//			}
//		}
//
//		/// <summary>
//		/// Gets the testemailbutton.
//		/// </summary>
//		private Button TestEmailButton
//		{
//			get
//			{
//				// returns the value...
//				if(_testEmailButton == null)
//				{
//					_testEmailButton = new Button();
//					_testEmailButton.CssClass = "field";
//					_testEmailButton.Text = "Send Test E-mail";
//					_testEmailButton.Click += new EventHandler(_testEmailButton_Click);
//				}
//				return _testEmailButton;
//			}
//		}
//
//		private void _testEmailButton_Click(object sender, EventArgs e)
//		{
//			if(Settings == null)
//				throw new InvalidOperationException("Settings is null.");
//
//			// info...
//			if(this.Settings.IsEmailReportingProperlyConfigured)
//			{
//				// send...
//				try
//				{
//					Settings.SendTestEmailReport();
//					this.Info = "E-mail sent OK.";
//				}
//				catch(Exception ex)
//				{
//					this.ReportException("The e-mail could not be set.", ex, true);
//				}
//			}
//			else
//				this.Problem = "E-mail reporting configuration is invalid.  You must provide the host name, a from address and a to address.";
//		}

		/// <summary>
		/// Gets the testeventlogbutton.
		/// </summary>
		internal Button TestEventLogButton
		{
			get
			{
				if(_testEventLogButton == null)
				{
					_testEventLogButton = new Button();
					_testEventLogButton.CssClass = "field";
					_testEventLogButton.Text = "Write Test Entry";
					_testEventLogButton.Click += new EventHandler(_testEventLogButton_Click);
				}
				return _testEventLogButton;
			}
		}

		private void _testEventLogButton_Click(object sender, EventArgs e)
		{
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// try...
			try
			{
				this.Settings.WriteTestEventLogEntry();
				this.FeedbackWidget.SetInfo("The Windows Event Log entry was written.");
			}
			catch(Exception ex)
			{
				this.ReportException("The entry could not be written.", ex, true);
			}
		}

        ///// <summary>
        ///// Gets the testdatabasebutton.
        ///// </summary>
        //private Button TestDatabaseButton
        //{
        //    get
        //    {
        //        // returns the value...
        //        if(_testDatabaseButton == null)
        //        {
        //            _testDatabaseButton = new Button();
        //            _testDatabaseButton.CssClass = "field";
        //            _testDatabaseButton.Text = "Write Test Entry";
        //            _testDatabaseButton.Click += new EventHandler(_testDatabaseButton_Click);
        //        }
        //        return _testDatabaseButton;
        //    }
        //}

        //private void _testDatabaseButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.Settings.WriteTestDatabaseLogEntry();
        //        this.FeedbackWidget.SetInfo("The database log entry was written.");
        //    }
        //    catch(Exception ex)
        //    {
        //        this.ReportException("The entry could not be written..", ex);
        //    }
        //}

//		/// <summary>
//		/// Gets the phonehomeurl.
//		/// </summary>
//		private TextBox PhoneHomeUrl
//		{
//			get
//			{
//				if(_phoneHomeUrl == null)
//					_phoneHomeUrl = this.CreateTextBox("textPhUrl", new Unit(400, UnitType.Pixel));
//				return _phoneHomeUrl;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the phonehomeurl.
//		/// </summary>
//		private TextBox PhoneHomeKey
//		{
//			get
//			{
//				if(_phoneHomeKey == null)
//					_phoneHomeKey = this.CreateTextBox("textPhKey", new Unit(200, UnitType.Pixel));
//				return _phoneHomeKey;
//			}
//		}
//		
//		/// <summary>
//		/// Gets the phonehomecheckbox.
//		/// </summary>
//		private CheckBox PhoneHomeCheckBox
//		{
//			get
//			{
//				// returns the value...
//				if(_phoneHomeCheckBox == null)
//				{
//					_phoneHomeCheckBox = new CheckBox();
//					_phoneHomeCheckBox.CssClass = "field";
//					_phoneHomeCheckBox.Text = "Send error reports via 'Phone Home' service";
//					_phoneHomeCheckBox.AutoPostBack = true;
//					_phoneHomeCheckBox.CheckedChanged += new EventHandler(_phoneHomeCheckBox_CheckedChanged);
//				}
//				return _phoneHomeCheckBox;
//			}
//		}

//		private void _phoneHomeCheckBox_CheckedChanged(object sender, EventArgs e)
//		{
//			this.RefreshPhoneHomeState();
//		}
//
//		private void RefreshPhoneHomeState()
//		{
//			this.PhoneHomeUrl.Enabled = this.PhoneHomeCheckBox.Checked;
//			this.PhoneHomeKey.Enabled = this.PhoneHomeCheckBox.Checked;
//			this.PhoneHomeTestButton.Enabled = this.PhoneHomeCheckBox.Checked;
//		}

//		/// <summary>
//		/// Gets the phonehometestbutton.
//		/// </summary>
//		private Button PhoneHomeTestButton
//		{
//			get
//			{
//				// returns the value...
//				if(_phoneHomeTestButton == null)
//				{
//					_phoneHomeTestButton = new Button();
//					_phoneHomeTestButton.CssClass = "field";
//					_phoneHomeTestButton.Text = "Test Connectivity";
//					_phoneHomeTestButton.Click += new EventHandler(_phoneHomeTestButton_Click);
//				}
//				return _phoneHomeTestButton;
//			}
//		}
//
//		private void _phoneHomeTestButton_Click(object sender, EventArgs e)
//		{
//			// url...
//			FormChecker checker = new FormChecker(this);
//			string url = checker.GetStringValue(new ControlReference(this.PhoneHomeUrl, "URL"), true);
//			string key = checker.GetStringValue(new ControlReference(this.PhoneHomeKey, "Key"), true);
//			if(checker.HasErrors)
//			{
//				this.Problem = checker.GetAllErrorsSeparatedByBrTag();
//				return;
//			}
//
//			try
//			{
//				PhoneHomeAppender.TestConnectivity(url, key);
//				this.Info = string.Format("A connection to '{0}' with key '{1}' was established.", url, key);
//			}
//			catch(Exception ex)
//			{
//				this.Problem = "Connectivity to the 'Phone Home' Web service experienced a problem:<BR />&nbsp;&nbsp;&nbsp;&nbsp;" + ex.ToString().Replace("\n", "<BR />&nbsp;&nbsp;&nbsp;&nbsp;");
//			}
//		}

		/// <summary>
		/// Gets the websmartcheckbox.
		/// </summary>
		private CheckBox WebSmartCheckBox
		{
			get
			{
				if(_webSmartCheckBox == null)
					_webSmartCheckBox = this.CreateCheckBox("checkSmartWeb", "Use smart error handling on Web applications");
				return _webSmartCheckBox;
			}
		}

		/// <summary>
		/// Gets the websmartcheckbox.
		/// </summary>
		private CheckBox ShowExceptionOnUiCheckBox
		{
			get
			{
				if(_showExceptionOnUiCheckBox == null)
					_showExceptionOnUiCheckBox = this.CreateCheckBox("checkShowExceptions", "Show exception information on user interface");
				return _showExceptionOnUiCheckBox;
			}
		}

        ///// <summary>
        ///// Gets the maxhistorydaysaudit.
        ///// </summary>
        //private DropDownList MaxHistoryDaysAuditList
        //{
        //    get
        //    {
        //        if(_maxHistoryDaysAudit == null)
        //            _maxHistoryDaysAudit = this.CreateHistoryDaysDropDown();
        //        return _maxHistoryDaysAudit;
        //    }
        //}

        ///// <summary>
        ///// Gets the maxhistorydaysaudit.
        ///// </summary>
        //private DropDownList MaxHistoryDaysNonAuditList
        //{
        //    get
        //    {
        //        if(_maxHistoryDaysNonAudit == null)
        //            _maxHistoryDaysNonAudit = this.CreateHistoryDaysDropDown();
        //        return _maxHistoryDaysNonAudit;
        //    }
        //}

        //private DropDownList CreateHistoryDaysDropDown()
        //{
        //    DropDownList list = new DropDownList();
        //    list.CssClass = "field";
        //    for(int index = 0; index <= 180; index += 5)
        //    {
        //        if(index == 0)
        //            list.Items.Add(new ListItem("Do not delete", index.ToString()));
        //        else
        //            list.Items.Add(new ListItem(index.ToString()));
        //    }

        //    // return...
        //    return list;
        //}

        //private void _databaseLoggingNoneLogLevelRadioButton_CheckedChanged(object sender, EventArgs e)
        //{
        //    RefreshDatabaseLogEnableDisable();
        //}

        //private void _databaseLoggingInfoLogLevelRadioButton_CheckedChanged(object sender, EventArgs e)
        //{
        //    RefreshDatabaseLogEnableDisable();
        //}

        //private void _databaseLoggingWarnLogLevelRadioButton_CheckedChanged(object sender, EventArgs e)
        //{
        //    RefreshDatabaseLogEnableDisable();
        //}

        //private void _databaseLoggingErrorLogLevelRadioButton_CheckedChanged(object sender, EventArgs e)
        //{
        //    RefreshDatabaseLogEnableDisable();
        //}

		/// <summary>
		/// Updates the enable/disable state of the history drop-down lists.
		/// </summary>
		// mbr - 20-08-2007 - added.		
        //private void RefreshDatabaseLogEnableDisable()
        //{
        //    this.MaxHistoryDaysAuditList.Enabled = !(this.DatabaseLoggingNoneLogLevelRadioButton.Checked);
        //    this.MaxHistoryDaysNonAuditList.Enabled = this.MaxHistoryDaysAuditList.Enabled;
        //}
	}
}
