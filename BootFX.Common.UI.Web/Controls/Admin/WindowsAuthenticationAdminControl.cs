// BootFX - Application framework for .NET applications
// 
// File: WindowsAuthenticationAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.UI;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>WindowsAuthenticationUserControl</c>.
	/// </summary>
	public class WindowsAuthenticationAdminControl : AdminControl
	{
		/// <summary>
		/// Private field to support <see cref="UsernameTextBox"/> property.
		/// </summary>
		private TextBox _username;

		/// <summary>
		/// Private field to support <see cref="PasswordTextBox"/> property.
		/// </summary>
		private TextBox _password;

		/// <summary>
		/// Private field to support <see cref="ConfirmTextBox"/> property.
		/// </summary>
		private TextBox _confirm;

		/// <summary>
		/// Private field to support <see cref="DomainTextBox"/> property.
		/// </summary>
		private TextBox _domain;
		
		public WindowsAuthenticationAdminControl()
		{
			this.SaveButtonVisible = false;
		}

		protected override void BuildViewBody()
		{
			base.BuildViewBody ();

			// add...
			this.Rows.Add(TableHelper.CreateControlRow("Username:&nbsp;", this.UsernameTextBox));
			this.Rows.Add(TableHelper.CreateControlRow("Password:&nbsp;", this.PasswordTextBox));
			this.Rows.Add(TableHelper.CreateControlRow("Confirm:&nbsp;", this.ConfirmTextBox));
			this.Rows.Add(TableHelper.CreateControlRow("Domain:&nbsp;", this.DomainTextBox));
		}

		/// <summary>
		/// Gets the domain.
		/// </summary>
		private TextBox DomainTextBox
		{
			get
			{
				if(_domain == null)
					_domain = this.CreateTextBox("textDomain");
				return _domain;
			}
		}
		
		/// <summary>
		/// Gets the confirm.
		/// </summary>
		private TextBox ConfirmTextBox
		{
			get
			{
				if(_confirm == null)
				{
					_confirm = this.CreateTextBox("textConfirm");
					_confirm.TextMode = TextBoxMode.Password;
				}
				return _confirm;
			}
		}
		
		/// <summary>
		/// Gets the password.
		/// </summary>
		private TextBox PasswordTextBox
		{
			get
			{
				if(_password == null)
				{
					_password = this.CreateTextBox("textPassword");
					_password.TextMode = TextBoxMode.Password;
				}
				return _password;
			}
		}
		
		/// <summary>
		/// Gets the username.
		/// </summary>
		private TextBox UsernameTextBox
		{
			get
			{
				if(_username == null)
					_username = this.CreateTextBox("textUsername");
				return _username;
			}
		}

		public void Validate(FormChecker checker)
		{
			if(checker == null)
				throw new ArgumentNullException("checker");
			
			// get...
			checker.GetStringValue(new ControlReference(this.UsernameTextBox, "Username"), true);
			string password = checker.GetStringValue(new ControlReference(this.PasswordTextBox, "Password"), true);
			string confirm = checker.GetStringValue(new ControlReference(this.ConfirmTextBox, "Confirm"), true);

			// match?
			if(password != confirm)
				checker.AddError("The passwords you entered do not match.");
		}

		public string Confirm
		{
			get
			{
				return this.ConfirmTextBox.Text.Trim();
			}
			set
			{
				this.ConfirmTextBox.Text = value;
			}
		}

		public string Password
		{
			get
			{
				return this.PasswordTextBox.Text.Trim();
			}
			set
			{
				this.PasswordTextBox.Text = value;
			}
		}

		public string Domain
		{
			get
			{
				return this.DomainTextBox.Text.Trim();
			}
			set
			{
				this.DomainTextBox.Text = value;
			}
		}

		public string Username
		{
			get
			{
				return this.UsernameTextBox.Text.Trim();
			}
			set
			{
				this.UsernameTextBox.Text = value;
			}
		}
	}
}
