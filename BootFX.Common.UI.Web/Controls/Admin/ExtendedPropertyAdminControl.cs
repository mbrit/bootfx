// BootFX - Application framework for .NET applications
// 
// File: ExtendedPropertyAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ComponentModel;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	public class ExtendedPropertyAdminControl : AdminControl
	{
		/// <summary>
		/// Private field to support <see cref="MultiValueCheckBox"/> property.
		/// </summary>
		private CheckBox _multiValueCheckBox;
		
		/// <summary>
		/// Private field to support <see cref="SizeTextBox"/> property.
		/// </summary>
		private TextBox _sizeTextBox;
		
		/// <summary>
		/// Private field to support <see cref="DeleteLookupButton"/> property.
		/// </summary>
		private LinkButton _deleteLookupButton;
		
		/// <summary>
		/// Private field to support <see cref="NewLookupValueTextBox"/> property.
		/// </summary>
		private TextBox _newLookupValueTextBox;

		/// <summary>
		/// Private field to support <c>NewLookupValueButton</c> property.
		/// </summary>
		private Button _newLookupValueButton;
		
		/// <summary>
		/// Private field to support <c>LookupValuesGrid</c> property.
		/// </summary>
		private DataGrid _lookupValuesGrid;
		
		/// <summary>
		/// Private field to support <c>EntityTypeList</c> property.
		/// </summary>
		private DropDownList _entityTypeList;

		/// <summary>
		/// Private field to support <c>TypeList</c> property.
		/// </summary>
		private DropDownList _typeList;

		/// <summary>
		/// Private field to support <c>ExtendedPropertiesList</c> property.
		/// </summary>
		private DataGrid _extendedPropertiesGrid;

		/// <summary>
		/// Private field to support <c>LookupList</c> property.
		/// </summary>
		private DropDownList _lookupList;

		/// <summary>
		/// Private field to support <c>CreatePropertyButton</c> property.
		/// </summary>
		private Button _createPropertyButton;

		/// <summary>
		/// Private field to support <c>CreateLookupButton</c> property.
		/// </summary>
		private Button _createLookupButton;

		/// <summary>
		/// Private field to support <c>PropertyNameTextBox</c> property.
		/// </summary>
		private TextBox _propertyNameTextBox;

		/// <summary>
		/// Private field to support <c>LookupNameTextBox</c> property.
		/// </summary>
		private TextBox _lookupNameTextBox;

		public ExtendedPropertyAdminControl()
		{
			this.SaveButtonVisible = false;
		}

		protected override void BuildViewBody()
		{
			this.Rows.Add(this.CreateHeadingRow("Extended Property Configuration"));

			// form...
			Table table = new Table();
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(table);

			// pair...
			table.Rows.Add(TableHelper.CreateControlRow("Database entity type:", this.EntityTypeList));
			table.Rows.Add(TableHelper.CreateControlRow("Extended properties:",this.ExtendedPropertiesGrid));

			// add...
			table.Rows.Add(TableHelper.CreateSpacerRow());
			table.Rows.Add(this.CreateSubheadingRow("Add property"));
			table.Rows.Add(TableHelper.CreateControlRow("Property type:", this.TypeList));
			if(this.SelectedPropertyType != null)
			{
				if(this.SelectedPropertyType.HasSize)
				{
					table.Rows.Add(TableHelper.CreateControlRow("Property size:", this.SizeTextBox));
					if(this.SizeTextBox.Text.Length == 0)
						this.SizeTextBox.Text = "50";
				}
				if(this.SelectedPropertyType.SupportsMultiValue)
					table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.MultiValueCheckBox));
			}
			table.Rows.Add(TableHelper.CreateControlRow("Property name:", this.PropertyNameTextBox));
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.CreatePropertyButton));
			
			// div...
			this.AddDivider();
			this.Rows.Add(this.CreateHeadingRow("Lookup Configuration"));

			table = new Table();
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(table);

			table.Rows.Add(TableHelper.CreateControlRow("Lookups:", TableHelper.CreateControlTable(this.LookupList, this.DeleteLookupButton)));
			table.Rows.Add(TableHelper.CreateControlRow("Values:" , this.LookupValuesGrid));

			// add a value...
			Table valueTable = new Table();
			row = new TableRow();
			table.Rows.Add(TableHelper.CreateSpacerRow());
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, valueTable));
			valueTable.Rows.Add(TableHelper.CreateControlRow("Description:", this.NewLookupValueTextBox));
			valueTable.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.NewLookupValueButton));

			// add...
			table.Rows.Add(TableHelper.CreateSpacerRow());
			table.Rows.Add(this.CreateSubheadingRow("Add lookup"));
			table.Rows.Add(TableHelper.CreateControlRow("Lookup name:", this.LookupNameTextBox));
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.CreateLookupButton));
		}

		/// <summary>
		/// Gets the newlookupvaluebutton.
		/// </summary>
		private Button NewLookupValueButton
		{
			get
			{
				// returns the value...
				if(_newLookupValueButton == null)
				{
					_newLookupValueButton = new Button();
					_newLookupValueButton.CssClass = "field";
					_newLookupValueButton.Text = "Add Value";
					_newLookupValueButton.Click += new EventHandler(_newLookupValueButton_Click);
				}
				return _newLookupValueButton;
			}
		}
		
		/// <summary>
		/// Gets the newlookupvaluetextbox.
		/// </summary>
		internal TextBox NewLookupValueTextBox
		{
			get
			{
				if(_newLookupValueTextBox == null)
				{
					_newLookupValueTextBox = new TextBox();
					_newLookupValueTextBox.CssClass = "field";
					_newLookupValueTextBox.MaxLength = 255;
				}
				return _newLookupValueTextBox;
			}
		}

		/// <summary>
		/// Gets the lookupvaluesgrid.
		/// </summary>
		private DataGrid LookupValuesGrid
		{
			get
			{
				// returns the value...
				if(_lookupValuesGrid == null)
				{				
					_lookupValuesGrid = new DataGrid();
					_lookupValuesGrid.AutoGenerateColumns = false;
					_lookupValuesGrid.Columns.Add(this.CreateBoundGridColumn("Description", "Description", 250));
					_lookupValuesGrid.Columns.Add(this.CreateBoundGridColumn("Value", "Value", 150));
					_lookupValuesGrid.Columns.Add(this.CreateTemplateGridColumn(string.Empty, new ArrayList(), 75));
					_lookupValuesGrid.ItemDataBound += new DataGridItemEventHandler(_lookupValuesGrid_ItemDataBound);
					_lookupValuesGrid.DeleteCommand += new DataGridCommandEventHandler(_lookupValuesGrid_DeleteCommand);
				}
				return _lookupValuesGrid;
			}
		}

		/// <summary>
		/// Gets the typelist.
		/// </summary>
		private DropDownList EntityTypeList
		{
			get
			{
				// returns the value...
				if(_entityTypeList == null)
				{
					_entityTypeList = new DropDownList();
					_entityTypeList.CssClass = "field";
					_entityTypeList.AutoPostBack = true;
					_entityTypeList.SelectedIndexChanged +=new EventHandler(_entityTypeList_SelectedIndexChanged);

					// set...
					ArrayList list = new ArrayList(EntityType.GetEntityTypes());
					list.Sort(new EntityTypeNameComparer());

					// create...
					_entityTypeList.Items.Clear();
					foreach(EntityType type in list)
						_entityTypeList.Items.Add(new ListItem(string.Format("{0} ({1})", type.Name, type.NativeName), type.Id));
				}
				return _entityTypeList;
			}
		}

		/// <summary>
		/// Gets the typelist.
		/// </summary>
		private DropDownList TypeList
		{
			get
			{
				// returns the value...
				if(_typeList == null)
				{
					_typeList = new DropDownList();
					_typeList.CssClass = "field";
					_typeList.AutoPostBack = true;
					_typeList.SelectedIndexChanged += new EventHandler(_typeList_SelectedIndexChanged);
				}
				return _typeList;
			}
		}

		/// <summary>
		/// Gets the typelist.
		/// </summary>
		private DataGrid ExtendedPropertiesGrid
		{
			get
			{
				// returns the value...
				if(_extendedPropertiesGrid == null)
				{
					_extendedPropertiesGrid = new DataGrid();
					_extendedPropertiesGrid.AutoGenerateColumns = false;
					_extendedPropertiesGrid.ItemDataBound += new DataGridItemEventHandler(_extendedPropertiesGrid_ItemDataBound);
					_extendedPropertiesGrid.DeleteCommand += new DataGridCommandEventHandler(_extendedPropertiesGrid_DeleteCommand);
					_extendedPropertiesGrid.Columns.Add(this.CreateBoundGridColumn("Display Name", "Name", 200));
					_extendedPropertiesGrid.Columns.Add(this.CreateBoundGridColumn("Name", "NativeName", 200));
					_extendedPropertiesGrid.Columns.Add(this.CreateBoundGridColumn("Type", "Metrics", 200));
					_extendedPropertiesGrid.Columns.Add(this.CreateTemplateGridColumn(string.Empty, new ArrayList(), 75));
				}
				return _extendedPropertiesGrid;
			}
		}

		/// <summary>
		/// Gets the lookup list.
		/// </summary>
		private DropDownList LookupList
		{
			get
			{
				// returns the value...
				if(_lookupList == null)
				{
					_lookupList = new DropDownList();
					_lookupList.CssClass = "field";
					_lookupList.DataTextField = "Name";
					_lookupList.DataValueField = "Name";
					_lookupList.AutoPostBack = true;
					_lookupList.SelectedIndexChanged += new EventHandler(_lookupList_SelectedIndexChanged);
				}
				return _lookupList;
			}
		}

		/// <summary>
		/// Gets the create property button.
		/// </summary>
		private Button CreatePropertyButton
		{
			get
			{
				// returns the value...
				if(_createPropertyButton == null)
				{
					_createPropertyButton = new Button();
					_createPropertyButton.ID = "buttonCreateProperty";
					_createPropertyButton.CssClass = "field";
					_createPropertyButton.Text = "Add Property";
					_createPropertyButton.Click += new EventHandler(_createPropertyButton_Click);
				}
				return _createPropertyButton;			
			}
		}

		/// <summary>
		/// Gets the create lookup button.
		/// </summary>
		private Button CreateLookupButton
		{
			get
			{
				// returns the value...
				if(_createLookupButton == null)
				{
					_createLookupButton = new Button();
					_createLookupButton.ID = "buttonCreateLookup";
					_createLookupButton.CssClass = "field";
					_createLookupButton.Text = "Add Lookup";
					_createLookupButton.Click += new EventHandler(_createLookupButton_Click);
				}
				return _createLookupButton;			
			}
		}

		/// <summary>
		/// Gets the property name text box.
		/// </summary>
		private TextBox PropertyNameTextBox
		{
			get
			{
				// returns the value...
				if(_propertyNameTextBox == null)
				{
					_propertyNameTextBox = new TextBox();
					_propertyNameTextBox.ID = "propertyNameTextBox";
					_propertyNameTextBox.CssClass = "field";
					_propertyNameTextBox.MaxLength = DatabaseExtensibilityProvider.MaxNativeNameLength;
					_propertyNameTextBox.Text = "";
					_propertyNameTextBox.Width = new Unit(300, UnitType.Pixel);
				}
				return _propertyNameTextBox;			
			}
		}

		/// <summary>
		/// Gets the property name text box.
		/// </summary>
		private TextBox LookupNameTextBox
		{
			get
			{
				// returns the value...
				if(_lookupNameTextBox == null)
				{
					_lookupNameTextBox = new TextBox();
					_lookupNameTextBox.ID = "lookupNameTextBox";
					_lookupNameTextBox.CssClass = "field";
					_lookupNameTextBox.Text = "";
					_lookupNameTextBox.Width = new Unit(300, UnitType.Pixel);
				}
				return _lookupNameTextBox;			
			}
		}

		private EntityType SelectedEntityType
		{
			get
			{
				string id = this.SelectedEntityTypeId;
				if(id != null && id.Length > 0)
					return EntityType.GetEntityTypeForId(id, OnNotFound.ThrowException);
				else
					return null;
			}
		}

		private string SelectedEntityTypeId
		{
			get
			{
				if(EntityTypeList.SelectedItem != null && this.EntityTypeList.SelectedItem.Value != null && this.EntityTypeList.SelectedItem.Value.Length > 0)
					return this.EntityTypeList.SelectedItem.Value;
				else
					return null;
			}
		}

		private ExtendedPropertyDataType SelectedPropertyType
		{
			get
			{
				if(TypeList.SelectedItem != null)
				{
					string asString = this.TypeList.SelectedItem.Value;
					return ExtendedPropertyDataType.GetExtendedPropertyDataType(asString);
				}
				else
					return null;
			}
		}

		private string SelectedPropertyName
		{
			get
			{
				return null;
			}
		}

		private string SelectedLookupName
		{
			get
			{
				if(LookupList.SelectedItem == null)
					return null;

				return this.LookupList.SelectedItem.Value;
			}
		}

		private string PropertyName
		{
			get
			{
				return this.PropertyNameTextBox.Text.Trim();
			}
		}

		private string LookupName
		{
			get
			{
				return this.LookupNameTextBox.Text.Trim();
			}
		}

		protected override bool Apply()
		{
			// save...
			EntityType.SaveExtendedPropertySettings();
			return true;
		}

		private void _createPropertyButton_Click(object sender, EventArgs e)
		{
			FormChecker checker = new FormChecker(this);
			if(PropertyName == string.Empty)
			{
				this.FeedbackWidget.SetError("You must enter a property name.");
				return;
			}
			ExtendedPropertyDataType type = this.SelectedPropertyType;
			if(type == null)
			{
				this.FeedbackWidget.SetError("You must select a property type.");
				return;
			}

			// check...
			long size = 0;
			if(type.HasSize)
				size = checker.GetInt64Value(this.SizeTextBox, true);

			// errors?
			if(checker.HasErrors)
			{
				this.FeedbackWidget.SetError(checker);
				return;
			}

			// mbr - 08-12-2005 - defer creation...
			try
			{
				EntityType.ExtendedPropertySettings.CreateProperty(this.SelectedEntityType, this.PropertyName, type, size, 
					this.MultiValueCheckBox.Checked);
			}
			catch(Exception ex)
			{			
				this.ReportException("Failed to create property.", ex);
			}

			// clear...
			this.PropertyNameTextBox.Text = string.Empty;

			// apply...
			this.Apply();

			// update the view...
			RefreshView();
		}

		private void _createLookupButton_Click(object sender, EventArgs e)
		{
			string name = this.LookupName;
			if(name.Length == 0)
			{
				this.FeedbackWidget.SetError("Please enter a lookup name.");
				return;
			}

			if(EntityType.ExtendedPropertySettings.Lookups.Contains(LookupName))
			{
				this.FeedbackWidget.SetError("This lookup name is already in use. Please try a different lookup name.");
				return;
			}

			ExtendedLookupDefinition lookup = new ExtendedLookupDefinition(name);
			EntityType.ExtendedPropertySettings.Lookups.Add(lookup);

			// clear...
			this.LookupNameTextBox.Text = string.Empty;

			// apply...
			this.Apply();

			// select...
			FormHelper.ClearListSelection(this.LookupList);
			foreach(ListItem item in this.LookupList.Items)
			{		
				if(item.Text == name)
				{
					item.Selected = true;
					break;
				}
			}

			// update...
			RefreshView();
		}

		/// <summary>
		/// Gets the deletelookupbutton.
		/// </summary>
		internal LinkButton DeleteLookupButton
		{
			get
			{
				if(_deleteLookupButton == null)
				{
					_deleteLookupButton = new LinkButton();
					_deleteLookupButton.CssClass = "link";
					_deleteLookupButton.Text = "Delete";
					_deleteLookupButton.Click += new EventHandler(_deleteLookupButton_Click);
				}
				return _deleteLookupButton;
			}
		}

		private void _deleteLookupButton_Click(object sender, EventArgs e)
		{
			if(SelectedLookupName == null)
			{
				this.FeedbackWidget.SetError("You must select a lookup to delete.");
				return;	
			}

			// delete...
			try
			{
				EntityType.ExtendedPropertySettings.DeleteLookup(this.SelectedLookupName);
			}
			catch(Exception ex)
			{
				this.ReportException(string.Format("The lookup '{0}' could not be deleted.", this.SelectedLookupName), ex);
				return;
			}

			// apply...
			this.Apply();

			// update...
			RefreshView();
		}

		private void _entityTypeList_SelectedIndexChanged(object sender, EventArgs e)
		{
			RefreshView();
		}

		protected override void RefreshView()
		{
			base.RefreshView ();

			// add...
			if(this.SelectedEntityType != null)
				this.ExtendedPropertiesGrid.DataSource = EntityType.ExtendedPropertySettings.Properties.GetPropertiesForEntityType(this.SelectedEntityType);
			else
				this.ExtendedPropertiesGrid.DataSource = null;
			this.ExtendedPropertiesGrid.DataBind();

			// list...
			if(TypeList == null)
				throw new InvalidOperationException("TypeList is null.");
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// get...
			string selected = null;
			if(this.TypeList.SelectedItem != null)
				selected = this.TypeList.SelectedItem.Value;
			this.TypeList.Items.Clear();
			foreach(ExtendedPropertyDataType type in EntityType.ExtendedPropertySettings.GetExtendedPropertyDataTypes())
				this.TypeList.Items.Add(new ListItem(type.DisplayName, type.Id));
			FormHelper.SelectListItemByValue(this.TypeList, selected);

			// lookups
			selected = null;
			if(this.LookupList.SelectedItem != null)
				selected = this.LookupList.SelectedItem.Value;
			LookupList.DataSource = EntityType.ExtendedPropertySettings.Lookups;
			LookupList.DataBind();	
			FormHelper.SelectListItemByValue(this.LookupList, selected);

			// lookup values...
			if(this.SelectedLookupName != null && this.SelectedLookupName.Length > 0)
				this.LookupValuesGrid.DataSource = LookupValue.GetByName(this.SelectedLookupName);
			else
				this.LookupValuesGrid.DataSource = null;
			this.LookupValuesGrid.DataBind();
		}

		private void _extendedPropertiesGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.DataItem == null)
				return;

			// get...
			ExtendedPropertyDefinition prop = (ExtendedPropertyDefinition)e.Item.DataItem;

			// get...
			LinkButton delete = this.CreateDeleteLinkButton();
			delete.CommandArgument = prop.NativeName;
			((TableCell)e.Item.Controls[e.Item.Controls.Count - 1]).Controls.Add(delete);
		}

		private void _extendedPropertiesGrid_DeleteCommand(object source, DataGridCommandEventArgs e)
		{
			// delete it...
			string nativeName = (string)e.CommandArgument;
			try
			{
				EntityType.ExtendedPropertySettings.DeleteProperty(this.SelectedEntityType, nativeName);

				// apply...
				this.Apply();

				// update...
				this.RefreshView();
			}
			catch(Exception ex)
			{
				this.ReportException(string.Format("The property '{0}' could not be deleted.", nativeName), ex);
			}
		}

		private void _lookupList_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.RefreshView();
		}

		private void _newLookupValueButton_Click(object sender, EventArgs e)
		{
			string value = this.NewLookupValueTextBox.Text.Trim();
			if(value.Length == 0)
			{
				this.FeedbackWidget.SetError("You must enter a value.");
				return;
			}
			string name = this.SelectedLookupName;
			if(name == null || name.Length == 0)
			{
				this.FeedbackWidget.SetError("You must select a lookup name.");
				return;
			}

			// create...
			try
			{
				LookupValue.CreateAndSave(name, value);
			}
			catch(Exception ex)
			{
				this.ReportException(string.Format("Couldn't add '{0}' to lookup '{1}'.", value, name), ex);
			}

			// clear...
			this.NewLookupValueTextBox.Text = string.Empty;

			// update...
			this.RefreshView();
		}

		private void _lookupValuesGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.DataItem == null)
				return;

			// get...
			LookupValue value = (LookupValue)EntityView.Unwrap(e.Item.DataItem);

			// get...
			LinkButton delete = this.CreateDeleteLinkButton();
			delete.CommandArgument = value.LookupId.ToString();
			((TableCell)e.Item.Controls[e.Item.Controls.Count - 1]).Controls.Add(delete);
		}

		private void _lookupValuesGrid_DeleteCommand(object source, DataGridCommandEventArgs e)
		{
			// get it...
			LookupValue value = (LookupValue)LookupValue.GetById(ConversionHelper.ToInt32(e.CommandArgument, Cultures.System));
			if(value == null)
				throw new InvalidOperationException("value is null.");

			// delete...
			value.MarkForDeletion();
			value.SaveChanges();

			// refresh...
			this.RefreshView();
		}

		/// <summary>
		/// Gets the sizetextbox.
		/// </summary>
		internal TextBox SizeTextBox
		{
			get
			{
				if(_sizeTextBox == null)
				{			 
					_sizeTextBox = new TextBox();
					_sizeTextBox.MaxLength = 4;
					_sizeTextBox.Width = new Unit(60, UnitType.Pixel);
					_sizeTextBox.CssClass = "field";
				}
				return _sizeTextBox;
			}
		}

		private void _typeList_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.RefreshView();
		}

		/// <summary>
		/// Gets the multivaluecheckbox.
		/// </summary>
		private CheckBox MultiValueCheckBox
		{
			get
			{
				if(_multiValueCheckBox == null)
				{
					_multiValueCheckBox	= new CheckBox();
					_multiValueCheckBox.CssClass = "field";
					_multiValueCheckBox.Text = "Allow multiple values";
				}
				return _multiValueCheckBox;
			}
		}

		protected override void OnFirstLoad(EventArgs e)
		{
			base.OnFirstLoad (e);

			// set...
			this.MultiValueCheckBox.Checked = true;
		}
	}
}
