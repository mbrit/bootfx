// BootFX - Application framework for .NET applications
// 
// File: AdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AdminControl</c>.
	/// </summary>
	public class AdminControl : LoggableTable
	{
		public const string DeleteControlId = "linkDelete";

		/// <summary>
		/// Private field to support <see cref="FeedbackWidget"/> property.
		/// </summary>
		private UserFeedbackWidget _feedbackWidget;
		
		/// <summary>
		/// Raised when the refresh button is clicked.
		/// </summary>
		public event EventHandler Refresh;
		
		/// <summary>
		/// Private field to support <see cref="RefreshButton"/> property.
		/// </summary>
		private Button _refreshButton;
		
		/// <summary>
		/// Private field to support <c>SaveButtonVisible</c> property.
		/// </summary>
		private bool _saveButtonVisible = true;
		
		/// <summary>
		/// Private field to support <see cref="SaveButton"/> property.
		/// </summary>
		private Button _saveButton;

		/// <summary>
		/// Raised on the first load.
		/// </summary>
		public event EventHandler FirstLoad;

		// mbr - 19-05-2008 - replaced with feedback widget.	
//		/// <summary>
//		/// Private field to support <see cref="ProblemLabel"/> property.
//		/// </summary>
//		private Label _problemLabel;
//		
//		/// <summary>
//		/// Private field to support <see cref="ProblemLabel"/> property.
//		/// </summary>
//		private Label _infoLabel;
		
		public AdminControl()
		{
			// width...
			this.Width = new Unit(100, UnitType.Percentage);
		}

		/// <summary>
		/// Throws an exception if the Windows user is not authenticated.
		/// </summary>
		private void AssertNotAnonymous()
		{
			// mbr - 14-05-2008 - changed this to allow the application to handle its own authentication.
			// this is helpful when bfx authentication needs to be used...

			// page...
			if(this.Page is BasePage)
			{
				if(((BasePage)this.Page).AuthorizeAdminControl())
					return;

				// if we're not auth'd, if we're flagged as secure by definition we must be using BFX authentication, so go
				// to the logon page...
				if(((BasePage)this.Page).IsSecure)
				{
					((BasePage)this.Page).GoToLogonPage();
					return;
				}
			}

			// mbr - 14-05-2008 - revert to default if the page doens't want to handle it...	
			if(!(Thread.CurrentPrincipal.Identity.IsAuthenticated))
				throw new InvalidOperationException("The user is not authenticated.  Ensure that anonymous authentication is turned off for this virtual directory.");
		}

		/// <summary>
		/// Does the view refresh.
		/// </summary>
		protected virtual void BuildViewBody()
		{
		}

		protected InstallationSettings Settings
		{
			get
			{
				InstallationSettings settings = Runtime.Current.InstallationSettings;
				if(settings == null)
					throw new InvalidOperationException("settings is null.");
				return settings;
			}
		}

		// mbr - 19-05-2008 - replaced with feedback widget.	
//		/// <summary>
//		/// Gets the Infolabel.
//		/// </summary>
//		private Label InfoLabel
//		{
//			get
//			{
//				if(_infoLabel == null)
//				{
//					_infoLabel = new Label();
//					_infoLabel.CssClass = "info";
//					_infoLabel.EnableViewState = false;
//				}
//				return _infoLabel;
//			}
//		}
//
//		/// <summary>
//		/// Gets the problemlabel.
//		/// </summary>
//		private Label ProblemLabel
//		{
//			get
//			{
//				if(_problemLabel == null)
//				{
//					_problemLabel = new Label();
//					_problemLabel.CssClass = "problem";
//					_problemLabel.EnableViewState = false;
//				}
//				return _problemLabel;
//			}
//		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// design mode?
			if(this.Site != null && this.Site.DesignMode)
				return;

			// check the identity of the user...
			this.AssertNotAnonymous();

			// mbr - 19-05-2008 - replaced with feedback...
		
//			this.ProblemLabel.Text = string.Empty;
//			this.InfoLabel.Text = string.Empty;

			// postback?
			if(!(this.Page.IsPostBack))
				this.OnFirstLoad();
		
			// update...
			this.RefreshView();
		}

		/// <summary>
		/// Raises the <c>FirstLoad</c> event.
		/// </summary>
		private void OnFirstLoad()
		{
			OnFirstLoad(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>FirstLoad</c> event.
		/// </summary>
		protected virtual void OnFirstLoad(EventArgs e)
		{
			// raise...
			if(FirstLoad != null)
				FirstLoad(this, e);
		}

		// mbr - 20-08-2007 - used tablehelper.		
//		protected TableRow CreateNameValueRow(string name, string value)
//		{
//			TableRow row = new TableRow();
//			TableCell cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Text = string.Format("{0}&nbsp;", name);
//			cell.CssClass = "bodytext";
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Text = value;
//			cell.CssClass = "bodytext";
//
//			// return...
//			return row;
//		}

		protected virtual void RefreshView()
		{
			// mbr - 19-05-2008 - replaced with feedback widget...	
//			this.Problem = string.Empty;
//			this.Info = string.Empty;

			// clear...
			this.Rows.Clear();

			// update...
			bool doSave = this.SaveButtonVisible;
			try
			{
				this.BuildViewBody();
			}
			catch(Exception ex)
			{
				this.ReportException("An unhandled exception occurred.", ex);
				doSave = false;
			}

			// save?
			TableRow row = null;
			TableCell cell = null;
			if(doSave)
			{
				// add save changes and problem/info...
				this.AddDivider();

				// save...
				row = new TableRow();
				this.Rows.Add(row);
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.Controls.Add(TableHelper.CreateControlTable(this.SaveButton));
			}
			else
			{
				// mbr - 19-05-2008 - replaced with feedback...
				//if((this.Problem != null && this.Problem.Length > 0) || (this.Info != null && this.Info.Length > 0))
				if(this.FeedbackWidget.IsShowingMessage)
				{
					this.AddDivider();
				}
			}

			// problem...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);

			// mbr - 19-05-2008 - replaced with feedback...
//			cell.Controls.Add(this.ProblemLabel);
//			cell.Controls.Add(this.InfoLabel);
			cell.Controls.Add(this.FeedbackWidget);
		}

		// mbr - 20-08-2007 - use tablehelper.		
//		protected TableRow CreateSpacerRow()
//		{
//			TableRow row = new TableRow();
//			TableCell cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Height = new Unit(15, UnitType.Pixel);
//			return row;
//		}

		protected void AddDivider()
		{			
			// row...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Height = new Unit(8, UnitType.Pixel);

			// row...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Text = "<hr color=#000000>";

			// row...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Height = new Unit(8, UnitType.Pixel);
		}

		/// <summary>
		/// Gets the savebutton.
		/// </summary>
		private Button SaveButton
		{
			get
			{
				if(_saveButton == null)
				{
					_saveButton = new Button();
					_saveButton.Text = "Save Changes";
					_saveButton.CssClass = "field";
					_saveButton.Click += new EventHandler(_saveButton_Click);
				}
				return _saveButton;
			}
		}

		private void _saveButton_Click(object sender, EventArgs e)
		{
			try
			{
				this.Apply();
			}
			catch(Exception ex)
			{
				this.ReportException("Failed to save changes.", ex);
			}
		}

		protected virtual bool Apply()
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		// mbr - 19-05-2008 - replaced with feedback widget...	
//		protected string Info
//		{
//			get
//			{
//				return this.InfoLabel.Text;
//			}
//			set
//			{
//				this.InfoLabel.Text = value;
//			}
//		}
//
//		protected string Problem
//		{
//			get
//			{
//				return this.ProblemLabel.Text;
//			}
//			set
//			{
//				if(value != this.ProblemLabel.Text)
//				{
//					this.ProblemLabel.Text = value;
//					this.Info = string.Empty;
//				}
//			}
//		}

		/// <summary>
		/// Gets the feedbackwidget.
		/// </summary>
		// mbr - 19-05-2008 - added.
		protected UserFeedbackWidget FeedbackWidget
		{
			get
			{
				if(_feedbackWidget == null)
					_feedbackWidget = new UserFeedbackWidget();
				return _feedbackWidget;
			}
		}

		protected void ReportException(string message, Exception ex)
		{
			this.ReportException(message, ex, false);
		}

		protected void ReportException(string message, Exception ex, bool suppressLogging)
		{
			if(message == null)
				throw new ArgumentNullException("message");
			if(message.Length == 0)
				throw new ArgumentOutOfRangeException("'message' is zero-length.");
			if(ex == null)
				throw new ArgumentNullException("ex");

			// log...
			if(!(suppressLogging))
			{
				if(this.Log.IsErrorEnabled)
					this.Log.Error(message, ex);
			}

			// mbr - 19-05-2008 - replaced with widget...		
//			StringBuilder builder = new StringBuilder();
//			builder.Append(message);
//			builder.Append("<br>&nbsp;&nbsp;&nbsp;");
//			builder.Append(ex.ToString().Replace("\r\n", "<BR />"));
//
//			// set...
//			this.Problem = builder.ToString();
			this.FeedbackWidget.SetError(ex);
		}

		protected TableRow CreateHeadingRow(string heading)
		{
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "heading4";
			cell.Text = heading;
			cell.ColumnSpan = 10;

			// return...
			return row;
		}
		
		/// <summary>
		/// Gets or sets the savebuttonvisible
		/// </summary>
		internal bool SaveButtonVisible
		{
			get
			{
				return _saveButtonVisible;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _saveButtonVisible)
				{
					// set the value...
					_saveButtonVisible = value;
				}
			}
		}

		// mbr - 20-08-2007 - used tablehelper.		
//		protected TableRow CreateCaptionControlRow(string caption, Control control)
//		{
//			if(control == null)
//				throw new ArgumentNullException("control");
//			
//			// create...
//			TableRow row = new TableRow();
//			TableCell cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.CssClass = "bodytext";
//			cell.Text = string.Format("{0}&nbsp;", caption);
//			cell.VerticalAlign = VerticalAlign.Top;
//
//			// next...
//			cell = new TableCell();
//			row.Cells.Add(cell);
//			cell.Controls.Add(control);
//			cell.VerticalAlign = VerticalAlign.Top;
//
//			// return...
//			return row;
//		}

		// mbr - 20-08-2007 - used tablehelper.		
//		protected Table CreateControlTable(params Control[] controls)
//		{
//			if(controls == null)
//				throw new ArgumentNullException("controls");
//			
//			// create...
//			Table table = new Table();
//			TableRow row = new TableRow();
//			table.Rows.Add(row);
//
//			foreach(Control control in controls)
//			{
//				TableCell cell = new TableCell();
//				row.Cells.Add(cell);
//				cell.Controls.Add(control);
//			}
//
//			// return...
//			return table;
//		}

		/// <summary>
		/// Gets the refreshbutton.
		/// </summary>
		protected Button RefreshButton
		{
			get
			{
				if(_refreshButton == null)
				{
					_refreshButton = new Button();
					_refreshButton.ID = "buttonRefresh";
					_refreshButton.CssClass = "field";
					_refreshButton.Text = "Refresh";
					_refreshButton.Click += new EventHandler(_refreshButton_Click);
				}
				return _refreshButton;
			}
		}

		/// <summary>
		/// Raises the <c>Refresh</c> event.
		/// </summary>
		private void OnRefresh()
		{
			OnRefresh(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>Refresh</c> event.
		/// </summary>
		protected virtual void OnRefresh(EventArgs e)
		{
			// raise...
			if(Refresh != null)
				Refresh(this, e);
		}

		private void _refreshButton_Click(object sender, EventArgs e)
		{
			this.OnRefresh();
		}

		protected LinkButton CreateDeleteLinkButton()
		{
			LinkButton button = new LinkButton();
			button.ID = DeleteControlId;
			button.Text = "Delete";
			button.CssClass = "link";
			button.CommandName = "delete";

			// return...
			return button;
		}
		
		protected BoundColumn CreateBoundGridColumn(string caption, string name, int width)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");

			// create...
			BoundColumn bound = new BoundColumn();
			bound.DataField = name;
			bound.HeaderText = caption;
			if(width > 0)
				bound.ItemStyle.Width = new Unit(width, UnitType.Pixel);

			// return,...
			return bound;
		}

		/// <summary>
		/// Creates a template grid column.
		/// </summary>
		/// <param name="caption"></param>
		/// <param name="controls"></param>
		/// <returns></returns>
		protected TemplateColumn CreateTemplateGridColumn(string caption, IList controls, int width)
		{
			if(controls == null)
				throw new ArgumentNullException("controls");
		
			// create...
			TemplateColumn column = new TemplateColumn();
			column.HeaderText = caption;
			if(width > 0)
				column.ItemStyle.Width = new Unit(width, UnitType.Pixel);

			// create...
			column.ItemTemplate = new ControlsTemplate(controls);

			// return...
			return column;
		}

		private class ControlsTemplate : ITemplate
		{
			/// <summary>
			/// Private field to support <see cref="Controls"/> property.
			/// </summary>
			private IList _controls;
			
			internal ControlsTemplate(IList controls)
			{
				if(controls == null)
					throw new ArgumentNullException("controls");
				_controls = controls;
			}

			/// <summary>
			/// Gets the controls.
			/// </summary>
			internal IList Controls
			{
				get
				{
					return _controls;
				}
			}

			public void InstantiateIn(Control container)
			{
				foreach(Control control in this.Controls)
					container.Controls.Add(control);
			}
		}
		
		public TableRow CreateSubheadingRow(string heading)
		{
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "heading5";
			cell.Text = heading;
			cell.ColumnSpan = 10;

			// return...
			return row;
		}

		protected DropDownList CreateDropDownList(string id)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");

			DropDownList list = new DropDownList();
			list.ID = id;
			list.CssClass = "field";
			return list;
		}

		protected TextBox CreateTextBox(string id)
		{
			return this.CreateTextBox(id, Unit.Empty);
		}

		protected TextBox CreateTextBox(string id, Unit width)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");

			TextBox box = new TextBox();
			box.ID = id;
			box.CssClass = "field";
			if(width != Unit.Empty)
				box.Width = width;
			return box;
		}

		protected Label CreateInfoLabel(string id)
		{
			return this.CreateLabel(id, UserFeedbackWidget.DefaultInfoCssClass);
		}

		protected Label CreateProblemLabel(string id)
		{
			return this.CreateLabel(id, UserFeedbackWidget.DefaultErrorCssClass);
		}

		protected Label CreateLabel(string id)
		{
			return this.CreateLabel(id, "bodytext");
		}

		protected Label CreateLabel(string id, string cssClass)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			if(cssClass == null)
				throw new ArgumentNullException("cssClass");
			if(cssClass.Length == 0)
				throw new ArgumentOutOfRangeException("'cssClass' is zero-length.");
			
			// label...
			Label label = new Label();
			label.ID = id;
			label.CssClass = cssClass;
			return label;
		}

		protected LinkButton CreateLinkButton(string id, string text)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			LinkButton lb = new LinkButton();
			lb.ID = id;
			lb.CssClass = "link";
			lb.Text = text;
			return lb;
		}

		protected Button CreateButton(string id, string text)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			Button button = new Button();
			button.ID = id;
			button.CssClass = "field";
			button.Text = text;
			return button;
		}

		protected CheckBox CreateCheckBox(string id, string text)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			CheckBox button = new CheckBox();
			button.ID = id;
			button.CssClass = "field";
			button.Text = text;

			// return...
			return button;
		}

		protected RadioButton CreateRadioButton(string id, string text, string group)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			if(group == null)
				throw new ArgumentNullException("group");
			if(group.Length == 0)
				throw new ArgumentOutOfRangeException("'group' is zero-length.");
			
			RadioButton button = new RadioButton();
			button.ID = id;
			button.CssClass = "field";
			button.Text = text;
			button.GroupName = group;

			// return...
			return button;
		}

		protected BoundColumn GetBoundColumn(string name, string header, Unit width)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// bound...
			BoundColumn column = new BoundColumn();
			column.DataField = name;
			column.HeaderText = header;
			column.ItemStyle.CssClass = "smallbodytext";
			column.ItemStyle.VerticalAlign = VerticalAlign.Top;
			column.HeaderStyle.CssClass = "boldbodytext";
			if(width != Unit.Empty)
				column.ItemStyle.Width = width;

			// return...
			return column;
		}

		// mbr - 20-08-2007 - used tablehelper.		
//		protected TableRow CreateControlRow(string caption, Control control)
//		{
//			Label label = new Label();
//			label.CssClass = "bodytext";
//			label.Text = caption;
//
//			// return...
//			return TableHelper.CreateControlRow(label, control);
//		}
//
//		protected TableRow CreateControlRow(params Control[] controls)
//		{
//			if(controls == null)
//				throw new ArgumentNullException("controls");
//
//			// mbr - 20-08-2007 - changed to helper.			
////			TableRow row = new TableRow();
////			this.Rows.Add(row);
////			foreach(Control control in controls)
////			{
////				TableCell cell = new TableCell();
////				row.Cells.Add(cell);
////				cell.Controls.Add(control);
////			}
////
////			// return...
////			return row;
//
//			// return...
//			return TableHelper.CreateControlRow(controls);
//		}

		// mbr - 20-08-2007 - added.		
		protected string ControlCssClass
		{
			get
			{
				return "field";
			}
		}
	}
}
