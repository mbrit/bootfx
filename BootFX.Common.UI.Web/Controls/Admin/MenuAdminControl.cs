// BootFX - Application framework for .NET applications
// 
// File: MenuAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>MenuAdminControl</c>.
	/// </summary>
	public class MenuAdminControl : AdminControl
	{
		public MenuAdminControl()
		{
		}

		protected override void BuildViewBody()
		{
			base.BuildViewBody ();

			// add...
			this.AddOption("Configure database settings", "~/admindatabase.aspx");
			this.AddOption("Configure application logging", "~/admindatabase.aspx");
			this.AddOption("Configure proxy settings", "~/adminproxy.aspx");
			this.AddOption("Configure shared settings", "~/adminsettings.aspx");
			this.AddOption("Start or stop services", "~/adminservices.aspx");
			this.AddOption("View database log", "~/adminviewdatabaselog.aspx");
		}

		private void AddOption(string name, string url)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			
			// create...
		}
	}
}
