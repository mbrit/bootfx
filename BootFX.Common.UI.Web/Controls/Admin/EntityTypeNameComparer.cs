// BootFX - Application framework for .NET applications
// 
// File: EntityTypeNameComparer.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>EntityTypeNameComparer</c>.
	/// </summary>
	internal class EntityTypeNameComparer : IComparer
	{
		internal EntityTypeNameComparer()
		{
		}

		public int Compare(object x, object y)
		{
			EntityType a = (EntityType)x;
			EntityType b = (EntityType)y;

			// compare...
			return string.Compare(a.Name, b.Name, true, Cultures.User);
		}
	}
}
