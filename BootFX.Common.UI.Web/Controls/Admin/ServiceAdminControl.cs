// BootFX - Application framework for .NET applications
// 
// File: ServiceAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ServiceProcess;
using System.ComponentModel;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	public class ServiceAdminControl : AdminControl
	{		
		/// <summary>
		/// Private field to support <see cref="StartButton"/> property.
		/// </summary>
		private Button _startButton;

		/// <summary>
		/// Private field to support <see cref="StopButton"/> property.
		/// </summary>
		private Button _stopButton;

		/// <summary>
		/// Private field to support <c>Controller</c> property.
		/// </summary>
		private ServiceController _controller;
		
		public ServiceAdminControl()
		{
			this.SaveButtonVisible = false;
		}

		/// <summary>
		/// Gets the controller.
		/// </summary>
		private ServiceController Controller
		{
			get
			{
				// returns the value...
				if(_controller == null)
				{				
					// check...
					if(MachineName == null)
						throw new InvalidOperationException("'MachineName' is null.");
					if(MachineName.Length == 0)
						throw new InvalidOperationException("'MachineName' is zero-length.");
					if(ServiceName == null)
						throw new InvalidOperationException("'ServiceName' is null.");
					if(ServiceName.Length == 0)
						throw new InvalidOperationException("'ServiceName' is zero-length.");

					// create...
					_controller = new ServiceController(this.ServiceName, this.MachineName);
				}
				return _controller;
			}
		}

		/// <summary>
		/// Updates the view.
		/// </summary>
		protected override void BuildViewBody()
		{
			// check...
			if(Controller == null)
				throw new InvalidOperationException("Controller is null.");

			// header...
			this.Rows.Add(this.CreateHeadingRow(string.Format("Service Control - {0}", this.FullServiceName)));

			// row...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "bodytext";

			// installed?
			ServiceControllerStatus status = ServiceControllerStatus.Stopped;
			try
			{
				this.Controller.Refresh();
				status = this.Controller.Status;
			}
			catch(Exception ex)
			{
				this.ReportException("Could not retrieve service status.", ex);
				return;
			}

			// can...
			bool canStop = this.Controller.CanStop;
			bool canStart = true;

			// show...
			StringBuilder builder = new StringBuilder();
			builder.Append("The service is: <b>");
			this.StopButton.Enabled = false;
			this.StartButton.Enabled = false;
			switch(status)
			{
				case ServiceControllerStatus.Running:
					builder.Append("Running");
					this.StopButton.Enabled = canStop;
					break;
				case ServiceControllerStatus.Stopped:
					builder.Append("Stopped");
					this.StartButton.Enabled = canStart;
					break;
				case ServiceControllerStatus.Paused:
					builder.Append("Paused");
					break;
				case ServiceControllerStatus.StartPending:
					builder.Append("Starting...");
					break;
				case ServiceControllerStatus.StopPending:
					builder.Append("Stopping...");
					break;
				case ServiceControllerStatus.PausePending:
					builder.Append("Pausing...");
					break;
				case ServiceControllerStatus.ContinuePending:
					builder.Append("Continuing from pause...");
					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", status, status.GetType()));
			}
			builder.Append("</b>");
			cell.Text = builder.ToString();

			// spacer...
			this.Rows.Add(TableHelper.CreateSpacerRow());

			// buttons...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);

			// table...
			Table table = TableHelper.CreateControlTable(this.StopButton, this.StartButton, this.RefreshButton);
			cell.Controls.Add(table);
		}

		private string FullServiceName
		{
			get
			{
				return string.Format("{0}\\{1}", this.MachineName, this.ServiceName);
			}
		}

		/// <summary>
		/// Gets or sets the machine name.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>MachineName</c> for storage.
		/// </remarks>
		[Category("Behavior"), Browsable(true), Description("Gets or sets the machine name.")]
		public string MachineName
		{
			get
			{
				object value = ViewState["MachineName"];
				if(value == null)
					return Environment.MachineName;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != MachineName)
				{
					// set the value...
					ViewState["MachineName"] = value;
					this.RefreshView();
				}
			}
		}

		/// <summary>
		/// Gets or sets the service name.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>ServiceName</c> for storage.
		/// </remarks>
		[Category("Behavior"), Browsable(true), Description("Gets or sets the service name.")]
		public string ServiceName
		{
			get
			{
				object value = ViewState["ServiceName"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != ServiceName)
				{
					// set the value...
					ViewState["ServiceName"] = value;
					this.RefreshView();
				}
			}
		}

		/// <summary>
		/// Gets the stopbutton.
		/// </summary>
		private Button StopButton
		{
			get
			{
				if(_stopButton == null)
				{
					_stopButton = new Button();
					_stopButton.CssClass = "field";
					_stopButton.Text = "Stop Service";
					_stopButton.Click += new EventHandler(_stopButton_Click);
				}
				return _stopButton;
			}
		}
		
		/// <summary>
		/// Gets the startbutton.
		/// </summary>
		private Button StartButton
		{
			get
			{
				if(_startButton == null)
				{
					_startButton = new Button();
					_startButton.CssClass = "field";
					_startButton.Text = "Start Service";
					_startButton.Click += new EventHandler(_startButton_Click);
				}
				return _startButton;
			}
		}

		private void _stopButton_Click(object sender, EventArgs e)
		{
			if(Controller == null)
				throw new InvalidOperationException("Controller is null.");
			this.ControlService(ServiceControllerStatus.Stopped);
		}

		private void _startButton_Click(object sender, EventArgs e)
		{
			if(Controller == null)
				throw new InvalidOperationException("Controller is null.");
			this.ControlService(ServiceControllerStatus.Running);
		}

		private void ControlService(ServiceControllerStatus changeTo)
		{
			try
			{
				if(Controller == null)
					throw new InvalidOperationException("Controller is null.");

				try
				{
					switch(changeTo)
					{
						case ServiceControllerStatus.Running:
							this.Controller.Start();
							break;

						case ServiceControllerStatus.Stopped:
							this.Controller.Stop();
							break;

						default:
							throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", changeTo, changeTo.GetType()));
					}

					// wait...
					this.Controller.WaitForStatus(changeTo, new TimeSpan(0,0,30));
				}
				catch(System.ServiceProcess.TimeoutException ex)
				{
					// mbr - 28-09-2007 - log...
					string message = string.Format("The '{0}' service failed to change to '{1}' in the time provided.  Use the 'Refresh' button to view the current service status.", 
						this.FullServiceName, changeTo);
					if(this.Log.IsWarnEnabled)
						this.Log.Warn(message, ex);

					// get...
					this.FeedbackWidget.SetError(message);
				}

				// update...
				this.RefreshView();
			}
			catch(Exception ex)
			{
				this.ReportException("Failed to issue 'Start' command.", ex);
			}
		}

		protected override void OnRefresh(EventArgs e)
		{
			base.OnRefresh (e);
			this.RefreshView();
		}
	}
}
