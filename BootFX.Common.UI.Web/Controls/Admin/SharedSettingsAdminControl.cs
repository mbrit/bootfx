// BootFX - Application framework for .NET applications
// 
// File: SharedSettingsAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>SharedSettingsAdminControl</c>.
	/// </summary>
	public class SharedSettingsAdminControl : AdminControl
	{
		/// <summary>
		/// Private field to support <c>ValuesGrid</c> property.
		/// </summary>
		private DataGrid _valuesGrid;
		
		/// <summary>
		/// Private field to support <c>AddInfoLabel</c> property.
		/// </summary>
		private Label _addInfoLabel;
		
		/// <summary>
		/// Private field to support <c>AddProblemLabel</c> property.
		/// </summary>
		private Label _addProblemLabel;
		
		/// <summary>
		/// Private field to support <c>AddButton</c> property.
		/// </summary>
		private Button _addButton;
		
		/// <summary>
		/// Private field to support <c>NewName</c> property.
		/// </summary>
		private TextBox _newName;

		/// <summary>
		/// Private field to support <see cref="NewValue"/> property.
		/// </summary>
		private TextBox _newValue;

//		/// <summary>
//		/// Private field to support <see cref="NewType"/> property.
//		/// </summary>
//		private DropDownList _newType;
		
		public SharedSettingsAdminControl()
		{
			this.SaveButtonVisible = false;
		}

		protected override void OnFirstLoad(EventArgs e)
		{
			base.OnFirstLoad (e);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// reset...
			this.AddProblemLabel.Text = string.Empty;

//			// set...
//			FormHelper.PopulateListFromEnumeration(this.NewType, typeof(ConfigItemValueType));

			// mbr - 02-03-2006 - make sure we have a table...
			if(!(this.Page.IsPostBack))
				ConfigItem.EnsureConfigTableExists();

			// update...
			this.RefreshGrid();
		}

		private void RefreshGrid()
		{
			// grid...
			this.ValuesGrid.DataSource = Runtime.Current.SharedSettings.ToDataTable();
			this.ValuesGrid.DataBind();
		}

		protected override void BuildViewBody()
		{
			TableRow row = null;
			TableCell cell = null;

			this.Rows.Add(this.CreateHeadingRow("Existing Values"));

			// create a place to add new settings...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);

			// table...
			Table table = new Table();
			cell.Controls.Add(table);

			// grid...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.ValuesGrid);

			// add...
			this.Rows.Add(TableHelper.CreateSpacerRow());
			this.Rows.Add(this.CreateHeadingRow("Add/Edit Value"));

			// create a place to add new settings...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);

			// table...
			table = new Table();
			cell.Controls.Add(table);

			// name/value...
			table.Rows.Add(TableHelper.CreateControlRow("Name", this.NewName));
//			table.Rows.Add(TableHelper.CreateControlRow("Type", this.NewType));
			table.Rows.Add(TableHelper.CreateControlRow("Value", this.NewValue));
			table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.AddButton));
		}

//		/// <summary>
//		/// Gets the newtype.
//		/// </summary>
//		private DropDownList NewType
//		{
//			get
//			{
//				if(_newType == null)
//					_newType = this.CreateDropDownList("listNewType");
//				return _newType;
//			}
//		}
		
		/// <summary>
		/// Gets the newvalue.
		/// </summary>
		private TextBox NewValue
		{
			get
			{
				if(_newValue == null)
					_newValue = this.CreateTextBox("textNewValue", new Unit(400, UnitType.Pixel));
				return _newValue;
			}
		}
		
		/// <summary>
		/// Gets the newname.
		/// </summary>
		private TextBox NewName
		{
			get
			{
				// returns the value...
				if(_newName == null)
					_newName = this.CreateTextBox("textNewName", new Unit(400, UnitType.Pixel));
				return _newName;
			}
		}

		/// <summary>
		/// Gets the addbutton.
		/// </summary>
		private Button AddButton
		{
			get
			{
				// returns the value...
				if(_addButton== null)
				{
					_addButton = this.CreateButton("buttonAdd", "Save Changes");
					_addButton.Click += new EventHandler(_addButton_Click);
				}
				return _addButton;
			}
		}

		/// <summary>
		/// Gets the addproblemlabel.
		/// </summary>
		private Label AddProblemLabel
		{
			get
			{
				// returns the value...
				if(_addProblemLabel == null)
					_addProblemLabel = this.CreateProblemLabel("labelAddProblem");
				return _addProblemLabel;
			}
		}

		/// <summary>
		/// Gets the addinfolabel.
		/// </summary>
		private Label AddInfoLabel
		{
			get
			{
				// returns the value...
				if(_addInfoLabel == null)
					_addInfoLabel = this.CreateInfoLabel("labelAddInfo");
				return _addInfoLabel;
			}
		}

		private void _addButton_Click(object sender, EventArgs e)
		{
			FormChecker checker = new FormChecker(this);
			string name = checker.GetStringValue(this.NewName, true);
//			ConfigItemValueType type = (ConfigItemValueType)checker.GetEnumerationValue(this.NewType, typeof(ConfigItemValueType), true);
			string value = checker.GetStringValue(this.NewValue, true);

			// problems?
			if(checker.HasErrors)
			{
				this.AddProblemLabel.Text = checker.GetAllErrorsSeparatedByBrTag();
				return;
			}

			// create...
			Runtime.Current.SharedSettings.SetValue(name, value);

			// update...
			this.RefreshGrid();

			// reset...
			this.NewName.Text = string.Empty;
			this.NewValue.Text = string.Empty;
		}

		/// <summary>
		/// Gets the valuesgrid.
		/// </summary>
		private DataGrid ValuesGrid
		{
			get
			{
				// returns the value...
				if(_valuesGrid == null)
				{
					_valuesGrid = new DataGrid();
					_valuesGrid.AutoGenerateColumns = false;
					
					// columns...
					_valuesGrid.Columns.Add(this.GetBoundColumn("Name", "Name", Unit.Empty));
					_valuesGrid.Columns.Add(this.GetBoundColumn("Data", "Value", Unit.Empty));

					// template...
					TemplateColumn column = new TemplateColumn();
					column.ItemTemplate = new ItemTemplate(this);
					_valuesGrid.Columns.Add(column);

					// sub...
					_valuesGrid.ItemCreated += new DataGridItemEventHandler(_valuesGrid_ItemCreated);
					_valuesGrid.DeleteCommand += new DataGridCommandEventHandler(_valuesGrid_DeleteCommand);
					_valuesGrid.EditCommand += new DataGridCommandEventHandler(_valuesGrid_EditCommand);
				}
				return _valuesGrid;
			}
		}

		private class ItemTemplate : ITemplate
		{
			/// <summary>
			/// Private field to support <see cref="Main"/> property.
			/// </summary>
			private SharedSettingsAdminControl _main;
			
			internal ItemTemplate(SharedSettingsAdminControl main)
			{
				if(main == null)
					throw new ArgumentNullException("main");
				_main = main;
			}

			/// <summary>
			/// Gets the main.
			/// </summary>
			private SharedSettingsAdminControl Main
			{
				get
				{
					return _main;
				}
			}

			public void InstantiateIn(Control container)
			{
				if(container == null)
					throw new ArgumentNullException("container");

				// edit...
				LinkButton edit = this.Main.CreateLinkButton("linkEdit", "Edit");
				edit.CommandName = "edit";
				container.Controls.Add(edit);

				// label...
				Label sep = new Label();
				sep.Text = "&nbsp;|&nbsp;";
				container.Controls.Add(sep);
				
				// delete...
				LinkButton delete = this.Main.CreateLinkButton("linkDelete", "Delete");
				delete.CommandName = "delete";
				container.Controls.Add(delete);
			}
		}

		private void _valuesGrid_ItemCreated(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.DataItem == null)
				return;

			// get...
			DataRowView row = (DataRowView)e.Item.DataItem;
			this.SetConfigItem(e.Item, row, "linkDelete");
			this.SetConfigItem(e.Item, row, "linkEdit");
		}

		private void SetConfigItem(DataGridItem item, DataRowView row, string id)
		{
			if(item == null)
				throw new ArgumentNullException("item");
			if(row == null)
				throw new ArgumentNullException("row");
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");

			// get...
			LinkButton delete = (LinkButton)FormHelper.GetControl(item, id, typeof(LinkButton));
			if(delete == null)
				throw new InvalidOperationException("delete is null.");
			delete.CommandArgument = row["configid"].ToString();
		}

		private void _valuesGrid_DeleteCommand(object source, DataGridCommandEventArgs e)
		{
			// get...
			ConfigItem item = ConfigItem.GetById(ConversionHelper.ToInt32(e.CommandArgument, Cultures.System));
			if(item != null)
			{
				item.MarkForDeletion();
				item.SaveChanges();

				// update...
				this.RefreshGrid();
			}
		}

		private void _valuesGrid_EditCommand(object source, DataGridCommandEventArgs e)
		{
			// get...
			ConfigItem item = ConfigItem.GetById(ConversionHelper.ToInt32(e.CommandArgument, Cultures.System));
			if(item != null)
			{
				// set...
				this.NewName.Text = item.Name;
				this.NewValue.Text = item.Data;
			}
		}
	}
}
