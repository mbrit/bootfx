// BootFX - Application framework for .NET applications
// 
// File: FooterAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.ServiceProcess;
using System.ComponentModel;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	public class FooterAdminControl : AdminControl
	{		
		public FooterAdminControl()
		{
		}

		protected override void RefreshView()
		{
			Table table = new Table();
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(table);
			table.Style["border"] = "1px solid black";

			// details...
			DateTime now = DateTime.UtcNow;
			table.Rows.Add(TableHelper.CreateNameValueRow("Application:", Runtime.Current.Application.ToString()));
			
			// mbr - 05-09-2007 - more...
			if(Runtime.Current.InstallationName != null && Runtime.Current.InstallationName.Length > 0)
				table.Rows.Add(TableHelper.CreateNameValueRow("Installation:", Runtime.Current.InstallationName));

			// stuff...
			table.Rows.Add(TableHelper.CreateNameValueRow("CLR version:", "v" + Environment.Version.ToString()));
			table.Rows.Add(TableHelper.CreateNameValueRow("Local date/time:", string.Format("{0} {1} UTC", now.ToLongDateString(), now.ToLongTimeString())));
			table.Rows.Add(TableHelper.CreateNameValueRow("Machine name:", Environment.MachineName));
			table.Rows.Add(TableHelper.CreateNameValueRow("Operating system:", string.Format("{0} v{1}", Environment.OSVersion.Platform, Environment.OSVersion.Version)));
			table.Rows.Add(TableHelper.CreateNameValueRow("Authenticated to Windows as:", Thread.CurrentPrincipal.Identity.Name));
			table.Rows.Add(TableHelper.CreateSpacerRow());
			table.Rows.Add(TableHelper.CreateNameValueRow("Settings configuration file:", Runtime.Current.InstallationSettingsFilePath));
			table.Rows.Add(TableHelper.CreateNameValueRow(string.Empty, "Use the 'BfxInstallationSettingsFilePath' appSettings key to override the default installation file location.<br>Tilde paths are acceptable, e.g. '~/foo/myapp.dat'."));

			// update...
			foreach(TableRow r in table.Rows)
			{
				foreach(TableCell c in r.Cells)
					c.CssClass = "smallbodytext";
			}
		}
	}
}
