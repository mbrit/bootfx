// BootFX - Application framework for .NET applications
// 
// File: DatabaseAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.ComponentModel;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	[DefaultEvent("RunDatabaseUpdate")]
	public class DatabaseAdminControl : AdminControl
	{
		/// <summary>
		/// Private field to support <c>Builder</c> property.
		/// </summary>
		private ConnectionStringBuilder _builder;
		
		/// <summary>
		/// Raised when the database update button is clicked.
		/// </summary>
		public event EventHandler RunDatabaseUpdate;
		
		/// <summary>
		/// Private field to support <c>DatabaseUpdateButton</c> property.
		/// </summary>
		private Button _databaseUpdateButton;
		
		/// <summary>
		/// Private field to support <c>TestButton</c> property.
		/// </summary>
		private Button _testButton;
		
		/// <summary>
		/// Private field to support <c>ConnectionStringTextBox</c> property.
		/// </summary>
		private TextBox _connectionStringTextBox;
		
		/// <summary>
		/// Private field to support <c>TypeList</c> property.
		/// </summary>
		private DropDownList _typeList;
		
		public DatabaseAdminControl()
		{
			if(Runtime.ConfigurationMode == RuntimeConfigMode.AppConfig)
				this.SaveButtonVisible = false;
		}

		protected override void BuildViewBody()
		{
			this.Rows.Add(this.CreateHeadingRow("Database Configuration"));

			// form...
			Table table = new Table();
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(table);

			// mbr - 18-03-2007 - config?
			switch(Runtime.ConfigurationMode)
			{
				case RuntimeConfigMode.SharedFile:

					// pair...
					table.Rows.Add(TableHelper.CreateControlRow("Connection type:", this.TypeList));

					// what do we have?
					if(this.Builder != null)
					{
						// spacer...
						table.Rows.Add(TableHelper.CreateControlRow(string.Empty, this.Builder));
						table.Rows.Add(TableHelper.CreateSpacerRow());
					}

					// connection string...
					table.Rows.Add(TableHelper.CreateControlRow("Connection string:", this.ConnectionStringTextBox));

					// stop...
					break;

				case RuntimeConfigMode.AppConfig:

					// cell...
					row = new TableRow();
					table.Rows.Add(row);
					cell = new TableCell();
					row.Cells.Add(cell);
					cell.Text = "This application uses application configuration files to specify database connections.";

					// add...
					row = new TableRow();
					table.Rows.Add(row);
					cell = new TableCell();
					row.Cells.Add(cell);
					if(Database.HasDefaultDatabaseSettings())
						cell.Text = Database.DefaultConnectionString;
					else
						cell.Text = "(Connection settings are not defined)";

					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Runtime.ConfigurationMode, Runtime.ConfigurationMode.GetType()));
			}


			// test....
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(TableHelper.CreateControlTable(this.TestButton, this.DatabaseUpdateButton));
		}

		/// <summary>
		/// Gets the testbutton.
		/// </summary>
		private Button TestButton
		{
			get
			{
				// returns the value...
				if(_testButton == null)
				{
					_testButton = new Button();
					_testButton.Text = "Test Connection";
					_testButton.CssClass = "field";
					_testButton.Click += new EventHandler(_testButton_Click);
				}
				return _testButton;
			}
		}

		/// <summary>
		/// Gets the typelist.
		/// </summary>
		private DropDownList TypeList
		{
			get
			{
				// returns the value...
				if(_typeList == null)
				{
					_typeList = new DropDownList();
					_typeList.CssClass = "field";
					_typeList.AutoPostBack = true;
					_typeList.SelectedIndexChanged += new EventHandler(_typeList_SelectedIndexChanged);

					// add...
					_typeList.Items.Add(new ListItem("SQL Server", this.FormatTypeName(typeof(SqlServerConnection))));
				}
				return _typeList;
			}
		}

		private string FormatTypeName(Type type)
		{
			if(type == null)
				throw new ArgumentNullException("type");
			
			return string.Format("{0}, {1}", type.FullName, type.Assembly.GetName().Name);
		}

		protected override void OnFirstLoad(EventArgs e)
		{
			base.OnFirstLoad (e);

			// list...
			if(TypeList == null)
				throw new InvalidOperationException("TypeList is null.");
			if(ConnectionStringTextBox == null)
				throw new InvalidOperationException("ConnectionStringTextBox is null.");
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// select the type...
			if(this.Settings.ConnectionType != null && !(FormHelper.SelectListItemByValue(this.TypeList, this.FormatTypeName(this.Settings.ConnectionType))))
			{
				// add...
				ListItem item = new ListItem(this.Settings.ConnectionType.FullName, this.FormatTypeName(this.Settings.ConnectionType));
				item.Selected = true;
				this.TypeList.Items.Add(item);
			}

			// set the string...
			this.ConnectionStringTextBox.Text = this.Settings.ConnectionString;

			// do we have one?
			if(Database.HasDefaultDatabaseSettings())
				this.FeedbackWidget.SetInfo("Database settings have been configured already for this application.  Changes will require a restart.  Other associated services may also require restarting when changes are made.");
		}

		/// <summary>
		/// Gets the connectionstringtextbox.
		/// </summary>
		private TextBox ConnectionStringTextBox
		{
			get
			{
				// returns the value...
				if(_connectionStringTextBox == null)
				{
					_connectionStringTextBox = new TextBox();
					_connectionStringTextBox.CssClass = "field";
					_connectionStringTextBox.TextMode = TextBoxMode.MultiLine;
					_connectionStringTextBox.Rows = 4;
					_connectionStringTextBox.Columns = 60;
				}
				return _connectionStringTextBox;
			}
		}

		private Type SelectedConnectionType
		{
			get
			{
				if(this.TypeList.SelectedItem == null)
					return null;
				else
				{
					Type type = Type.GetType(this.TypeList.SelectedItem.Value, true, true);
					if(type == null)
						throw new InvalidOperationException("type is null.");
					return type;
				}
			}
		}

		private bool CheckForm(ref Type type, ref string connectionString)
		{
			type = null;
			connectionString = null;

			// type...
			if(this.TypeList.SelectedItem == null)
			{
				this.FeedbackWidget.SetError("You must selected a connection type.");
				return false;
			}
			type = Type.GetType(this.TypeList.SelectedItem.Value, true, true);

			// string...
			connectionString = this.ConnectionStringTextBox.Text.Trim();
			if(connectionString.Length == 0)
			{
				this.FeedbackWidget.SetError("You must enter a connection string.");
				return false;
			}

			// ok...
			return true;
		}

		private void _testButton_Click(object sender, EventArgs e)
		{
			Type type = null;
			string connectionString = null;
			this.CheckConnection(ref type, ref connectionString);
		}

		private bool CheckConnection(ref Type type, ref string connectionString)
		{
			switch(Runtime.ConfigurationMode)
			{
				case RuntimeConfigMode.SharedFile:
					if(!(this.CheckForm(ref type, ref connectionString)))
						return false;
					else
						break;

				case RuntimeConfigMode.AppConfig:
					type = typeof(SqlServerConnection);
					connectionString = Database.DefaultConnectionString;
					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", Runtime.ConfigurationMode, Runtime.ConfigurationMode.GetType()));
			}


			try
			{
				ConnectionSettings settings = new ConnectionSettings(null, type, connectionString);
				Connection.TestConnection(settings);

				// set...
				this.FeedbackWidget.SetInfo("Connection OK.");

				// ok...
				return true;
			}
			catch(Exception ex)
			{
				this.ReportException("The connection test failed.", ex);
				return false;
			}
		}

		protected override bool Apply()
		{
			// can we?
			Type type = null;
			string connectionString = null;
			if(!(this.CheckConnection(ref type, ref connectionString)))
				return false;

			// save...
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");
			this.Settings.ConnectionType = type;
			this.Settings.ConnectionString = connectionString;

			// save...
			this.Settings.Save();

			// show...
			if(Database.HasDefaultDatabaseSettings())
				this.FeedbackWidget.SetInfo("Database settings have been saved.  Changes will not take effect until the application and any associated services are restarted.");
			else
			{
				Database.SetDefaultDatabaseFromInstallationSettings();
				this.FeedbackWidget.SetInfo("Database settings have been saved for the first time for this application.  Changes are effective now.  Other services may require a restart.");
			}

			// ok...
			return true;
		}

		/// <summary>
		/// Gets the databaseupdatebutton.
		/// </summary>
		private Button DatabaseUpdateButton
		{
			get
			{
				// returns the value...
				if(_databaseUpdateButton == null)
				{
					_databaseUpdateButton = new Button();
					_databaseUpdateButton.CssClass = "field";
					_databaseUpdateButton.Text = "Database Update";
					_databaseUpdateButton.Click += new EventHandler(_databaseUpdateButton_Click);
				}
				return _databaseUpdateButton;	
			}
		}

		private void _databaseUpdateButton_Click(object sender, EventArgs e)
		{
			this.OnRunDatabaseUpdate();
		}

		/// <summary>
		/// Raises the <c>RunDatabaseUpdate</c> event.
		/// </summary>
		private void OnRunDatabaseUpdate()
		{
			OnRunDatabaseUpdate(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>RunDatabaseUpdate</c> event.
		/// </summary>
		protected virtual void OnRunDatabaseUpdate(EventArgs e)
		{
			// raise...
			if(RunDatabaseUpdate != null)
				RunDatabaseUpdate(this, e);
			else
				this.FeedbackWidget.SetError("Database Update is not configured.");
		}

		/// <summary>
		/// Gets or sets the databasename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>DatabaseName</c> for storage.
		/// </remarks>
		[Category("Behavior"), Browsable(true), Description("Gets or sets the database name.")]
		public string DatabaseName
		{
			get
			{
				object value = ViewState["DatabaseName"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != DatabaseName)
				{
					// set the value...
					ViewState["DatabaseName"] = value;
					this.RefreshView();
				}
			}
		}

		private bool IsDefaultDatabase
		{
			get
			{
				if(this.DatabaseName == null || this.DatabaseName.Length == 0)
					return true;
				else
					return false;
			}
		}

		private void _typeList_SelectedIndexChanged(object sender, EventArgs e)
		{
			// rebuild...
			_builder = null;
			this.RefreshView();
		}

		/// <summary>
		/// Gets the builder.
		/// </summary>
		private ConnectionStringBuilder Builder
		{
			get
			{
				// returns the value...
				if(_builder == null)
				{
					Type type = this.SelectedConnectionType;
					if(typeof(SqlServerConnection).IsAssignableFrom(type))
						_builder = new SqlServerConnectionStringBuilder();

					// check...
					if(_builder != null)
						_builder.StringBuilt += new StringEventHandler(_builder_StringBuilt);
				}
				return _builder;
			}
		}

		private void _builder_StringBuilt(object sender, StringEventArgs e)
		{
			this.ConnectionStringTextBox.Text = e.Value;
		}
	}
}
