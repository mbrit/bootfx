// BootFX - Application framework for .NET applications
// 
// File: DatabaseUpdateAdminControl.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using System.Collections;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatabaseConnectionAdminControl</c>.
	/// </summary>
	public class DatabaseUpdateAdminControl : AdminControl
	{
		/// <summary>
		/// Private field to support <c>CancelButton</c> property.
		/// </summary>
		private Button _cancelButton;
		
		/// <summary>
		/// Private field to support <c>CheckStatusButton</c> property.
		/// </summary>
		private Button _checkStatusButton;

		/// <summary>
		/// Private field to support <c>RunButton</c> property.
		/// </summary>
		private Button _runButton;
		
		public DatabaseUpdateAdminControl()
		{
			this.SaveButtonVisible = false;
		}

		protected override void BuildViewBody()
		{
			this.Rows.Add(this.CreateHeadingRow("Database Update"));

			// description...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "smallbodytext";
			cell.Text = "This utility will update your database to be in-line with that required for this application.";

			// spacer...
			this.Rows.Add(TableHelper.CreateSpacerRow());

			// table...
			Table table = new Table();
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(table);

			// use the type and settings from the appdomain...
			if(!(Database.HasDefaultDatabaseSettings()))
			{
				Database.SetDefaultDatabase(this.Settings.ConnectionType, this.Settings.ConnectionString);
				if(!(Database.HasDefaultDatabaseSettings()))
					throw new InvalidOperationException("Connection settings have not been defined.");
			}
			table.Rows.Add(TableHelper.CreateNameValueRow("Connection type:", Database.DefaultConnectionType.FullName));
			table.Rows.Add(TableHelper.CreateNameValueRow("Connection string:", Database.DefaultConnectionString));

			// spacer...
			table.Rows.Add(TableHelper.CreateSpacerRow());

			// status...
			table.Rows.Add(TableHelper.CreateNameValueRow("Current activity:", WebDatabaseUpdate.Current.State.ToString()));
			string message = null;
			if(WebDatabaseUpdate.Current.LastUpdateTime != DateTime.MinValue)
			{
				message = string.Format("{0} - {1}", WebDatabaseUpdate.Current.LastUpdateTime.ToLocalTime().ToLongTimeString(), 
					WebDatabaseUpdate.Current.Status);
			}
			else
				message = "(None)";
			table.Rows.Add(TableHelper.CreateNameValueRow("Last message:", message));

			// progress...
			IOperationItem item = WebDatabaseUpdate.Current;
			if(item.ProgressMaximum != 0 && WebDatabaseUpdate.Current.State != WebDatabaseUpdateState.Idle)
			{
				// create...
				table.Rows.Add(TableHelper.CreateSpacerRow());

				// percentage...
				int percentage = 0;
				if(item.ProgressMaximum - item.ProgressMinimum != 0)
					percentage = (int)((double)item.ProgressValue / (double)(item.ProgressMaximum - item.ProgressMinimum) * 20.0);

				// progress...
				Table progress = new Table();
				progress.Style["border"] = "1px solid black";
				progress.Width = new Unit(275, UnitType.Pixel);
				progress.Height = new Unit(18, UnitType.Pixel);
				row = new TableRow();
				progress.Rows.Add(row);
				for(int index = 0; index < 20; index++)
				{
					cell = new TableCell();
					row.Cells.Add(cell);
					cell.Width = new Unit(5, UnitType.Percentage);
					if(index <= percentage)
						cell.BackColor = Color.Blue;
				}

//				// percentage...
//				cell = new TableCell();
//				row.Cells.Add(cell);
//				cell.CssClass = "bodytext";	
//				const string format = "&nbsp;&nbsp;&nbsp;&nbsp;{0}%";
//				if(item.ProgressMaximum - item.ProgressMinimum != 0)
//					cell.Text = string.Format(format, (int)((double)item.ProgressValue / (double)(item.ProgressMaximum - item.ProgressMinimum) * 100));
//				else
//					cell.Text = string.Format(format, 0);

				// add...
				row = new TableRow();
				table.Rows.Add(row);
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.CssClass = "bodytext";
				cell.Text = "Progress:&nbsp;";
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.Controls.Add(progress);
			}

			// results...
			if(WebDatabaseUpdate.Current.CheckResults != null)
			{
				// get the messages...
				string[] messages = WebDatabaseUpdate.Current.CheckResults.GetFriendlyUnitWorkMessages();
				if(messages == null)
					throw new InvalidOperationException("messages is null.");

				// anything...
				if(messages.Length > 0)
				{
					this.Rows.Add(TableHelper.CreateSpacerRow());

						// row...
						foreach(string buf in messages)
						{
							row = new TableRow();
							this.Rows.Add(row);
							cell = new TableCell();
							row.Cells.Add(cell);
							cell.CssClass = "smallbodytext";
							cell.Text = buf;
						}
				}
			}

			// error?
			if(WebDatabaseUpdate.Current.LastError != null)
			{
				this.Rows.Add(TableHelper.CreateSpacerRow());

				// show...
				row = new TableRow();
				this.Rows.Add(row);
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.CssClass = "smallbodytext";
				cell.Text = HttpUtility.HtmlEncode(WebDatabaseUpdate.Current.LastError.ToString()).Replace("\n", "<BR />");
			}

			// spacer...
			this.Rows.Add(TableHelper.CreateSpacerRow());

			// state...
			this.CheckStatusButton.Enabled = false;
			this.RunButton.Enabled = false;
			this.CancelButton.Enabled = false;

			// switch...
			switch(WebDatabaseUpdate.Current.State)
			{
				case WebDatabaseUpdateState.Idle:
					this.CheckStatusButton.Enabled = true;
					this.RunButton.Enabled = true;
					break;

				case WebDatabaseUpdateState.CheckingStatus:
				case WebDatabaseUpdateState.Updating:
					this.CancelButton.Enabled = true;
					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", WebDatabaseUpdate.Current.State, WebDatabaseUpdate.Current.State.GetType()));
			}

			// cancel?
			if(WebDatabaseUpdate.Current.IsCancelled && WebDatabaseUpdate.Current.State != WebDatabaseUpdateState.Idle)
			{
				this.CancelButton.Text = "Cancelling...";
				this.CancelButton.Enabled = false;
			}
			else
			{
				this.CancelButton.Text = "Cancel";
				if(WebDatabaseUpdate.Current.State == WebDatabaseUpdateState.Idle)
					this.CancelButton.Enabled = false;
				else
					this.CancelButton.Enabled = true;
			}

			// are we working?
			if(WebDatabaseUpdate.Current.State != WebDatabaseUpdateState.Idle)
			{
    			this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "bfxrefresh", 
				    "<script language=\"javascript\">\r\nfunction doRefresh()\r\n{\r\nwindow.location = window.location;\r\n}\r\nsetTimeout('doRefresh()', 2000);\r\n</script>");
			}

			// add...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(TableHelper.CreateControlTable(this.CheckStatusButton, this.RunButton, this.CancelButton, this.RefreshButton));
		}

		/// <summary>
		/// Gets the runbutton.
		/// </summary>
		private Button RunButton
		{
			get
			{
				// returns the value...
				if(_runButton == null)
				{
					_runButton = new Button();
					_runButton.ID = "buttonRun";
					_runButton.CssClass = "field";
					_runButton.Text = "Run Database Update";
					_runButton.Click += new EventHandler(_runButton_Click);
				}
				return _runButton;
			}
		}
		
		/// <summary>
		/// Gets the checkstatusbutton.
		/// </summary>
		private Button CheckStatusButton
		{
			get
			{
				// returns the value...
				if(_checkStatusButton == null)
				{
					_checkStatusButton = new Button();
					_checkStatusButton.ID = "buttonCheckStatus";
					_checkStatusButton.CssClass = "field";
					_checkStatusButton.Text = "Check Database Status";
					_checkStatusButton.Click += new EventHandler(_checkStatusButton_Click);
				}
				return _checkStatusButton;
			}
		}

		/// <summary>
		/// Gets the cancelbutton.
		/// </summary>
		private Button CancelButton
		{
			get
			{
				// returns the value...
				if(_cancelButton == null)
				{
					_cancelButton = new Button();
					_cancelButton.ID = "buttonCancel";
					_cancelButton.CssClass = "field";
					_cancelButton.Text = "Cancel";
					_cancelButton.Click += new EventHandler(_cancelButton_Click);
				}
				return _cancelButton;
			}
		}

		private void _runButton_Click(object sender, EventArgs e)
		{
			try
			{
				WebDatabaseUpdate.Current.Update();

				// wait a bit, then refresh the view...
				Thread.Sleep(1500);
				this.RefreshView();
			}
			catch(Exception ex)
			{
				this.ReportException("Failed to start status check.", ex);
			}
		}

		private void _checkStatusButton_Click(object sender, EventArgs e)
		{
			try
			{
				WebDatabaseUpdate.Current.CheckStatus();

				// wait a bit, then refresh the view...
				Thread.Sleep(1500);
				this.RefreshView();
			}
			catch(Exception ex)
			{
				this.ReportException("Failed to start status check.", ex);
			}
		}

		private void _cancelButton_Click(object sender, EventArgs e)
		{
			try
			{
				WebDatabaseUpdate.Current.Cancel();

				// wait a bit, then refresh the view...
				Thread.Sleep(2500);
				this.RefreshView();
			}
			catch(Exception ex)
			{
				this.ReportException("Failed to cancel.", ex);
			}
		}

		protected override void OnRefresh(EventArgs e)
		{
			base.OnRefresh(e);
			this.RefreshView();
		}
	}
}
