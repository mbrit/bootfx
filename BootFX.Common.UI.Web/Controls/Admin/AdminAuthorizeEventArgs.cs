// BootFX - Application framework for .NET applications
// 
// File: AdminAuthorizeEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	public delegate void AdminAuthorizeEventHandler(object sender, AdminAuthorizeEventArgs e);

	/// <summary>
	/// Summary description for AdminAuthorizeEventArgs.
	/// </summary>
	public class AdminAuthorizeEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <c>IsAuthorized</c> property.
		/// </summary>
		private bool _isAuthorized;
		
		public AdminAuthorizeEventArgs()
		{
		}

		/// <summary>
		/// Gets or sets the isauthorized
		/// </summary>
		public bool IsAuthorized
		{
			get
			{
				return _isAuthorized;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _isAuthorized)
				{
					// set the value...
					_isAuthorized = value;
				}
			}
		}
	}
}
