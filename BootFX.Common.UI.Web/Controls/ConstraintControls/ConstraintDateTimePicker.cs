// BootFX - Application framework for .NET applications
// 
// File: ConstraintDateTimePicker.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common;
using BootFX.Common.UI.Web;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;
using BootFX.Common.UI;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintDateTimePicker.
	/// </summary>
	public class ConstraintDateTimePicker : LoggableTable, IConstraintControl, INamingContainer
	{
		/// <summary>
		/// Private field to support <c>FromTextBox</c> property.
		/// </summary>
		private TextBox _fromTextBox;

		/// <summary>
		/// Private field to support <c>ToTextBox</c> property.
		/// </summary>
		private TextBox _toTextBox;
		
		public ConstraintDateTimePicker()
		{
			this.RebuildView();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);
		}

		private void RebuildView()
		{
			this.Rows.Clear();

			// add...
			TableRow row = new TableRow();
			this.Rows.Add(row);

			// cell...
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.FromTextBox);

			// next...
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.CssClass = "bodytext";
			cell.Text = "&nbsp;<i>to</i>&nbsp";

			// to...
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.ToTextBox);
		}

		/// <summary>
		/// Gets the totextbox.
		/// </summary>
		private TextBox ToTextBox
		{
			get
			{
				// returns the value...
				if(_toTextBox == null)
					_toTextBox = this.GetTextBox("textTo");
				return _toTextBox;
			}
		}

		private TextBox GetTextBox(string id)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			
			// set...
			TextBox textBox = new TextBox();
			textBox.ID = id;
			textBox.CssClass = "textbox";
			textBox.MaxLength = 20;
			textBox.Width = new Unit(75, UnitType.Pixel);

			// return...
			return textBox;
		}
		
		/// <summary>
		/// Gets the fromtextbox.
		/// </summary>
		private TextBox FromTextBox
		{
			get
			{
				// returns the value...
				if(_fromTextBox == null)
					_fromTextBox = this.GetTextBox("textFrom");
				return _fromTextBox;
			}
		}

		/// <summary>
		/// Returns the mangler id for the control, this will be the ID of the control
		/// </summary>
		public string ManglerId
		{
			get
			{
				return ID;		
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				ID = value;
			}		
		}

		/// <summary>
		/// Gets or sets the entitytypename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>EntityTypeName</c> for storage.
		/// </remarks>
		public string EntityTypeId
		{
			get
			{
				object value = ViewState["EntityTypeId"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != EntityTypeId)
				{
					// set the value...
					ViewState["EntityTypeId"] = value;
				}
			}
		}

		/// <summary>
		/// Used to automatically create a filter for the control, this is the field name on the entity
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		public virtual string FieldName
		{
			get
			{
				string fieldName = (string) this.ViewState["FieldName"];
				if (fieldName != null)
				{
					return fieldName;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["FieldName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses")]
		public virtual string ChildTableName
		{
			get
			{
				string childTable = (string) this.ViewState["ChildTableName"];
				if (childTable != null)
				{
					return childTable;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildTableName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId")]
		public virtual string ChildFieldName
		{
			get
			{
				string childField = (string) this.ViewState["ChildFieldName"];
				if (childField != null)
				{
					return childField;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildFieldName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(SqlOperator.EqualTo), PersistenceMode(PersistenceMode.Attribute)]
		[Description("The operator used for the constraint")]
		public virtual SqlOperator Operator
		{
			get
			{
				// mbr - 21-04-2006 - if we have to, then we are always between...
				if(this.IsToSet)
					return SqlOperator.Between;
				else
				{
					if(this.ViewState["Operator"] == null)
						return SqlOperator.EqualTo;

					SqlOperator sqlOperator = (SqlOperator) this.ViewState["Operator"];
					return sqlOperator;
				}
			}
			set
			{
				this.ViewState["Operator"] = value;
			}
		}

		bool IConstraintControl.IsValueSet
		{
			get
			{
				if(this.FromTextBox.Text != null && this.FromTextBox.Text.Trim().Length > 0)
					return true;
				else
					return false;
			}
		}

		private DateTime ParseDate(TextBox textBox)
		{
			if(textBox == null)
				throw new ArgumentNullException("textBox");
			
			// get...
			string text = textBox.Text;
			if(text != null && text.Length > 0)
				return ConversionHelper.ToDateTime(text, Cultures.User);
			else
				return DateTime.MinValue;
		}

		public DateTime From
		{
			get
			{
				return this.ParseDate(this.FromTextBox);
			}
		}

		private bool IsFromSet
		{
			get
			{
				if(this.FromTextBox.Text != null && this.FromTextBox.Text.Length > 0)
					return true;
				else
					return false;
			}
		}

		private bool IsToSet
		{
			get
			{
				if(this.ToTextBox.Text != null && this.ToTextBox.Text.Length > 0)
					return true;
				else
					return false;
			}
		}

		public DateTime To
		{
			get
			{
				DateTime from = this.From;
				if(from == DateTime.MinValue)
					return DateTime.MinValue;
				else
				{
					// do we have a to?
					if(this.IsToSet)
					{
						DateTime to = this.ParseDate(this.ToTextBox);
						return new DateTime(to.Year, to.Month, to.Day, 23, 59, 59, 0);
					}
					else
						return new DateTime(from.Year, from.Month, from.Day, 23, 59, 59, 0);
				}
			}
		}

		public object Value
		{
			get
			{
				if(this.IsFromSet)
				{
					if(this.IsToSet)
						return new DateTime[] {this.From, this.To};
					else
						return new DateTime[] {this.From, DateTime.MinValue};
				}
				else
					return null;
			}
			set
			{
				if(value == null)
				{
					this.FromTextBox.Text = "";
					this.ToTextBox.Text = "";
				}
				else if(value is DateTime)
				{
					// min?
					DateTime date = (DateTime)value;
					if(date == DateTime.MinValue)
						this.FromTextBox.Text = string.Empty;
					else
					{
						// mbr - 11-09-2006 - format...
						string format = this.DateTimeFormat;
						if(format != null && format.Length > 0)
							this.FromTextBox.Text = date.ToString(format, Cultures.User);
						else
						{
							if(date.Hour == 0 && date.Minute == 0 && date.Second == 0)
								this.FromTextBox.Text = date.ToShortDateString();
							else
								this.FromTextBox.Text = date.ToString();
						}
					}
				}
				else if(value is DateTime[])
				{
					DateTime[] dateValues = (DateTime[]) value;
					DateTime from = dateValues[0];
					DateTime to = dateValues[1];
					
					// Do the from text box
					if(from == DateTime.MinValue)
						this.FromTextBox.Text = string.Empty;
					else
					{
						if(from.Hour == 0 && from.Minute == 0 && from.Second == 0)
							this.FromTextBox.Text = from.ToShortDateString();
						else
							this.FromTextBox.Text = from.ToString();
					}
					
					// Do the to text box
					if(to == DateTime.MinValue)
						this.ToTextBox.Text = string.Empty;
					else
					{
						if(to.Hour == 0 && to.Minute == 0 && to.Second == 0)
							this.ToTextBox.Text = to.ToShortDateString();
						else
							this.ToTextBox.Text = to.ToString();
					}
					
				}
				else
				{
					// set...
					throw new NotSupportedException(string.Format("Cannot handle '{0}'.", value.GetType()));
				}
			}
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void DeserializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.DeserializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void SerializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//
//			// defer...
//			ConstraintControlHelper.SerializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Appends the constraint of the filter to the 
//		/// </summary>
//		/// <param name="filter"></param>
//		public virtual  void MangleFilter(SqlFilter filter)
//		{
//			if(filter == null)
//				throw new ArgumentNullException("filter");
//			
//			// defer...
//			ConstraintControlHelper.MangleFilter(this, filter);
//		}

		/// <summary>
		/// Gets or sets whether the control can understand DBNull.  
		/// </summary>
		/// <remarks>If this method returns null, BootFX will convert DBNullvalues into the closest CLR-legal equivalent.</remarks>
		public virtual ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.None;
			}
		}

		public void Initialize(IFlexField field, LayoutHint hint)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			if(hint == null)
				throw new ArgumentNullException("hint");

			// set...
			this.DateTimeFormat = hint.DateTimeFormat;
		}

		/// <summary>
		/// Gets or sets the datetimeformat.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>DateTimeFormat</c> for storage.
		/// </remarks>
		private string DateTimeFormat
		{
			get
			{
				object value = ViewState["DateTimeFormat"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != DateTimeFormat)
				{
					// set the value...
					ViewState["DateTimeFormat"] = value;
				}
			}
		}
	}
}
