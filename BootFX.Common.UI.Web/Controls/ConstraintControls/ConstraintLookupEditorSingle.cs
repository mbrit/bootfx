// BootFX - Application framework for .NET applications
// 
// File: ConstraintLookupEditorSingle.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.UI.WebControls;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ConstraintLookupEditorSingle</c>.
	/// </summary>
	public class ConstraintLookupEditorSingle : ConstraintDropDownList
	{
		public ConstraintLookupEditorSingle()
		{
			this.DataTextField = "description";
			this.DataValueField = "value";
		}

		protected override void OnFieldNameChanged(EventArgs e)
		{
			base.OnFieldNameChanged (e);
			RebindList();
		}

		protected override void OnEntityTypeIdChanged(EventArgs e)
		{
			base.OnEntityTypeIdChanged (e);
			RebindList();
		}

		private void RebindList()
		{
			// current...
			string selected = null;
			if(this.SelectedItem != null)
				selected = this.SelectedItem.Value;

			// set...
			this.Items.Clear();
			this.Items.Add(new ListItem("(None)", "0"));
			if(this.EntityTypeId != null && this.EntityTypeId.Length > 0 && this.FieldName != null && this.FieldName.Length > 0)
			{
				// get...
				EntityType entityType = EntityType.GetEntityTypeForId(this.EntityTypeId, OnNotFound.ThrowException);
				if(entityType == null)
					throw new InvalidOperationException("entityType is null.");

				// get the field...
				EntityField field = entityType.Fields[this.FieldName];
				if(field == null)
					throw new InvalidOperationException("field is null.");

				// lookup...
				if(!(field.IsLookupProperty))
					throw new InvalidOperationException(string.Format("Field '{0}' on '{1}' is not a lookup.", this.FieldName, entityType.Name));

				// get...
				foreach(LookupValue value in LookupValue.GetByName(((LookupExtendedPropertyDataType)field.ExtendedPropertyDefinition.DataType).LookupName))
				{
					ListItem item = new ListItem(value.Description, value.Value.ToString());
					this.Items.Add(item);
				}
			}

			// select...
			if(selected != null)
				FormHelper.SelectListItemByValue(this, selected);
		}
	}
}
