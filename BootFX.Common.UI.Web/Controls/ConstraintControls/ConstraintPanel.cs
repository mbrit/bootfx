// BootFX - Application framework for .NET applications
// 
// File: ConstraintPanel.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintPanel.
	/// </summary>
	public class ConstraintPanel : Panel
	{
		/// <summary>
		/// Private field to support <c>Mode</c> property.
		/// </summary>
		private ExtendedFieldViewMode _mode = ExtendedFieldViewMode.Edit;
		
		/// <summary>
		/// Private field to support <c>DataSource</c> property.
		/// </summary>
		private object _dataSource;
		
		public ConstraintPanel()
		{
		}

		private ConstraintControlHost[] GetHosts()
		{
			ArrayList hosts = new ArrayList();
			this.WalkAndGetHosts(hosts, this);

			// return...
			return (ConstraintControlHost[])hosts.ToArray(typeof(ConstraintControlHost));
		}

		/// <summary>
		/// Gets or sets the <c>EntityTypeName</c> value from view state.
		/// </summary>
		public string EntityTypeName
		{
			get
			{
				object asObject = this.ViewState["EntityTypeName"];
				if (asObject == null)
					return null;
				else
					return (string)asObject;
			}
			set
			{
				this.ViewState["EntityTypeName"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the datasource
		/// </summary>
		public object DataSource
		{
			get
			{
				return _dataSource;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataSource)
				{
					// set the value...
					_dataSource = value;
				}
			}
		}

		/// <summary>
		/// Binds the child hosts.
		/// </summary>
		public override void DataBind()
		{
			ConstraintControlHost[] hosts = this.GetHosts();
			if(hosts == null)
				throw new InvalidOperationException("hosts is null.");

			// walk...
			foreach(ConstraintControlHost host in hosts)
				host.DataBind();
		}


		private void WalkAndGetHosts(ArrayList results, Control control)
		{
			if(results == null)
				throw new ArgumentNullException("results");
			if(control == null)
				throw new ArgumentNullException("control");
			
			// are we?
			if(control is ConstraintControlHost)
				results.Add(control);

			// walk...
			foreach(Control child in control.Controls)
				this.WalkAndGetHosts(results, child);
		}

		/// <summary>
		/// Pushes changes made in the control back to the entity.
		/// </summary>
		public void PushChangesToDataSource()
		{
			ConstraintControlHost[] hosts = this.GetHosts();
			if(hosts == null)
				throw new InvalidOperationException("hosts is null.");

			// walk...
			foreach(ConstraintControlHost host in hosts)
				host.PushChangesToDataSource();
		}

		/// <summary>
		/// Saves changes to the datasource.
		/// </summary>
		public void SaveChanges()
		{
			// push...
			this.PushChangesToDataSource();

			// save...
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		/// <summary>
		/// Gets or sets the mode
		/// </summary>
		public ExtendedFieldViewMode Mode
		{
			get
			{
				return _mode;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _mode)
				{
					// set the value...
					_mode = value;
				}
			}
		}
	}
}
