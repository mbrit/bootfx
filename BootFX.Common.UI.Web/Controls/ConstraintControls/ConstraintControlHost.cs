// BootFX - Application framework for .NET applications
// 
// File: ConstraintControlHost.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BootFX.Common;
using BootFX.Common.UI;
using BootFX.Common.UI.Web;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;
using BootFX.Common.Services;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
    public class ConstraintControlHost : BaseControl, IEntityType
    {
		private IFlexField _field = null;

		/// <summary>
		/// Private field to support <see cref="ModeSet"/> property.
		/// </summary>
		private bool _isModeSet = false;
		
		/// <summary>
		/// Private field to support <c>Mode</c> property.
		/// </summary>
		private ExtendedFieldViewMode _mode = ExtendedFieldViewMode.Edit;
		
		/// <summary>
        /// Private value to support the <see cref="DataSource">DataSource</see> property.
        /// </summary>
        private object _dataSource;

        /// <summary>
        /// Constructor.
        /// </summary>
        public ConstraintControlHost()
        {
        }

        /// <summary>
        /// Gets or sets the <c>EntityTypeName</c> value from view state.
        /// </summary>
        public string EntityTypeName
        {
            get
            {
                object asObject = this.ViewState["EntityTypeName"];
				if (asObject == null)
				{
					// find the panel...
					ConstraintPanel panel = this.GetOwnerPanel();
					if(panel != null)
						return panel.EntityTypeName;
					else
						return null;
				}
				else
					return (string)asObject;
            }
            set
            {
                this.ViewState["EntityTypeName"] = value;
				_field = null;
            }
        }

        /// <summary>
        /// Gets or sets the <c>MemberName</c> value from view state.
        /// </summary>
        public string MemberName
        {
            get
            {
                object asObject = this.ViewState["MemberName"];
                if (asObject == null)
                    return null;
                else
                    return (string)asObject;
            }
            set
            {
                this.ViewState["MemberName"] = value;
				_field = null;
			}
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // refresh...
            RefreshControl();
        }

        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        public IFlexField Field
        {
            get
            {
				if(_field == null)
				{
					if (this.EntityTypeName != null && this.EntityTypeName.Length > 0 && 
						this.MemberName != null && this.MemberName.Length > 0)
					{
						EntityType et = this.EntityType;
						if (et == null)
							throw new InvalidOperationException("'et' is null.");

						// does the name have many parts?
						if(this.MemberName.IndexOf(".") == -1)
							_field = this.ResolveSinglePartName(et, this.MemberName);
						else
							_field = this.ResolveMultiPartName(et, this.MemberName);

						// check...
						if(_field == null)
							throw new InvalidOperationException(string.Format("Member name expression '{0}' could not be resolved.", this.MemberName));
					}
				}

				// return...
				return _field;
            }
            set
            {
                if (value != null)
                {
                    // member...
                    if(!(value is EntityMember))
                        throw new NotSupportedException(string.Format("A value of type '{0}' is not supported.", value.GetType()));

                    // set...
                    EntityMember member = (EntityMember)value;
                    this.EntityTypeName = member.EntityType.TypeAssemblyQualifiedName;
                    this.MemberName = member.Name;
                }
                else
                {
                    this.EntityTypeName = null;
                    this.MemberName = null;
                }

                // update...
                this.RefreshControl();
            }
        }

		private IFlexField ResolveSinglePartName(EntityType et, string name)
		{
			if(et == null)
				throw new ArgumentNullException("et");
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// get...
			EntityMember member = et.GetMember(name, OnNotFound.ThrowException);
			if(member == null)
				throw new InvalidOperationException("member is null.");

			// return...
			return (IFlexField)member;
		}

		private IFlexField ResolveMultiPartName(EntityType et, string name)
		{
			if(et == null)
				throw new ArgumentNullException("et");
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// parts...
			string[] parts = name.Split('.');
			if(parts == null)
				throw new InvalidOperationException("'parts' is null.");
			if(parts.Length < 2)
				throw new InvalidOperationException(string.Format("'parts' is an invalid length of '{0}'.", parts.Length));

			// datasource...
			object dataSource = this.DataSource;
			if(dataSource == null)
				throw new InvalidOperationException("dataSource is null.");

			// walk, but leave the last part to the regular binding...
			IFlexField forPart = null;
			for(int index = 0; index < parts.Length - 1; index++)
			{
				// walk...
				forPart = this.ResolveSinglePartName(et, parts[index]);
				if(forPart == null)
					throw new InvalidOperationException("forPart is null.");

				// next...
				dataSource = forPart.GetValue(dataSource);
            }

			// force this to be readonly (can't edit related data)...
			this.Mode = ExtendedFieldViewMode.View;

			// set the data source...
			this.DataSource = dataSource;


			// return...
			return forPart;
		}

		/// <summary>
        /// Gets or sets the control's value.
        /// </summary>
        public object Value
        {
            get
            {
                if (InnerControl == null)
                    throw new InvalidOperationException("'InnerControl' is null.");
                return this.InnerControl.Value;
            }
            set
            {
                if (InnerControl == null)
                    throw new InvalidOperationException("'InnerControl' is null.");
                this.InnerControl.Value = value;
            }
        }

        /// <summary>
        /// Resets the control.
        /// </summary>
        private void RefreshControl()
        {
            // get the field...
            IFlexField field = this.Field;
            if (field == null)
                return;

            // create and show...
            IConstraintControl control = ConstraintControlHelper.GetConstraintControlForField(field, this.Mode);
            if (control == null)
                throw new InvalidOperationException("'control' is null.");

            // if we have one already, we need to blat the value back in...
            if (this.HasInnerControl)
                control.Value = this.InnerControl.Value;

            // set...
            this.InnerControl = control;

        }

        /// <summary>
        /// Returns true if there is already an inner control.
        /// </summary>
        private bool HasInnerControl
        {
            get
            {
                if (this.Controls.Count > 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Gets or sets the inner control
        /// </summary>
        private IConstraintControl InnerControl
        {
            get
            {
                // if we have no controls - try and refresh...
                if (this.Controls.Count == 0)
                    this.RefreshControl();

                // whatever we have, return it...
                if (this.Controls.Count > 0)
                    return (IConstraintControl)this.Controls[0];
                else
                    return null;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                // set...
                this.Controls.Clear();
                this.Controls.Add((Control)value);
            }
        }

        /// <summary>
        /// Gets the DataSource value.
        /// </summary>
        public object DataSource
        {
            get
            {
				if(_dataSource == null)
				{
					ConstraintPanel panel = this.GetOwnerPanel();
					if(panel != null)
						return panel.DataSource;
				}

				// return...
                return _dataSource;
            }
            set
            {
                _dataSource = value;
            }
        }

		/// <summary>
		/// Gets the panel that parents this control.
		/// </summary>
		/// <returns></returns>
		private ConstraintPanel GetOwnerPanel()
		{
			Control scan = this.Parent;
			while(scan != null)
			{
				if(scan is ConstraintPanel)
					return (ConstraintPanel)scan;
				scan = scan.Parent;
			}

			// return...
			return null;
		}

        /// <summary>
        /// Data binds this control.
        /// </summary>
        public override void DataBind()
        {
            if (this.DataSource != null)
            {
                // make sure we have a field...
                IFlexField field = this.Field;
                if (field == null)
                    throw new InvalidOperationException("'field' is null.");

                // get...
                EntityType et = this.EntityType;
                if (et == null)
                    throw new InvalidOperationException("'et' is null.");

				// mbr - 19-02-2008 - changed - the flex field needs to know how to get it...
//                this.Value = et.Storage.GetValue(this.DataSource, field);
				this.Value = field.GetValue(this.DataSource);
            }
            else
                this.Value = null;
        }

        /// <summary>
        /// Gets the entity type.
        /// </summary>
        public EntityType EntityType
        {
            get 
            {
				if(_field != null)
					return _field.EntityType;
				else
				{
					if (this.EntityTypeName != null && this.EntityTypeName.Length > 0)
					{
						Type type = Type.GetType(this.EntityTypeName);
						if (type == null)
							throw new InvalidOperationException(string.Format("A type with name '{0}' was not found.", this.EntityTypeName));

						// find...
						EntityType et = EntityType.GetEntityType(type, OnNotFound.ThrowException);
						if (et == null)
							throw new InvalidOperationException("'et' is null.");

						// return...
						return et;
					}
					else
						return null;
				}
            }
        }

		/// <summary>
		/// Pushes changes back to the entity.
		/// </summary>
		public void PushChangesToDataSource()
		{
			if(DataSource == null)
				throw new InvalidOperationException("DataSource is null.");
			if(Field == null)
				throw new InvalidOperationException("Field is null.");

			// set...
			object value = this.Value;
			this.Field.SetValue(this.DataSource, value, SetValueReason.UserSet);
		}

		/// <summary>
		/// Gets the modeset.
		/// </summary>
		private bool IsModeSet
		{
			get
			{
				return _isModeSet;
			}
		}

		/// <summary>
		/// Gets or sets the mode
		/// </summary>
		public ExtendedFieldViewMode Mode
		{
			get
			{
				if(!(this.IsModeSet))
				{
					// get...
					ConstraintPanel panel = this.GetOwnerPanel();
					if(panel != null)
						return panel.Mode;
				}

				// return...
				return _mode;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _mode)
				{
					// set the value...
					_mode = value;
				}

				// set...
				_isModeSet = true;
			}
		}
    }
}
