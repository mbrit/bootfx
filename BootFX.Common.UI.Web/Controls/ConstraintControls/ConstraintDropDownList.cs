// BootFX - Application framework for .NET applications
// 
// File: ConstraintDropDownList.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common;
using BootFX.Common.UI;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintDropDownList.
	/// </summary>
	public class ConstraintDropDownList : DropDownList, IConstraintControl
	{
		internal const string NoneText = "(None)";

		/// <summary>
		/// Returns the mangler id for the control, this will be the ID of the control
		/// </summary>
		public string ManglerId
		{
			get
			{
				return ID;		
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				ID = value;
			}
		}

        /// <summary>
		/// Raised when the <c>FieldName</c> property has changed.
		/// </summary>
		[Browsable(true), Category("Property Changed"), Description("Raised when the FieldName property has changed.")]
		public event EventHandler FieldNameChanged;

		/// <summary>
		/// Raised when the <c>EntityTypeId</c> property has changed.
		/// </summary>
		[Browsable(true), Category("Property Changed"), Description("Raised when the EntityTypeId property has changed.")]
		public event EventHandler EntityTypeIdChanged;
		
		/// <summary>
		/// Gets or sets the entitytypename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>EntityTypeName</c> for storage.
		/// </remarks>
		public string EntityTypeId
		{
			get
			{
				object value = ViewState["EntityTypeId"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != EntityTypeId)
				{
					// set the value...
					ViewState["EntityTypeId"] = value;
					this.OnEntityTypeIdChanged();
				}
			}
		}

		/// <summary>
		/// Used to automatically create a filter for the control, this is the field name on the entity
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		public virtual string FieldName
		{
			get
			{
				string fieldName = (string) this.ViewState["FieldName"];
				if (fieldName != null)
				{
					return fieldName;
				}
				return string.Empty;
			}
			set
			{
				if(value != this.FieldName)
				{
					this.ViewState["FieldName"] = value;
					this.OnFieldNameChanged();
				}
			}
		}

		/// <summary>
		/// Raises the <c>EntityTypeIdChanged</c> event.
		/// </summary>
		private void OnEntityTypeIdChanged()
		{
			OnEntityTypeIdChanged(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>EntityTypeIdChanged</c> event.
		/// </summary>
		protected virtual void OnEntityTypeIdChanged(EventArgs e)
		{
			if(EntityTypeIdChanged != null)
				EntityTypeIdChanged(this, e);
		}
		
		/// <summary>
		/// Raises the <c>FieldNameChanged</c> event.
		/// </summary>
		private void OnFieldNameChanged()
		{
			OnFieldNameChanged(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>FieldNameChanged</c> event.
		/// </summary>
		protected virtual void OnFieldNameChanged(EventArgs e)
		{
			if(FieldNameChanged != null)
				FieldNameChanged(this, e);
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses")]
		public virtual string ChildTableName
		{
			get
			{
				string childTable = (string) this.ViewState["ChildTableName"];
				if (childTable != null)
				{
					return childTable;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildTableName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId")]
		public virtual string ChildFieldName
		{
			get
			{
				string childField = (string) this.ViewState["ChildFieldName"];
				if (childField != null)
				{
					return childField;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildFieldName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(SqlOperator.EqualTo), PersistenceMode(PersistenceMode.Attribute)]
		[Description("The operator used for the constraint")]
		public virtual SqlOperator Operator
		{
			get
			{
				if(this.ViewState["Operator"] == null)
					return SqlOperator.EqualTo;

				SqlOperator sqlOperator = (SqlOperator) this.ViewState["Operator"];
				return sqlOperator;
			}
			set
			{
				this.ViewState["Operator"] = value;
			}
		}

		object IConstraintControl.Value
		{
			get
			{
				return this.GetValue();
			}
			set
			{
				this.SetValue(value);
			}
		}

		protected virtual object GetValue()
		{
			if(this.SelectedItem != null)
				return this.SelectedItem.Value;
			else
				return null;
		}

		protected virtual void SetValue(object value)
		{
			string asString = ConversionHelper.ToString(value, Cultures.System);

			// set...
			if(!(FormHelper.SelectListItemByValue(this, asString)))
			{
				// reset...
				FormHelper.ClearListSelection(this);

				// add...
				ListItem item = new ListItem(asString);
				item.Selected = true;
				this.Items.Add(item);
			}
		}

		bool IConstraintControl.IsValueSet
		{
			get
			{
				return this.IsValueSet;	
			}
		}

		protected virtual bool IsValueSet
		{
			get
			{
				if(this.SelectedItem == null || this.SelectedItem.Value == null || 
					this.SelectedItem.Value.Length == 0 || this.SelectedItem.Value == "0")
					return false;
				else
					return true;
			}
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void DeserializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.DeserializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void SerializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.SerializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Appends the constraint of the filter to the 
//		/// </summary>
//		/// <param name="filter"></param>
//		public virtual void MangleFilter(SqlFilter filter)
//		{
//			if(filter == null)
//				throw new ArgumentNullException("filter");
//			
//			// defer...
//			ConstraintControlHelper.MangleFilter(this, filter);
//		}

		/// <summary>
		/// Gets or sets whether the control can understand DBNull.  
		/// </summary>
		/// <remarks>If this method returns null, BootFX will convert DBNullvalues into the closest CLR-legal equivalent.</remarks>
		public virtual ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.None;
			}
		}

		public void Initialize(IFlexField field, LayoutHint hint)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			
		}
	}
}
