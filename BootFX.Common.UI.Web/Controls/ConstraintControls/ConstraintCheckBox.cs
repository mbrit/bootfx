// BootFX - Application framework for .NET applications
// 
// File: ConstraintCheckBox.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common;
using BootFX.Common.UI.Web;
using BootFX.Common.Data;
using BootFX.Common.Entities;
using BootFX.Common.UI;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintTextBox.
	/// </summary>
	public class ConstraintCheckBox : ConstraintDropDownList
	{
		private const string DbNullEquivalent = "-1";

		internal const string YesText = "Yes";
		internal const string NoText = "No";

		public ConstraintCheckBox()
		{			
		}

		protected override void OnFieldNameChanged(EventArgs e)
		{
			base.OnFieldNameChanged (e);
			RebindList();
		}

		protected override void OnEntityTypeIdChanged(EventArgs e)
		{
			base.OnEntityTypeIdChanged (e);
			RebindList();
		}

		private void RebindList()
		{
			// current...
			string selected = null;
			if(this.SelectedItem != null)
				selected = this.SelectedItem.Value;

			// mbr - 17-03-2006 - clear.			
			this.Items.Clear();

			// add...
//			if(this.IsNullable)
				this.Items.Add(new ListItem(NoneText, DbNullEquivalent));
			this.Items.Add(new ListItem(YesText, "1"));
			this.Items.Add(new ListItem(NoText, "0"));

			// select...
			if(selected != null)
				FormHelper.SelectListItemByValue(this, selected);
		}	

		protected override bool IsValueSet
		{
			get
			{
				if(this.GetValue() is DBNull)
					return false;
				else
					return true;
			}
		}

		public override ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return base.SupportFlags | ConstraintControlSupportFlags.SupportsDbNull;
			}
		}

		protected override void SetValue(object value)
		{
			if(value is DBNull)
				FormHelper.SelectListItemByValue(this, DbNullEquivalent);
			else
			{
				bool asBool = ConversionHelper.ToBoolean(value, Cultures.System);
				if(asBool)
					FormHelper.SelectListItemByValue(this, "1");
				else
					FormHelper.SelectListItemByValue(this, "0");
			}
		}

		protected override object GetValue()
		{
			// selected...
			if(this.SelectedItem == null)
				return DBNull.Value;
			else
			{
				// get...
				string asString = this.SelectedItem.Value;
				if(asString == DbNullEquivalent)
					return DBNull.Value;
				else if(asString == "1")
					return true;
				else if(asString == "0")
					return false;
				else
					throw new NotSupportedException(string.Format("Cannot handle '{0}'.", asString));
			}
		}
	}
}
