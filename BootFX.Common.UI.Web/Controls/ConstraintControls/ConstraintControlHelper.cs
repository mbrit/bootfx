// BootFX - Application framework for .NET applications
// 
// File: ConstraintControlHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ConstraintControlHelper</c>.
	/// </summary>
	public sealed class ConstraintControlHelper
	{
		private ConstraintControlHelper()
		{
		}

		/// <summary>
		/// Gets a control for the given field.
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public static IConstraintControl GetConstraintControlForField(IFlexField field, ExtendedFieldViewMode mode)
		{
			LayoutHint hint = null;
			return GetConstraintControlForField(field, mode, ref hint);
		}

		/// <summary>
		/// Gets a control for the given field.
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public static IConstraintControl GetConstraintControlForField(IFlexField field, ExtendedFieldViewMode mode, ref LayoutHint hint)
		{
			if(field == null)
				throw new ArgumentNullException("field");

			// reset...
			hint = null;

			// mbr - 19-02-2008 - is the field a link?
			if(field is ChildToParentEntityLink)
				return GetConstraintControlForLink((ChildToParentEntityLink)field, mode, ref hint);

			// mbr - 19-02-2008 - if we have a read-only property, force to be view mode...
			if(field is EntityProperty && ((EntityProperty)field).IsReadOnly)
				mode = ExtendedFieldViewMode.View;
			
			// mbr - 09-12-2005 - lookup?
			IConstraintControl control = null;
			if(field.IsLookupProperty)
			{
				if(field.IsMultiValue)
					control = new ConstraintLookupEditorMulti();
				else
					control = new ConstraintLookupEditorSingle();
			}
			else
			{
				// get...
				control = GetConstraintControlForType(field.DBType, mode);
				if(control == null)
					throw new InvalidOperationException("control is null.");

				// get a hint as to how the field should be layed out...
				hint = new LayoutHint(field);
				
				// hint...
				if(control is TextBox)
				{
					TextBox textBox = (TextBox)control;

					// setup...
					if(hint.Multiline)
					{
						textBox.TextMode = TextBoxMode.MultiLine;
						textBox.Width = new Unit(400, UnitType.Pixel);
						textBox.Rows = 4;
					}
					else
					{
						if(field.Size >= 64)
							textBox.Width = new Unit(400, UnitType.Pixel);
					}

					// size...
					if(hint.MaxLength != -1)
						textBox.MaxLength = hint.MaxLength;
				}

				// mbr - 19-02-2008 - operator deprecated.
//				switch(field.DBType)
//				{
//					case DbType.String:
//					case DbType.StringFixedLength:
//					case DbType.AnsiString:
//					case DbType.AnsiStringFixedLength:
//						control.Operator = SqlOperator.StartsWith;
//						break;
//
//					default:
//						control.Operator = SqlOperator.EqualTo;
//						break;
//				}
			}

			// mbr - 19-02-2008 - deprecated.
//			if(control.ManglerId == null || control.ManglerId.Length == 0)
//				control.ManglerId = "bfxcc_" + field.NativeName;

			// setup...
			if(field.EntityType != null)
				control.EntityTypeId = field.EntityType.Id;

			// name...
			control.FieldName = field.Name;

			// mbr - 11-09-2006 - init...
			control.Initialize(field, hint);

			// return...
			return control;
		}

		/// <summary>
		/// Gets the constraint control to use for a link.
		/// </summary>
		/// <param name="link"></param>
		/// <param name="model"></param>
		/// <param name="hint"></param>
		/// <returns></returns>
		// mbr - 19-02-2008 - added.
		private static IConstraintControl GetConstraintControlForLink(ChildToParentEntityLink link, ExtendedFieldViewMode mode, ref LayoutHint hint)
		{
			hint = new LayoutHint(link);

			// mode...
			switch(mode)
			{
				case ExtendedFieldViewMode.Edit:
				case ExtendedFieldViewMode.View:
					return new ConstraintLink();

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", mode, mode.GetType()));
			}
		}

		/// <summary>
		/// Gets a constraint control for the specified type
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		// mbr - 21-04-2006 - made private.		
		// mbr - 11-09-2006 - changed to dbtype.		
		private static IConstraintControl GetConstraintControlForType(DbType type, ExtendedFieldViewMode mode)
		{
			IConstraintControl control = null;
			if(type == DbType.Boolean)
				control = new ConstraintCheckBox();
			else if(type == DbType.DateTime || type == DbType.Date || type == DbType.Time)
			{
				switch(mode)
				{
					case ExtendedFieldViewMode.View:
						control = new ConstraintDateTimePicker();
						break;

					case ExtendedFieldViewMode.Edit:
						control = new ConstraintTextBox();
						break;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", mode, mode.GetType()));
				}
			}
			else
				control = new ConstraintTextBox();

			// check...
			if(control == null)
				throw new InvalidOperationException("control is null.");
			return control;
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Walks the control tree and serializes all of the constraints.
//		/// </summary>
//		public static void WalkAndAddConstraintControls(SqlSearchSettings settings, Control owner)
//		{
//			if(settings == null)
//				throw new ArgumentNullException("settings");
//			if(owner == null)
//				throw new ArgumentNullException("owner");
//			
//			// document...
//			XmlDocument doc = new XmlDocument();
//
//			// walk...
//			IList all = GetAllChildManglers(owner);
//
//			// walk...
//			foreach(ISqlFilterMangler mangler in all)
//			{
//				// mbr - 21-04-2006 - assert that we have an ID...
//				if(mangler.ManglerId == null)
//					throw new InvalidOperationException("'mangler.ManglerId' is null.");
//				if(mangler.ManglerId.Length == 0)
//					throw new InvalidOperationException("'mangler.ManglerId' is zero-length.");
//
//				// do we have it?
//				if(!(settings.Manglers.Contains(mangler.ManglerId)))
//				{
//					SimpleXmlPropertyBag settingsBag = new SimpleXmlPropertyBag();
//					mangler.SerializeSqlSearchSettings(settingsBag);
//
//					FilterMangler filterMangler = new FilterMangler(mangler.ManglerId,settingsBag,mangler);
//					settings.Manglers.Add(filterMangler);
//				}
//			}
//		}
//
//		/// <summary>
//		/// Walks the control tree and serializes all of the constraints.
//		/// </summary>
//		public static void WalkAndExtractSettings(SqlSearchSettings settings, Control owner)
//		{
//			// walk...
//			IList all = GetAllChildManglers(owner);
//
//			// walk...
//			foreach(ISqlFilterMangler mangler in all)
//			{
//				if(mangler.ManglerId == null)
//					throw new InvalidOperationException("'mangler.ManglerId' is null.");
//				if(mangler.ManglerId.Length == 0)
//					throw new InvalidOperationException("'mangler.ManglerId' is zero-length.");
//
//				// check...
//				if(settings.Manglers.Contains(mangler.ManglerId))
//				{
//					FilterMangler filterMangler = settings.Manglers[mangler.ManglerId];
//					mangler.DeserializeSqlSearchSettings(filterMangler.Settings);
//				}
//			}
//		}
//
//		private static IList GetAllChildManglers(Control owner)
//		{
//			ArrayList results = new ArrayList();
//			WalkAndGetAllChildManglers(owner, results);
//			return results;
//		}
//
//		private static void WalkAndGetAllChildManglers(Control owner, IList results)
//		{
//			if(owner == null)
//				throw new ArgumentNullException("owner");
//			if(results == null)
//				throw new ArgumentNullException("results");
//			
//			// check...
//			if(owner is ISqlFilterMangler)
//				results.Add(owner);
//
//			// children...
//			foreach(Control child in owner.Controls)
//				WalkAndGetAllChildManglers(child, results);
//		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Performs common serialization.
//		/// </summary>
//		/// <param name="control"></param>
//		/// <param name="bag"></param>
//		internal static void DeserializeSqlSearchSettings(IConstraintControl control, IDictionary bag)
//		{
//			if(control == null)
//				throw new ArgumentNullException("control");
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//
//			// patch...
//			if(bag.Contains("EntityTypeId"))
//				control.EntityTypeId = ConversionHelper.ToString(bag["EntityTypeId"], Cultures.System);
//			else
//				control.EntityTypeId = null;
//
//			// patch...
//			if(bag.Contains("FieldName"))
//				control.FieldName = ConversionHelper.ToString(bag["FieldName"], Cultures.System);
//			else
//				control.FieldName = null;
//
//			// patch...
//			if(bag.Contains("Operator"))
//				control.Operator = (SqlOperator)Enum.Parse(typeof(SqlOperator), ConversionHelper.ToString(bag["Operator"], Cultures.System), true);
//			else
//				control.Operator = SqlOperator.EqualTo;
//
//			// value...
//			control.Value = bag["Value"];
//		}
//
//		/// <summary>
//		/// Performs common serialization.
//		/// </summary>
//		/// <param name="control"></param>
//		/// <param name="bag"></param>
//		internal static void SerializeSqlSearchSettings(IConstraintControl control, IDictionary bag)
//		{
//			if(control == null)
//				throw new ArgumentNullException("control");
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// field...
//			if(IsConfiguredToMangleFilter(control))
//			{
//				// set...
//				bag["EntityTypeId"] = control.EntityTypeId;
//				bag["FieldName"] = control.FieldName;
//				bag["Operator"] = control.Operator;
//				bag["Value"] = control.Value;
//			}
//		}
//
//		/// <summary>
//		/// Returns true if the given control is properly set up to mangle a filter.
//		/// </summary>
//		/// <param name="control"></param>
//		/// <returns></returns>
//		private static bool IsConfiguredToMangleFilter(IConstraintControl control)
//		{
//			if(control == null)
//				throw new ArgumentNullException("control");
//			
//			// basics...
//			if(control.FieldName == null || control.FieldName.Length == 0)
//				return false;
//
//			// check...
//			if(!(control.IsValueSet))
//				return false;
//
//			// yes...
//			return true;
//		}
//
//				
//		/// <summary>
//		/// Appends the constraint of the filter to the 
//		/// </summary>
//		/// <param name="filter"></param>
//		internal static void MangleFilter(IConstraintControl control, SqlFilter filter)
//		{
//			if(control == null)
//				throw new ArgumentNullException("control");
//
//			// mangle?
//			if(!(IsConfiguredToMangleFilter(control)))
//				return;
//
//			// find the field...
//			EntityField field = filter.EntityType.Fields.GetField(control.FieldName, OnNotFound.ThrowException);
//			if(field == null)
//				throw new InvalidOperationException("field is null.");
//
//			// mbr - 21-04-2006 - get the value...			
//			object useValue = control.Value;
//
//			// mbr - 21-04-2006 - do we have a between operator?
//			if(control.Operator != SqlOperator.Between && control.Operator != SqlOperator.NotBetween)
//			{
//				// convert it and add it later...
//				useValue = ConversionHelper.ChangeType(useValue, field.Type, Cultures.User, ConversionFlags.ClrNullToLegal);
//			}
//			else
//			{
//				// check...
//				if(useValue == null)
//					throw new InvalidOperationException("useValue is null.");
//				if(!(useValue is Array) && !(useValue is ArrayList))
//					throw new InvalidOperationException(string.Format("Between operator specified without array values.  Values supplied were '{0}'.", useValue.GetType()));
//
//				// check...
//				Array array = null;
//				if(useValue is Array)
//					array = (Array)useValue;
//				else if(useValue is ArrayList)
//				{
//					array = ((ArrayList)useValue).ToArray();
//					useValue = array;
//				}
//				
//				if(array.Length != 2)
//					throw new InvalidOperationException(string.Format("Array length invalid: {0} cf 2.", array.Length));
//			}
//
//			// add it...
//			filter.Constraints.Add(control.FieldName, control.Operator, useValue);
//		}
	}
}
