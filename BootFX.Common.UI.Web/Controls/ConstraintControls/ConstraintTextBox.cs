// BootFX - Application framework for .NET applications
// 
// File: ConstraintTextBox.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common;
using BootFX.Common.UI.Web;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;
using BootFX.Common.UI;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintTextBox.
	/// </summary>
	public class ConstraintTextBox : TextBox, IConstraintControl
	{
		/// <summary>
		/// Returns the mangler id for the control, this will be the ID of the control
		/// </summary>
		public string ManglerId
		{
			get
			{
				return ID;		
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				ID = value;
			}		
		}

		/// <summary>
		/// Gets or sets the entitytypename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>EntityTypeName</c> for storage.
		/// </remarks>
		public string EntityTypeId
		{
			get
			{
				object value = ViewState["EntityTypeId"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != EntityTypeId)
				{
					// set the value...
					ViewState["EntityTypeId"] = value;
				}
			}
		}

		/// <summary>
		/// Used to automatically create a filter for the control, this is the field name on the entity
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		public virtual string FieldName
		{
			get
			{
				string fieldName = (string) this.ViewState["FieldName"];
				if (fieldName != null)
				{
					return fieldName;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["FieldName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. Addresses")]
		public virtual string ChildTableName
		{
			get
			{
				string childTable = (string) this.ViewState["ChildTableName"];
				if (childTable != null)
				{
					return childTable;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildTableName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		[Description("Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId")]
		public virtual string ChildFieldName
		{
			get
			{
				string childField = (string) this.ViewState["ChildFieldName"];
				if (childField != null)
				{
					return childField;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["ChildFieldName"] = value;
			}
		}

		/// <summary>
		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
		/// If this value is empty it will use the same field name as the parent
		/// </summary>
		[Bindable(true), DefaultValue(SqlOperator.EqualTo), PersistenceMode(PersistenceMode.Attribute)]
		[Description("The operator used for the constraint")]
		public virtual SqlOperator Operator
		{
			get
			{
				if(this.ViewState["Operator"] == null)
					return SqlOperator.StartsWith;

				SqlOperator sqlOperator = (SqlOperator) this.ViewState["Operator"];
				return sqlOperator;
			}
			set
			{
				this.ViewState["Operator"] = value;
			}
		}

		bool IConstraintControl.IsValueSet
		{
			get
			{
				if(this.Text != null && this.Text.Trim().Length > 0)
					return true;
				else
					return false;
			}
		}

		object IConstraintControl.Value
		{
			get
			{
				if(this.Text != null)
					return this.Text.Trim();
				else
					return null;
			}
			set
			{
				// date?
				if(value is DateTime)
				{
					// min?
					DateTime date = (DateTime)value;
					if(date == DateTime.MinValue)
						this.Text = string.Empty;
					else
					{
						// mbr - 11-09-2006 - format?
						string format = this.DateTimeFormat;
						if(format != null && format.Length > 0)
							this.Text = date.ToString(format, Cultures.User);
						else
						{
							if(date.Hour == 0 && date.Minute == 0 && date.Second == 0)
								this.Text = date.ToShortDateString();
							else
								this.Text = date.ToString();
						}
					}
				}
				else if(value is Guid)
					this.Text = ((Guid)value).ToString();
				else
				{
					// set...
					this.Text = ConversionHelper.ToString(value, Cultures.User);
				}
			}
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void DeserializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.DeserializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public virtual void SerializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//
//			// defer...
//			ConstraintControlHelper.SerializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Appends the constraint of the filter to the 
//		/// </summary>
//		/// <param name="filter"></param>
//		public virtual  void MangleFilter(SqlFilter filter)
//		{
//			if(filter == null)
//				throw new ArgumentNullException("filter");
//			
//			// defer...
//			ConstraintControlHelper.MangleFilter(this, filter);
//		}

		/// <summary>
		/// Gets or sets whether the control can understand DBNull.  
		/// </summary>
		/// <remarks>If this method returns null, BootFX will convert DBNullvalues into the closest CLR-legal equivalent.</remarks>
		public virtual ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.None;
			}
		}

		public void Initialize(IFlexField field, LayoutHint hint)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			if(hint == null)
				throw new ArgumentNullException("hint");
			
			// set...
			this.DateTimeFormat = hint.DateTimeFormat;
		}

		/// <summary>
		/// Gets or sets the datetimeformat.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>DateTimeFormat</c> for storage.
		/// </remarks>
		private string DateTimeFormat
		{
			get
			{
				object value = ViewState["DateTimeFormat"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != DateTimeFormat)
				{
					// set the value...
					ViewState["DateTimeFormat"] = value;
				}
			}
		}
	}
}
