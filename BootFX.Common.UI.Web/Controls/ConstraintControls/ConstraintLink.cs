// BootFX - Application framework for .NET applications
// 
// File: ConstraintLink.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for ConstraintLink.
	/// </summary>
	public class ConstraintLink : HyperLink, IConstraintControl
	{
		/// <summary>
		/// Private field to support <c>Value</c> property.
		/// </summary>
		private object _value;
		
		public ConstraintLink()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public BootFX.Common.UI.ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.SupportsDbNull;
			}
		}

		/// <summary>
		/// Gets or sets the value
		/// </summary>
		public object Value
		{
			get
			{
				return _value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _value)
				{
					// set the value...
					_value = value;

					// set...
					if(this.Value != null)
						this.Text = _value.ToString();
					else
					{
						this.Text = string.Empty;
						this.NavigateUrl = string.Empty;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the entitytypename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>EntityTypeName</c> for storage.
		/// </remarks>
		public string EntityTypeId
		{
			get
			{
				object value = ViewState["EntityTypeId"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != EntityTypeId)
				{
					// set the value...
					ViewState["EntityTypeId"] = value;
				}
			}
		}

		public bool IsValueSet
		{
			get
			{
				if(this.Value != null)
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Used to automatically create a filter for the control, this is the field name on the entity
		/// </summary>
		public string FieldName
		{
			get
			{
				string fieldName = (string) this.ViewState["FieldName"];
				if (fieldName != null)
				{
					return fieldName;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["FieldName"] = value;
			}
		}

		public void Initialize(BootFX.Common.Data.FlexData.IFlexField field, LayoutHint hint)
		{
		}
	}
}
