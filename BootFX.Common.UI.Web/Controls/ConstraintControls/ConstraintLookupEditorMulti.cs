// BootFX - Application framework for .NET applications
// 
// File: ConstraintLookupEditorMulti.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	///		Summary description for header.
	/// </summary>
	/// , 
	[ToolboxData("<{0}:ConstraintLookupEditor runat=server></{0}:ConstraintLookupEditor>")]
	public class ConstraintLookupEditorMulti : Table, INamingContainer, IConstraintControl
	{
		/// <summary>
		/// Returns the mangler id for the control, this will be the ID of the control
		/// </summary>
		public string ManglerId
		{
			get
			{
				return ID;		
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				ID = value;
			}		
		}

		protected System.Web.UI.WebControls.PlaceHolder placeHolderLookups;

		/// <summary>
		/// Private field to support <c>EntityType</c> property.
		/// </summary>
		private BootFX.Common.Entities.EntityType _entityType;

		/// <summary>
		/// Private field to support <c>ReadOnly</c> property.
		/// </summary>
		private bool _readOnly = false;

		/// <summary>
		/// Private field to support <c>ForFiltering</c> property.
		/// </summary>
		private bool _forFiltering = false;

		/// <summary>
		/// Gets and sets if the view is read only or not
		/// </summary>
		[Bindable(true)]
		public bool ReadOnly
		{
			get
			{
				return _readOnly;	
			}
			set
			{
				if(_readOnly == value)
					return;

				_readOnly = value;
			}
		}

		/// <summary>
		/// Gets and sets if the view is for filtering
		/// </summary>
		[Bindable(true)]
		public bool ForFiltering
		{
			get
			{
				return _forFiltering;	
			}
			set
			{
				if(_forFiltering == value)
					return;

				_forFiltering = value;
				if(value == true)
					ReadOnly = true;
			}
		}

		/// <summary>
		/// Appends the constraints selected by the user to an SqlFilter
		/// </summary>
		public void AppendConstraints(SqlFilter filter)
		{
			if(!(ForFiltering))
				throw new InvalidOperationException("A filter can only be retrieved when the ForFiltering property is true.");

			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			EnsureChildControls();
		}

		/// <summary>
		/// Refresh the main lists of each control
		/// </summary>
		protected override void CreateChildControls() 
		{
			base.CreateChildControls();
			this.RefreshView();
		}

		/// <summary>
		/// Gets or sets the columnsacross.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>ColumnsAcross</c> for storage.
		/// </remarks>
		[Browsable(true), Category("Appearance")]
		public int ColumnsAcross
		{
			get
			{
				object value = ViewState["ColumnsAcross"];
				if(value == null)
					return 3;
				else
					return (int)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != ColumnsAcross)
				{
					// set the value...
					ViewState["ColumnsAcross"] = value;

					// update...
					this.RefreshView();
				}
			}
		}

		private void RefreshView()
		{
			if(EntityType == null)
				return;

			// reset...
			this.Rows.Clear();

			// check...
			if(this.EntityTypeId == null || this.EntityTypeId.Length == 0 || this.FieldName == null || this.FieldName.Length == 0)
				return;

			// get...
			EntityType et = this.EntityType;
			if(et == null)
				return;
			EntityField field = et.Fields.GetField(this.FieldName, OnNotFound.ReturnNull);
			if(field == null)
				return;

			// lookup?
			if(!(field is EntityLookupField))
				throw new InvalidOperationException(string.Format("Field '{0}' is not a lookup field.", field));

			// get the values...
			TableRow row = null;
			foreach(LookupValue value in LookupValue.GetByName(((EntityLookupField)field).LookupName))
			{
				// create...
				if(row == null || row.Cells.Count >= this.ColumnsAcross)
				{
					row = new TableRow();
					this.Rows.Add(row);
				}

				// cell...
				TableCell cell = new TableCell();
				row.Cells.Add(cell);
				cell.Width = new Unit(125, UnitType.Pixel);

				// add...
				LookupValueCheckBox box = new LookupValueCheckBox(value);

				// set...
				if((this.StoredValue & box.Value.Value) == box.Value.Value)
					box.Checked = true;
				else
					box.Checked = false;

				// add...
				cell.Controls.Add(box);
			}
		}

		private class LookupValueCheckBox : CheckBox
		{
			/// <summary>
			/// Private field to support <see cref="Value"/> property.
			/// </summary>
			private LookupValue _value;
			
			internal LookupValueCheckBox(LookupValue value)
			{
				if(value	 == null)
					throw new ArgumentNullException("value	");
				_value = value;

				// set...
				this.CssClass = "field";
				this.Text = value.Description;
			}

			/// <summary>
			/// Gets the value.
			/// </summary>
			internal LookupValue Value
			{
				get
				{
					return _value;
				}
			}
		}

		private LookupValueCheckBox[] GetCheckBoxes()
		{
			ArrayList list = new ArrayList();
			this.WalkAndGetCheckBoxes(this, list);

			// return...
			return (LookupValueCheckBox[])list.ToArray(typeof(LookupValueCheckBox));
		}

		private void WalkAndGetCheckBoxes(Control control, IList list)
		{
			if(control is LookupValueCheckBox)
				list.Add(control);

			// children...
			foreach(Control child in control.Controls)
				this.WalkAndGetCheckBoxes(child, list);
		}

		public object Value
		{
			get
			{
				// result...
				int result = 0;

				// walk the controls...
				LookupValueCheckBox[] boxes = this.GetCheckBoxes();
				foreach(LookupValueCheckBox box in boxes)
				{
					if(box.Checked)
						result |= box.Value.Value;
				}

				// return...
				return result;
			}
			set
			{
				int intValue = ConversionHelper.ToInt32(value, Cultures.System);
				if(intValue != this.StoredValue)
				{
					this.StoredValue = intValue;
					this.RefreshView();
				}
			}
		}

		/// <summary>
		/// Gets or sets the storedvalue.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>StoredValue</c> for storage.
		/// </remarks>
		private int StoredValue
		{
			get
			{
				object value = ViewState["StoredValue"];
				if(value == null)
					return 0;
				else
					return (int)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != StoredValue)
				{
					// set the value...
					ViewState["StoredValue"] = value;
				}
			}
		}

		public bool IsValueSet
		{
			get
			{
				// anything?
				foreach(CheckBox box in this.GetCheckBoxes())
				{
					if(box.Checked)
						return true;
				}

				// nope...
				return false;
			}
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Used to apply a free constraint when field not directly accessible from the base filter type.\r\ne.g. CompanyId
//		/// If this value is empty it will use the same field name as the parent
//		/// </summary>
//		[Bindable(true), DefaultValue(SqlOperator.EqualTo), PersistenceMode(PersistenceMode.Attribute)]
//		[Description("The operator used for the constraint")]
//		public virtual SqlOperator Operator
//		{
//			get
//			{
//				if(this.ViewState["Operator"] == null)
//					return SqlOperator.BitwiseAnd;
//
//				SqlOperator sqlOperator = (SqlOperator) this.ViewState["Operator"];
//				return sqlOperator;
//			}
//			set
//			{
//				this.ViewState["Operator"] = value;
//			}
//		}

		/// <summary>
		/// Gets or sets the entitytypename.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>EntityTypeName</c> for storage.
		/// </remarks>
		public string EntityTypeId
		{
			get
			{
				object value = ViewState["EntityTypeId"];
				if(value == null)
					return null;
				else
					return (string)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != EntityTypeId)
				{
					// set the value...
					ViewState["EntityTypeId"] = value;

					// reset...
					_entityType = null;

					// update...
					this.RefreshView();
				}
			}
		}

		/// <summary>
		/// Gets the entity type.
		/// </summary>
		private EntityType EntityType
		{
			get
			{
				if(_entityType == null)
				{
					string id = this.EntityTypeId;
					if(id != null && id.Length > 0)
					{
						_entityType = EntityType.GetEntityTypeForId(id, OnNotFound.ThrowException);
						if(_entityType == null)
							throw new InvalidOperationException("_entityType is null.");
					}
				}
				return _entityType;
			}
		}

		/// <summary>
		/// Used to automatically create a filter for the control, this is the field name on the entity
		/// </summary>
		[Bindable(true), DefaultValue(""), PersistenceMode(PersistenceMode.Attribute)]
		public virtual string FieldName
		{
			get
			{
				string fieldName = (string) this.ViewState["FieldName"];
				if (fieldName != null)
				{
					return fieldName;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["FieldName"] = value;
			}
		}

		// mbr - 19-02-2008 - deprecated.
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public void DeserializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.DeserializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Serializes values to a dictionary.
//		/// </summary>
//		/// <param name="bag"></param>
//		public void SerializeSqlSearchSettings(IDictionary bag)
//		{
//			if(bag == null)
//				throw new ArgumentNullException("bag");
//			
//			// defer...
//			ConstraintControlHelper.SerializeSqlSearchSettings(this, bag);
//		}
//
//		/// <summary>
//		/// Appends the constraint of the filter to the 
//		/// </summary>
//		/// <param name="filter"></param>
//		public void MangleFilter(SqlFilter filter)
//		{
//			if(filter == null)
//				throw new ArgumentNullException("filter");
//			
//			// defer...
//			ConstraintControlHelper.MangleFilter(this, filter);
//		}	

		/// <summary>
		/// Gets or sets whether the control can understand DBNull.  
		/// </summary>
		/// <remarks>If this method returns null, BootFX will convert DBNullvalues into the closest CLR-legal equivalent.</remarks>
		public virtual ConstraintControlSupportFlags SupportFlags
		{
			get
			{
				return ConstraintControlSupportFlags.None;
			}
		}

		public void Initialize(IFlexField field, LayoutHint hint)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			
		}
	}
}
