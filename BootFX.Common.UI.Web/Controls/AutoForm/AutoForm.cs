// BootFX - Application framework for .NET applications
// 
// File: AutoForm.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoForm</c>.
	/// </summary>
	public class AutoForm : AutoControlBase, INamingContainer
	{
		private AutoColumnSpecificationCollection _columns = new AutoColumnSpecificationCollection();

		/// <summary>
		/// Raised when an entity is needed.
		/// </summary>
		public event EventHandler EntityNeeded;
		
		/// <summary>
		/// Private field to support <c>Entity</c> property.
		/// </summary>
		private object _entity;
		
		/// <summary>
		/// Raised before changes are saved.
		/// </summary>
		public event AutoControlApplyEventHandler BeforeSaveChanges;
		
		/// <summary>
		/// Raised after changes are saved.
		/// </summary>
		public event EntityEventHandler AfterSaveChanges;
		
		/// <summary>
		/// Private field to support <c>applyButtonText</c> property.
		/// </summary>
		private string _applyButtonText = "Save Changes";
		
		/// <summary>
		/// Private field to support <see cref="applyButton"/> property.
		/// </summary>
		private Button _applyButton;
		
		/// <summary>
		/// Private field to support <see cref="buddyGrid"/> property.
		/// </summary>
		private AutoDataGrid _buddyGrid;
		
		public AutoForm()
		{
		}

		internal AutoForm(AutoDataGrid buddyGrid)
		{
			if(buddyGrid == null)
				throw new ArgumentNullException("buddyGrid");
			_buddyGrid = buddyGrid;
		}

		internal void RefreshView()
		{
			this.Rows.Clear();

			// create a separate table to put the form into...
			Table table = new Table();
			TableHelper.AddControlRow(this, table);

			// how many columns?
			if(this.BuddyGrid == null && this.Columns.Count == 0)
				this.OnColumnsNeeded(EventArgs.Empty);

			// walk...
			TableRow row = null;
			foreach(AutoColumnSpecification specification in this.GetColumnsForView())
			{
				row = new TableRow();
				table.Rows.Add(row);

				// add...
				TableHelper.AddTextCell(row, string.Format("{0}:&nbsp;", specification.HeaderText));
				TableHelper.AddControlCell(row, (Control)this.CreateControlForField(specification, ExtendedFieldViewMode.Edit));
			}

			// add...
			row = new TableRow();
			table.Rows.Add(row);
			TableHelper.AddTextCell(row, string.Empty);
			TableHelper.AddControlCell(row, this.ApplyButton);

			// ok, then problem!
			TableHelper.AddControlRow(this, this.FeedbackWidget);
		}

		private AutoColumnSpecificationCollection GetColumnsForView()
		{
			if(this.BuddyGrid != null)
				return this.BuddyGrid.GetColumnsForAddForm();
			else
			{
				if(this.Columns.Count == 0)
				{
					this.OnColumnsNeeded(EventArgs.Empty);

					// still nothing?
					if(this.Columns.Count == 0 && this.Entity != null)
						this.Columns.AddRange(this.InferColumnsFromEntityType(EntityType.GetEntityType(this.Entity, OnNotFound.ThrowException), false));
				}
				return this.Columns;
			}
		}

		/// <summary>
		/// Gets the buddygrid.
		/// </summary>
		private AutoDataGrid BuddyGrid
		{
			get
			{
				return _buddyGrid;
			}
		}

		public AutoColumnSpecificationCollection Columns
		{
			get
			{
				return _columns;
			}
		}

		public override EntityType EntityType
		{
			get
			{
				if(BuddyGrid == null)
					throw new InvalidOperationException("BuddyGrid is null.");
				return this.BuddyGrid.EntityType;
			}
		}

		/// <summary>
		/// Gets the applyButton.
		/// </summary>
		private Button ApplyButton
		{
			get
			{
				if(_applyButton == null)
				{
					_applyButton = new Button();
					_applyButton.Text = this.ApplyButtonText;
					_applyButton.Click += new EventHandler(_applyButton_Click);
				}
				return _applyButton;
			}
		}

		/// <summary>
		/// Gets or sets the applyButtonText
		/// </summary>
		[Browsable(true)]
		public string ApplyButtonText
		{
			get
			{
				return _applyButtonText;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _applyButtonText)
				{
					// set the value...
					_applyButtonText = value;

					// check...
					if(_applyButton != null)
						_applyButton.Text = value;
				}
			}
		}

		private void _applyButton_Click(object sender, EventArgs e)
		{
			try
			{
				this.Apply();
			}
			catch(Exception ex)
			{
				this.HandleException("An error occurred when applying changes to the item.", ex);
			}
		}

		private bool Apply()
		{
			// get an entity...
			object useEntity = this.Entity;
			if(useEntity == null)
			{
				// create...
				if(EntityType == null)
					throw new InvalidOperationException("EntityType is null.");
				useEntity = this.EntityType.CreateInstance();
				if(useEntity == null)
					throw new InvalidOperationException("useEntity is null.");
			}

			// defer...
			bool result = this.Apply(useEntity, this, this.BeforeSaveChanges);
			if(result)
			{
				// update...
				this.RefreshView();

				// raise...
				this.OnAfterSaveChanges(new EntityEventArgs(useEntity));
			}

			// return...
			return result;
		}

		/// <summary>
		/// Raises the <c>BeforeSaveChanges</c> event.
		/// </summary>
		protected virtual void OnBeforeSaveChanges(AutoControlApplyEventArgs e)
		{
			// raise...
			if(BeforeSaveChanges != null)
				BeforeSaveChanges(this, e);
		}

		/// <summary>
		/// Raises the <c>AfterSaveChanges</c> event.
		/// </summary>
		protected virtual void OnAfterSaveChanges(EntityEventArgs e)
		{
			// raise...
			if(AfterSaveChanges != null)
				AfterSaveChanges(this, e);
		}

		/// <summary>
		/// Gets or sets the entity
		/// </summary>
		public object Entity
		{
			get
			{
				if(_entity == null)
					this.OnEntityNeeded();
				return _entity;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _entity)
				{
					// set the value...
					_entity = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>EntityNeeded</c> event.
		/// </summary>
		private void OnEntityNeeded()
		{
			OnEntityNeeded(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>EntityNeeded</c> event.
		/// </summary>
		protected virtual void OnEntityNeeded(EventArgs e)
		{
			// raise...
			if(EntityNeeded != null)
				EntityNeeded(this, e);
		}

		public override void DataBind()
		{
			// defer...
			this.RefreshView();
		}
	}
}
