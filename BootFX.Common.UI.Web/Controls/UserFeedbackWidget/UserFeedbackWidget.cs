// BootFX - Application framework for .NET applications
// 
// File: UserFeedbackWidget.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for UserMessageLabel.
	/// </summary>
	public class UserFeedbackWidget : LoggableTable
	{
		/// <summary>
		/// Private field to support <see cref="SendReportButton"/> property.
		/// </summary>
		private Button _sendReportButton;

		/// <summary>
		/// Private field to support <see cref="SendFeedbackWidget"/> property.
		/// </summary>
		private Label _sendFeedbackLabel;
		
		/// <summary>
		/// Private field to support <see cref="MoreInfoButton"/> property.
		/// </summary>
		private LinkButton _moreInfoButton;
		
		/// <summary>
		/// Private field to support <c>ErrorCssClass</c> property.
		/// </summary>
		private static string _defaultErrorCssClass = "problem";

		/// <summary>
		/// Private field to support <c>InfoCssClass</c> property.
		/// </summary>
		private static string _defaultInfoCssClass = "info";

		private const string ErrorNotProvidedText = "An error occurred but no error information was provided.";
		
		/// <summary>
		/// Private field to support <see cref="Label"/> property.
		/// </summary>
		private Label _label;
		
		public UserFeedbackWidget()
		{
			this.CellSpacing = 0;
			this.CellPadding = 0;
		
			// build the view and get everything subscribed...
			this.BuildView();
		}

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // if...
            if (this.ParentBlocksMoreInfo)
                this.MoreInfoButton.Visible = false;
        }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit (e);

			// subscribe...
			this.MoreInfoButton.Click += new EventHandler(_moreInfoButton_Click);
			this.SendReportButton.Click += new EventHandler(_sendReportButton_Click);
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState (savedState);

			// aha!  if we got here we must have an error in the view state...
			this.BuildView();
		}

		private void BuildView()
		{
			this.Rows.Clear();

			// ok...
			TableHelper.AddControlRow(this, this.Label);

			// more information...
			if(Runtime.Current.InstallationSettings.ShowExceptionDetailOnUi)
				TableHelper.AddControlRow(this, this.MoreInfoButton);

			// mbr - 16-05-2008 - do we have extended error information...		
			if(this.Error != null)
			{
				// ok...
				TableHelper.AddSpacerRow(this, 10);
				TableHelper.AddControlRow(this, this.SendReportButton);
				TableHelper.AddControlRow(this, this.SendFeedbackLabel);
			}
		}

        private bool ParentBlocksMoreInfo
        {
            get
            {
                Control walk = this.Parent;
                while (walk != null)
                {
                    if (walk is IUserFeedbackWidgetHost)
                        return !(((IUserFeedbackWidgetHost)walk).AllowExtendedErrorInformation);

                    walk = walk.Parent;
                }

                return false;
            }
        }

		/// <summary>
		/// Gets the label.
		/// </summary>
		private Label Label
		{
			get
			{
				if(_label == null)
					_label = new Label();
				return _label;
			}
		}

		/// <summary>
		/// Gets the moreinfobutton.
		/// </summary>
		private LinkButton MoreInfoButton
		{
			get
			{
				if(_moreInfoButton == null)
				{
					_moreInfoButton = new LinkButton();
					_moreInfoButton.Visible = false;
					_moreInfoButton.Text = "Show more information on this error...";
				}
				return _moreInfoButton;
			}
		}

		/// <summary>
		/// Sets an error from an object that supports user messages.  
		/// </summary>
		/// <param name="checker"></param>
		/// <remarks>For example, the <c>FormChecker</c> class.</remarks>
		public void SetError(IUserMessages source)
		{
			if(source == null)
				throw new ArgumentNullException("source");
			
			// get...
			string[] messages = source.GetUserMessages();
			if(messages == null)
				throw new InvalidOperationException("messages is null.");
			if(messages.Length > 0)
				this.SetError(messages);
			else
				this.SetError(ErrorNotProvidedText);
		}

		private static void LogException(Exception ex, ILog log)
		{
			if(ex == null)
				throw new ArgumentNullException("ex");
			
			// log...
			if(log == null)
				log = LogSet.GetLog(typeof(UserFeedbackWidget));

            // mbr - 2010-06-07 - removed...
            //if(log.IsErrorEnabled)
            //    log.Error("An error was displayed by the user feedback widget.", ex);
		}

		/// <summary>
		/// Sets an error from an exception.
		/// </summary>
		/// <param name="ex"></param>
		public void SetError(Exception ex)
		{
			this.SetError(null, ex);
		}

		/// <summary>
		/// Sets an error from an exception.
		/// </summary>
		/// <param name="ex"></param>
		public void SetError(string message, Exception ex)
		{
			if(ex == null)
			{
				this.SetError(ErrorNotProvidedText);
				return;
			}

			// wrap and defer...
			if(HttpContext.Current == null)
				throw new InvalidOperationException("HttpContext.Current is null.");
			this.SetError(new WebError(HttpContext.Current.Request, ex));
		}

		/// <summary>
		/// Sets an error from a Web error.
		/// </summary>
		/// <param name="error"></param>
		internal void SetError(WebError error)
		{
			this.SetError(null, error);
		}

		/// <summary>
		/// Sets an error from a Web error.
		/// </summary>
		/// <param name="error"></param>
		internal void SetError(string message, WebError error)
		{
			if(error == null)
				throw new ArgumentNullException("error");
			
			// set...
			LogException(error.Exception, this.Log);

			// show...
			this.Label.CssClass = DefaultErrorCssClass;
			SetLabel(this.Label, error.Exception);

			// more information...
			this.Error = error;
		}

		/// <summary>
		/// Sets an error.
		/// </summary>
		/// <param name="ex"></param>
		public void SetError(string message)
		{
			// set...
			this.Label.CssClass = DefaultErrorCssClass;
			this.Label.Text = FormatUserMessage(message);
		}

		/// <summary>
		/// Sets an error.
		/// </summary>
		/// <param name="ex"></param>
		public void SetError(string[] messages)
		{
			// set...
			this.Label.CssClass = DefaultErrorCssClass;
			this.Label.Text = FormatUserMessages(messages);
		}

		/// <summary>
		/// Sets an information message.
		/// </summary>
		/// <param name="ex"></param>
		public void SetInfo(string message)
		{
			this.ResetError();
			this.BuildView();

			// set...
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = FormatUserMessage(message);
		}

		/// <summary>
		/// Sets an information message.
		/// </summary>
		/// <param name="ex"></param>
		public void SetInfo(string[] messages)
		{
			this.ResetError();
			this.BuildView();

			// set...
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = FormatUserMessages(messages);
		}

		/// <summary>
		/// Clears any messages.
		/// </summary>
		/// <param name="ex"></param>
		public void Reset()
		{
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = string.Empty;

			// error...
			this.ResetError();
		}

		private void ResetError()
		{
			// clear the error...
			this.Error = null;
			this.SendFeedbackLabel.Text = null;
			this.SendReportButton.Enabled = true;
		}

		/// <summary>
		/// Gets or sets the exceptiontext.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>ExceptionText</c> for storage.
		/// </remarks>
		private WebError Error
		{
			get
			{
				object value = ViewState["Error"];
				if(value == null)
					return null;
				else
					return (WebError)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != Error)
				{
					// set the value...
					ViewState["Error"] = value;

					// show/hide?
					if(value != null)
						this.MoreInfoButton.Visible = true;
					else
						this.MoreInfoButton.Visible = false;

					// reset...
					this.SendFeedbackLabel.Text = null;
					this.SendReportButton.Enabled = true;

					// rebuild the view...
					this.BuildView();
				}
			}
		}

		/// <summary>
		/// Sets the label from the given exception message.
		/// </summary>
		/// <param name="label"></param>
		/// <param name="ex"></param>
		public static void SetLabel(Label label, Exception ex)
		{
			SetLabel(label, null, ex);
		}

		/// <summary>
		/// Sets the label from the given exception message.
		/// </summary>
		/// <param name="label"></param>
		/// <param name="ex"></param>
		public static void SetLabel(Label label, string message, Exception ex)
		{
			if(label == null)
				throw new ArgumentNullException("label");

			// error...
			if(ex != null)
			{
				// log...
				LogException(ex, null);
			
				// get the messages...
				string[] userMessages = ExceptionWithUserMessages.GetUserMessages(ex);
				if(userMessages == null)
					throw new InvalidOperationException("userMessages is null.");

				// buidler...
				label.Text = FormatUserMessages(userMessages);
			}
			else
				label.Text = ErrorNotProvidedText;
		}

		/// <summary>
		/// Formats user messages for display.
		/// </summary>
		/// <param name="userMessages"></param>
		/// <returns></returns>
		public static string FormatUserMessages(string[] userMessages)
		{
			return FormatUserMessages(null, userMessages);
		}

		/// <summary>
		/// Formats user messages for display.
		/// </summary>
		/// <param name="userMessages"></param>
		/// <returns></returns>
		public static string FormatUserMessages(string preamble, string[] userMessages)
		{
			if(userMessages == null)
				throw new ArgumentNullException("userMessages");
			
			// mbr - 19-05-2008 - added preamble...
			if(preamble != null && preamble.Length > 0)
			{
				// create a new one...
				string[] newUserMessages = new string[userMessages.Length + 1];
				newUserMessages[0] = preamble;
				Array.Copy(userMessages, 0, newUserMessages, 1, userMessages.Length);

				// swap...
				userMessages = newUserMessages;
			}

			// builder...
			StringBuilder builder = new StringBuilder();
			foreach(string userMessage in userMessages)
			{
				if(builder.Length > 0)
					builder.Append("<br />");

				// format...
				FormatUserMessage(builder, userMessage);
			}

			// return...
			return builder.ToString();
		}

		private static string FormatUserMessage(string userMessage)
		{
			StringBuilder builder = new StringBuilder();
			FormatUserMessage(builder, userMessage);

			// return...
			return builder.ToString();
		}

		private static void FormatUserMessage(StringBuilder builder, string userMessage)
		{
			if(builder == null)
				throw new ArgumentNullException("builder");
			if(userMessage == null)
				throw new ArgumentNullException("userMessage");
			
			// trim spurious whitespace off of the end...
			userMessage = userMessage.TrimEnd();

			// read it line by line so that we can properly format line breaks and tabs...
			bool first = true;
			using(StringReader reader = new StringReader(userMessage))
			{
				while(true)
				{
					string buf = reader.ReadLine();
					if(buf == null)
						break;

					// blank...
					if(first)
						first = false;
					else
						builder.Append("<br />");

					// format...
					int replaceWhitespace = 0;
					foreach(char c in buf)
					{	
						if(!(char.IsWhiteSpace(c)))
							break;

						// replace...
						replaceWhitespace++;
					}

					// replace?
					if(replaceWhitespace > 0)
					{
						for(int index = 0; index < replaceWhitespace; index++)
							builder.Append("&nbsp;");

						// add...
						buf = buf.Substring(replaceWhitespace);
					}

					// mbr - 14-05-2008 - encode...
					buf = HttpUtility.HtmlEncode(buf);

					// append it...
					builder.Append(buf);
				}
			}
		}

		/// <summary>
		/// Gets or sets the infocssclass
		/// </summary>
		internal static string DefaultInfoCssClass
		{
			get
			{
				return _defaultInfoCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultInfoCssClass)
				{
					// set the value...
					_defaultInfoCssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the errorcssclass
		/// </summary>
		internal static string DefaultErrorCssClass
		{
			get
			{
				return _defaultErrorCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultErrorCssClass)
				{
					// set the value...
					_defaultErrorCssClass = value;
				}
			}
		}

		/// <summary>
		/// Gets the exception text.
		/// </summary>
		private string ExceptionText
		{
			get
			{
				if(this.Error != null && this.Error.Exception != null)
					return this.Error.Exception.ToString();
				else
					return null;
			}
		}

		private void _moreInfoButton_Click(object sender, EventArgs e)
		{
			// mbr - 19-05-2008 - added...
			if(!(Runtime.Current.InstallationSettings.ShowExceptionDetailOnUi) || this.ParentBlocksMoreInfo)
				return;

			// flip it...
			string text = this.ExceptionText;
			if(text != null && text.Length > 0)
				this.SetError(this.ExceptionText);
			else
				this.SetError("The error information is no longer available.");

			// hide...
			this.MoreInfoButton.Visible = false;
		}

		private void _sendReportButton_Click(object sender, EventArgs e)
		{
			this.SendFeedbackLabel.Text = null;
			try
			{
				if (Error == null)
					throw new InvalidOperationException("Error is null.");
				if (Error.ErrorXml == null)
					throw new InvalidOperationException("'Error.ErrorXml' is null.");
				if (Error.ErrorXml.Length == 0)
					throw new InvalidOperationException("'Error.ErrorXml' is zero-length.");

				// doc...
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(this.Error.ErrorXml);

				// run...
				PhoneHomeResult result = PhoneHomeHelper.Current.SendErrorReport(Runtime.Current.PhoneHomeUrl, doc);
				if (result == null)
					throw new InvalidOperationException("result is null.");

				// ok...
				this.SendFeedbackLabel.CssClass = DefaultInfoCssClass;
				this.SendFeedbackLabel.Text = string.Format("The error report has been sent.  Your support ID is '{0}'.", result.SupportId);
			}
			catch (Exception ex)
			{
				const string message = "The error report could not be sent.";
				if (this.Log.IsWarnEnabled)
					this.Log.Warn(message, ex);

				// ok...
				this.SendFeedbackLabel.CssClass = DefaultErrorCssClass;
				this.SendFeedbackLabel.Text = message;
			}
			finally
			{
				this.SendReportButton.Enabled = false;
			}
		}

		/// <summary>
		/// Gets the sendFeedbackWidget.
		/// </summary>
		internal Label SendFeedbackLabel
		{
			get
			{
				if(_sendFeedbackLabel == null)
					_sendFeedbackLabel = new Label();
				return _sendFeedbackLabel;
			}
		}
		
		/// <summary>
		/// Gets the sendreportbutton.
		/// </summary>
		private Button SendReportButton
		{
			get
			{
				if(_sendReportButton == null)
				{
					_sendReportButton = new Button();
					_sendReportButton.Text = "Send an Error Report";
				}
				return _sendReportButton;
			}
		}

		/// <summary>
		/// Returns true if a message is being displayed.
		/// </summary>
		public bool IsShowingMessage
		{
			get
			{
				if(this.Error != null || (this.Label.Text != null && this.Label.Text.Length > 0))
					return true;
				else
					return false;
			}
		}
	}
}
