// BootFX - Application framework for .NET applications
// 
// File: UserMessageLabel.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Summary description for UserMessageLabel.
	/// </summary>
	public class UserMessageLabel : Control
	{
		/// <summary>
		/// Private field to support <c>ErrorCssClass</c> property.
		/// </summary>
		private static string _defaultErrorCssClass = "problem";

		/// <summary>
		/// Private field to support <c>InfoCssClass</c> property.
		/// </summary>
		private static string _defaultInfoCssClass = "info";
		
		/// <summary>
		/// Private field to support <see cref="Label"/> property.
		/// </summary>
		private Label _label;
		
		public UserMessageLabel()
		{
		}

		/// <summary>
		/// Gets the label.
		/// </summary>
		private Label Label
		{
			get
			{
				if(_label == null)
				{
					_label = new Label();
					this.Controls.Add(_label);
				}
				return _label;
			}
		}

		public void SetError(Exception ex)
		{
			this.Label.CssClass = DefaultErrorCssClass;
			SetLabel(this.Label, ex);
		}

		public void SetError(string message)
		{
			this.Label.CssClass = DefaultErrorCssClass;
			this.Label.Text = FormatUserMessage(message);
		}

		public void SetError(string[] messages)
		{
			this.Label.CssClass = DefaultErrorCssClass;
			this.Label.Text = FormatUserMessages(messages);
		}

		public void SetInfo(string message)
		{
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = FormatUserMessage(message);
		}

		public void SetInfo(string[] messages)
		{
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = FormatUserMessages(messages);
		}

		public void Clear()
		{
			this.Label.CssClass = DefaultInfoCssClass;
			this.Label.Text = string.Empty;
		}

		/// <summary>
		/// Sets the label from the given exception message.
		/// </summary>
		/// <param name="label"></param>
		/// <param name="ex"></param>
		public static void SetLabel(Label label, Exception ex)
		{
			if(label == null)
				throw new ArgumentNullException("label");
			if(ex == null)
				throw new ArgumentNullException("ex");
			
			// get the messages...
			string[] userMessages = ExceptionWithUserMessages.GetUserMessages(ex);
			if(userMessages == null)
				throw new InvalidOperationException("userMessages is null.");

			// buidler...
			label.Text = FormatUserMessages(userMessages);
		}

		/// <summary>
		/// Formats user messages for display.
		/// </summary>
		/// <param name="userMessages"></param>
		/// <returns></returns>
		public static string FormatUserMessages(string[] userMessages)
		{
			if(userMessages == null)
				throw new ArgumentNullException("userMessages");
			
			// builder...
			StringBuilder builder = new StringBuilder();
			foreach(string userMessage in userMessages)
			{
				if(builder.Length > 0)
					builder.Append("<br />");

				// format...
				FormatUserMessage(builder, userMessage);
			}

			// return...
			return builder.ToString();
		}

		private static string FormatUserMessage(string userMessage)
		{
			StringBuilder builder = new StringBuilder();
			FormatUserMessage(builder, userMessage);

			// return...
			return builder.ToString();
		}

		private static void FormatUserMessage(StringBuilder builder, string userMessage)
		{
			if(builder == null)
				throw new ArgumentNullException("builder");
			if(userMessage == null)
				throw new ArgumentNullException("userMessage");
			
			// format...
			int replaceWhitespace = 0;
			foreach(char c in userMessage)
			{	
				if(!(char.IsWhiteSpace(c)))
					break;

				// replace...
				replaceWhitespace++;
			}

			// replace?
			if(replaceWhitespace > 0)
			{
				for(int index = 0; index < replaceWhitespace; index++)
					builder.Append("&nbsp;");

				// add...
				userMessage = userMessage.Substring(replaceWhitespace);
			}

			// append it...
			builder.Append(userMessage);
		}

		/// <summary>
		/// Gets or sets the infocssclass
		/// </summary>
		internal static string DefaultInfoCssClass
		{
			get
			{
				return _defaultInfoCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultInfoCssClass)
				{
					// set the value...
					_defaultInfoCssClass = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the errorcssclass
		/// </summary>
		internal static string DefaultErrorCssClass
		{
			get
			{
				return _defaultErrorCssClass;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultErrorCssClass)
				{
					// set the value...
					_defaultErrorCssClass = value;
				}
			}
		}
	}
}
