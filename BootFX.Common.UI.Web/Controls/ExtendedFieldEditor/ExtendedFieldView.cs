// BootFX - Application framework for .NET applications
// 
// File: ExtendedFieldView.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Globalization;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.UI.Web;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ExtendedFieldView</c>.
	/// </summary>
	public class ExtendedFieldView : LoggableTable, IEntityType, INamingContainer
	{
		/// <summary>
		/// Raised when changes are about to be applied.
		/// </summary>
		public event EventHandler Applying;

		/// <summary>
		/// Raised when changes are applied.
		/// </summary>
		public event EventHandler Applied;
		
		/// <summary>
		/// Private field to support <see cref="HasEntityType"/> property.
		/// </summary>
		private bool _hasEntityType = false;
		
		/// <summary>
		/// Private field to support <c>ViewTable</c> property.
		/// </summary>
		private Table _viewTable;
		
		/// <summary>
		/// Private field to support <c>EntityType</c> property.
		/// </summary>
		private IFlexFieldProvider _flexFieldProvider;
		
		/// <summary>
		/// Private field to support <see cref="Editors"/> property.
		/// </summary>
		private Lookup _editors;

		/// <summary>
		/// Constructor.
		/// </summary>
		public ExtendedFieldView()
		{
			// editors...
			_editors = new Lookup();
			_editors.CreateItemValue += new CreateLookupItemEventHandler(_editors_CreateItemValue);

			// mbr - 08-12-2005 - width...
			this.Width = new Unit(100, UnitType.Percentage);
		}

		/// <summary>
		/// Gets and sets the type for this lookup edit
		/// </summary>
		public Type Type
		{
			get
			{
				return (Type) ViewState["Type"];	
			}
			set
			{
				if(ViewState["Type"] == value)
					return;

				ViewState["Type"] = value;

				// mbr - 31-07-2006 - reset...
//				_entityType = null;
				_flexFieldProvider = null;
				_hasEntityType = false;
			}
		}

		/// <summary>
		/// Gets the Entity Type for this Lookup View
		/// </summary>
		public IFlexFieldProvider FlexFieldProvider
		{
			get
			{
				if(_flexFieldProvider == null)
				{
					// reset...
					_hasEntityType = false;

					// check...
					if(Type == null)
						throw new InvalidOperationException("Type is null.");

					// try and create us from an entity...
					_flexFieldProvider = EntityType.GetEntityType(this.Type, OnNotFound.ReturnNull);
					if(_flexFieldProvider == null)
					{
						_flexFieldProvider =  this.GetFlexFieldProvider();
						if(_flexFieldProvider == null)
							throw new InvalidOperationException("_flexFieldProvider is null.");
					}
					else
						_hasEntityType = true;
				}
				return _flexFieldProvider;
			}
		}

		internal virtual IFlexFieldProvider GetFlexFieldProvider()
		{
			throw new InvalidOperationException("Flex data providers are not supported on the 'view' control.   Use the 'edit' control instead.");
		}

		/// <summary>
		/// Gets the Entity Type for this Lookup View
		/// </summary>
		public BootFX.Common.Entities.EntityType EntityType
		{
			get
			{
//				if(_entityType == null)
//				{
//					// Get the entity type
//					_entityType = BootFX.Common.Entities.EntityType.GetEntityType(Type,OnNotFound.ReturnNull);
//				}
//
//				return _entityType;

				// check...
				if(FlexFieldProvider == null)
					throw new InvalidOperationException("FlexFieldProvider is null.");
				if(!(this.FlexFieldProvider is EntityType))
					throw new InvalidOperationException(string.Format("Flex metadata provider of type '{0}' is not EntityType.", this.FlexFieldProvider.GetType()));
				return (EntityType)this.FlexFieldProvider;
			}
		}

		/// <summary>
		/// Gets or sets the read only state for the control
		/// </summary>
		[Bindable(true), DefaultValue(false)]
		public virtual bool ReadOnly
		{
			get
			{
				object readOnly = this.ViewState["ReadOnly"];
				if (readOnly != null)
				{
					return (bool) readOnly;
				}
				return false;
			}
			set
			{
				this.ViewState["ReadOnly"] = value;
				SetReadOnly();
			}
		}
		
		protected override object SaveViewState()
		{		
			foreach(DictionaryEntry entry in this.Editors)
			{
				// mbr - 04-12-2005 - add the results to the bin...
				IFlexField field = (IFlexField)entry.Key;
				Control editor = ((ControlPair)entry.Value).Editor;

				// check...
				if(editor is IConstraintControl)
				{
					// set?
					IConstraintControl cc = (IConstraintControl)editor;
					if(cc.IsValueSet)
						ViewState[editor.ID] = cc.Value;
				}
				else
				{
					// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
					throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
				}
			}
			
			return base.SaveViewState ();
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState (savedState);
			
			EnsureChildControls();
			
			foreach(DictionaryEntry entry in this.Editors)
			{
				// mbr - 04-12-2005 - add the results to the bin...
				IFlexField field = (IFlexField)entry.Key;
				Control editor = ((ControlPair)entry.Value).Editor;

				// check...
				if(editor is IConstraintControl)
				{
					// set?
					IConstraintControl cc = (IConstraintControl)editor;
					if(ViewState[editor.ID] != null)
						cc.Value = ViewState[editor.ID];
				}
				else
				{
					// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
					throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
				}
			}
		}


		/// <summary>
		/// Refresh the main lists of each control
		/// </summary>
		protected override void CreateChildControls()
		{
			// mbr - 31-07-2006 - changed.
//			if(Type == null || EntityType == null)
			if(Type == null || FlexFieldProvider == null)
				return;

			// clear...
			this.Rows.Clear();

			// mbr - 08-12-2005 - create a content table...
			_viewTable = new Table();
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(_viewTable);

			// mbr - 31-07-2006 - changed.
			foreach(IFlexField field in this.FlexFieldProvider.GetFlexFields())
			{
				// get the editor...
				ControlPair pair = (ControlPair)Editors[field];
				if(pair == null)
					throw new InvalidOperationException("pair is null.");
				if(pair.Editor == null)
					throw new InvalidOperationException("pair.Editor is null.");

				// note create a row for the editor and add it to the table...
				row = new TableRow();
				_viewTable.Rows.Add(row);

				// caption...
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.CssClass = "captioncontrol";
				cell.Text = string.Format("{0}:&nbsp;", field.Name);
				cell.VerticalAlign = VerticalAlign.Top;
				cell.Style["padding-top"] = "3px";

				// control...
				cell = new TableCell();
				row.Cells.Add(cell);
				cell.Controls.Add(pair.Editor);

				// mbr - 19-03-2006 - do we have a hint...
				if(pair.Hint != null && pair.Hint.FormatHint != null && pair.Hint.FormatHint.Length > 0)
				{
					Label label = new Label();
					label.CssClass = "smallbodytext";
					label.Text = "&nbsp;" + pair.Hint.FormatHint;

					// add...
					cell.Controls.Add(label);
				}

				// mbr - 11-09-2006 - required?
				if(field.IsRequired)
				{
					Label label = new Label();
					label.CssClass = "smallbodytext";
					label.Text = "&nbsp;*";

					// add...
					cell.Controls.Add(label);
				}
			}

			// mbr - 08-12-2005 - create the other stuff...
			this.AddFooter();

			// Set the read only state for the controls
			SetReadOnly();
		}

		protected virtual void AddFooter()
		{
			
		}

		/// <summary>
		/// Gets the viewtable.
		/// </summary>
		private Table ViewTable
		{
			get
			{
				// returns the value...
				return _viewTable;
			}
		}

		/// <summary>
		/// Sets the read only state for the controls
		/// </summary>
		private void SetReadOnly()
		{
			SetReadOnlyWalkControls(Controls);

		}

		/// <summary>
		/// Walks all the controls setting the readonly
		/// </summary>
		/// <param name="controls"></param>
		private void SetReadOnlyWalkControls(ControlCollection controls)
		{
			foreach(Control control in controls)
			{
				if(control is ConstraintTextBox)
					((ConstraintTextBox)control).ReadOnly = ReadOnly;
				else if(control is ConstraintCheckBox)
					((ConstraintCheckBox)control).Enabled = !ReadOnly;

				SetReadOnlyWalkControls(control.Controls);
			}
		}

		/// <summary>
		/// Gets the editors.
		/// </summary>
		protected Lookup Editors
		{
			get
			{
				return _editors;
			}
		}

		// mbr - 15-12-2005 - not needed as the controls will do this.		
		//		public void MangleFilter(SqlFilter filter)
		//		{
		//			if(filter == null)
		//				throw new ArgumentNullException("filter");
		//			
		//			// walk...
		//			IDictionary values = this.GetValues();
		//			if(values == null)
		//				throw new InvalidOperationException("values is null.");
		//
		//			// walk...
		//			foreach(DictionaryEntry entry in values)
		//			{
		//				EntityField field = (EntityField)entry.Key;	
		//				object value = entry.Value;
		//
		//				// set...
		//				filter.Constraints.Add(field, value);
		//			}
		//		}
		//
		//		public void SerializeSqlSearchSettings(IDictionary bag)
		//		{
		//			if(bag == null)
		//				throw new ArgumentNullException("bag");
		//			
		//			// create...
		//			IDictionary values = this.GetValues();
		//			if(values == null)
		//				throw new InvalidOperationException("values is null.");
		//
		//			// walk...
		//			foreach(DictionaryEntry entry in values)
		//				bag[entry.Key] = entry.Value;
		//		}
		//
		//		public void DeserializeSqlSearchSettings(IDictionary bag)
		//		{
		//			if(bag == null)
		//				throw new ArgumentNullException("bag");
		//
		//			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
		//			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		//		}

		private void _editors_CreateItemValue(object sender, CreateLookupItemEventArgs e)
		{
			IFlexField field = (IFlexField) e.Key;

			// lookup?
			LayoutHint hint = null;
			IConstraintControl constraintControl = ConstraintControlHelper.GetConstraintControlForField(field, this.Mode, ref hint);

			// setup...
			Control control = (Control)constraintControl;
			if(control is TextBox)
				((TextBox)control).CssClass = "textbox";
			else if(control is CheckBox)
				((TextBox)control).CssClass = "check";
			else if(control is WebControl)
				((WebControl)control).CssClass = "field";

			// set...
			ControlPair pair = new ControlPair();
			pair.Editor = control;
			pair.Hint = hint;
			e.NewValue = pair;

			// defer...
			EditorCreated(field, control);
		}

		internal virtual void EditorCreated(IFlexField field, Control editor)
		{
		}

		/// <summary>
		/// Gets the values from the editors.
		/// </summary>
		/// <returns></returns>
		public void Validate(FormChecker checker)
		{
			if(checker == null)
				throw new ArgumentNullException("checker");
			
			// defer...
			this.GetValuesInternal(true, checker);
		}

		/// <summary>
		/// Gets the values from the editors.
		/// </summary>
		/// <returns></returns>
		internal IDictionary GetValues(bool includeUnsetFields)
		{
			return this.GetValuesInternal(includeUnsetFields, null);
		}

		private object ValidateValue(IFlexField field, object value, FormChecker checker)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			if(value == null)
				throw new ArgumentNullException("value");
			
			// try it...
			CultureInfo culture = Cultures.User;
			try
			{
				value = ConversionHelper.ChangeType(value, field.Type, culture);
			}
			catch(Exception ex)
			{
				if(this.Log.IsWarnEnabled)
					this.Log.Warn(string.Format("Failed to convert '{0}' to '{1}' (aka '{2}').", value, field.DBType, field.Type), ex);

				// checker...
				StringBuilder builder = new StringBuilder();
				if(checker != null)
				{
					builder.Append(field.Name);
					builder.Append(": the value '");
					builder.Append(value);
					builder.Append("' ");

					// switch...
					if(field.DBType == DbType.DateTime)
						builder.Append("is not a valid date/time.");
					else if(field.DBType == DbType.Date)
						builder.Append("is not a valid date.");
					else if(field.DBType == DbType.Time)
						builder.Append("is not a valid time.");
					else
						builder.Append("is invalid -> " + ex.Message);
				}

				if(checker != null)
					checker.AddError(builder.ToString());
				else
					throw new InvalidOperationException(builder.ToString(), ex);
			}

			// return...
			return value;
		}

		/// <summary>
		/// Gets the values from the editors.
		/// </summary>
		/// <returns></returns>
		internal IDictionary GetValuesInternal(bool includeUnsetFields, FormChecker checker)
		{
			IDictionary results = new HybridDictionary();
			foreach(DictionaryEntry entry in this.Editors)
			{
				// mbr - 04-12-2005 - add the results to the bin...
				IFlexField field = (IFlexField)entry.Key;
				Control editor = ((ControlPair)entry.Value).Editor;

				// check...
				if(editor is IConstraintControl)
				{
					// set?
					IConstraintControl cc = (IConstraintControl)editor;
					if(cc.IsValueSet)
					{
						// get...
						object value = cc.Value;

						// mbr - 11-09-2006 - validate...
						value = this.ValidateValue(field, value, checker);

						// set...
						results[field] = value;
					}
					else if(includeUnsetFields)
					{
						// mbr - 11-09-2006 - required...
						if(field.IsRequired)
						{
							// are we validating?
							string message = string.Format("{0}: a value is required for this field.", field.Name);
							if(checker != null)
								checker.AddError(message);
							else
								throw new InvalidOperationException(message);
						}

						// check...
						results[field] = DBNull.Value;
					}
				}
				else
				{
					// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
					throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
				}
			}

			// return...
			return results;
		}

		/// <summary>
		/// Raises the <c>Applied</c> event.
		/// </summary>
		private void OnApplied()
		{
			OnApplied(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>Applied</c> event.
		/// </summary>
		protected virtual void OnApplied(EventArgs e)
		{
			// raise...
			if(Applied != null)
				Applied(this, e);
		}
		
		/// <summary>
		/// Raises the <c>Applying</c> event.
		/// </summary>
		private void OnApplying()
		{
			OnApplying(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>Applying</c> event.
		/// </summary>
		protected virtual void OnApplying(EventArgs e)
		{
			// raise...
			if(Applying != null)
				Applying(this, e);
		}

		/// <summary>
		/// Applies values in the control to the entity.
		/// </summary>
		/// <param name="entity"></param>
		public void Apply(object entity)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");

			// set...
			this.OnApplying();

			// get...
			EntityType et = EntityType.GetEntityType(entity, OnNotFound.ThrowException);
			if(et == null)
				throw new InvalidOperationException("entity type is null.");
			
			// walk...
			foreach(DictionaryEntry entry in this.Editors)
			{
				// mbr - 04-12-2005 - add the results to the bin...
				IFlexField field = (IFlexField)entry.Key;
				Control editor = ((ControlPair)entry.Value).Editor;

				// check...
				if(editor is IConstraintControl)
				{
					// set?
					IConstraintControl cc = (IConstraintControl)editor;
					if(cc.IsValueSet)
					{
						// mbr - 31-07-2006 - changed.
//						et.Storage.SetValue(entity, field, cc.Value, SetValueReason.UserSet);
						field.SetValue(entity, cc.Value, SetValueReason.UserSet);
					}
					else
					{
						// mbr - 31-07-2006 - changed.
//						et.Storage.SetDBNull(entity, field);
						field.SetValue(entity, DBNull.Value, SetValueReason.UserSet);
					}
				}
				else
				{
					// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
					throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
				}
			}

			// applies...
			this.OnApplied();
		}

		protected class ControlPair
		{
			internal Control Editor;
			internal LayoutHint Hint;
		}

		/// <summary>
		/// Gets or sets the mode.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>Mode</c> for storage.
		/// </remarks>
		// mbr - 19-05-2006 - made public.		
		public ExtendedFieldViewMode Mode
		{
			get
			{
				object value = ViewState["Mode"];
				if(value == null)
					return this.DefaultMode;
				else
					return (ExtendedFieldViewMode)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != Mode)
				{
					// set the value...
					ViewState["Mode"] = value;

					// mbr - 01-05-2006 - reset...
					_editors.Clear();
				}
			}
		}

		protected virtual ExtendedFieldViewMode DefaultMode
		{
			get
			{
				return ExtendedFieldViewMode.View;
			}
		}

		/// <summary>
		/// Gets the hasentitytype.
		/// </summary>
		internal bool HasEntityType
		{
			get
			{
				return _hasEntityType;
			}
		}
	}
}
