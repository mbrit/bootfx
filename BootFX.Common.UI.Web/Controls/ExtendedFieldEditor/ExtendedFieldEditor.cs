// BootFX - Application framework for .NET applications
// 
// File: ExtendedFieldEditor.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using BootFX.Common.UI.Web;
using BootFX.Common.Data;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>ExtendedFieldView</c>.
	/// </summary>
	public class ExtendedFieldEditor : ExtendedFieldView
	{
		/// <summary>
		/// Called when the entity needs to be jit loaded.
		/// </summary>
		public event JitEntityEventHandler JitLoadEntity;
		
		/// <summary>
		/// Private field to support <c>ProblemLabel</c> property.
		/// </summary>
		private Label _problemLabel;

		/// <summary>
		/// Private field to support <c>InfoLabel</c> property.
		/// </summary>
		private Label _infoLabel;
		
		/// <summary>
		/// Private field to support <see cref="SaveChangesButton"/> property.
		/// </summary>
		private Button _saveChangesButton;
		
		/// <summary>
		/// Private field to support <c>Entity</c> property.
		/// </summary>
		private object _entity = null;

		/// <summary>
		/// Gets and sets the entity for this lookup edit
		/// </summary>
		public object Entity
		{
			get
			{
				return _entity;	
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				
				if(_entity != value)
				{
					_entity = value;

					// set...
					this.Type = _entity.GetType();

					// update...
					if(!(EntityBound))
						UpdateBindings();
				}
			}
		}

		private void EnsureEntityLoaded()
		{
			if(_entity != null)
				return;

			// call...
			JitEntityEventArgs e = new JitEntityEventArgs();
			this.OnJitLoadEntity(e);

			// check...
			if(e.Entity == null)
				throw new InvalidOperationException("Entity is still null, even though a JIT request was called.");

			// set...
			_entity = e.Entity;
		}

		internal override IFlexFieldProvider GetFlexFieldProvider()
		{
			this.EnsureEntityLoaded();
			 if(Entity == null)
			 	throw new InvalidOperationException("Entity is null.");

			// check...
			if(!(this.Entity is IFlexFieldProvider))
				throw new InvalidOperationException(string.Format("An object of type '{0}' does not support IFlexFieldProvider.", this.Entity.GetType()));

			// return...
			return (IFlexFieldProvider)this.Entity;
		}

		/// <summary>
		/// Raises the <c>JitLoadEntity</c> event.
		/// </summary>
		protected virtual void OnJitLoadEntity(JitEntityEventArgs e)
		{
			// raise...
			if(JitLoadEntity != null)
				JitLoadEntity(this, e);
		}

		/// <summary>
		/// Gets or sets whether the entity has been bound
		/// </summary>
		private bool EntityBound
		{
			get
			{
				if(ViewState["EntityBound"] == null)
					return false;

				return (bool) ViewState["EntityBound"];
			}
			set
			{
				ViewState["EntityBound"] = value;	
			}
		}

		/// <summary>
		/// Saves the changes.
		/// </summary>
		public bool SaveChanges()
		{
			this.OnApplying(EventArgs.Empty);

			// mbr - 10-10-2007 - needed ensure call...
			this.EnsureEntityLoaded();

			// check...
			if(Entity == null)
				throw new InvalidOperationException("Entity is null.");

			// mbr - 11-09-2006 - added validation to this...		
			FormChecker checker = new FormChecker();
			IDictionary values = this.GetValuesInternal(true, checker);
			if(values == null)
				throw new InvalidOperationException("values is null.");

			// do we have errors?
			if(checker.HasErrors)
			{
				this.ProblemLabel.Text = checker.GetAllErrorsSeparatedByBrTag();
				return false;
			}

			// log...
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Setting '{0}' extended props on '{1}' ({2})...", values.Count, this.Entity, this.FlexFieldProvider));

			// walk and set...
			foreach(DictionaryEntry entry in values)
			{
				IFlexField field = (IFlexField)entry.Key;
				object value = entry.Value;

				// set...
				// mbr - 17-03-2006 - handle dbnull...
				if(value is DBNull)
				{
					if(this.Log.IsInfoEnabled)
						this.Log.Info(string.Format("\t{0}: (db null)", field.Name));

					// mbr - 31-07-2006 - changed.					
//					this.EntityType.Storage.SetDBNull(this.Entity, field);
					field.SetValue(this.Entity, DBNull.Value, SetValueReason.UserSet);
				}
				else
				{
					if(this.Log.IsInfoEnabled)
						this.Log.Info(string.Format("\t{0}: {1}", field.Name, value));

					// mbr - 19-03-2006 - if we have a value, do a user context conversion to the field's type...
					value = ConversionHelper.ChangeType(value, field.Type, Cultures.User);

					// mbr - 31-07-2006 - changed.					
//					this.EntityType.Storage.SetValue(this.Entity, field, value, SetValueReason.UserSet);
					field.SetValue(this.Entity, value, SetValueReason.UserSet);
				}
			}

			// save...
			if(this.HasEntityType)
				this.EntityType.Persistence.SaveChanges(this.Entity);

			// applied...
			this.OnApplied(EventArgs.Empty);

			// ok...
			return true;
		}

		internal override void EditorCreated(IFlexField field, Control editor)
		{
			base.EditorCreated (field, editor);

			UpdateBindings(field, editor);
		}

		/// <summary>
		/// Called when the entity is set
		/// </summary>
		internal void UpdateBindings()
		{
			if(Editors.Count == 0)
				EnsureChildControls();

			EntityBound = true;
			foreach(DictionaryEntry entry in Editors)
			{
				// mbr - 04-12-2005 - add the results to the bin...
				IFlexField field = (IFlexField)entry.Key;
				Control editor = ((ControlPair)entry.Value).Editor;

				UpdateBindings(field,editor);
			}
			
		}

		/// <summary>
		/// Updates the bindings for a specific field and editor
		/// </summary>
		/// <param name="field"></param>
		/// <param name="editor"></param>
		internal void UpdateBindings(IFlexField field, Control editor)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			if(editor == null)
				throw new ArgumentNullException("editor");

			if(Entity == null)
				return;

			// mbr - 17-03-2006 - changed this to allow the editors to be given and handle DB null values.  (required for 
			// boolean extended props.)

			try
			{
				// web...
				IConstraintControl webEditor = editor as IConstraintControl;

				// get it...
				object value = null;
				if(webEditor != null)
				{
					ConstraintControlSupportFlags flags = webEditor.SupportFlags;
					if((int)(flags & ConstraintControlSupportFlags.SupportsDbNull) != 0)
					{
						// check...
						if(FlexFieldProvider == null)
							throw new InvalidOperationException("FlexFieldProvider is null.");

						// mbr - 11-09-2006 - et?						
						if(this.FlexFieldProvider is EntityType)
						{
							if(EntityType == null)
								throw new InvalidOperationException("EntityType is null.");

							// check...
							if(!(field is EntityField))
								throw new NotSupportedException(string.Format("Field must be of type 'EntityField', not '{0}'.", field.GetType()));

							// are we null?
							if(this.EntityType.Storage.IsDBNull(this.Entity, (EntityField)field))
								value = DBNull.Value;
							else
								value = field.GetValue(this.Entity);
						}
						else
							value = field.GetValue(this.Entity);
					}
					else
						value = field.GetValue(this.Entity);
				}
				else
					value = field.GetValue(this.Entity);

				// check...
				if(editor is IConstraintControl)
					((IConstraintControl)editor).Value = value;
				else
					throw new NotSupportedException(string.Format("Cannot handle '{0}'.", editor));
			}
			catch(Exception ex)
			{
				throw new InvalidOperationException(string.Format("Failed when updating bindings for '{0}' ({1}, DB type: {2}, type: {3}).", field.Name, field.GetType(), field.DBType, field.Type), ex);
			}
		}

		protected override void AddFooter()
		{
			base.AddFooter ();

			// row...
			TableRow row = new TableRow();
			this.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Height = new Unit(10, UnitType.Pixel);

			// save...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.SaveChangesButton);

			// problems...
			row = new TableRow();
			this.Rows.Add(row);
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(this.ProblemLabel);
			cell.Controls.Add(this.InfoLabel);
		}

		/// <summary>
		/// Gets the infolabel.
		/// </summary>
		private Label InfoLabel
		{
			get
			{
				// returns the value...
				if(_infoLabel == null)
					_infoLabel = this.CreateLabel("info");
				return _infoLabel;
			}
		}
		
		/// <summary>
		/// Gets the problemlabel.
		/// </summary>
		private Label ProblemLabel
		{
			get
			{
				// returns the value...
				if(_problemLabel == null)
					_problemLabel = this.CreateLabel("problem");
				return _problemLabel;
			}
		}

		private Label CreateLabel(string cssClass)
		{
			Label label = new Label();
			label.CssClass = cssClass;
			return label;
		}

		/// <summary>
		/// Gets the savechangesbutton.
		/// </summary>
		internal Button SaveChangesButton
		{
			get
			{
				if(_saveChangesButton == null)
				{
					_saveChangesButton = new Button();
					_saveChangesButton.Text = "Save Changes";
					_saveChangesButton.CssClass = "button";
					_saveChangesButton.Click += new EventHandler(_saveChangesButton_Click);
				}
				return _saveChangesButton;
			}
		}

		private void _saveChangesButton_Click(object sender, EventArgs e)
		{
			try
			{
				this.ProblemLabel.Text = string.Empty;
				if(this.SaveChanges())
					this.InfoLabel.Text = "Your changes have been saved.";
				else
					this.InfoLabel.Text = string.Empty;
			}
			catch(Exception ex)
			{
				this.ProblemLabel.Text = string.Format("Your changes could not be saved.<br>&nbsp;&nbsp;&nbsp;-&gt; {0}", ex.ToString().Replace("\n", "<BR />"));
				return;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// clear...
			this.ProblemLabel.Text = string.Empty;
			this.InfoLabel.Text = string.Empty;
		}

		protected override ExtendedFieldViewMode DefaultMode
		{
			get
			{
				return ExtendedFieldViewMode.Edit;
			}
		}
	}
}
