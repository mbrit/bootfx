// BootFX - Application framework for .NET applications
// 
// File: JitEntityEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.UI.Web
{
	public delegate void JitEntityEventHandler(object sender, JitEntityEventArgs e);

	/// <summary>
	/// Summary description for JitEntityEventArgs.
	/// </summary>
	public class JitEntityEventArgs
	{
		/// <summary>
		/// Private field to support <c>Entity</c> property.
		/// </summary>
		private object _entity;
		
		public JitEntityEventArgs()
		{
		}

		/// <summary>
		/// Gets or sets the entity
		/// </summary>
		public object Entity
		{
			get
			{
				return _entity;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _entity)
				{
					// set the value...
					_entity = value;
				}
			}
		}
	}
}
