// BootFX - Application framework for .NET applications
// 
// File: DatePicker.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Collections;
using System.ComponentModel;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>DatePicker</c>.
	/// </summary>
	public class DatePicker : LoggableTable
	{
		/// <summary>
		/// Raised when the <c>VisibleDates</c> property has changed.
		/// </summary>
		[Browsable(true), Category("Property Changed"), Description("Raised when the VisibleDates property has changed.")]
		public event EventHandler VisibleDatesChanged;
		
		/// <summary>
		/// Raised when the <c>Dates</c> property has changed.
		/// </summary>
		[Browsable(true), Category("Property Changed"), Description("Raised when the Dates property has changed.")]
		public event EventHandler DatesChanged;
		
		/// <summary>
		/// Private field to support <c>SelectionChangeCount</c> property.
		/// </summary>
		private int _selectionChangeCount;
		
		/// <summary>
		/// Private field to support <c>From</c> property.
		/// </summary>
		private Calendar _fromCalendar;

		/// <summary>
		/// Private field to support <c>To</c> property.
		/// </summary>
		private Calendar _toCalendar;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public DatePicker()
		{
		}

		/// <summary>
		/// Gets the to.
		/// </summary>
		private Calendar ToCalendar
		{
			get
			{
				// returns the value...
				if(_toCalendar == null)
				{
					_toCalendar = this.CreateCalendar();

					// set...
					if(this.StoredTo != DateTime.MinValue)
						_toCalendar.SelectedDate = this.StoredTo;

					// subscribe...
					_toCalendar.SelectionChanged += new EventHandler(_toCalendar_SelectionChanged);
					_toCalendar.VisibleMonthChanged += new MonthChangedEventHandler(_toCalendar_VisibleMonthChanged);
				}
				return _toCalendar;
			}
		}
		
		/// <summary>
		/// Gets the from.
		/// </summary>
		private Calendar FromCalendar
		{
			get
			{
				// returns the value...
				if(_fromCalendar == null)
				{
					_fromCalendar = this.CreateCalendar();

					// set...
					if(this.StoredFrom != DateTime.MinValue)
						_fromCalendar.SelectedDate = this.StoredFrom;

					// subscribe...
					_fromCalendar.SelectionChanged += new EventHandler(_fromCalendar_SelectionChanged);
					_fromCalendar.VisibleMonthChanged += new MonthChangedEventHandler(_fromCalendar_VisibleMonthChanged);
				}
				return _fromCalendar;
			}
		}

		private Calendar CreateCalendar()
		{
//			<asp:calendar id="calendarDateTime" Runat="server" BackColor="White" Width="200px" DayNameFormat="FirstLetter"
//											ForeColor="Black" Height="180px" Font-Size="8pt" Font-Names="Verdana" BorderColor="#999999"
//											CellPadding="4">
//											<TodayDayStyle ForeColor="Black" BackColor="#CCCCCC"></TodayDayStyle>
//											<SelectorStyle BackColor="#CCCCCC"></SelectorStyle>
//											<NextPrevStyle VerticalAlign="Bottom"></NextPrevStyle>
//											<DayHeaderStyle Font-Size="7pt" Font-Bold="True" BackColor="#CCCCCC"></DayHeaderStyle>
//											<SelectedDayStyle Font-Bold="True" ForeColor="White" BackColor="#666666"></SelectedDayStyle>
//											<TitleStyle Font-Bold="True" BorderColor="Black" BackColor="#999999"></TitleStyle>
//											<WeekendDayStyle BackColor="#FFFFCC"></WeekendDayStyle>
//											<OtherMonthDayStyle ForeColor="#808080"></OtherMonthDayStyle>
//										</asp:calendar>

			Calendar calendar = new Calendar();

			// setup...
			calendar.BackColor = Color.White;
			calendar.ForeColor = Color.Black;
			calendar.BorderColor = Color.FromArgb(0x99, 0x99, 0x99);
			calendar.Width = new Unit(200, UnitType.Pixel);
			calendar.Height = new Unit(180, UnitType.Pixel);
			calendar.DayNameFormat = DayNameFormat.FirstLetter;
			calendar.Font.Size = 8;
			calendar.Font.Name = "Verdana";
			calendar.CellPadding = 4;

			// today...

			// return...
			return calendar;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// refresh...
			this.RebuildView();
		}

		private void RebuildView()
		{
			this.Rows.Clear();

			// add...
			TableRow row = new TableRow();
			this.Rows.Add(row);

			// cell...
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.VerticalAlign = VerticalAlign.Top;
			cell.Controls.Add(this.FromCalendar);

			// another...
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.VerticalAlign = VerticalAlign.Top;
			cell.Controls.Add(this.ToCalendar);

			// links...
			cell = new TableCell();
			row.Cells.Add(cell);
			cell.VerticalAlign = VerticalAlign.Top;
			cell.Controls.Add(this.GetLinksTable());
		}

		private Table GetLinksTable()
		{
			Table table = new Table();
			table.CellSpacing = 0;
			table.CellPadding = 0;

			// las tmonth...
			LinkButton lastMonth = this.GetButton(table, "Last month");
			lastMonth.Click += new EventHandler(lastMonth_Click);

			// this month...
			LinkButton thisMonth = this.GetButton(table, "This month");
			thisMonth.Click += new EventHandler(thisMonth_Click);

			// this week...
			LinkButton thisWeek = this.GetButton(table, "This week");
			thisWeek.Click += new EventHandler(thisWeek_Click);

			// yesterday...
			LinkButton yesterday = this.GetButton(table, "Yesterday");
			yesterday.Click += new EventHandler(yesterday_Click);

			// today...
			LinkButton today = this.GetButton(table, "Today");
			today.Click += new EventHandler(today_Click);

			// return...
			return table;
		}

		private LinkButton GetButton(Table table, string caption)
		{
			if(table == null)
				throw new ArgumentNullException("table");
			if(caption == null)
				throw new ArgumentNullException("caption");
			if(caption.Length == 0)
				throw new ArgumentOutOfRangeException("'caption' is zero-length.");
			
			// get...
			LinkButton button = new LinkButton();
			button.Text = caption;
			button.CssClass = "link";

			// add...
			TableRow row = new TableRow();
			table.Rows.Add(row);
			TableCell cell = new TableCell();
			row.Cells.Add(cell);
			cell.Controls.Add(button);

			// return...
			return button;
		}

		/// <summary>
		/// Gets or sets the from.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>From</c> for storage.
		/// </remarks>
		public DateTime From
		{
			get
			{
				DateTime from = this.FromCalendar.SelectedDate;
				if(this.MidnightToMidnight && from != DateTime.MinValue)
					return new DateTime(from.Year, from.Month, from.Day, 0,0,0);
				else
					return from;
			}
			set
			{
				this.SelectionChangeCount++;
				try
				{
					this.FromCalendar.SelectedDate = value;
					this.FromCalendar.VisibleDate = value;
				}
				finally
				{
					this.SelectionChangeCount--;
				}
			}
		}

		/// <summary>
		/// Gets or sets the To.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>To</c> for storage.
		/// </remarks>
		public DateTime To
		{
			get
			{
				DateTime to = this.ToCalendar.SelectedDate;
				if(this.MidnightToMidnight && to != DateTime.MinValue)
					return new DateTime(to.Year, to.Month, to.Day, 23, 59, 59, 999);
				else
					return to;
			}
			set
			{
				this.SelectionChangeCount++;
				try
				{
					this.ToCalendar.SelectedDate = value;
					this.ToCalendar.VisibleDate = value;
				}
				finally
				{
					this.SelectionChangeCount--;
				}
			}
		}

		private void _toCalendar_SelectionChanged(object sender, EventArgs e)
		{
			this.StoredTo = this.ToCalendar.SelectedDate;

			// check...
			if(this.To < this.From)
				this.From = this.To;

			// raise...
			this.OnDatesChanged();
		}

		private void _fromCalendar_SelectionChanged(object sender, EventArgs e)
		{
			// set...
			this.StoredFrom = this.FromCalendar.SelectedDate;

			// check...
			if(this.From > this.To)
				this.To = this.From;

			// raise...
			this.OnDatesChanged();
		}

		/// <summary>
		/// Raises the <c>DatesChanged</c> event.
		/// </summary>
		private void OnDatesChanged()
		{
			OnDatesChanged(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DatesChanged</c> event.
		/// </summary>
		protected virtual void OnDatesChanged(EventArgs e)
		{
			if(DatesChanged != null)
				DatesChanged(this, e);
		}

		/// <summary>
		/// Gets or sets the storedfrom.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>StoredFrom</c> for storage.
		/// </remarks>
		private DateTime StoredFrom
		{
			get
			{
				object value = ViewState["StoredFrom"];
				if(value == null)
					return DateTime.MinValue;
				else
					return (DateTime)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != StoredFrom)
				{
					// set the value...
					ViewState["StoredFrom"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the storedto.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>StoredTo</c> for storage.
		/// </remarks>
		private DateTime StoredTo
		{
			get
			{
				object value = ViewState["StoredTo"];
				if(value == null)
					return DateTime.MinValue;
				else
					return (DateTime)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != StoredTo)
				{
					// set the value...
					ViewState["StoredTo"] = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the selectionchangecount
		/// </summary>
		private int SelectionChangeCount
		{
			get
			{
				return _selectionChangeCount;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _selectionChangeCount)
				{
					// set the value...
					_selectionChangeCount = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the midnighttomidnight.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>MidnightToMidnight</c> for storage.
		/// </remarks>
		public bool MidnightToMidnight
		{
			get
			{
				object value = ViewState["MidnightToMidnight"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != MidnightToMidnight)
				{
					// set the value...
					ViewState["MidnightToMidnight"] = value;
				}
			}
		}

		/// <summary>
		/// Selects this calendar month.
		/// </summary>
		public void SelectThisMonth()
		{
			DateTime today = DateTime.Today;

			// set...
			DateTime to = today;
			DateTime from = new DateTime(today.Year, today.Month, 1);

			// raise...
			this.SelectDates(from, to);
		}

		/// <summary>
		/// Selects this calendar month.
		/// </summary>
		public void SelectToday()
		{
			// select...
			DateTime today = DateTime.Today;
			this.SelectDates(today, today);
		}

		private void SelectDates(DateTime from, DateTime to)
		{
			// date...
			this.SelectionChangeCount++;
			try
			{
				this.From = from;
				this.To = to;
			}
			finally
			{
				this.SelectionChangeCount--;
			}

			// raise...
			this.OnDatesChanged();
		}

		private void today_Click(object sender, EventArgs e)
		{
			this.SelectToday();
		}

		private void thisWeek_Click(object sender, EventArgs e)
		{
			this.SelectThisWeek();
		}

		private void SelectThisWeek()
		{
			// from...
			DateTime to = DateTime.Today;
			DateTime from = to;
			while(from.DayOfWeek != DayOfWeek.Monday)
				from = from.AddDays(-1);

			// select...
			this.SelectDates(from, to);
		}

		private void thisMonth_Click(object sender, EventArgs e)
		{
			this.SelectThisMonth();
		}

		public void SelectLastMonth()
		{
			DateTime to = DateTime.Today;
			to = new DateTime(to.Year, to.Month, 1).AddMilliseconds(-1);
			DateTime from = new DateTime(to.Year, to.Month, 1);

			// show...
			this.SelectDates(from, to);
		}

		private void lastMonth_Click(object sender, EventArgs e)
		{
			this.SelectLastMonth();
		}

		public void SelectYesterday()
		{
			DateTime today = DateTime.Today;
			today = today.AddDays(-1);

			// show...
			this.SelectDates(today, today);
		}

		private void yesterday_Click(object sender, EventArgs e)
		{
			this.SelectYesterday();
		}

		private void _toCalendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
		{
			this.OnVisibleDatesChanged();
		}

		private void _fromCalendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
		{
			this.OnVisibleDatesChanged();

		}

		/// <summary>
		/// Raises the <c>VisibleDatesChanged</c> event.
		/// </summary>
		private void OnVisibleDatesChanged()
		{
			OnVisibleDatesChanged(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>VisibleDatesChanged</c> event.
		/// </summary>
		protected virtual void OnVisibleDatesChanged(EventArgs e)
		{
			if(VisibleDatesChanged != null)
				VisibleDatesChanged(this, e);
		}
	}
}
