// BootFX - Application framework for .NET applications
// 
// File: EntityDataGrid.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using BootFX.Common.Data;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>EntityDataGrid</c>.
	/// </summary>
	public class EntityDataGrid : DataGrid
	{
		public EntityDataGrid()
		{
		}

		public override void DataBind()
		{
			bool autoBind = this.UseThreadLocalCacheOnDataBind;

			// bind...
            //if(autoBind)
            //    Connection.BindThreadLocalCache();
            //try
            //{
				base.DataBind ();
            //}
            //finally
            //{
            //    if(autoBind)
            //        Connection.UnbindThreadLocalCache();
            //}
		}

		/// <summary>
		/// Gets or sets where to use thread-local cache binding when data binding is initiated.
		/// </summary>
		/// <remarks>
		/// This property uses view state item <c>UseThreadLocalCacheOnDataBind</c> for storage.
		/// </remarks>
		[Category("Behavior"), DefaultValue(true), Description("Gets or sets where to use thread-local cache binding when data binding is initiated.")]
		public bool UseThreadLocalCacheOnDataBind
		{
			get
			{
				object value = ViewState["UseThreadLocalCacheOnDataBind"];
				if(value == null)
					return true;
				else
					return (bool)value;
			}
			set
			{
				// check to see if the value has changed...
				if(value != UseThreadLocalCacheOnDataBind)
				{
					// set the value...
					ViewState["UseThreadLocalCacheOnDataBind"] = value;
				}
			}
		}
	}
}
