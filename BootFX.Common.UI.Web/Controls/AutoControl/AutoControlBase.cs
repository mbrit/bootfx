// BootFX - Application framework for .NET applications
// 
// File: AutoControlBase.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using BootFX.Common;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoControlBase</c>.
	/// </summary>
	public abstract class AutoControlBase : LoggableTable
	{
		/// <summary>
		/// Called when columns are required for the grid.
		/// </summary>
		public event EventHandler ColumnsNeeded;
		
		/// <summary>
		/// Raised when validation is required.
		/// </summary>
		public event AutoControlApplyEventHandler Validate;
		
		/// <summary>
		/// Private field to support <see cref="ProblemLabel"/> property.
		/// </summary>
		private UserFeedbackWidget _feedback;
		
		/// <summary>
		/// Private field to support <see cref="ControlMap"/> property.
		/// </summary>
		private IDictionary _controlMap = new HybridDictionary();

		protected AutoControlBase()
		{
			this.CellSpacing = 0;
			this.CellPadding = 0;
		}

		internal IConstraintControl CreateControlForField(AutoColumnSpecification specification, ExtendedFieldViewMode mode)
		{
			if(specification == null)
				throw new ArgumentNullException("specification");			
			
			// check...
			if(EntityType == null)
				throw new InvalidOperationException("EntityType is null.");
			EntityMember member = this.EntityType.GetMember(specification.Name, OnNotFound.ThrowException);
			if(member == null)
				throw new InvalidOperationException("member is null.");

			// field...
			EntityField field = member as EntityField;

			// create...
			IConstraintControl cc = null;
			if(mode == ExtendedFieldViewMode.View)
			{
				if(field != null)
				{
					// set...
					if(specification.HasLinkTemplate)
					{
						AutoDataGridHyperLink link = new AutoDataGridHyperLink();
						link.LinkTemplate = specification.LinkTemplate;

						// set...
						cc = link;
					}
					else
						cc = new AutoDataGridLabel();
				}
				else 
					cc = new AutoDataGridLabel();
			}
			else
			{
				// do we have a list?
				if(specification.IsSingleSelectList || (field != null && field.HasEnumerationType))
				{
					ConstraintDropDownList list = new ConstraintDropDownList();

					// setup...
					list.Items.Clear();

					// get...
					object subItems = null;
					if(field != null && field.HasEnumerationType)
					{
						if(field.EnumerationType == null)
							throw new InvalidOperationException("field.EnumerationType is null.");
						subItems = Enum.GetNames(field.EnumerationType);
					}
					else
					{
						subItems = specification.DataSource;

						// mbr - 29-01-2007 - infer if linked...
						if(subItems == null && member is ChildToParentEntityLink)
						{
							ChildToParentEntityLink link = (ChildToParentEntityLink)member;

							// grab...
							SqlFilter filter = SqlFilter.CreateGetAllFilter(link.ParentEntityType);
							if(filter == null)
								throw new InvalidOperationException("filter is null.");

							// sub...
							subItems = filter.ExecuteEntityCollection();
						}
					}

					// walk...
					if(subItems != null)
					{
						if(subItems is IEnumerable)
						{
							foreach(object subItem in (IEnumerable)subItems)
								list.Items.Add(this.CreateListItemForSubItem(subItem, specification));
						}
						else
							throw new NotSupportedException(string.Format("'{0}' does not support IEnumerable.", subItems.GetType()));
					}

					// set...
					cc = list;
				}
				else
				{
					if(member is EntityField)
						cc = ConstraintControlHelper.GetConstraintControlForField((EntityField)member, mode);
					else
						cc = new AutoDataGridLabel();
				}
			}

			// check...
			if(cc == null)
				throw new InvalidOperationException("cc is null.");

			// textbox?
			if(cc is TextBox && ((TextBox)cc).TextMode == TextBoxMode.MultiLine)
				((TextBox)cc).TextMode = TextBoxMode.SingleLine;

			// set...
			if(cc.FieldName == null || cc.FieldName.Length == 0)
				cc.FieldName = member.Name;

			// style...
			if(cc is WebControl)
			{
				WebControl wc = (WebControl)cc;
				if(wc.CssClass == null || wc.CssClass.Length == 0)
					wc.CssClass = "field";
			}

			// map...
			this.AddToControlMap((Control)cc, specification, mode);

			// return...
			return cc;
		}

		internal void AddToControlMap(Control control, AutoColumnSpecification specification, ExtendedFieldViewMode mode)
		{
			if(control == null)
				throw new ArgumentNullException("control");
			if(specification == null)
				throw new ArgumentNullException("specification");			
			
			// set...
			this.ControlMap[control] = new AutoColumnPair(specification, mode);
		}

		private ListItem CreateListItemForSubItem(object subItem, AutoColumnSpecification specification)
		{
			if(subItem == null)
				throw new ArgumentNullException("subItem");
			if(specification == null)
				throw new ArgumentNullException("specification");

			// primitive?
			ListItem item = new ListItem();
			item.Text = this.GetSubItemText(subItem, specification);
			item.Value = this.GetSubItemValueAsString(subItem, specification);

			// return...
			return item;
		}

		private object GetSubItemValue(object subItem, AutoColumnSpecification specification)
		{
			if(subItem == null)
				throw new ArgumentNullException("subItem");
			if(specification == null)
				throw new ArgumentNullException("specification");

			// check...
			if(subItem is string || subItem.GetType().IsValueType)
				return ConversionHelper.ToString(subItem, Cultures.System);
			else
			{
				// get...
				EntityType subEt = EntityType.GetEntityType(subItem, OnNotFound.ThrowException);
			
				// get...
				object[] key = subEt.Storage.GetKeyValues(subItem);
				if(key == null)
					throw new InvalidOperationException("'key' is null.");
				if(key.Length != 1)
					throw new InvalidOperationException(string.Format("'key' is an invalid length ({0}).", key.Length));

				// return...
				return key[0];
			}
		}

		protected string GetSubItemValueAsString(object subItem, AutoColumnSpecification specification)
		{
			object value = this.GetSubItemValue(subItem, specification);
			if(value == null)
				throw new InvalidOperationException("value is null.");

			// return...
			return ConversionHelper.ToString(value, Cultures.System);
		}

		/// <summary>
		/// Gets the controlmap.
		/// </summary>
		private IDictionary ControlMap
		{
			get
			{
				return _controlMap;
			}
		}

		public abstract EntityType EntityType
		{
			get;
		}

		protected string GetSubItemText(object subItem, AutoColumnSpecification specification)
		{
			if(subItem == null)
				throw new ArgumentNullException("subItem");
			if(specification == null)
				throw new ArgumentNullException("specification");
			
			// primitive?
			if(subItem is string || subItem.GetType().IsValueType)
				return ConversionHelper.ToString(subItem, Cultures.System);
			else
			{
				// get...
				object value = null;
				if(specification.SubItemDataTextField != null && specification.SubItemDataTextField.Length > 0)
				{
					// get...
					EntityType subEt = EntityType.GetEntityType(subItem, OnNotFound.ThrowException);

					// set...
					value = subEt.Storage.GetValue(subItem, subEt.Fields.GetField(specification.SubItemDataTextField, OnNotFound.ThrowException));
				}
				else
					value = subItem;

				// return...
				if(value == null)
					return string.Empty;
				else if(value is IConvertible)
					return ConversionHelper.ToString(value, Cultures.User);
				else
					return value.ToString();
			}
		}

		/// <summary>
		/// Gets the problemlabel.
		/// </summary>
		// mbr - 2008-11-28 - changed name and protection.
//		protected UserFeedbackWidget Feedback
		public UserFeedbackWidget FeedbackWidget
		{
			get
			{
				if(_feedback == null)
					_feedback = new UserFeedbackWidget();
				return _feedback;
			}
		}

		protected virtual bool Apply(object entity, Control container, AutoControlApplyEventHandler beforeUpdateHandler)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");
			if(container == null)
				throw new ArgumentNullException("container");
			
			// get...
			ArrayList ccs = new ArrayList();
			this.WalkAndFindConstraintControls(container, ccs);

			// validate...
			FormChecker checker = new FormChecker();
			IDictionary toPatch = new HybridDictionary();
			this.ValidateForm(entity, ccs, checker, toPatch);

			// ok?
			if(checker.HasErrors)
			{
				// mbr - 23-03-2008 - changed to widget...
//				this.ProblemLabel.Text = checker.GetAllErrorsSeparatedByBrTag();
				this.FeedbackWidget.SetError(checker);

				// return...
				return false;
			}

			// patch...
			foreach(DictionaryEntry entry in toPatch)
			{
				// get...
				EntityField field = this.EntityType.Fields.GetField((string)entry.Key, OnNotFound.ThrowException);
				if(field == null)
					throw new InvalidOperationException("field is null.");

				// enum?
				object value = entry.Value;
				if(field.HasEnumerationType)
				{
					if(field.EnumerationType == null)
						throw new InvalidOperationException("field.EnumerationType is null.");

					// now what?
					if(value == null)
						throw new InvalidOperationException("value is null.");
					else if(value is String)
						value = Enum.Parse(field.EnumerationType, (string)value, true);
					else
						throw new NotSupportedException(string.Format("Cannot handle '{0}' against an enumeration type of '{1}'.", value, field.EnumerationType));
				}

				// mbr - 17-04-2007 - if the value is null or dbnull, replace with a nullable type...
				bool setOk = true;
				if(value == null || value is DBNull)
					value = ConversionHelper.GetClrLegalDBNullEquivalent(field.DBType);
				else
				{
					// mbr - 03-08-2007 - try and do a conversion - if it fails - give the user a better message...					
					try
					{
						value = ConversionHelper.ChangeType(value, field.Type, Cultures.User);
					}
					catch(Exception ex)
					{
						// message
						StringBuilder builder = new StringBuilder();
						builder.Append("The value '");
						builder.Append(value);
						builder.Append("' cannot be converted ");

						// adjust...
						switch(field.DBType)
						{
							case DbType.Int16:
							case DbType.Int32:
							case DbType.Int64:
							case DbType.Single:
							case DbType.Double:
							case DbType.Decimal:
							case DbType.Byte:
							case DbType.Currency:
								builder.Append(" to a numeric value.");
								break;

							default:
								builder.Append(" to a value of type '");
								builder.Append(field.DBType);
								builder.Append("'.");
								break;
						}

						// add...
						checker.AddError(builder.ToString());

						// warning...
						if(this.Log.IsWarnEnabled)
							this.Log.Warn(builder.ToString(), ex);

						// nope...
						setOk = false;
					}
				}

				// set...
				if(setOk)
					this.EntityType.Storage.SetValue(entity, field, value, SetValueReason.UserSet);
			}

			// run...
			if(beforeUpdateHandler != null)
				beforeUpdateHandler(this, new AutoControlApplyEventArgs(entity, ccs, checker));

			// stil ok?
			if(checker.HasErrors)
			{
				// mbr - 23-03-2008 - replaced with feedback...
//				this.ProblemLabel.Text = checker.GetAllErrorsSeparatedByBrTag();
				this.FeedbackWidget.SetError(checker);

				// return...
				return false;
			}

			// save...
			this.EntityType.Persistence.SaveChanges(entity);

			// ok...
			return true;
		}

		internal void WalkAndFindConstraintControls(Control control, ArrayList ccs)
		{
			if(control == null)
				throw new ArgumentNullException("control");
			if(ccs == null)
				throw new ArgumentNullException("ccs");
			
			// check..
			if(this.GetColumnPair(control, false) != null)
				ccs.Add(control);

			// child...
			foreach(Control child in control.Controls)
				this.WalkAndFindConstraintControls(child, ccs);
		}

		/// <summary>
		/// Validates form entry.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="ccs"></param>
		/// <param name="checker"></param>
		/// <returns></returns>
		private void ValidateForm(object entity, IList ccs, FormChecker checker, IDictionary toPatch)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");
			if(ccs == null)
				throw new ArgumentNullException("ccs");			
			if(checker == null)
				throw new ArgumentNullException("checker");
			if(toPatch == null)
				throw new ArgumentNullException("toPatch");
			
			// check...
			if(EntityType == null)
				throw new InvalidOperationException("EntityType is null.");
	
			// walk...
			foreach(Control scan in ccs)
			{
				// get...
				AutoColumnPair pair = this.GetColumnPair((Control)scan, true);
				if(pair == null)
					throw new InvalidOperationException("pair is null.");

				// check...
				if(pair.Specification is CustomAutoColumnSpecification)
				{
					((CustomAutoColumnSpecification)pair.Specification).OnValidate(new CustomControlEntityEventArgs(scan, ExtendedFieldViewMode.Edit, 
						entity, checker));
				}
				else
				{
					IConstraintControl cc = (IConstraintControl)scan;

					// check...
					object value = cc.Value;
					if(pair.Specification.IsRequired && !(cc.IsValueSet))
						checker.AddError(string.Format("A value for '{0}' is required.", pair.Specification.HeaderText));
					else
					{
						// get the member...
						EntityMember member = this.EntityType.GetMember(pair.Specification.Name, OnNotFound.ThrowException);
						if(member == null)
							throw new InvalidOperationException("member is null.");

						// check...
						if(member is EntityField)
							toPatch[pair.Specification.Name] = value;
						else if(member is ChildToParentEntityLink)
						{
							ChildToParentEntityLink link = (ChildToParentEntityLink)member;

							// get...
							EntityField[] linkFields = link.GetLinkFields();
							if(linkFields == null)
								throw new InvalidOperationException("'linkFields' is null.");
							if(linkFields.Length != 1)
								throw new InvalidOperationException(string.Format("'linkFields' is is an invalid length ({0}).", linkFields.Length));

							// set...
							toPatch[linkFields[0].Name] = value;
						}
						else
							throw new NotSupportedException(string.Format("Cannot handle '{0}'.", member.GetType()));
					}
				}
			}

			// raise an event on us...
			this.OnValidate(new AutoControlApplyEventArgs(entity, ccs, checker));
		}

		internal AutoColumnPair GetColumnPair(Control control, bool throwIfNotFound)
		{
			if(control == null)
				throw new ArgumentNullException("control");
			
			// find...
			AutoColumnPair pair = (AutoColumnPair)this.ControlMap[control];
			if(pair == null)
			{
				if(throwIfNotFound)
					throw new InvalidOperationException("pair is null.");
				else
					return null;
			}
			else
				return pair;
		}

		protected string FormatEnumerationValue(Type enumType, object value)
		{
			if(enumType == null)
				throw new ArgumentNullException("enumType");
			if(value == null)
				throw new ArgumentNullException("value");
			
			// get...
			string asString = Enum.GetName(enumType, value);

			// return...
			return asString;
		}

		/// <summary>
		/// Raises the <c>Validate</c> event.
		/// </summary>
		protected virtual void OnValidate(AutoControlApplyEventArgs e)
		{
			// raise...
			if(Validate != null)
				Validate(this, e);
		}

		/// <summary>
		/// Raises the <c>ColumnsNeeded</c> event.
		/// </summary>
		private void OnColumnsNeeded()
		{
			OnColumnsNeeded(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>ColumnsNeeded</c> event.
		/// </summary>
		protected virtual void OnColumnsNeeded(EventArgs e)
		{
			// raise...
			if(ColumnsNeeded != null)
				ColumnsNeeded(this, e);
		}

		protected AutoColumnSpecificationCollection InferColumnsFromEntityType(EntityType et, bool includeKeys)
		{
			if(et == null)
				throw new ArgumentNullException("et");

			// walk...
			AutoColumnSpecificationCollection columns = new AutoColumnSpecificationCollection();
			foreach(EntityField field in et.Fields)
			{
				if(!(field.IsKey()))
					columns.Add(new AutoColumnSpecification(field.Name, null, Unit.Empty));
			}
			foreach(ChildToParentEntityLink link in et.ChildLinks)
				columns.Add(new AutoColumnSpecification(link.Name, null, Unit.Empty));

			// return...
			return columns;
		}
	
		/// <summary>
		/// Handles an exception that occurs in response to a user action.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="ex"></param>
		protected void HandleException(string message, Exception ex)
		{
			if(ex == null)
				throw new ArgumentNullException("ex");
			
			// check..
			if(message == null || message.Length == 0)
				message = "An error has occurred.";

			// log...
			if(this.Log.IsErrorEnabled)
				this.Log.Error(message, ex);

			// mbr - 23-03-2008 - changed to feedback...
//			this.ProblemLabel.Text = string.Format("{0}<BR />{1}", message, ex.ToString().Replace("\n", "<BR />"));
			this.FeedbackWidget.SetError(ex);
		}
	}
}
