// BootFX - Application framework for .NET applications
// 
// File: AutoControlApplyEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	public delegate void AutoControlApplyEventHandler(object sender, AutoControlApplyEventArgs e);

	/// <summary>
	/// Defines an instance of <c>AutoControlApplyEventArgs</c>.
	/// </summary>
	public class AutoControlApplyEventArgs : EntityEventArgs
	{
		/// <summary>
		/// Private field to support <see cref="Checker"/> property.
		/// </summary>
		private FormChecker _checker;
		
		/// <summary>
		/// Private field to support <see cref="Controls"/> property.
		/// </summary>
		private IList _controls;
		
		internal AutoControlApplyEventArgs(object entity, IList controls, FormChecker checker) : base(entity)
		{
			if(controls == null)
				throw new ArgumentNullException("controls");
			
			_controls = controls;
			_checker = checker;
		}

		/// <summary>
		/// Gets the checker.
		/// </summary>
		public FormChecker Checker
		{
			get
			{
				if(_checker == null)
					throw new InvalidOperationException("A form checker was not provided to this event.");
				return _checker;
			}
		}

		/// <summary>
		/// Gets the controls.
		/// </summary>
		public IList Controls
		{
			get
			{
				return _controls;
			}
		}

		/// <summary>
		/// Gets a control from the ones passed in.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="type"></param>
		/// <returns></returns>
		public Control GetControl(string id, Type type)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(id.Length == 0)
				throw new ArgumentOutOfRangeException("'id' is zero-length.");
			if(type == null)
				throw new ArgumentNullException("type");
			
			// walk...
			foreach(Control control in this.Controls)
			{
				if(string.Compare(control.ID, id, true, System.Globalization.CultureInfo.InvariantCulture) == 0)
				{
					if(type.IsAssignableFrom(control.GetType()))
						return control;
					else
						throw new InvalidOperationException(string.Format("A control of type '{0}' is not compatible with '{1}'.", control.GetType(), type));
				}
			}

			// throw...
			throw new InvalidOperationException(string.Format("A control with ID '{0}' was not found.", id));
		}
	}
}
