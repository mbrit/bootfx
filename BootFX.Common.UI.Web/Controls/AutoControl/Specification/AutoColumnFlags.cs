// BootFX - Application framework for .NET applications
// 
// File: AutoColumnFlags.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoColumnFlags</c>.
	/// </summary>
	[Flags()]
	public enum AutoColumnFlags
	{
		Normal = 0,
		ReadOnly = 1,
		Required = 2,
		SingleSelectList = 4,
		ExcludeFromAddForm = 8,
	}
}
