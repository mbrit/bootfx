// BootFX - Application framework for .NET applications
// 
// File: CustomAutoColumnSpecification.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common;
using BootFX.Common.Entities;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>CustomAutoColumnSpecification</c>.
	/// </summary>
	public class CustomAutoColumnSpecification : AutoColumnSpecification
	{
		/// <summary>
		/// Raised when the column is databound.
		/// </summary>
		public event CustomControlEntityEventHandler ItemDataBound;
		
		/// <summary>
		/// Raised when the column is databound.
		/// </summary>
		public event CustomControlEntityEventHandler Validate;
		
		/// <summary>
		/// Called when a control is required.
		/// </summary>
		public event CustomControlEventHandler ControlRequired;
		
		public CustomAutoColumnSpecification(string name, string headerText, Unit width, AutoColumnFlags flags) 
			: base(name, headerText, width, flags)
		{
		}

		/// <summary>
		/// Raises the <c>ControlRequired</c> event.
		/// </summary>
		protected internal virtual void OnControlRequired(CustomControlEventArgs e)
		{
			// raise...
			if(ControlRequired != null)
				ControlRequired(this, e);
		}

		/// <summary>
		/// Raises the <c>ItemDataBound</c> event.
		/// </summary>
		protected internal virtual void OnItemDataBound(CustomControlEntityEventArgs e)
		{
			// raise...
			if(ItemDataBound != null)
				ItemDataBound(this, e);
		}
		
		/// <summary>
		/// Raises the <c>Validate</c> event.
		/// </summary>
		protected internal virtual void OnValidate(CustomControlEntityEventArgs e)
		{
			// raise...
			if(Validate != null)
				Validate(this, e);
		}

	}
}
