// BootFX - Application framework for .NET applications
// 
// File: AutoColumnSpecification.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoGridColumnSpecification</c>.
	/// </summary>
	public class AutoColumnSpecification
	{
		/// <summary>
		/// Private field to support <c>Width</c> property.
		/// </summary>
		private Unit _width = new Unit(0, UnitType.Pixel);
		
		/// <summary>
		/// Private field to support <c>SubItemDataTextField</c> property.
		/// </summary>
		private string _subItemDataTextField;
		
		/// <summary>
		/// Raised when a data source is required.
		/// </summary>
		public event EventHandler DataSourceNeeded;
		
		/// <summary>
		/// Private field to support <c>DataSource</c> property.
		/// </summary>
		private object _dataSource;
		
		/// <summary>
		/// Private field to support <see cref="Flags"/> property.
		/// </summary>
		private AutoColumnFlags _flags;
		
		/// <summary>
		/// Private field to support <c>HeaderText</c> property.
		/// </summary>
		private string _headerText;
		
		/// <summary>
		/// Private field to support <see cref="Name"/> property.
		/// </summary>
		private string _name;

		/// <summary>
		/// Private field to support <see cref="LinkTemplate"/> property.
		/// </summary>
		private string _linkTemplate;
		
		public AutoColumnSpecification(string name, string headerText, Unit width) : this(name, headerText, width, AutoColumnFlags.Normal)
		{
		}

		public AutoColumnSpecification(string name, string headerText, Unit width, AutoColumnFlags flags)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// set...
			_name = name;
			_headerText = headerText;
			_flags = flags;
			_width = width;
		}

		/// <summary>
		/// Gets the linktemplate.
		/// </summary>
		public string LinkTemplate
		{
			get
			{
				return _linkTemplate;
			}
			set
			{
				_linkTemplate = value;
			}
		}
		
		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}

		/// <summary>
		/// Gets or sets the headertext
		/// </summary>
		public string HeaderText
		{
			get
			{
				if(_headerText == null || _headerText.Length == 0)
					return this.Name;
				else
					return _headerText;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _headerText)
				{
					// set the value...
					_headerText = value;
				}
			}
		}

		internal bool HasLinkTemplate
		{
			get
			{
				if(this.LinkTemplate == null || this.LinkTemplate.Length == 0)
					return false;
				else
					return true;
			}
		}

		/// <summary>
		/// Gets the flags.
		/// </summary>
		internal AutoColumnFlags Flags
		{
			get
			{
				return _flags;
			}
		}

		private bool IsFlagSet(AutoColumnFlags flags)
		{
			if((int)(flags & this.Flags) != 0)
				return true;
			else
				return false;
		}

		internal bool IsRequired
		{
			get
			{
				return this.IsFlagSet(AutoColumnFlags.Required);
			}
		}

		internal bool IsReadOnly
		{
			get
			{
				return this.IsFlagSet(AutoColumnFlags.ReadOnly);
			}
		}

		internal bool IsSingleSelectList
		{
			get
			{
				return this.IsFlagSet(AutoColumnFlags.SingleSelectList);
			}
		}

		internal bool IsOnAddForm
		{
			get
			{
				return !(this.IsFlagSet(AutoColumnFlags.ExcludeFromAddForm));
			}
		}

		internal bool IsOnGrid
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets or sets the datasource
		/// </summary>
		public object DataSource
		{
			get
			{
				if(_dataSource == null)
					this.OnDataSourceNeeded();
				return _dataSource;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _dataSource)
				{
					// set the value...
					_dataSource = value;
				}
			}
		}

		/// <summary>
		/// Raises the <c>DataSourceNeeded</c> event.
		/// </summary>
		private void OnDataSourceNeeded()
		{
			OnDataSourceNeeded(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DataSourceNeeded</c> event.
		/// </summary>
		protected virtual void OnDataSourceNeeded(EventArgs e)
		{
			// raise...
			if(DataSourceNeeded != null)
				DataSourceNeeded(this, e);
		}

		/// <summary>
		/// Gets or sets the subitemdatatextfield
		/// </summary>
		public string SubItemDataTextField
		{
			get
			{
				return _subItemDataTextField;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _subItemDataTextField)
				{
					// set the value...
					_subItemDataTextField = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the width
		/// </summary>
		public Unit Width
		{
			get
			{
				return _width;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _width)
				{
					// set the value...
					_width = value;
				}
			}
		}
	}
}
