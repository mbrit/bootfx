// BootFX - Application framework for .NET applications
// 
// File: AutoColumnPair.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common.UI.Web
{
	/// <summary>
	/// Defines an instance of <c>AutoColumnPair</c>.
	/// </summary>
	internal class AutoColumnPair
	{
		internal AutoColumnPair(AutoColumnSpecification specification, ExtendedFieldViewMode mode)
		{
			if(specification == null)
				throw new ArgumentNullException("specification");
			_specification = specification;
			_mode = mode;
		}

		/// <summary>
		/// Private field to support <see cref="Specification"/> property.
		/// </summary>
		private AutoColumnSpecification _specification;

		/// <summary>
		/// Private field to support <see cref="Mode"/> property.
		/// </summary>
		private ExtendedFieldViewMode _mode;
		
		/// <summary>
		/// Gets the mode.
		/// </summary>
		internal ExtendedFieldViewMode Mode
		{
			get
			{
				return _mode;
			}
		}
		
		/// <summary>
		/// Gets the specification.
		/// </summary>
		internal AutoColumnSpecification Specification
		{
			get
			{
				return _specification;
			}
		}
	}
}
