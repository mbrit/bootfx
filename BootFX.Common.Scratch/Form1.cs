// BootFX - Application framework for .NET applications
// 
// File: Form1.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using BootFX.Common;
using BootFX.Common.UI.Desktop;
using BootFX.Common.Email;
using BootFX.Common.Data;
using BootFX.Common.Data.Rss;
using BootFX.Common.Data.Text;
using BootFX.Common.Management;

namespace BootFX.Common.Scratch
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : BaseForm
	{
		private const string FolderPath = @"c:\temp\fsptest";
		private const string LogName = "Test Log";
        private const string Trailing = "'TRAILER'";
        private Button button1;
        private Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(179, 98);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(179, 127);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(816, 534);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			// args...
			RuntimeStartArgs args = new RuntimeStartArgs();

			// start...
			Startup start = new Startup("BootFX", "BootFX", "Scratch", typeof(Runtime).Assembly.GetName().Version, args);
			start.StartApplication += new EventHandler(start_StartApplication);
			start.ConfigureRuntime += new CancelEventHandler(start_ConfigureRuntime);

			// ok...
			start.Run();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
			}
			catch(Exception ex)
			{
				Alert.ShowWarning(this, "Wow.", ex);
			}
		}

		private static void start_StartApplication(object sender, EventArgs e)
		{
			// run...
			Application.Run(new Form1());
		}

		private static void start_ConfigureRuntime(object sender, CancelEventArgs e)
		{
			Database.SetDefaultDatabase(typeof(SqlServerConnection), 
				"integrated security=sspi;data source=localhost;initial catalog=project-shl.mobile-live");

			// partitioning...
			PartitionIdProvider.DefaultProvider = new NullPartitionIdProvider();
//			PartitioningStrategy.StrategyFactory = new FooIdStrategyFactory();

			// configure...

		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
			}
			catch(Exception ex)
			{
				Alert.ShowWarning(this, "Failed.", ex);
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			Exception ex = new InvalidOperationException("sdfkjsdf");
			Alert.ShowWarning(this, "Hello.", ex, AlertFlags.Normal);
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
            if (this.DesignMode)
                return;

            // ok...
            try
            {
                Runtime.Current.Remoting.RegisterServerTcpChannel(12345);
                Runtime.Current.Remoting.RegisterWellKnownServices(this.GetType().Assembly);
            }
            catch (Exception ex)
            {
                Alert.ShowWarning(this, "Failed.", ex);
            }
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			try
			{
			}
			catch(Exception ex)
			{
				Alert.ShowWarning(this, "Failed.", ex);
			}
		}

		private void resource_BeforeSaveChanges(object sender, BootFX.Common.Entities.BeforeSaveChangesEventArgs e)
		{
		}

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                throw new NotImplementedException("This operation has not been implemented.");
            }
            catch (Exception ex)
            {
                Alert.ShowWarning(this, "Failed.", ex);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            // mbr - 28-06-2007 - changed this to use an option dialog...
            using (OptionDialog dialog = new OptionDialog("You have not sent an error report.  Are you sure you do not want to send an error report?"))
            {
                dialog.AddButton("&Send Error Report", DialogResult.Yes);
                dialog.AddButton("Do &Not Send Error Report", DialogResult.No);
                dialog.AddButton("Cancel", DialogResult.Cancel);

                dialog.ShowDialog(this);
            }
        }
	}
}
