// BootFX - Application framework for .NET applications
// 
// File: MyService.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Text;
using BootFX.Common;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Services;
using BootFX.Common.Remoting;
using BootFX.Common.Management;

namespace BootFX.Common.Scratch
{
    [WellKnownService(typeof(IMyService))]
    public class MyService : MarshalByRefObject, IMyService
    {
        public string DoMagic()
        {
            return "Martha says, \"Go!\"";
        }
    }
}
