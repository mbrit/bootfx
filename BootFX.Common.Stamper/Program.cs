// BootFX - Application framework for .NET applications
// 
// File: Program.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;

namespace BootFX.Common.Stamper
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // out...
                Console.WriteLine("BootFX Source Stamper");
                Assembly asm = typeof(Program).Assembly;
                Version version = asm.GetName().Version;
                Console.WriteLine(version);
                Console.WriteLine();

                // start...
                Runtime.Start("BootFX", "BootFX", "Stamper", version);

                // load the stamp...
                string stamp = ResourceHelper.GetString(asm, "BootFX.Common.Stamper.Stamp.txt");
                if(stamp == null)
	                throw new InvalidOperationException("'stamp' is null.");

                // walk...
                string path = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(new Uri(asm.CodeBase).LocalPath))));

                // check the checkout state...
                string entityPath = Path.Combine(path, @"BootFX.Common\Entities\Entity.cs");
                FileAttributes attrs = File.GetAttributes(entityPath);
                if ((int)(attrs & FileAttributes.ReadOnly) != 0)
                    throw new InvalidOperationException("Entity.cs is not writable.  Check that the source tree has been checked out before stamping.");

                // update...
                Walk(new DirectoryInfo(path), stamp);

                // ok...
                Console.WriteLine("OK.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (Debugger.IsAttached)
                    Console.ReadLine();
            }
        }

        private static void Walk(DirectoryInfo folder, string stamp)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");
            if (stamp == null)
                throw new ArgumentNullException("stamp");
            if (stamp.Length == 0)
                throw new ArgumentException("'stamp' is zero-length.");

            // write...
            Console.WriteLine(folder.FullName);

            // get...
            foreach (FileInfo file in folder.GetFiles("*.cs"))
                DoStamp(file, stamp);

            // child...
            foreach (DirectoryInfo child in folder.GetDirectories())
                Walk(child, stamp);
        }

        private static void DoStamp(FileInfo file, string stamp)
        {
            if (file == null)
                throw new ArgumentNullException("file");
            if (stamp == null)
                throw new ArgumentNullException("stamp");
            if (stamp.Length == 0)
                throw new ArgumentException("'stamp' is zero-length.");

            // values...
            IDictionary values = CollectionsUtil.CreateCaseInsensitiveHashtable();
            values["filename"] = file.Name;
            values["build"] = typeof(Program).Assembly.GetName().Version.ToString();

            // walk...
            List<string> headers = new List<string>();
            List<string> usings = new List<string>();
            List<string> codes = new List<string>();
            LineType mode = LineType.Comment;
            using (StreamReader reader = new StreamReader(file.FullName))
            {
                // walk...
                while(true)
                {
                    string buf = reader.ReadLine();
                    if(buf == null)
                        break;

                    // what do we have?
                    LineType type = GetCodeType(buf);

                    // now what?
                    if(mode == LineType.Comment)
                    {
                        if(type == LineType.Comment)
                            headers.Add(buf);
                        else if(type == LineType.Using)
                            mode = LineType.Using;
                        else if(type == LineType.Code)
                            mode = LineType.Code;
                        else
                            throw new NotSupportedException(string.Format("Cannot handle '{0}'.", type));

                    }

                    // using?
                    if(mode == LineType.Using)
                    {
                        if(type == LineType.Comment)
                        {
                            // no-op...
                        }
                        else if(type == LineType.Using)
                            usings.Add(buf);
                        else if(type == LineType.Code)
                            mode = LineType.Code;
                        else
                            throw new NotSupportedException(string.Format("Cannot handle '{0}'.", type));

                    }

                    // code?
                    if(mode == LineType.Code)
                        codes.Add(buf);
                }
            }
            
            // usings...
            SanitizeUsings(usings);

            // code...
            string temp = Path.GetTempFileName();
            using (StreamWriter writer = new StreamWriter(temp))
            {
                // write the new header....
                StringTokenizer tok = new StringTokenizer();
                string header = tok.Replace(stamp, values);
                writer.Write(header);
                writer.WriteLine();

                // write using systems...
                Write(writer, usings);
                writer.WriteLine();

                // code...
                Write(writer, codes);
            }

            // reset...
            FileAttributes attrs = file.Attributes;
            if ((int)(attrs & FileAttributes.ReadOnly) != 0)
                file.Attributes ^= FileAttributes.ReadOnly;

            // show...
            file.Delete();
            File.Move(temp, file.FullName);
        }

        private static void Write(StreamWriter writer, List<string> codes)
        {
            if (writer == null)
                throw new ArgumentNullException("writer");
            if (codes == null)
                throw new ArgumentNullException("codes");

            // walk...
            foreach (string code in codes)
                writer.WriteLine(code);
        }

        private static void SanitizeUsings(List<string> usings)
        {
            if (usings == null)
                throw new ArgumentNullException("usings");

            // walk...
            List<string> systems = new List<string>();
            foreach (string u in usings)
            {
                if (u.StartsWith("using System;"))
                    systems.Add(u);
            }

            // how many?
            if (systems.Count == 0)
                usings.Insert(0, "using System;");
            else
            {
                while (systems.Count > 1)
                {
                    usings.Remove(systems[systems.Count - 1]);
                    systems.RemoveAt(systems.Count - 1);
                }
            }
        }

        private static LineType GetCodeType(string buf)
        {
            if (buf == null)
                throw new ArgumentNullException("buf");

            // get...
            string check = buf.TrimStart();
            if(check.Length == 0 || check.StartsWith("//"))
                return LineType.Comment;
            else if(check.StartsWith("using "))
                return LineType.Using;
            else
                return LineType.Code;
        }
    }
}
