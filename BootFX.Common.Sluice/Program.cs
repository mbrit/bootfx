// BootFX - Application framework for .NET applications
// 
// File: Program.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace BootFX.Common.Sluice
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            int errorLevel = 0;
            string solutionName = args[0];
            string assemblyName = args[1];
            string inputPath = args[2];
            try
            {
                Console.Write("BootFX Sluice ");
                Console.WriteLine(typeof(Program).Assembly.GetName().Version);
                Console.WriteLine();

                // get...
                if (args.Length != 3)
                {
                    Console.WriteLine(@"Usage: c:\sluice\BootFX.Common.Sluice.exe $(SolutionName) $(ProjectName) $(TargetDir)");
                    DumpArgs(args);
                    errorLevel = 2;
                }
                else
                {
                    // get...
                    int index = solutionName.IndexOf(".");
                    if (index == -1)
                        throw new InvalidOperationException(string.Format("Solution name '{0}' is invalid.", solutionName));

                    // path...
                    string outputFolderPath = string.Format(@"c:\sluice\{0}\{1}\bin", solutionName.Substring(0, index), solutionName.Substring(index + 1));
                    if (!(Directory.Exists(outputFolderPath)))
                        Directory.CreateDirectory(outputFolderPath);

                    // copy...
                    string spec = string.Format(@"{0}\*.*", inputPath, assemblyName);
                    Console.WriteLine(spec);
                    foreach (FileInfo file in new DirectoryInfo(inputPath).GetFiles())
                    {
                        string filename = file.Name;
                        if (filename.StartsWith(assemblyName))
                        {
                            // new path...
                            string newPath = Path.Combine(outputFolderPath, file.Name);
                            Console.WriteLine("   " + newPath);
                            File.Copy(file.FullName, newPath, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("-----------------------");
                DumpArgs(args);
                Console.WriteLine(ex);

                // set...
                errorLevel = 1;
            }
            finally
            {
                if (Debugger.IsAttached)
                {
                    Console.WriteLine();
                    Console.WriteLine("Press Return...");
                    Console.ReadLine();
                }

                // set...
                Environment.ExitCode = errorLevel;
            }
        }

        private static void DumpArgs(string[] args)
        {
            for (int index = 0; index < args.Length; index++)
                Console.WriteLine("Argument #{0}: {1}", index, args[index]);
            Console.WriteLine("-----------------------");
        }
    }
}
