// BootFX - Application framework for .NET applications
// 
// File: Form1.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using BootFX.Common.UI.Desktop;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : BaseForm
	{
		/// <summary>
		/// Private field to support <c>Engine</c> property.
		/// </summary>
		private AppUpdateServiceHost _updater = null;
		private System.Windows.Forms.Timer timerStart;
		private OperationBox button1;
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private Form1(string url) : this()
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");

			// setup..
			_updater = new AppUpdateServiceHost(url);
			_updater.DownloadError += new System.Threading.ThreadExceptionEventHandler(_updater_DownloadError);
			_updater.DownloadFinished += new EventHandler(_updater_DownloadFinished);
			_updater.RunError += new System.Threading.ThreadExceptionEventHandler(_updater_RunError);
			_updater.RunStarted += new EventHandler(_updater_RunStarted);
			_updater.RunFinished += new EventHandler(_updater_RunFinished);
			_updater.VersionCheckError += new System.Threading.ThreadExceptionEventHandler(_updater_VersionCheckError);
			_updater.NewVersionAvailable += new EventHandler(_updater_NewVersionAvailable);

			// set...
			_updater.Operation = this.button1;

			// run...
			_updater.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			DisposeUpdater();

			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void DisposeUpdater()
		{
			if(_updater != null)
			{
				_updater.Dispose();
				_updater = null;
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timerStart = new System.Windows.Forms.Timer(this.components);
			this.button1 = new BootFX.Common.UI.Desktop.OperationBox();
			this.SuspendLayout();
			// 
			// timerStart
			// 
			this.timerStart.Enabled = true;
			this.timerStart.Interval = 500;
			this.timerStart.Tick += new System.EventHandler(this.timerStart_Tick);
			// 
			// button1
			// 
			this.button1.CopyStatusChangesToLog = true;
			this.button1.Location = new System.Drawing.Point(8, 12);
			this.button1.Name = "button1";
			this.button1.ProgressMaximum = 0;
			this.button1.ProgressMinimum = 0;
			this.button1.ProgressValue = 0;
			this.button1.Size = new System.Drawing.Size(276, 40);
			this.button1.Status = "Working, please wait...";
			this.button1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 62);
			this.Controls.Add(this.button1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Application Update";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			// lpm - 25-05-2008 - vista compatibility.
			//UacHelper.Elevate(this.Log, 
			Startup startup = new Startup("BootFX", "BootFX AppDeploy", "Client", Runtime.Version);
			startup.ConfigureRuntime += new CancelEventHandler(startup_ConfigureRuntime);
			startup.StartApplication += new EventHandler(startup_StartApplication);
			startup.Run();
		}

		private static void startup_StartApplication(object sender, EventArgs e)
		{
			// set...
			Alert.Caption = AppSettingsPropertyBag.Current.GetStringValue("Caption", "Application Update", Cultures.System, OnNotFound.ReturnNull);

			// set up error reporting...
            string url = AppSettingsPropertyBag.Current.GetStringValue("ErrorReportUrl", null, Cultures.System, OnNotFound.ReturnNull);
            if (!(string.IsNullOrEmpty(url)))
                Runtime.Current.PhoneHomeUrl = url;
            
			// check...
			url = AppSettingsPropertyBag.Current.GetStringValue("CatalogUrl", null, Cultures.System, OnNotFound.ReturnNull);
			if(url == null || url.Length == 0)
				Alert.ShowWarning(null, "The URL of the application catalog was not specified.");
			else
				Application.Run(new Form1(url));
		}

		private void NextStep()
		{
			// what now?
			if(Updater == null)
				throw new InvalidOperationException("Updater is null.");

            // what's optional update status...
            bool optionalUpdate = AppSettingsPropertyBag.Current.GetBooleanValue("OptionalUpdate", false, Cultures.System, OnNotFound.ReturnNull);

            // get...
			AppUpdateStatus status = Updater.GetStatus(true);
			switch(status)
			{
				case AppUpdateStatus.UpToDate:
					this.Run();
					break;

				case AppUpdateStatus.CouldUpdate:
                    if(optionalUpdate)
                        this.AskAndUpdate("A new version of the application is available.  Do you want to download it?", status);
                    else
                    {
                        // mbr - 2009-02-13 - w...eird - if you have to update, why ask them if they want to update?
                        //			this.AskAndUpdate("A new version of the application must be downloaded before you can proceed.  Do you want to proceed?", AppUpdateStatus.MustUpdate);
                        this.UpdateAndRun();
                    }

					break;

				case AppUpdateStatus.MustUpdate:
					//this.AskAndUpdate("A new version of the application must be downloaded before you can proceed.  Do you want to download it?", status);
                    this.UpdateAndRun();
					break;

				case AppUpdateStatus.CouldNotDetermine:
					this.AskAndUpdate("The server could not be contacted in order to determine the latest application version.  Do you want to proceed?", status);
					break;

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", status, status.GetType()));
			}
		}

		private void AskAndUpdate(string message, AppUpdateStatus status)
		{
			// lpm - 25-05-2008 - vista compatibility.
			UacHelper.Elevate(this.Log, Application.StartupPath, Application.ExecutablePath.Substring(Application.StartupPath.Length+1));

            // run...
			DialogResult result = Alert.AskYesNoQuestion(this, message);
			if(result == DialogResult.Yes)
			{
				if(status != AppUpdateStatus.CouldNotDetermine)
					this.UpdateAndRun();
				else
					this.Run();
			}
			else
			{
				if(status == AppUpdateStatus.MustUpdate || status == AppUpdateStatus.CouldNotDetermine)
					this.Close();
				else
					this.Run();
			}
		}

		/// <summary>
		/// Updates the application.
		/// </summary>
		private void UpdateAndRun()
		{
			// start...
			this.Updater.DownloadLatestVersion();
		}

		/// <summary>
		/// Gets the engine.
		/// </summary>
		private AppUpdateServiceHost Updater
		{
			get
			{
				// returns the value...
				return _updater;
			}
		}

		private void _updater_DownloadError(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			if(this.InvokeRequired)
			{
				System.Threading.ThreadExceptionEventHandler d = new System.Threading.ThreadExceptionEventHandler(this._updater_DownloadError);
				this.Invoke(d, new object[] { sender, e });
				return;
			}

			if(this.Log.IsErrorEnabled)
				this.Log.Error("Download failed.", e.Exception);
			Alert.ShowError(this,string.Format("App Deploy was unable to download the files to update the software and will now exit. A log of this error can be found at {0}", Runtime.Current.LogsFolderPath), e.Exception);
			this.Close();
		}

		private void _updater_DownloadFinished(object sender, EventArgs e)
		{
			this.NextStep();
		}

		/// <summary>
		/// Runs the latest installed version.
		/// </summary>
		private void Run()
		{
			if(Updater == null)
				throw new InvalidOperationException("Updater is null.");

			// get...
			Updater.Run();

			// hide...
			this.Visible = false;
		}

		private void _updater_RunError(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			if(this.InvokeRequired)
			{
				System.Threading.ThreadExceptionEventHandler d = new System.Threading.ThreadExceptionEventHandler(this._updater_DownloadError);
				this.Invoke(d, new object[] { sender, e });
				return;
			}

			Alert.ShowWarning(this, "An error occurred when attempting to run the application.", e.Exception);
			this.Close();
		}

		private void _updater_VersionCheckError(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			if(this.InvokeRequired)
			{
				System.Threading.ThreadExceptionEventHandler d = new System.Threading.ThreadExceptionEventHandler(this._updater_DownloadError);
				this.Invoke(d, new object[] { sender, e });
				return;
			}

			Alert.ShowWarning(this, "A version occurred when checking for a new version of the application.", e.Exception);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing (e);

			// hide...
			if(!(e.Cancel))
				this.Visible = false;
		}

		private void _updater_RunStarted(object sender, EventArgs e)
		{
            if(this.InvokeRequired)
            {
                this.Invoke(new EventHandler(this._updater_RunStarted), sender, e);
                return;
            }

            // hide...
			this.Visible = false;
		}

		private void timerStart_Tick(object sender, System.EventArgs e)
		{
			timerStart.Enabled = false;
			this.NextStep();
		}

		private delegate void CloseDelegate();

		private void _updater_RunFinished(object sender, EventArgs e)
		{
			if(this.InvokeRequired)
			{
				this.BeginInvoke(new CloseDelegate(this.Close));
				return;
			}

			// close...
			this.Close();
		}

		private void _updater_NewVersionAvailable(object sender, EventArgs e)
		{
			if(this.InvokeRequired)
			{
				System.Threading.ThreadExceptionEventHandler d = new System.Threading.ThreadExceptionEventHandler(this._updater_DownloadError);
				this.Invoke(d, new object[] { sender, e });
				return;
			}

			Alert.ShowInformation(this, "A new version of the application is available.  To change to the new version, restart the application.");
		}

		private static void startup_ConfigureRuntime(object sender, CancelEventArgs e)
		{
		}
	}
}
