// BootFX - Application framework for .NET applications
// 
// File: WellKnownServiceRegistrationArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Text;

namespace BootFX.Common.Remoting
{
    public class WellKnownServiceRegistrationArgs
    {
        /// <summary>
        /// Defines the <c>ServiceRegistered</c> event.
        /// </summary>
        public event TypeEventHandler ServiceRegistered;

        /// <summary>
        /// Private value to support the <see cref="AppName">AppName</see> property.
        /// </summary>
        private string _appName;

        public WellKnownServiceRegistrationArgs()
            : this(Runtime.Current.Remoting.DefaultAppName)
        {
        }

        internal WellKnownServiceRegistrationArgs(string appName)
        {
            if (appName == null)
                throw new ArgumentNullException("appName");
            if (appName.Length == 0)
                throw new ArgumentException("'appName' is zero-length.");

            // set...
            _appName = appName;
        }

        /// <summary>
        /// Gets the AppName value.
        /// </summary>
        internal string AppName
        {
            get
            {
                return _appName;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                if (value.Length == 0)
                    throw new ArgumentException("'value' is zero-length.");

                // set...
                _appName = value;
            }
        }

        /// <summary>
        /// Raises the <c>ServiceRegistered</c> event.
        /// </summary>
        protected internal virtual void OnServiceRegistered(TypeEventArgs e)
        {
            // raise...
            if (ServiceRegistered != null)
                ServiceRegistered(this, e);
        }
    }
}
