// BootFX - Application framework for .NET applications
// 
// File: WellKnownServiceAttribute.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Runtime.Remoting;

namespace BootFX.Common.Remoting
{
	/// <summary>
	/// Summary description for WellKnownServiceAttribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class WellKnownServiceAttribute : Attribute
	{
		/// <summary>
		/// Private field to support <see cref="Mode"/> property.
		/// </summary>
		private WellKnownObjectMode _mode;
		
		/// <summary>
		/// Private field to support <see cref="InterfaceType"/> property.
		/// </summary>
		private Type _interfaceType;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public WellKnownServiceAttribute(Type interfaceType) : this(interfaceType, WellKnownObjectMode.Singleton)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public WellKnownServiceAttribute(Type interfaceType, WellKnownObjectMode mode)
		{
			if(interfaceType == null)
				throw new ArgumentNullException("interfaceType");
			
			_interfaceType = interfaceType;
			_mode = mode;
		}

		/// <summary>
		/// Gets the interfacetype.
		/// </summary>
		public Type InterfaceType
		{
			get
			{
				return _interfaceType;
			}
		}

		/// <summary>
		/// Gets the mode.
		/// </summary>
		public WellKnownObjectMode Mode
		{
			get
			{
				return _mode;
			}
		}
	}
}
