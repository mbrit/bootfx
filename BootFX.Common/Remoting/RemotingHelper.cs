// BootFX - Application framework for .NET applications
// 
// File: RemotingHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net.Security;
using System.Web;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Serialization.Formatters;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;
using System.Diagnostics;
using BootFX.Common.Management;
using BootFX.Common.BusinessServices;

namespace BootFX.Common.Remoting
{
	/// <summary>
	/// Defines a helper class for Remoting.
	/// </summary>
	public class RemotingHelper : Loggable, IDisposable
	{
        /// <summary>
        /// Private value to support the <see cref="ManageClientRegistrations">ManageClientRegistrations</see> property.
        /// </summary>
        private bool _manageClientRegistrations;

        /// <summary>
        /// Private value to support the <see cref="ManageReciprocalClient">ManageReciprocalClient</see> property.
        /// </summary>
        private bool _manageReciprocalClient;

        /// <summary>
        /// Private value to support the <see cref="ClientChannelRegistered">ClientChannelRegistered</see> property.
        /// </summary>
        private bool _clientChannelRegistered = false;
        private bool _serverChannelRegistered = false;
        private ReaderWriterLock _channelRegistrationLock = new ReaderWriterLock();

        /// <summary>
		/// Private field to support <c>IisVdir</c> property.
		/// </summary>
		private string _iisVdir = null;

		/// <summary>
		/// Defines the IIS channel name.
		/// </summary>
		private const string IisChannelName = "BootFX";
		
		/// <summary>
		/// Private field to support <c>DefaultAppName</c> property.
		/// </summary>
		private string _defaultAppName = "BootFX";
		
		/// <summary>
		/// Defines the URI extension.
		/// </summary>
		private const string UriExtension = ".rem";

        /// <summary>
        /// Private value to support the <see cref="ClientRegistrations">ClientRegistrations</see> property.
        /// </summary>
        private Lookup _clientRegistrations;

        /// <summary>
		/// Constructor.
		/// </summary>
		internal RemotingHelper(bool manageReciprocalClient, bool manageClientRegistrations)
		{
            _manageReciprocalClient = manageReciprocalClient;
            _manageClientRegistrations = manageClientRegistrations;

            // create...
            _clientRegistrations = new Lookup();
            _clientRegistrations.CreateItemValue += new CreateLookupItemEventHandler(_clientRegistrations_CreateItemValue);

            // mbr - 2013-12-04 - added...
            RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off;
		}

		/// <summary>
		/// Finalizer.
		/// </summary>
		~RemotingHelper()
		{
			try
			{
				this.Dispose(DisposeReason.FromFinalizer);
			}
			catch
			{
				// ignore exceptions...
			}
		}

		/// <summary>
		/// Registers the well known services in the core library, together with any assemblies specified in the .config file.
		/// </summary>
		/// <param name="appName"></param>
		/// <param name="asm"></param>
        [Obsolete("Use a version that requires an assembly.")]
		public void RegisterWellKnownServices()
		{
			// core...
            // mbr - 2009-04-15 - removed, not used.
//			this.RegisterCoreServices();

			// asm...
			string rawAsmNames = AppSettingsPropertyBag.Current.GetStringValue("ServiceAssemblies", null, Cultures.System, OnNotFound.ThrowException);
			if(rawAsmNames == null)
				throw new InvalidOperationException("'rawAsmNames' is null.");
			if(rawAsmNames.Length == 0)
				throw new InvalidOperationException("'rawAsmNames' is zero-length.");

			// walk...
			string[] asmNames = rawAsmNames.Split(';');
			foreach(string asmName in asmNames)
			{
				string useName = asmName.Trim();
				if(useName.Length > 0)
					this.RegisterWellKnownServices(useName);
			}
		}

		/// <summary>
		/// Registers the well known services in the given assembly.
		/// </summary>
		/// <param name="appName"></param>
		/// <param name="asm"></param>
		public void RegisterWellKnownServices(string asmName)
		{
			if(asmName == null)
				throw new ArgumentNullException("asmName");
			if(asmName.Length == 0)
				throw new ArgumentOutOfRangeException("'asmName' is zero-length.");
			
			// defer...
            // mbr - 2009-04-15 - changed to args...
//			this.RegisterWellKnownServices(this.DefaultAppName, asmName);
            this.RegisterWellKnownServices(asmName, new WellKnownServiceRegistrationArgs());
		}

		/// <summary>
		/// Registers the well known services in the given assembly.
		/// </summary>
		/// <param name="appName"></param>
		/// <param name="asm"></param>
        [Obsolete("Use a version that takes a WellKnownServicesRegistrationArgs instance.")]
		public void RegisterWellKnownServices(string appName, string asmName)
		{
            this.RegisterWellKnownServices(asmName, new WellKnownServiceRegistrationArgs(appName));
        }

        /// <summary>
        /// Registers services.
        /// </summary>
        /// <param name="asmName"></param>
        /// <param name="args"></param>
        public void RegisterWellKnownServices(string asmName, WellKnownServiceRegistrationArgs args)
        {
			if(asmName == null)
				throw new ArgumentNullException("asmName");
			if(asmName.Length == 0)
				throw new ArgumentOutOfRangeException("'asmName' is zero-length.");
            if (args == null)
                throw new ArgumentNullException("args");

			// info...
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format(Cultures.Log, string.Format("Loading assembly by name: {0}", asmName)));
			
			// get...
			Assembly asm = Assembly.Load(asmName);
			if(asm == null)
				throw new InvalidOperationException(string.Format("The assembly '{0}' could not be loaded.", asmName));

			// defer...
			this.RegisterWellKnownServices(asm, args);
		}

		/// <summary>
		/// Registers the well known services in the given assembly.
		/// </summary>
		/// <param name="asm"></param>
		public void RegisterWellKnownServices(Assembly asm)
		{
			if(asm == null)
				throw new ArgumentNullException("asm");
			
			// defer...
            // mbr - 2009-04-15 - changed to args...
//			this.RegisterWellKnownServices(this.DefaultAppName, asm);
            this.RegisterWellKnownServices(asm, new WellKnownServiceRegistrationArgs());
		}

		/// <summary>
		/// Registers the well known services in the given assembly.
		/// </summary>
		/// <param name="asm"></param>
        [Obsolete("Use a version that takes a WellKnownServicesRegistrationArgs instance.")]
        public void RegisterWellKnownServices(string appName, Assembly asm)
        {
            RegisterWellKnownServices(asm, new WellKnownServiceRegistrationArgs(appName));
        }
    
        /// <summary>
        /// Registers well known services.
        /// </summary>
        /// <param name="asm"></param>
        /// <param name="args"></param>
        public void RegisterWellKnownServices(Assembly asm, WellKnownServiceRegistrationArgs args)
		{
			if(asm == null)
				throw new ArgumentNullException("asm");
            if (args == null)
                throw new ArgumentNullException("args");
			
			// info...
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format(Cultures.Log, string.Format("Registering services in: {0}", asm)));

			// walk...
			Type[] types = null;
			try
			{
				types = asm.GetTypes();
			}
			catch(Exception ex)
            {
                // mbr - 2009-04-14 - added more detail...
                StringBuilder builder = new StringBuilder();
                builder.Append("Failed to load types for '");
                builder.Append(asm);
                builder.Append("'.");

                // walk...
                Exception walk = ex;
                while (walk != null)
                {
                    if (walk is ReflectionTypeLoadException)
                    {
                        ReflectionTypeLoadException rtle = (ReflectionTypeLoadException)walk;
                        if (rtle.LoaderExceptions == null)
                            builder.Append("  (Type load exceptions list was CLR null.)");
                        else if (rtle.LoaderExceptions.Length == 0)
                            builder.Append("  (Type load exceptions list was zero-length.)");
                        else
                        {
                            builder.Append("  Type load exceptions were (");
                            builder.Append(rtle.LoaderExceptions.Length);
                            builder.Append("):");
                            foreach (Exception rtleEx in rtle.LoaderExceptions)
                            {
                                builder.Append("\r\n    - ");
                                builder.Append(rtleEx.ToString());
                            }
                        }
                    }

                    // up...
                    walk = walk.InnerException;
                }

                // dump that to a file...
                FileLog log = LogSet.CreateFileLogger("Remoting - Load Errors", FileLoggerFlags.Default);
                log.Error(builder.ToString());

                // throw...
				throw new InvalidOperationException(string.Format("Failed to load types for '{0}'.  More information was written to: {1}", asm, log.Path), ex);
			}

			// walk...
			foreach(Type type in types)
			{
                if (type.IsDefined(typeof(WellKnownServiceAttribute), false) || type.IsDefined(typeof(BusinessServiceAttribute), false))
                {
                    // mbr - 2009-04-15 - changed to args...
//                    RegisterWellKnownService(appName, type);
                    RegisterWellKnownService(type, args);
                }
			}
		}

		/// <summary>
		/// Registers the well known type.
		/// </summary>
		/// <param name="type"></param>
		private void RegisterWellKnownService(Type type)
		{
			if(type == null)
				throw new ArgumentNullException("type");
			
            // mbr - 2009-04-15 - changed...
			//this.RegisterWellKnownService(this.DefaultAppName, type);
            this.RegisterWellKnownService(type, new WellKnownServiceRegistrationArgs());
		}

		/// <summary>
		/// Registers the well known type.
		/// </summary>
		/// <param name="type"></param>
        private void RegisterWellKnownService(Type type, WellKnownServiceRegistrationArgs args)
		{
			if(type == null)
				throw new ArgumentNullException("type");
            if (args == null)
                throw new ArgumentNullException("args");

			// log...
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Registering '{0}' to app name '{1}'.", type, args.AppName));

			// get the attribute...
			WellKnownServiceAttribute[] attrs = (WellKnownServiceAttribute[])type.GetCustomAttributes(typeof(WellKnownServiceAttribute), false);
			if(attrs == null)
				throw new InvalidOperationException("attrs is null.");
			Type interfaceType = null;
			WellKnownObjectMode mode = WellKnownObjectMode.Singleton;
			if(attrs.Length == 0)
			{
				BusinessServiceAttribute[] serviceAttrs = (BusinessServiceAttribute[])type.GetCustomAttributes(typeof(BusinessServiceAttribute), false);
				if(serviceAttrs == null)
					throw new InvalidOperationException("serviceAttrs is null.");
				if(serviceAttrs.Length == 0)
					throw new InvalidOperationException(string.Format("'{0}' is not decorated with WellKnownService or BusinessService attributes."));

				// set...
				interfaceType = serviceAttrs[0].InterfaceType;
				mode = serviceAttrs[0].Mode;
			}
			else
			{
				interfaceType = attrs[0].InterfaceType;
				mode = attrs[0].Mode;
			}

			// check the marshal by ref...
			if(typeof(MarshalByRefObject).IsAssignableFrom(type) == false)
				throw new InvalidOperationException(string.Format("Type '{0}' does not extend MarshalByRefObject.", type));

			// check the interface type...
			if(interfaceType == null)
				throw new InvalidOperationException("interfaceType is null.");
			if(interfaceType.IsAssignableFrom(type) == false)
				throw new InvalidOperationException(string.Format("Type '{0}' does not implement '{1}'.", type, interfaceType));

			// get the uri...
			string uri = this.GetUri(args.AppName, interfaceType, true);
			if(uri == null)
				throw new InvalidOperationException("'uri' is null.");
			if(uri.Length == 0)
				throw new InvalidOperationException("'uri' is zero-length.");

            // debug...
            if (DebugRegistration)
                WriteDebugRegistrationLog(string.Format("Registering well-known service type on '{0}' (uri: {1}, mode: {2})...", type, uri, mode));

			// register it...
			RemotingConfiguration.RegisterWellKnownServiceType(type, uri, mode);

            // done...
            args.OnServiceRegistered(new TypeEventArgs(type));
		}

		/// <summary>
		/// Gets the URI for the given type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private string GetUri(string appName, Type type, bool forRegistration)
		{
			if(appName == null)
				throw new ArgumentNullException("appName");
			if(appName.Length == 0)
				throw new ArgumentOutOfRangeException("'appName' is zero-length.");
			if(type == null)
				throw new ArgumentNullException("type");
			if(forRegistration && type.IsInterface == false)
				throw new InvalidOperationException(string.Format("'{0}' is not an interface.", type));

			if(this.IsIis)
				return type.FullName + UriExtension;
			else
				return string.Format("{0}/{1}{2}", appName, type, UriExtension);
		}

		/// <summary>
		/// Registers an HTTP channel for use inside of IIS.
		/// </summary>
		/// <param name="vdirName"></param>
		/// <returns></returns>
        [Obsolete("HTTP channels not supported.")]
        public IChannel RegisterIisHttpChannel()
		{
			// get it...
			HttpContext context = HttpContext.Current;
			if(context == null)
				throw new InvalidOperationException("HTTP context not found.  This method can only be used from within an ASP.NET worker process.");

			// get the name...
			string vdir = context.Request.ApplicationPath;
			if(vdir == null)
				throw new InvalidOperationException("'vdir' is null.");
			if(vdir.Length == 0)
				throw new InvalidOperationException("'vdir' is zero-length.");

			// remove the leading '/'...
			_iisVdir = vdir.Substring(1);

			// log...
			const int priority = 100;
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Register HTTP channel for IIS, name: {0}, priority: {1}", 
					IisChannelName, priority));

			// props...
			IDictionary props = new HybridDictionary();
			props["name"] = IisChannelName;
			props["priority"] = priority.ToString();

			// register...
			HttpChannel channel = new HttpChannel(props, null, null);
			this.RegisterChannel(channel);

			// return...
			return channel;
		}

		private void RegisterChannel(IChannel channel)
		{
			ChannelServices.RegisterChannel(channel, true);
		}

        private void RegisterChannel(IChannel channel, bool isSecured)
        {
            ChannelServices.RegisterChannel(channel, isSecured);
        }
        
        /// <summary>
		/// Gets the isiis.
		/// </summary>
        private bool IsIis
		{
			get
			{
				// returns the value...  note - we can have a zero-length vdir name, so 
				// this is solely a null check...
				if(this.IisVdir == null)
					return false;
				else
					return true;
			}
		}

		/// <summary>
		/// Registers a server-side HTTP channel.
		/// </summary>
		/// <param name="port"></param>
		/// <returns></returns>
        [Obsolete("HTTP channels not supported.")]
        public IChannel RegisterServerHttpChannel(int port)
		{
			SoapServerFormatterSinkProvider provider = new SoapServerFormatterSinkProvider();
			return this.RegisterHttpChannel(port, null, provider);
		}

		/// <summary>
		/// Registers an HTTP channel.
		/// </summary>
		/// <param name="port"></param>
		/// <param name="clientProvider"></param>
		/// <param name="serverProvider"></param>
		/// <returns></returns>
        [Obsolete("HTTP channels not supported.")]
        private IChannel RegisterHttpChannel(int port, IClientChannelSinkProvider clientProvider, IServerChannelSinkProvider serverProvider)
		{
			if(clientProvider == null && serverProvider == null)
				throw new ArgumentException("Either clientProvider or serverProvider must be supplied.");

		    // set...
		    if(this.Log.IsInfoEnabled)
			    this.Log.Info(string.Format("Creating HTTP channel on port '{0}', client: {1}, server: {2}", port, clientProvider, serverProvider));

		    // settings...
		    IDictionary props = new HybridDictionary();
		    if(port != 0)
			    props["port"] = port;

		    // create...
		    HttpChannel channel = new HttpChannel(props, clientProvider, serverProvider);
		    this.RegisterChannel(channel);

		    // return...
		    return channel;
        }

		/// <summary>
		/// Registers a client side HTTP channel.
		/// </summary>
		/// <returns></returns>
        [Obsolete("HTTP channels not supported.")]
		internal IChannel RegisterClientHttpChannel()
		{
			IClientChannelSinkProvider provider = new SoapClientFormatterSinkProvider();
			return this.RegisterHttpChannel(0, provider, null);
		}

		/// <summary>
		/// Registers a client side TCP channel.
		/// </summary>
		/// <returns></returns>
		internal IChannel RegisterClientTcpChannel()
		{
            _channelRegistrationLock.AcquireWriterLock(-1);
            try
            {
                if (DebugRegistration)
                    WriteDebugRegistrationLog("Registering client channel...");

                // run...
                BinaryClientFormatterSinkProvider provider = new BinaryClientFormatterSinkProvider();
                IChannel channel = this.RegisterTcpChannel(0, provider, null);
                if (channel == null)
                    throw new InvalidOperationException("'channel' is null.");

                // set...
                _clientChannelRegistered = true;

                // return...
                return channel;
            }
            finally
            {
                _channelRegistrationLock.ReleaseLock();
            }
		}

        /// <summary>
        /// Gets the ClientChannelRegistered value.
        /// </summary>
        private bool ClientChannelRegistered
        {
            get
            {
                return _clientChannelRegistered;
            }
        }

        /// <summary>
        /// Gets the ClientChannelRegistered value.
        /// </summary>
        private bool ServerChannelRegistered
        {
            get
            {
                return _serverChannelRegistered;
            }
        }

        /// <summary>
		/// Creates a server side TCP channel.
		/// </summary>
		/// <param name="port"></param>
		/// <returns></returns>
		public IChannel RegisterServerTcpChannel(int port)
		{
            _channelRegistrationLock.AcquireWriterLock(-1);
            try
            {
                if (DebugRegistration)
                    WriteDebugRegistrationLog(string.Format("Registering server channel on '{0}'...", port));
                
                // ok...
                BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
                provider.TypeFilterLevel = TypeFilterLevel.Full;

                // return...
                IChannel channel = this.RegisterTcpChannel(port, null, provider);
                if (channel == null)
                    throw new InvalidOperationException("'channel' is null.");

                // set...
                _serverChannelRegistered = true;

                // return...
                return channel;
            }
            finally
            {
                _channelRegistrationLock.ReleaseLock();
            }
		}

		/// <summary>
		/// Creates a TCP channel.
		/// </summary>
		/// <param name="port"></param>
		/// <param name="clientProvider"></param>
		/// <param name="serverProvider"></param>
		/// <returns></returns>
		private IChannel RegisterTcpChannel(int port, IClientChannelSinkProvider clientProvider, IServerChannelSinkProvider serverProvider)
		{
			if(clientProvider == null && serverProvider == null)
				throw new ArgumentException("Either clientProvider or serverProvider must be supplied.");

			// set...
			if(DebugRegistration)
				WriteDebugRegistrationLog(string.Format("Creating TCP channel on port '{0}', client: {1}, server: {2}", port, clientProvider, serverProvider));

			// settings...
			IDictionary props = new HybridDictionary();
			if(port != 0)
				props["port"] = port;
            props.Add("secure", false);
            props.Add("typeFilterLevel", TypeFilterLevel.Full);

			// create...
			TcpChannel channel = new TcpChannel(props, clientProvider, serverProvider);
			this.RegisterChannel(channel, false);

			// return...
			return channel;
		}

		/// <summary>
		/// Disposes the object.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(DisposeReason.ExplicitCall);
		}
		
		/// <summary>
		/// Disposes the object.
		/// </summary>
		/// <param name="reason"></param>
		private void Dispose(DisposeReason reason)
		{
			// mbr - 14-05-2006 - unregister...
			ArrayList toDo = new ArrayList(ChannelServices.RegisteredChannels);
			foreach(IChannel channel in toDo)
				ChannelServices.UnregisterChannel(channel);
		
			// suppress...
			if(reason == DisposeReason.ExplicitCall)
				GC.SuppressFinalize(this);
		}

        // mbr - 2009-04-15 - not used.
        ///// <summary>
        ///// Registers the core services.
        ///// </summary>
        //private void RegisterCoreServices()
        //{
        //    this.RegisterCoreServices(DefaultAppName);
        //}

        // mbr - 2009-04-15 - not used.
        ///// <summary>
        ///// Registers the core services.
        ///// </summary>
        //private void RegisterCoreServices(string appName)
        //{
        //    if(appName == null)
        //        throw new ArgumentNullException("appName");
        //    if(appName.Length == 0)
        //        throw new ArgumentOutOfRangeException("'appName' is zero-length.");

        //    this.RegisterWellKnownServices(appName, this.GetType().Assembly);
        //}

        public T GetRemoteObject<T>(RemotingProtocol protocol, string hostName, int port, string appName = null)
        {
            return (T)GetRemoteObject(protocol, hostName, port, appName ?? this.DefaultAppName, typeof(T));
        }

        /// <summary>
        /// Gets a remote object.
        /// </summary>
        /// <param name="protocol"></param>
        /// <param name="hostName"></param>
        /// <param name="port"></param>
        /// <param name="appName"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        // mbr - 17-01-2006 - added.		
        public object GetRemoteObject(RemotingProtocol protocol, string hostName, int port, string appName, Type objectType)
		{
			return this.GetRemoteObject(protocol, hostName, port, appName, objectType, null);
		}

		/// <summary>
		/// Gets a remote object.
		/// </summary>
		/// <param name="protocol"></param>
		/// <param name="hostName"></param>
		/// <param name="port"></param>
		/// <param name="appName"></param>
		/// <param name="objectType"></param>
		/// <returns></returns>
		// mbr - 17-01-2006 - added.		
		public object GetRemoteObject(RemotingProtocol protocol, string hostName, int port, string appName, Type objectType, object state)
		{
			if(objectType == null)
				throw new ArgumentNullException("objectType");
			
			// get...
			string url = this.GetUrl(protocol, hostName, port, appName, objectType);
			if(url == null)
				throw new InvalidOperationException("'url' is null.");
			if(url.Length == 0)
				throw new InvalidOperationException("'url' is zero-length.");

            // mbr - 2009-01-28 - added an argument to stop this behaviour (as apparently, sometimes, no good...)
            if (this.ManageReciprocalClient)
            {
                // mbr - 2009-01-16 - if we are within IIS, we need to make sure we have a client channel registered...
                _channelRegistrationLock.AcquireReaderLock(-1);
                try
                {
                    if (!(this.ClientChannelRegistered) && !(this.ServerChannelRegistered))
                    {
                        _channelRegistrationLock.UpgradeToWriterLock(-1);

                        // still not registered?
                        if (!(this.ClientChannelRegistered))
                            this.RegisterClientTcpChannel();
                    }
                }
                finally
                {
                    _channelRegistrationLock.ReleaseLock();
                }
            }

            // mbr - 2009-08-12 - manage clients?
            if (this.ManageClientRegistrations)
                EnsureClientRegistered(objectType, url);

            // debug...
            if (DebugRegistration)
                WriteDebugRegistrationLog(string.Format("Getting object '{0}' (url: {1}, state: {2})...", objectType, url, state));

            // return...
            object obj = Activator.GetObject(objectType, url, state);
            if (obj == null)
                throw new InvalidOperationException("obj is null.");
            return obj;
        }

        private void EnsureClientRegistered(Type type, string url)
        {
            if (type == null)
                throw new ArgumentNullException("type");
            if (url == null)
                throw new ArgumentNullException("url");
            if (url.Length == 0)
                throw new ArgumentException("'url' is zero-length.");

            // get...
            string key = string.Format("{0}|{1}", type.AssemblyQualifiedName, url);
            bool registered = (bool)ClientRegistrations[key];
            if (!(registered))
                throw new InvalidOperationException("Client registration failed.");
        }

        void _clientRegistrations_CreateItemValue(object sender, CreateLookupItemEventArgs e)
        {
            // get...
            string key = (string)e.Key;

            // split...
            int index = key.IndexOf("|");
            if (index == -1)
                throw new InvalidOperationException(string.Format("The key '{0}' is invalid.", key));

            // get...
            string typeName = key.Substring(0, index);
            if (typeName == null)
                throw new InvalidOperationException("'typeName' is null.");
            Type type = Type.GetType(typeName, false);
            if (type == null)
                throw new InvalidOperationException(string.Format("A type with name '{0}' was not found.", typeName));
            string url = key.Substring(index + 1);
            if (url == null)
                throw new InvalidOperationException("'url' is null.");
            if (url.Length == 0)
                throw new InvalidOperationException("'url' is zero-length.");

            // debug...
            if (DebugRegistration)
                WriteDebugRegistrationLog(string.Format("Registering well-known client type on '{0}' (url: {1})...", type, url));

            // register...
            try
            {
                RemotingConfiguration.RegisterWellKnownClientType(type, url);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Registration of type '{0}' against URL '{1}' failed.", type, url), ex);
            }

            // set...
            e.NewValue = true;
        }

        private void WriteDebugRegistrationLog(string message)
        {
            string process = null;
            if (Runtime.Current.IsWebApp)
                process = "$iis$";
            else
                process = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

            // get...
            Debug.WriteLine(string.Format("[{0}] {1}", process, message));
        }

        /// <summary>
        /// Gets the ClientRegistrations value.
        /// </summary>
        private Lookup ClientRegistrations
        {
            get
            {
                return _clientRegistrations;
            }
        }

		/// <summary>
		/// Gets the URL.
		/// </summary>
		/// <param name="hostName"></param>
		/// <param name="port"></param>
		/// <param name="appName"></param>
		/// <param name="interfaceType"></param>
		/// <returns></returns>
		public string GetUrl(RemotingProtocol protocol, string hostName, int port, string appName, Type interfaceType)
		{
			if(hostName == null)
				throw new ArgumentNullException("hostName");
			if(hostName.Length == 0)
				throw new ArgumentOutOfRangeException("'hostName' is zero-length.");
			if(appName == null)
				throw new ArgumentNullException("appName");
			if(appName.Length == 0)
				throw new ArgumentOutOfRangeException("'appName' is zero-length.");
			if(interfaceType == null)
				throw new ArgumentNullException("interfaceType");

			// uri...
			string uri = this.GetUri(appName, interfaceType, false);
			if(uri == null)
				throw new InvalidOperationException("'uri' is null.");
			if(uri.Length == 0)
				throw new InvalidOperationException("'uri' is zero-length.");

			// build...
			return string.Format("{0}://{1}:{2}/{3}", this.GetProtocolString(protocol), hostName, port, uri);
		}

		/// <summary>
		/// Gets the string for the given protocol.
		/// </summary>
		/// <param name="protocol"></param>
		/// <returns></returns>
		private string GetProtocolString(RemotingProtocol protocol)
		{
			switch(protocol)
			{
				case RemotingProtocol.Tcp:
					return "tcp";
				case RemotingProtocol.Http:
					return "http";
				case RemotingProtocol.Https:
					return "https";

				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", protocol, protocol.GetType()));
			}
		}

		/// <summary>
		/// Gets or sets the defaultappname
		/// </summary>
		public string DefaultAppName
		{
			get
			{
				if(!(this.IsIis))
					return _defaultAppName;
				else
					return this.IisVdir;
			}
			set
			{
				if(this.IsIis)
					throw new InvalidOperationException("The default application name cannot be set when the remote objects are configured to be hosted within IIS.");

				// check to see if the value has changed...
				if(value != _defaultAppName)
				{
					// set the value...
					_defaultAppName = value;
				}
			}
		}

		/// <summary>
		/// Gets the iisvdir.
		/// </summary>
        private string IisVdir
		{
			get
			{
				// returns the value...
				return _iisVdir;
			}
		}

        /// <summary>
        /// Gets the ManageReciprocalClient value.
        /// </summary>
        private bool ManageReciprocalClient
        {
            get
            {
                return _manageReciprocalClient;
            }
        }

        /// <summary>
        /// Gets the ManageClientRegistrations value.
        /// </summary>
        private bool ManageClientRegistrations
        {
            get
            {
                return _manageClientRegistrations;
            }
        }

        private bool DebugRegistration
        {
            get
            {
                return true;
            }
        }
	}
}
