// BootFX - Application framework for .NET applications
// 
// File: RemotingProtocol.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Remoting
{
	/// <summary>
	/// Defines remoting protocol.
	/// </summary>
	public enum RemotingProtocol
	{
		/// <summary>
		/// Defines TCP/binary Remoting.
		/// </summary>
		Tcp = 0,

		/// <summary>
		/// Defines HTTP/SOAP Remoting.
		/// </summary>
		Http = 1,

		/// <summary>
		/// Defines HTTP/SOAP Remoting that runs over SSL.
		/// </summary>
		Https = 2
	}
}
