// BootFX - Application framework for .NET applications
// 
// File: AdLookup.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.DirectoryServices;
using BootFX.Common.Management;

namespace BootFX.Common
{
	/// <summary>
	/// Defines a class that can examine information in Active Directory.
	/// </summary>
	public class AdLookup : Loggable
	{
		/// <summary>
		/// Private field to support <c>PerUser</c> property.
		/// </summary>
		private Lookup _perUser;
		
		/// <summary>
		/// Private field to support <c>DirectoryEntry</c> property.
		/// </summary>
		private DirectoryEntry _directoryEntry;

		public AdLookup(object adsObject)
		{
			if(adsObject == null)
				throw new ArgumentNullException("adsObject");
			
			this.Initialize(new DirectoryEntry(adsObject));
		}

		public AdLookup(string path) : this(path, null, null, AuthenticationTypes.None)
		{
		}

		public AdLookup(string path, string username, string password) : this(path, username, password, AuthenticationTypes.Secure)
		{
		}

		public AdLookup(string path, string username, string password, AuthenticationTypes authType)
		{
			if(path == null)
				throw new ArgumentNullException("path");
			if(path.Length == 0)
				throw new ArgumentOutOfRangeException("'path' is zero-length.");
			
			// username?
			if(username != null && username.Length > 0)
				this.Initialize(new DirectoryEntry(path, username, password, authType));
			else
				this.Initialize(new DirectoryEntry(path));
		}

		private void Initialize(DirectoryEntry entry)
		{
			if(entry == null)
				throw new ArgumentNullException("entry");
			
			// set...
			_directoryEntry = entry;

			// create...
			_perUser = new Lookup();
			_perUser.CreateItemValue += new CreateLookupItemEventHandler(_perUser_CreateItemValue);
		}

		/// <summary>
		/// Gets the directoryentry.
		/// </summary>
		internal DirectoryEntry DirectoryEntry
		{
			get
			{
				// returns the value...
				return _directoryEntry;
			}
		}

		/// <summary>
		/// Gets the peruser.
		/// </summary>
		private Lookup PerUser
		{
			get
			{
				// returns the value...
				return _perUser;
			}
		}

		private void _perUser_CreateItemValue(object sender, CreateLookupItemEventArgs e)
		{
			e.NewValue = new AdPerUserLookup(this, (string)e.Key);
		}

		/// <summary>
		/// Gets the user's group memberships.
		/// </summary>
		/// <param name="domainUsername"></param>
		/// <returns></returns>
		public string[] GetGroupMemberships(string domainUsername)
		{
			if(domainUsername == null)
				throw new ArgumentNullException("domainUsername");

			// per user...
			AdPerUserLookup lookup = (AdPerUserLookup)PerUser[domainUsername];
			if(lookup == null)
				throw new InvalidOperationException("lookup is null.");
			
			// find...
			return lookup.GetStringArrayValue("memberOf", OnNotFound.ThrowException);
		}
	}
}
