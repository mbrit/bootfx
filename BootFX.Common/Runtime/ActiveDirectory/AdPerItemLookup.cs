// BootFX - Application framework for .NET applications
// 
// File: AdPerItemLookup.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.DirectoryServices;

namespace BootFX.Common
{
	/// <summary>
	/// Defines an instance of <c>AdPerItemLookup</c>.
	/// </summary>
	internal abstract class AdPerItemLookup : Lookup
	{
		/// <summary>
		/// Private field to support <c>Lookup</c> property.
		/// </summary>
		private AdLookup _parentLookup;

		/// <summary>
		/// Private field to support <c>searchSpecification</c> property.
		/// </summary>
		private string _searchSpecification;
		
		internal AdPerItemLookup(AdLookup lookup, string searchSpecification)
		{
			if(lookup == null)
				throw new ArgumentNullException("lookup");
			if(searchSpecification == null)
				throw new ArgumentNullException("searchSpecification");
			if(searchSpecification.Length == 0)
				throw new ArgumentOutOfRangeException("'searchSpecification' is zero-length.");
			
			_parentLookup = lookup;
			_searchSpecification = searchSpecification;
		}

		/// <summary>
		/// Gets the searchspecification.
		/// </summary>
		private string SearchSpecification
		{
			get
			{
				// returns the value...
				return _searchSpecification;
			}
		}

		protected override void OnCreateItemValue(CreateLookupItemEventArgs e)
		{
			string name = (string)e.Key;
			if(name == null)
				throw new InvalidOperationException("'name' is null.");
			if(name.Length == 0)
				throw new InvalidOperationException("'name' is zero-length.");

			// check...
			if(SearchSpecification == null)
				throw new InvalidOperationException("'SearchSpecification' is null.");
			if(SearchSpecification.Length == 0)
				throw new InvalidOperationException("'SearchSpecification' is zero-length.");

			// get...
			using(DirectorySearcher searcher = new DirectorySearcher(this.DirectoryEntry, this.SearchSpecification))
			{
				searcher.PropertiesToLoad.Add(name);

				// run...
				SearchResult result = searcher.FindOne();
				if(result == null)
				{
					throw new InvalidOperationException(string.Format("An LDAP search returned no results.\r\nSearch: {0}\r\nPath: {1}\r\nUsername: {2} ({3})", 
						searcher.Filter, this.DirectoryEntry.Path, this.DirectoryEntry.Username, this.DirectoryEntry.AuthenticationType));
				}

				// find the one that we asked for...
				foreach(DictionaryEntry entry in result.Properties)
				{
					string foundName = (string)entry.Key;
					if(string.Compare(foundName, name, true, System.Globalization.CultureInfo.InvariantCulture) == 0)
					{
						// walk...
						ArrayList results = new ArrayList();
						foreach(object value in (IEnumerable)entry.Value)
							results.Add(value);

						// return...
						e.NewValue = results;
						return;
					}
				}

				// not found...
				e.NewValue = DBNull.Value;
			}
		}

		/// <summary>
		/// Gets the lookup.
		/// </summary>
		private AdLookup ParentLookup
		{
			get
			{
				// returns the value...
				return _parentLookup;
			}
		}

		private DirectoryEntry DirectoryEntry
		{
			get
			{
				if(ParentLookup == null)
					throw new InvalidOperationException("ParentLookup is null.");
				return this.ParentLookup.DirectoryEntry;
			}
		}

		internal object GetValue(string name, Type type, object defaultValue, bool throwIfNotFound)
		{
			// get...
			object value = this[name];
			if(value != DBNull.Value)
				return value;
			else
			{
				if(throwIfNotFound)
					throw new InvalidOperationException(string.Format("A value for '{0}' was not found.", name));
				else
					return null;
			}
		}
	}
}
