// BootFX - Application framework for .NET applications
// 
// File: CertificatePolicy.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Management;

namespace BootFX.Common
{
	/// <summary>
	/// Defines an instance of <c>CertificatePolicy</c>.
	/// </summary>
	public class CertificatePolicy : Loggable, ICertificatePolicy
	{
		public const long CertVALIDITYPERIODNESTING = 0x800B0102L;
		public const long CertROLE = 0x800B0103L; 
		public const long CertPATHLENCONST = 0x800B0104L; 
		public const long CertCRITICAL = 0x800B0105L; 
		public const long CertPURPOSE = 0x800B0106L; 
		public const long CertISSUERCHAINING = 0x800B0107L; 
		public const long CertMALFORMED = 0x800B0108L; 
		public const long CertCHAINING = 0x800B010AL; 
		public const long CertREVOKED = 0x800B010CL; 
		public const long CertUNTRUSTEDTESTROOT = 0x800B010DL; 
		public const long CertREVOCATION_FAILURE = 0x800B010EL; 
		public const long CertWRONG_USAGE = 0x800B0110L; 
		public const long CertUNTRUSTEDCA = 0x800B0112L;
		public const long CertOUTOFDATE = 0x800B0101L;

		/// <summary>
		/// Raised when there is a problem with the certificate
		/// </summary>
		public event CertificateProblemEventHandler Problem;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public CertificatePolicy()
		{
		}

		public bool CheckValidationResult(ServicePoint srvPoint, System.Security.Cryptography.X509Certificates.X509Certificate certificate, 
			WebRequest request, int problemCode)
		{
			// ok?
			if(problemCode == 0)
				return true;
			else
			{			
				// create...
				CertificateProblemEventArgs e = new CertificateProblemEventArgs(srvPoint, certificate, request, problemCode);
				this.OnProblem(e);

				// return...
				return e.AllowRequest;
			}
		}

		/// <summary>
		/// Raises the <c>CertificateProblem</c> event.
		/// </summary>
		protected virtual void OnProblem(CertificateProblemEventArgs e)
		{
			// raise...
			if(Problem != null)
				Problem(this, e);
		}
	}
}
