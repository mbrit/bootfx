// BootFX - Application framework for .NET applications
// 
// File: FileSystemPoller.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using BootFX.Common.Management;

namespace BootFX.Common
{
	/// <summary>
	/// Summary description for FileSystemPoller.
	/// </summary>
	// mbr - 23-03-2006 - added loggable.	
	[DefaultEvent("Created")]
	public class FileSystemPoller : Component, ISupportInitialize, ILoggable
	{
		/// <summary>
		/// Private field to support <see cref="FlushOnStart"/> property.
		/// </summary>
		private bool _flushOnStart;
		
		/// <summary>
		/// Private field to support <c>MatchRegex</c> property.
		/// </summary>
		private Regex _matchRegex;
		
		/// <summary>
		/// Private field to support <c>Logs</c> property.
		/// </summary>
		private ILogSet _logs = null;

		private static char[] _wildCards = new char[2] { '?', '*' };
		private string _filter;
		private string _directory;
		private int _pollingInterval;
		private bool _enabled;
		private bool _initializing;
		private ISynchronizeInvoke _synchronizingObject;
 
		private Thread _pollingThread;
		private ArrayList _lastTime = new ArrayList();
 
		/// <summary>
		/// Created file system event
		/// </summary>
		public event FileSystemEventHandler Created;

		/// <summary>
		/// Deleted file system event
		/// </summary>
		public event FileSystemEventHandler Deleted;

		[Obsolete("Use the constructor that takes a 'flushOnStart' value.")]
		public FileSystemPoller() : this(false)
		{
		}

		public FileSystemPoller(bool flushOnStart)
		{
			_enabled = false;
			_initializing = false;
			_directory = string.Empty;
			_filter = "*.*";
			_pollingInterval = 1000;
			_flushOnStart = flushOnStart;
		}

		/// <summary>
		/// Gets or sets the filter used by the file system poller
		/// </summary>
		[TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DefaultValue("*.*")]
		public string Filter
		{
			get
			{
				return this._filter;
			}
			set
			{
				if ((value == null) || (value == string.Empty))
				{
					value = "*.*";
				}
				if (string.Compare(this._filter, value, true, CultureInfo.InvariantCulture) != 0)
				{
					this._filter = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the polling interval
		/// </summary>
		[DefaultValue(1000)]
		public int PollingInterval
		{
			get
			{
				return this._pollingInterval;
			}
			set
			{
				if ((value == 0))
				{
					value = 5000;
				}
				_pollingInterval = value;
			}
		}

		/// <summary>
		/// Gets or sets the path used by the poller
		/// </summary>
		[Editor("System.Diagnostics.Design.FSWPathEditor, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), DefaultValue(""), IODescription("FSW_Path")]
		public string Path
		{
			get
			{
				return this._directory;
			}
			set
			{
				value = (value == null) ? string.Empty : value;
				string exceptionMessage = string.Format("The directory name {0} is invalid.",value);

				if (string.Compare(this._directory, value, true, CultureInfo.InvariantCulture) == 0)
				{
					return;
				}
				if (base.DesignMode)
				{
					char[] invalid = null;
					invalid = System.IO.Path.GetInvalidPathChars();

					// now what?
					if ((value.IndexOfAny(_wildCards) == -1) && (value.IndexOfAny(invalid) == -1))
					{
						this._directory = value;
						this.Restart();
					}
					throw new ArgumentException(exceptionMessage);
				}

				this._directory = value;
				this.Restart();
			}
		}
 
		/// <summary>
		/// Gets or sets whether raising of events is enabled
		/// </summary>
		[DefaultValue(false)]
		public bool EnableRaisingEvents
		{
			get
			{
				return this._enabled;
			}
			set
			{
				if (this._enabled != value)
				{
					this._enabled = value;

					// ?
					if (!this.IsSuspended())
					{
						if (this._enabled)
							this.StartRaisingEvents();
						else
							this.StopRaisingEvents();
					}
				}
			}
		}

		/// <summary>
		/// Site of the component
		/// </summary>
		[Browsable(false)]
		public override ISite Site
		{
			get
			{
				return base.Site;
			}
			set
			{
				base.Site = value;
				if ((this.Site != null) && this.Site.DesignMode)
				{
					this.EnableRaisingEvents = true;
				}
			}
		}
 
		/// <summary>
		/// Is the current poller suspended
		/// </summary>
		/// <returns></returns>
		private bool IsSuspended()
		{
			if (!_initializing)
			{
				return base.DesignMode;
			}
			return true;
		}

		/// <summary>
		/// Used to match the relative path to the filters defined
		/// </summary>
		/// <param name="relativePath"></param>
		/// <returns></returns>
		private bool MatchPattern(string relativePath)
		{
			string fileName = System.IO.Path.GetFileName(relativePath);
			if (fileName != null)
			{
				return PatternMatcher.StrictMatchPattern(_filter.ToUpper(CultureInfo.InvariantCulture), fileName.ToUpper(CultureInfo.InvariantCulture));
			}
			return false;
		}
 
		/// <summary>
		/// Restarts the file system poller
		/// </summary>
		private void Restart()
		{
			if (!this.IsSuspended() && _enabled)
			{
				StopRaisingEvents();
				StartRaisingEvents();
			}
		}

		/// <summary>
		/// Starts raising events from the polling mechanism
		/// </summary>
		private void StartRaisingEvents()
		{
			if (this.IsSuspended())
			{
				_enabled = true;
			}
			else
			{
				// Start polling
				_pollingThread = new Thread(new ThreadStart(PollingThreadEntryPoint));
				_pollingThread.Name = string.Format("FileSystemPoller: {0} ({1})", this.Path, Thread.CurrentThread.GetHashCode());
				_pollingThread.IsBackground = true;
				_pollingThread.Priority = ThreadPriority.BelowNormal;
				_pollingThread.Start();

				// set as enabled...
				_enabled = true;
			}
		}
 
		/// <summary>
		/// Stops raising events from the polling mechanism
		/// </summary>
		private void StopRaisingEvents()
		{
			if (this.IsSuspended())
			{
				_enabled = false;
			}
			else
			{
				//Stop the polling
				if(_pollingThread != null && _pollingThread.IsAlive)
					_pollingThread.Abort();

				_enabled = false;
			}
		}

		// mbr - 23-03-2006 - initial.
		private void SetupInitialList()
		{
			// walk...
			while(true)
			{
				try
				{
					// get the files...
					_lastTime = this.GetPendingFiles();
					if(_lastTime == null)
						throw new InvalidOperationException("_lastTime is null.");

					// we're ok...
					return;
				}
				catch(ThreadAbortException)
				{
					break;
				}
				catch(Exception ex)
				{
					// log it...
					if(this.Log.IsWarnEnabled)
						this.Log.Warn("Failed to handle initial poll.", ex);

					// Weh have caught an exception, the problem here is for linux and unix we cannot tell if the folder exists
					// so we have to assume it does. If it doesn't we will wait 1 minute before trying again
					Thread.Sleep(new TimeSpan(0,1,0));
				}
			}
		}

		/// <summary>
		/// Gets the pending files in the folder.
		/// </summary>
		/// <returns></returns>
		private ArrayList GetPendingFiles()
		{
			ArrayList thisTime = new ArrayList();
			foreach(string path in Directory.GetFiles(Path))
			{
				if(this.MatchPattern(path))
					thisTime.Add(path.ToLower());
			}

			//r eturn...
			return thisTime;
		}

		/// <summary>
		/// Polling thread entry point
		/// </summary>
		private void PollingThreadEntryPoint()
		{
			// mbr - 08-12-2006 - if we're flushonstart, reset the initial list...
			if(!(this.FlushOnStart))
				this.SetupInitialList();
			else
				_lastTime = new ArrayList();

			// Start the loop to check for new files
			while(true)
			{
				try
				{
					this.RunNow();
				}
				catch(ThreadAbortException)
				{
					break;
				}
				catch(Exception ex)
				{
					if(this.Log.IsWarnEnabled)
						this.Log.Warn("Unhandled exception in general poll.", ex);

					// Weh have caught an exception, the problem here is for linux and unix we cannot tell if the folder exists
					// so we have to assume it does. If it doesn't we will wait 1 minute before trying again
					Thread.Sleep(new TimeSpan(0,1,0));
				}
				finally
				{
					// wait...
					Thread.Sleep(PollingInterval);
				}
			}
		}

		/// <summary>
		/// Runs the operation to raise events.
		/// </summary>
		private void RunNow()
		{
			// get the current state of the folder...
			ArrayList thisTime = this.GetPendingFiles();

			// get the delta...
			ArrayList createdFiles = new ArrayList();
			ArrayList deletedFiles = new ArrayList(_lastTime);
			foreach(string path in thisTime)
			{
				if(!(_lastTime.Contains(path)))
					createdFiles.Add(path);
				if(_lastTime.Contains(path))
					deletedFiles.Remove(path);
			}

			// do created files...
			foreach(string path in createdFiles)
			{
				if(IsUnlocked(path))
				{
					// raise...
					FileInfo info = new FileInfo(path);
					try
					{
						// raise it...
						this.OnCreated(new FileSystemEventArgs(WatcherChangeTypes.Created, info.DirectoryName, info.Name));

						// is it still there?
						if(File.Exists(path))
						{
							if(!(_lastTime.Contains(path)))
								_lastTime.Add(path);
						}
						else
						{
							// if it doesn't exist any more - raise a delete event...
							this.OnDeleted(new FileSystemEventArgs(WatcherChangeTypes.Deleted, info.DirectoryName, info.Name));
						}
					}
					catch(Exception ex)
					{
						throw new InvalidOperationException(string.Format("Failed to raise create notification for '{0}'.", info.FullName), ex);
					}
				}
			}

			// now check for deleted files - this is done by comparing the last-known state with this state for ones that are no longer there...
			foreach(string path in deletedFiles)
			{
				// mbr - 19-04-2006 - added change to do this method if the file is not there
				// (otherwise this will get called on every tick.)					
				if(!(File.Exists(path)) || IsUnlocked(path))
				{
					// raise...
					FileInfo info = new FileInfo(path);
					try
					{
						this.OnDeleted(new FileSystemEventArgs(WatcherChangeTypes.Deleted, info.DirectoryName, info.Name));

						// is it still there?
						if(!(File.Exists(path)) && _lastTime.Contains(path))
							_lastTime.Remove(path);
					}
					catch(Exception ex)
					{
						throw new InvalidOperationException(string.Format("Failed to raise delete notification for '{0}'.", info.FullName), ex);
					}
				}
			}
		}
 
		/// <summary>
		/// Returns true a lock can be obtained on a file.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		private bool IsUnlocked(string path)
		{
			if(path == null)
				throw new ArgumentNullException("path");
			if(path.Length == 0)
				throw new ArgumentOutOfRangeException("'path' is zero-length.");
			
			// regex...
			Regex regex = this.MatchRegex;

			// enabled...
			if(this.Log.IsInfoEnabled)
			{
				if(regex != null)
					this.Log.Info(string.Format("Attempting to get a lock on '{0}' (using regex matching)...", path));
				else
					this.Log.Info(string.Format("Attempting to get a lock on '{0}'...", path));
			}

			// mbr - 19-04-2006 - add exists assertion
			if(!(File.Exists(path)))
				throw new FileNotFoundException(string.Format("Cannot get lock on '{0}' as the file does not exist.", path), path);

			// try...
			int attempts = 0;
			while(attempts < 10)
			{
				try
				{
					// mbr - 01-09-2006 - do we have a regex?
					if(regex == null)
					{
						using(FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
						{
							// ok...
							if(this.Log.IsInfoEnabled)
								this.Log.Info("\tSucceeded (trailing match not required).");
							return true;
						}
					}
					else
					{
						// read the lot...
						string buf = null;
						using(StreamReader reader = new StreamReader(path))
						{
							buf = reader.ReadToEnd();

							// OK?
							Match match = regex.Match(buf);
							if(match.Success)
							{
								if(this.Log.IsInfoEnabled)
									this.Log.Info("\tSucceeded (File contents contained required expression)");
								return true;
							}
							else
							{
								if(this.Log.IsInfoEnabled)
									this.Log.Info("\tFailed (file was opened, but the required text was not found)");
								return false;
							}
						}
					}
				}
				catch(IOException)
				{
					// wait a bit...
					Thread.Sleep(500);
				}
				finally
				{
					// next...
					attempts++;
				}
			}

			// nope...
			if(this.Log.IsInfoEnabled)
				this.Log.Info("\tFailed.");

			// nope...
			return false;
		}

		/// <summary>
		/// Synchronizing object used to fire events
		/// </summary>
		[DefaultValue((string) null)]
		public ISynchronizeInvoke SynchronizingObject
		{
			get
			{
				if ((this._synchronizingObject == null) && base.DesignMode)
				{
					IDesignerHost host = (IDesignerHost) this.GetService(typeof(IDesignerHost));
					if (host != null)
					{
						if ((host.RootComponent != null) && (host.RootComponent is ISynchronizeInvoke))
						{
							this._synchronizingObject = (ISynchronizeInvoke) host.RootComponent;
						}
					}
				}
				return this._synchronizingObject;
			}
			set
			{
				this._synchronizingObject = value;
			}
		}

		/// <summary>
		/// Fires when a file is created
		/// </summary>
		/// <param name="e"></param>
		protected void OnCreated(FileSystemEventArgs e)
		{
			if (Created != null)
			{
				if ((this.SynchronizingObject != null) && this.SynchronizingObject.InvokeRequired)
					this.SynchronizingObject.BeginInvoke(Created, new object[] {this,e});
				else
					this.Created(this, e);
			}
		}

		/// <summary>
		/// Fires when a file is deleted
		/// </summary>
		/// <param name="e"></param>
		protected void OnDeleted(FileSystemEventArgs e)
		{
			if (Deleted != null)
			{
				if ((this.SynchronizingObject != null) && this.SynchronizingObject.InvokeRequired)
					this.SynchronizingObject.BeginInvoke(Deleted, new object[] {this,e});
				else
					this.Deleted(this, e);
			}
		}

		/// <summary>
		/// Override of the dispose method
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.StopRaisingEvents();
			}

			base.Dispose(disposing);
		}
 
		#region ISupportInitialize Members

		public void BeginInit()
		{
			bool oldEnabled = _enabled;
			StopRaisingEvents();
			_enabled = oldEnabled;
			_initializing = true;
		}

		public void EndInit()
		{
			_initializing = false;
			if ((_directory.Length != 0) && _enabled)
			{
				this.StartRaisingEvents();
			}

		}

		#endregion

		
		ILog ILoggable.Log
		{
			get
			{
				return this.Log;
			}
		}

		/// <summary>
		/// Gets the default log.
		/// </summary>
		protected ILog Log
		{
			get
			{
				return this.Logs.DefaultLog;
			}
		}

		ILogSet ILoggable.Logs
		{
			get
			{
				return this.Logs;
			}
		}

		/// <summary>
		/// Gets the set of logs for other activities.
		/// </summary>
		protected ILogSet Logs
		{
			get
			{
				// mbr - 11-10-2005 - provided an ability to invalidate logs if the context changes...				
				if(_logs != null && _logs.ContextId != LogSet.CurrentContextId)
					_logs = null;
				if(_logs == null)
					_logs = new LogSet(this.GetType());
				return _logs;
			}
		}

		/// <summary>
		/// Gets the matchregex.
		/// </summary>
		private Regex MatchRegex
		{
			get
			{
				// returns the value...
				return _matchRegex;
			}
		}

		public void SetMatchRegex(string expression, RegexOptions options)
		{
			if(expression == null)
				throw new ArgumentNullException("expression");
			if(expression.Length == 0)
				throw new ArgumentOutOfRangeException("'expression' is zero-length.");
			
			// set...
			_matchRegex = new Regex(expression, options);
		}

		/// <summary>
		/// Gets the flushonstart.
		/// </summary>
		private bool FlushOnStart
		{
			get
			{
				return _flushOnStart;
			}
		}
	}

	/// <summary>
	/// Naughty naughty, taken from internal class in framework
	/// </summary>
	internal class PatternMatcher
	{
		public static bool MatchPattern(string expression, string name)
		{
			if (string.Compare(expression, "*.*", false, CultureInfo.InvariantCulture) == 0)
			{
				return true;
			}
			if (string.Compare(expression, "*", false, CultureInfo.InvariantCulture) == 0)
			{
				return true;
			}
			int num1 = expression.Length;
			StringBuilder builder1 = new StringBuilder(expression);
			int num2 = expression.LastIndexOf('*');
			if (num2 == (num1 - 1))
			{
				builder1.Append(".*");
			}
			for (int num3 = 0; num3 < num1; num3++)
			{
				char ch1 = builder1[num3];
				if (ch1 == '*')
				{
					builder1[num3] = '>';
				}
				else if (ch1 == '?')
				{
					builder1[num3] = '<';
				}
				else if (ch1 == '.')
				{
					builder1[num3] = '"';
				}
				else
				{
					if (ch1 == '\\')
					{
						throw new ArgumentException("Invalid character in pattern.");
					}
					builder1[num3] = char.ToUpper(ch1, CultureInfo.InvariantCulture);
				}
			}
			return PatternMatcher.StrictMatchPattern(builder1.ToString(), name.ToUpper(CultureInfo.InvariantCulture));
		}

		public static bool StrictMatchPattern(string expression, string name)
		{
			int num9;
			char ch1 = '\0';
			char ch2 = '\0';
			int[] numArray1 = new int[0x10];
			int[] numArray2 = new int[0x10];
			bool flag1 = false;
			if (((name == null) || (name.Length == 0)) || ((expression == null) || (expression.Length == 0)))
			{
				return false;
			}
			if (expression.Equals("*") || expression.Equals("*.*"))
			{
				return true;
			}
			if ((expression[0] == '*') && (expression.IndexOf('*', 1) == -1))
			{
				int num10 = expression.Length - 1;
				if ((name.Length >= num10) && (string.Compare(expression, 1, name, name.Length - num10, num10, true, CultureInfo.InvariantCulture) == 0))
				{
					return true;
				}
			}
			numArray1[0] = 0;
			int num7 = 1;
			int num1 = 0;
			int num8 = expression.Length * 2;
			while (!flag1)
			{
				int num3;
				if (num1 < name.Length)
				{
					ch1 = name[num1];
					num3 = 1;
					num1++;
				}
				else
				{
					flag1 = true;
					if (numArray1[num7 - 1] == num8)
					{
						break;
					}
				}
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				while (num4 < num7)
				{
					int num2 = (numArray1[num4++] + 1) / 2;
					num3 = 0;
				Label_00F7:
					if (num2 != expression.Length)
					{
						num2 += num3;
						num9 = num2 * 2;
						if (num2 == expression.Length)
						{
							numArray2[num5++] = num8;
						}
						else
						{
							ch2 = expression[num2];
							num3 = 1;
							if (num5 >= 14)
							{
								int num11 = numArray2.Length * 2;
								int[] numArray3 = new int[num11];
								Array.Copy(numArray2, numArray3, numArray2.Length);
								numArray2 = numArray3;
								numArray3 = new int[num11];
								Array.Copy(numArray1, numArray3, numArray1.Length);
								numArray1 = numArray3;
							}
							if (ch2 == '*')
							{
								numArray2[num5++] = num9;
								numArray2[num5++] = num9 + 1;
								goto Label_00F7;
							}
							if (ch2 == '>')
							{
								bool flag2 = false;
								if (!flag1 && (ch1 == '.'))
								{
									int num13 = name.Length;
									for (int num12 = num1; num12 < num13; num12++)
									{
										char ch3 = name[num12];
										num3 = 1;
										if (ch3 == '.')
										{
											flag2 = true;
											break;
										}
									}
								}
								if ((flag1 || (ch1 != '.')) || flag2)
								{
									numArray2[num5++] = num9;
									numArray2[num5++] = num9 + 1;
									goto Label_00F7;
								}
								numArray2[num5++] = num9 + 1;
								goto Label_00F7;
							}
							num9 += (num3 * 2);
							if (ch2 == '<')
							{
								if (flag1 || (ch1 == '.'))
								{
									goto Label_00F7;
								}
								numArray2[num5++] = num9;
							}
							else
							{
								if (ch2 == '"')
								{
									if (flag1)
									{
										goto Label_00F7;
									}
									if (ch1 == '.')
									{
										numArray2[num5++] = num9;
										goto Label_0294;
									}
								}
								if (!flag1)
								{
									if (ch2 == '?')
									{
										numArray2[num5++] = num9;
									}
									else if (ch2 == ch1)
									{
										numArray2[num5++] = num9;
									}
								}
							}
						}
					}
				Label_0294:
					if ((num4 < num7) && (num6 < num5))
					{
						while (num6 < num5)
						{
							int num14 = numArray1.Length;
							while ((num4 < num14) && (numArray1[num4] < numArray2[num6]))
							{
								num4++;
							}
							num6++;
						}
					}
				}
				if (num5 == 0)
				{
					return false;
				}
				int[] numArray4 = numArray1;
				numArray1 = numArray2;
				numArray2 = numArray4;
				num7 = num5;
			}
			num9 = numArray1[num7 - 1];
			return (num9 == num8);
		}
	}
}
