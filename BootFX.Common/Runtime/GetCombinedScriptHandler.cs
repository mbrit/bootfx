// BootFX - Application framework for .NET applications
// 
// File: GetCombinedScriptHandler.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BootFX.Common.UI.Web
{
    public class GetCombinedScriptHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            var asString = context.Request.QueryString["type"];
            var type = (ScriptType)Enum.Parse(typeof(ScriptType), asString, true);
            var hash = context.Request.QueryString["hash"];
            var version = context.Request.QueryString["version"];

            var script = BfxScriptManager.GetScript(type, hash);

            if (type == ScriptType.JavaScript)
                context.Response.ContentType = "application/javascript";
            else if (type == ScriptType.Css)
                context.Response.ContentType = "text/css";
            context.Response.Write(script);
        }
    }
}
