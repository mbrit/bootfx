// BootFX - Application framework for .NET applications
// 
// File: CertificateProblemEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Collections;

namespace BootFX.Common
{
	public delegate void CertificateProblemEventHandler(object sender, CertificateProblemEventArgs e);

	/// <summary>
	/// Defines an instance of <c>CertificateProblemEventArgs</c>.
	/// </summary>
	public class CertificateProblemEventArgs
	{
		/// <summary>
		/// Private field to support <c>AllowRequest</c> property.
		/// </summary>
		private bool _allowRequest;
		
		/// <summary>
		/// Private field to support <see cref="ServicePoint"/> property.
		/// </summary>
		private ServicePoint _servicePoint;

		/// <summary>
		/// Private field to support <see cref="Certificate"/> property.
		/// </summary>
		private X509Certificate _certificate;

		/// <summary>
		/// Private field to support <see cref="Request"/> property.
		/// </summary>
		private WebRequest _request;

		/// <summary>
		/// Private field to support <see cref="ProblemCode"/> property.
		/// </summary>
		private long _problemCode;
		
		internal CertificateProblemEventArgs(ServicePoint servicePoint, X509Certificate certificate, 
			WebRequest request, long problemCode)
		{
			_servicePoint = servicePoint;
			_certificate = certificate;
			_request = request;
			_problemCode = problemCode;
		}

		/// <summary>
		/// Gets the problemcode.
		/// </summary>
		public long ProblemCode
		{
			get
			{
				return _problemCode;
			}
		}

		/// <summary>
		/// Gets the request.
		/// </summary>
		public WebRequest Request
		{
			get
			{
				return _request;
			}
		}
		
		/// <summary>
		/// Gets the certificate.
		/// </summary>
		public X509Certificate Certificate
		{
			get
			{
				return _certificate;
			}
		}
		
		/// <summary>
		/// Gets the servicepoint.
		/// </summary>
		public ServicePoint ServicePoint
		{
			get
			{
				return _servicePoint;
			}
		}

		/// <summary>
		/// Gets or sets the allowrequest
		/// </summary>
		public bool AllowRequest
		{
			get
			{
				return _allowRequest;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _allowRequest)
				{
					// set the value...
					_allowRequest = value;
				}
			}
		}
	}
}
