// BootFX - Application framework for .NET applications
// 
// File: AppSettingsPropertyBag.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using BootFX.Common.Xml;

namespace BootFX.Common
{
	/// <summary>
	/// Describes a property bag that uses the <c>AppSettings</c> block in the .config file as its backing store.
	/// </summary>
	public class AppSettingsPropertyBag : PropertyBag
	{
		/// <summary>
		/// Private field to support <c>Current</c> property.
		/// </summary>
		private static AppSettingsPropertyBag _current = new AppSettingsPropertyBag();
		
		/// <summary>
		/// Constructor.
		/// </summary>
		private AppSettingsPropertyBag()
		{
		}

		/// <summary>
		/// Gets or sets the current
		/// </summary>
		public static AppSettingsPropertyBag Current
		{
			get
			{
				return _current;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _current)
				{
					// set the value...
					_current = value;
				}
			}
		}

		/// <summary>
		/// Returns true to indicate that this property bag is read-only.
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		protected override object LoadValue(string name, ref bool found)
		{
			string value = ConfigurationManager.AppSettings[name];

			// what now?
			if(value != null)
			{
				found = true;
				return value;
			}
			else
			{
				found = false;
				return null;
			}
		}
	}
}
