// BootFX - Application framework for .NET applications
// 
// File: ImageHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace BootFX.Common
{
	/// <summary>
	/// Defines a class that can manipulate images.
	/// </summary>
	public static class ImageHelper
	{
		/// <summary>
		/// Gets the image format from the given extension.
		/// </summary>
		/// <param name="extension"></param>
		/// <returns></returns>
		public static ImageFormat GetImageFormatFromExtension(string extension)
		{
			if(extension == null)
				throw new ArgumentNullException("extension");
			if(extension.Length == 0)
				throw new ArgumentOutOfRangeException("'extension' is zero-length.");
			
			// check...
			if(string.Compare(extension, ".jpeg", true, Cultures.System) == 0 ||
				string.Compare(extension, ".jpg", true, Cultures.System) == 0)
			{
				return ImageFormat.Jpeg;
			}
			else
				throw new NotSupportedException(string.Format("An extension with name '{0}' was not found.", extension));
		}

		/// <summary>
		/// Creates a thumbnail from the given file.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="widget"></param>
		public static void CreateThumbnail(string filepath, string thumbnailPath, int width)
		{
			if(filepath == null)
				throw new ArgumentNullException("filepath");
			if(filepath.Length == 0)
				throw new ArgumentOutOfRangeException("'filepath' is zero-length.");
			if(thumbnailPath == null)
				throw new ArgumentNullException("thumbnailPath");
			if(thumbnailPath.Length == 0)
				throw new ArgumentOutOfRangeException("'thumbnailPath' is zero-length.");

			// get...
			using(Image inImage = Image.FromFile(filepath))
			{
				using(Image outImage = CreateThumbnail(inImage, width))
				{
					// ext...
					ImageFormat format = GetImageFormatFromExtension(Path.GetExtension(filepath));

					// save...
					try
					{
						outImage.Save(thumbnailPath, format);
					}
					catch(Exception ex)
					{
						throw new InvalidOperationException(string.Format("The thumbnail could not be saved to: {0}", thumbnailPath), ex);
					}
				}
			}
		}

		/// <summary>
		/// Creates a thumbnail from the given file.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="widget"></param>
		public static Image CreateThumbnail(Image image, int width)
		{
			if(image == null)
				throw new ArgumentNullException("image");
			
			// ratio...
			double ratio = (double)image.Width / (double)image.Height;

			// new...
			int newHeight = 0;
			//if(width <= image.Height)
			//  newHeight = (int)((double)width * ratio);
			//else
				newHeight = (int)((double)width / ratio);

			// create...
			Image newImage = new Bitmap(width, newHeight, PixelFormat.Format32bppPArgb);

			// get...
            using (Graphics g = Graphics.FromImage(newImage))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, width, newHeight);
                g.DrawImage(image, 0, 0, width, newHeight);
            }

			// return...
			return newImage;
		}

        public static Image CreateThumbnail(Image image, int width, int height)
        {
            if (image == null)
                throw new ArgumentNullException("image");

            // ratio...
            double ratio = (double)image.Width / (double)image.Height;

            // create...
            Image newImage = new Bitmap(width, height, PixelFormat.Format32bppPArgb);

            // get...
            using (Graphics g = Graphics.FromImage(newImage))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, width, height);

                DrawImageInBox(image, newImage);
            }

            // return...
            return newImage;
        }

        public static void DrawImageInBox(Image source, Image target)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            if (target == null)
                throw new ArgumentNullException("target");

            // ratio...
            float ratio = (float)source.Width / (float)source.Height;

            float useWidth = 0;
            float useHeight = 0;
            int left = 0;
            int top = 0;

            // landscape target...
            if (target.Width >= target.Height)
            {
                useHeight = target.Height;
                useWidth = useHeight * ratio;
                top = 0;
                left = Center(useWidth, target.Width);
            }
            else
            {
                useWidth = target.Width;
                useHeight = useWidth / ratio;
                left = 0;
                top = Center(useHeight, target.Height);
            }

            // render...
            using (Graphics g = Graphics.FromImage(target))
                g.DrawImage(source, left, top, useWidth, useHeight);
        }

        private static int Center(double value, int container)
        {
            return Center((int)value, container);
        }

        private static int Center(int value, int container)
        {
            return (container / 2) - (value / 2);
        }

        public static byte[] CreateThumbnailBytesPng(byte[] bs, int width)
        {
            return CreateThumbnailBytes(bs, width, ImageFormat.Png);
        }

        public static byte[] CreateThumbnailBytes(byte[] bs, int width, ImageFormat format)
        {
            using (var inStream = new MemoryStream(bs))
            {
                using (var image = Image.FromStream(inStream))
                {
                    using (var newImage = CreateThumbnail(image, width))
                    {
                        using (var outStream = new MemoryStream())
                        {
                            newImage.Save(outStream, format);
                            return outStream.ToArray();
                        }
                    }
                }
            }
        }

        public static byte[] ToByteArrayPng(this Image image)
        {
            return ToByteArray(image, ImageFormat.Png);
        }

        public static byte[] ToByteArray(this Image image, ImageFormat format)
        {
            using (var stream = new MemoryStream())
            {
                image.Save(stream, format);
                return stream.ToArray();
            }
        }

        public static Image FromBytes(byte[] bs)
        {
            using (var stream = new MemoryStream(bs))
                return Image.FromStream(stream);
        }
    }
}
