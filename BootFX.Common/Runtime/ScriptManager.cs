// BootFX - Application framework for .NET applications
// 
// File: ScriptManager.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BootFX.Common.UI.Web
{
    public class BfxScriptManager
    {
        private Dictionary<ScriptPurpose, Dictionary<ScriptType, List<string>>> Scripts { get; set; }

        private static Dictionary<string, string> Hashes { get; set; }
        private static Dictionary<string, string> Blocks { get; set; }
        private static Dictionary<string, FileSystemWatcher> Watchers { get; set; }
        private static object _lock = new object();

        public BfxScriptManager()
        {
            this.Scripts = new Dictionary<Web.ScriptPurpose, Dictionary<Web.ScriptType, List<string>>>();
            foreach (ScriptPurpose purpose in Enum.GetValues(typeof(ScriptPurpose)))
            {
                this.Scripts[purpose] = new Dictionary<Web.ScriptType, List<string>>();
                foreach (ScriptType type in Enum.GetValues(typeof(ScriptType)))
                    this.Scripts[purpose][type] = new List<string>();
            }
        }

        internal static string GetScript(ScriptType type, string hash)
        {
            return Blocks[hash];
        }

        static BfxScriptManager()
        {
            Hashes = new Dictionary<string, string>();
            Blocks = new Dictionary<string, string>();
            Watchers = new Dictionary<string, FileSystemWatcher>(StringComparer.InvariantCultureIgnoreCase);
        }

        public void AddApplicationScript(string url)
        {
            this.AddScript(ScriptPurpose.Application, ScriptType.JavaScript, url);
        }

        public void AddLibraryScript(string url)
        {
            this.AddScript(ScriptPurpose.Library, ScriptType.JavaScript, url);
        }

        public void AddApplicationStyleSheet(string url)
        {
            this.AddScript(ScriptPurpose.Application, ScriptType.Css, url);
        }

        public void AddLibraryStyleSheet(string url)
        {
            this.AddScript(ScriptPurpose.Library, ScriptType.Css, url);
        }

        private void AddScript(ScriptPurpose purpose, ScriptType type, string url)
        {
            this.Scripts[purpose][type].Add(url);
        }

        public string GetScriptSnippet()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var purpose in this.Scripts.Keys)
            {
                foreach (var type in this.Scripts[purpose].Keys)
                    this.BuildSnippet(purpose, type, builder);
            }

            return builder.ToString();
        }

        private void BuildSnippet(ScriptPurpose purpose, ScriptType type, StringBuilder builder)
        {
            if (purpose == ScriptPurpose.Application)
                this.BuildReferencesSnippet(this.Scripts[purpose][type], type, builder);
            else
                this.BuildBlobReferenceSnippet(this.Scripts[purpose][type], purpose, type, builder);
        }

        private void BuildBlobReferenceSnippet(IEnumerable<string> scripts, ScriptPurpose purpose, ScriptType type, StringBuilder builder)
        {
            // have we loaded this script?
            lock (_lock)
            {
                var version = ResolvedVersion;

                // update it...
                string combined = null;
                string hash = null;
                var key = purpose + "|" + type;
                if (!(Blocks.ContainsKey(key)))
                {
                    combined = this.GetCombinedScript(purpose, type, ref hash);

                    // set...
                    Hashes[key] = hash;
                    Blocks[hash] = combined;
                }
                else
                    hash = Hashes[key];

                // return...
                var template = this.GetTemplate(type);
                builder.Append(string.Format(template, string.Format("/GetCombinedScript.ashx?version={0}&type={1}&hash={2}",
                    WebUtility.UrlEncode(ResolvedVersion.ToString()), type, WebUtility.UrlEncode(hash))));
            }
        }

        private string GetCombinedScript(ScriptPurpose purpose, ScriptType type, ref string hash)
        {
            hash = null;

            StringBuilder builder = new StringBuilder();
            var context = HttpContext.Current;
            var scripts = this.Scripts[purpose][type];
            foreach (var script in scripts)
            {
                builder.Append("/* ");
                builder.Append(script);
                builder.Append(" */\r\n");

                var path = context.Server.MapPath(script);
                var text = File.ReadAllText(path);
                builder.Append(text);
                builder.Append("\r\n");

                // are we watching this?
                var folder = Path.GetDirectoryName(path);
                if (!(Watchers.ContainsKey(folder)))
                {
                    var watcher = new FileSystemWatcher(folder);
                    watcher.Changed += Watcher_Changed;
                    watcher.Created += Watcher_Changed;
                    watcher.Deleted += Watcher_Changed;
                    watcher.EnableRaisingEvents = true;

                    Watchers[folder] = watcher;
                }
            }

            var result = builder.ToString();
            hash = HashHelper.GetMd5HashOfStringAsBase64(result);
            return result;
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            lock (_lock)
                Blocks.Clear();
        }

        private string GetTemplate(ScriptType type)
        {
            if (type == ScriptType.JavaScript)
                return "<script src=\"{0}\"></script>";
            else if (type == ScriptType.Css)
                return "<link href=\"{0}\" rel=\"stylesheet\" />";
            else
                throw new NotSupportedException(string.Format("Cannot handle '{0}'.", type));
        }

        private string BuildReferencesSnippet(IEnumerable<string> scripts, ScriptType type, StringBuilder builder)
        {
            var values = new Dictionary<string, string>();
            values["version"] = ResolvedVersion.ToString();

            var template = this.GetTemplate(type);

            foreach (var script in scripts)
            {
                if (builder.Length > 0)
                    builder.Append("\r\n");

                var url = HttpHelper.AppendVariablesToQueryString(script, values);
                var wrapped = string.Format(template, url);
                builder.Append(wrapped);
            }

            return builder.ToString();
        }

        private static Version ResolvedVersion
        {
            get
            {
                return Runtime.Current.Application.ProductVersion;
            }
        }
    }
}
