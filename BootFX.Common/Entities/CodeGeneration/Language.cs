// BootFX - Application framework for .NET applications
// 
// File: Language.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.CodeGeneration
{
	/// <summary>
	/// Defines languages.
	/// </summary>
	public enum Language
	{
		/// <summary>
		/// Defines C#.
		/// </summary>
		CSharp = 0
	}
}
