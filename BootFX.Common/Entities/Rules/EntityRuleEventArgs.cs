// BootFX - Application framework for .NET applications
// 
// File: EntityRuleEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Entities.Rules
{
	/// <summary>
	/// Summary description for EntityRuleEventArgs.
	/// </summary>
	public class EntityRuleEventArgs
	{
		public EntityRuleEventArgs()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
