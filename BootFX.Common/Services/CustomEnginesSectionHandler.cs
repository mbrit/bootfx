// BootFX - Application framework for .NET applications
// 
// File: CustomEnginesSectionHandler.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using System.Reflection;
using System.Collections;
using System.Configuration;

namespace BootFX.Common.Services
{
	/// <summary>
	/// Summary description for CustomEnginesSectionHandler.
	/// </summary>
	public class CustomEnginesSectionHandler : IConfigurationSectionHandler
	{
		public CustomEnginesSectionHandler()
		{
		}

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			if(section == null)
				throw new ArgumentNullException("section");
			
			// walk...
			ArrayList results = new ArrayList();
			foreach(XmlElement element in section.SelectNodes("engine"))
			{
				// find the type attribute...
				XmlAttribute attr = element.Attributes["type"];
				if(attr == null)
					throw new InvalidOperationException("'type' attribute not found.");

				// get it...
				string asString = attr.Value;
				Type type = Type.GetType(asString, false, true);
				if(type == null)
					throw new InvalidOperationException(string.Format("Failed to find type for '{0}'.", asString));

				// add...
				results.Add(type);
			}

			// return...
			return results;
		}
	}
}
