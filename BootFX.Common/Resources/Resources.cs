// BootFX - Application framework for .NET applications
// 
// File: Resources.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.IO;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace BootFX.Common
{
	/// <summary>
	/// Summary description for Images.
	/// </summary>
	[Obsolete("Use 'ResourceHelper' instead.")]
	public class Resources
	{
		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static Resources _current = null;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private Resources()
		{
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static Resources()
		{
			_current = new Resources();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static Resources Current
		{
			get
			{
				return _current;
			}
		}

		/// <summary>
		/// Gets an image from the calling assembly.
		/// </summary>
		/// <returns></returns>
		public Image GetImage(string name)
		{
			return GetImage(Assembly.GetCallingAssembly(),name);
		}

		/// <summary>
		/// Gets an image from the calling assembly.
		/// </summary>
		/// <returns></returns>
		public Image GetImage(Assembly asm, string name)
		{
			Stream stream = GetResourceStream(asm,name);
			return Image.FromStream(stream);
		}

		/// <summary>
		/// Gets an icon from the calling assembly.
		/// </summary>
		/// <returns></returns>
		public Icon GetIcon(string name)
		{
			return GetIcon(Assembly.GetCallingAssembly(),name);
		}

		/// <summary>
		/// Gets an icon from the calling assembly.
		/// </summary>
		/// <returns></returns>
		private Icon GetIcon(Assembly asm, string name)
		{
			Stream stream = GetResourceStream(asm,name);
			return new Icon(stream);
		}

		public Assembly DefaultAssembly
		{
			get
			{
				return Assembly.GetCallingAssembly();	
			}
		}

		
		/// <summary>
		/// Opens a stream from a resource.
		/// </summary>
		/// <param name="assembly"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static Stream GetResourceStream(Assembly assembly, string name)
		{
			if(assembly == null)
				throw new ArgumentNullException("assembly");
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			// get...
			Stream stream = assembly.GetManifestResourceStream(name);
			if(stream != null)
				return stream;
			else
				throw new InvalidOperationException(string.Format("A resource with name '{0}' was not found in '{1}'.", name, assembly));
		}
	}
}
