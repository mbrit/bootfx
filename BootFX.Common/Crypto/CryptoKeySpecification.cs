// BootFX - Application framework for .NET applications
// 
// File: CryptoKeySpecification.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Defines the specification of a crypto key.
	/// </summary>
	internal class CryptoKeySpecification
	{
		/// <summary>
		/// Private field to support <c>Type</c> property.
		/// </summary>
		private Type _type;

		/// <summary>
		/// Private field to support <c>Key</c> property.
		/// </summary>
		private ICryptoKey _key;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public CryptoKeySpecification(Type type, ICryptoKey key)
		{
			if(type == null)
				throw new ArgumentNullException("type");
			if(key == null)
				throw new ArgumentNullException("key");
		
			_type = type;
			_key = key;
		}

		/// <summary>
		/// Gets the key.
		/// </summary>
		private ICryptoKey Key
		{
			get
			{
				// returns the value...
				return _key;
			}
		}
		
		/// <summary>
		/// Gets the type.
		/// </summary>
		private Type Type
		{
			get
			{
				// returns the value...
				return _type;
			}
		}

		/// <summary>
		/// Creates the crypto.
		/// </summary>
		/// <returns></returns>
		internal CryptoBase GetCrypto()
		{
			if(Type == null)
				throw new InvalidOperationException("Type is null.");
			if(Key == null)
				throw new InvalidOperationException("Key is null.");
			
			// create...
			CryptoBase crypto = (CryptoBase)Activator.CreateInstance(this.Type);
			crypto.SetKey(this.Key);

			// return...
			return crypto;
		}
	}
}
