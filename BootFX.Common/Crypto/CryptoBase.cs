// BootFX - Application framework for .NET applications
// 
// File: CryptoBase.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Security.Cryptography;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Summary description for Crypto.
	/// </summary>
	public abstract class CryptoBase
	{
		/// <summary>
		/// Private field to support <c>Algorithm</c> property.
		/// </summary>
		private object _algorithm;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		protected CryptoBase(object algorithm)
		{
			if(algorithm == null)
				throw new ArgumentNullException("algorithm");
			if(!(algorithm is SymmetricAlgorithm))
				throw new ArgumentException(string.Format("'{0}' is not a symmetric algorithm."));
			_algorithm = algorithm;
		}

		/// <summary>
		/// Gets the algorithm.
		/// </summary>
		protected object Algorithm
		{
			get
			{
				// returns the value...
				return _algorithm;
			}
		}

		/// <summary>
		/// Encrypts a set of bytes.
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public abstract byte[] Encrypt(byte[] bytes);

		/// <summary>
		/// Encrypts a set of bytes.
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public abstract byte[] Decrypt(byte[] bytes);

		/// <summary>
		/// Encrypts the string.
		/// </summary>
		/// <param name="buf"></param>
		/// <returns></returns>
        [Obsolete("Use 'EncryptUnicode' or use the overload that allows an Encoding instance to be provided.")]
		public byte[] Encrypt(string buf)
		{
			return this.Encrypt(buf, Encoding.Unicode);
		}

        /// <summary>
        /// Encrypts the string.
        /// </summary>
        /// <param name="buf"></param>
        /// <returns></returns>
        public byte[] EncryptUnicode(string buf)
        {
            return this.Encrypt(buf, Encoding.Unicode);
        }

        /// <summary>
		/// Encrypts the string.
		/// </summary>
		/// <param name="buf"></param>
		/// <returns></returns>
		public byte[] Encrypt(string buf, Encoding encoding)
		{
			if(buf == null)
				buf = string.Empty;
			if(encoding == null)
				throw new ArgumentNullException("encoding");
			
			// get...
			byte[] bs = encoding.GetBytes(buf);
			if(bs == null)
				throw new InvalidOperationException("'bs' is null.");
			if(bs.Length == 0)
				throw new InvalidOperationException("'bs' is zero-length.");

			// en...
			return this.Encrypt(bs);
		}

        /// <summary>
        /// Encrypts the string and returns a base-64 representation.
        /// </summary>
        /// <param name="buf"></param>
        /// <returns></returns>
        public string EncryptUnicodeToBase64String(string buf)
        {
            if (buf == null)
                throw new ArgumentNullException("buf");
            if (buf.Length == 0)
                throw new ArgumentException("'buf' is zero-length.");

            // take the string and convert it to a base 64 string...
            byte[] bs = Encoding.Unicode.GetBytes(buf);

            // encrypt...
            byte[] encrypted = this.Encrypt(bs);
            if (encrypted == null)
                throw new InvalidOperationException("'encrypted' is null.");
            if (encrypted.Length == 0)
                throw new InvalidOperationException("'encrypted' is zero-length.");

            // return...
            return Convert.ToBase64String(encrypted);
        }

        public byte[] Decrypt(string base64)
        {
            if (base64 == null)
                throw new ArgumentNullException("base64");
            if (base64.Length == 0)
                throw new ArgumentException("'base64' is zero-length.");

            // convert...
            byte[] bs = Convert.FromBase64String(base64);
            byte[] decrypted = this.Decrypt(bs);

            // return...
            return decrypted;
        }

        public string DecryptToUnicode(byte[] bs)
        {
            return this.DecryptToString(bs, Encoding.Unicode);
        }

        public string DecryptToString(byte[] bs, Encoding encoding)
        {
            if (bs == null)
                throw new ArgumentNullException("bs");
            if (bs.Length == 0)
                throw new ArgumentException("'bs' is zero-length.");
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            // get...
            byte[] decrypted = this.Decrypt(bs);
            if (decrypted == null)
                throw new InvalidOperationException("'decrypted' is null.");
            if (decrypted.Length == 0)
                throw new InvalidOperationException("'decrypted' is zero-length.");

            // return...
            return encoding.GetString(decrypted);
        }

        public string DecryptToUnicode(string base64)
        {
            return this.DecryptToString(base64, Encoding.Unicode);
        }

        public string DecryptToString(string base64, Encoding encoding)
        {
            if (base64 == null)
                throw new ArgumentNullException("base64");
            if (base64.Length == 0)
                throw new ArgumentException("'base64' is zero-length.");
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            // get...
            byte[] bs = Convert.FromBase64String(base64);
            return this.DecryptToString(bs, encoding);
        }

		/// <summary>
		/// Applies the key to the given provider.
		/// </summary>
		/// <param name="key"></param>
		public abstract void SetKey(ICryptoKey key);
	}
}
