// BootFX - Application framework for .NET applications
// 
// File: RijndaelCrypto.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Security.Cryptography;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Crypto provider that implements Rijndael.
	/// </summary>
	public class RijndaelCrypto : SymmetricCrypto
	{
		/// <summary>
		/// Constructor.
		/// </summary>
        [Obsolete("Use the overload that accepts 'setPadding'.")]
		public RijndaelCrypto() 
            : base(CreateRijandael(false))
		{
		}

        /// <summary>
		/// Constructor.
		/// </summary>
        public RijndaelCrypto(bool setPadding)
            : base(CreateRijandael(true))
        {
        }

        // mbr - 2009-04-27 - apparently required...
        private static RijndaelManaged CreateRijandael(bool setPadding)
        {
            RijndaelManaged rij = new RijndaelManaged();

            // set...
            if(setPadding)
                rij.Padding = PaddingMode.PKCS7;

            // return...
            return rij;
        }

        /// <summary>
        /// Sets the key and IV using .NET's <c>PasswordDeriveBytes</c> object.
        /// </summary>
        public void SetKeyAndIvFromPasswordDeriveBytes(PasswordDeriveBytes pdb)
        {
            if (pdb == null)
                throw new ArgumentNullException("pdb");

            // set...
            this.SetKey(pdb.GetBytes(32));
            this.SetIV(pdb.GetBytes(16));
        }

        /// <summary>
        /// Sets the key and IV using .NET's <c>PasswordDerivedBytes</c> object.
        /// </summary>
        public void SetKeyAndIvFromPasswordDeriveBytes(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (password.Length == 0)
                throw new ArgumentException("'password' is zero-length.");

            // defer...
            this.SetKeyAndIvFromPasswordDeriveBytes(new PasswordDeriveBytes(password,
                new MD5CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(password))));
        }
	}
}
