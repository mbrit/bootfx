// BootFX - Application framework for .NET applications
// 
// File: SymmetricCrypto.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Security.Cryptography;
using System.CodeDom;
using BootFX.Common.CodeGeneration;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Base class for symmetric crypto.
	/// </summary>
	public class SymmetricCrypto : CryptoBase
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public SymmetricCrypto(SymmetricAlgorithm algorithm) 
            : base(algorithm)
		{
		}

		/// <summary>
		/// Gets the symmetric algorithm used in this class.
		/// </summary>
		private SymmetricAlgorithm SymmetricAlgorithm
		{
			get
			{
				return this.Algorithm as SymmetricAlgorithm;
			}
		}

		/// <summary>
		/// Gets or sets the key
		/// </summary>
		internal byte[] Key
		{
			get
			{
                return this.SymmetricAlgorithm.Key;
			}
		}
		
		/// <summary>
		/// Gets or sets the iv
		/// </summary>
		internal byte[] IV
		{
			get
			{
                return this.SymmetricAlgorithm.IV;
            }
		}

		// mbr - 2008-12-15 - added...
		public string KeyAsBase64String
		{
			get
			{
				if(this.Key == null)
					return string.Empty;
				else
					return Convert.ToBase64String(this.Key);
			}
			set
			{
                if (value == null || value.Length == 0)
                    this.SetKey(null);
                else
                    this.SetKey(Convert.FromBase64String(value));
			}
		}

		// mbr - 2008-12-15 - added...
		public string IVAsBase64String
		{
			get
			{
				if(this.IV == null)
					return string.Empty;
				else
					return Convert.ToBase64String(this.IV);
			}
			set
			{
                if (value == null || value.Length == 0)
                    this.SetIV(null);
                else
                    this.SetIV(Convert.FromBase64String(value));
			}
		}

		/// <summary>
		/// Generates the IV.
		/// </summary>
		public void GenerateKey()
		{
			this.SymmetricAlgorithm.GenerateKey();
		}

		/// <summary>
		/// Generates the key.
		/// </summary>
		public void GenerateIV()
		{
			this.SymmetricAlgorithm.GenerateIV();
		}

		/// <summary>
		/// Generates the key and the IV.
		/// </summary>
		public void GenerateKeyAndIV()
		{
			this.GenerateKey();
			this.GenerateIV();
		}

		/// <summary>
		/// Encrypts a stream of bytes.
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public override byte[] Encrypt(byte[] bytes)
		{
			if(bytes == null)
				throw new ArgumentNullException("bytes");
			
			ICryptoTransform transform = this.CreateEncryptor();
			if(transform == null)
				throw new InvalidOperationException("transform is null.");

			// handle it...
			using(MemoryStream stream = new MemoryStream())
			{
				// mangle...
				using(CryptoStream crypto = new CryptoStream(stream, transform, CryptoStreamMode.Write))
				{
					crypto.Write(bytes, 0, bytes.Length);
					crypto.FlushFinalBlock();

					// get...
					return stream.ToArray();
				}
			}
		}

		/// <summary>
		/// Decrypts a stream of bytes.
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public override byte[] Decrypt(byte[] bytes)
		{
			if(bytes == null)
				throw new ArgumentNullException("bytes");
			if(bytes.Length == 0)
				throw new ArgumentOutOfRangeException("'bytes' is zero-length.");
			
			ICryptoTransform transform = this.CreateDecryptor();
			if(transform == null)
				throw new InvalidOperationException("transform is null.");

			// handle...
			using(MemoryStream inStream = new MemoryStream(bytes))
			{
				// read it all...
				using(MemoryStream outStream = new MemoryStream())
				{
					// mangle...
					using(CryptoStream crypto = new CryptoStream(inStream, transform, CryptoStreamMode.Read))
					{
						// walk..
						byte[] buf = new byte[10240];
						while(true)
						{
							int read = crypto.Read(buf, 0, buf.Length);
							if(read == 0)
								break;

							// write...
							outStream.Write(buf, 0, read);
						}
					}

					// results...
					return outStream.ToArray();
				}
			}
		}

		/// <summary>
		/// Creates an encryptor.
		/// </summary>
		/// <returns></returns>
		public ICryptoTransform CreateEncryptor()
		{
			if(SymmetricAlgorithm == null)
				throw new InvalidOperationException("SymmetricAlgorithm is null.");
			return this.SymmetricAlgorithm.CreateEncryptor();
		}
	
		/// <summary>
		/// Creates an encryptor.
		/// </summary>
		/// <returns></returns>
		public ICryptoTransform CreateDecryptor()
		{
			if(SymmetricAlgorithm == null)
				throw new InvalidOperationException("SymmetricAlgorithm is null.");
			return this.SymmetricAlgorithm.CreateDecryptor();
		}

		/// <summary>
		/// Creates key code.
		/// </summary>
		/// <param name="language"></param>
		/// <returns></returns>
		public string CreateKeyCode(Language language)
		{
			return CreateKeyCode(language, "myKey");
		}

		/// <summary>
		/// Creates key code.
		/// </summary>
		/// <param name="language"></param>
		/// <returns></returns>
		public string CreateKeyCode(Language language, string name)
		{
			return this.CreateKeyCodeInternal(language, name, true);
		}

		/// <summary>
		/// Creates a key or IV.
		/// </summary>
		/// <param name="language"></param>
		/// <param name="name"></param>
		/// <param name="createKey"></param>
		/// <returns></returns>
		private string CreateKeyCodeInternal(Language language, string name, bool createKey)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");

			// create...
			SymmetricAlgorithm newProvider = this.CreateNewProvider();
			if(newProvider == null)
				throw new InvalidOperationException("newProvider is null.");

			// create a key...
			byte[] bs = null;
			if(createKey)
			{
				newProvider.GenerateKey();
				bs = newProvider.Key;
			}
			else
			{
				// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
				throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
			}

			// check...
			if(bs == null)
				throw new InvalidOperationException("'bs' is null.");
			if(bs.Length == 0)
				throw new InvalidOperationException("'bs' is zero-length.");

			// array...
			CodeArrayCreateExpression array = new CodeArrayCreateExpression();
			array.CreateType = new CodeTypeReference(typeof(byte));
			foreach(byte b in bs)
				array.Initializers.Add(new CodePrimitiveExpression(b));

			// var...
			CodeVariableDeclarationStatement var = new CodeVariableDeclarationStatement(typeof(byte[]), name, array);

			// return...
			return CodeDomHelper.ToString(var, language);
		}

		/// <summary>
		/// Creates a new provider of the same algorithm.
		/// </summary>
		/// <returns></returns>
		private SymmetricAlgorithm CreateNewProvider()
		{
			// clone...
			if(SymmetricAlgorithm == null)
				throw new InvalidOperationException("SymmetricAlgorithm is null.");
			SymmetricAlgorithm algorithm = (SymmetricAlgorithm)Activator.CreateInstance(this.SymmetricAlgorithm.GetType());
			if(algorithm == null)
				throw new InvalidOperationException("algorithm is null.");
			return algorithm;
		}

		public override void SetKey(ICryptoKey key)
		{
			if(key == null)
				throw new ArgumentNullException("key");
			if(!(key is SymmetricKey))
				throw new ArgumentException(string.Format("Key '{0}' is not a symmetric key.", key));

			// set...
			this.SetIV(((SymmetricKey)key).GetIV());
			this.SetKey(((SymmetricKey)key).GetKey());
		}

		/// <summary>
		/// Sets the IV.
		/// </summary>
		/// <param name="iv"></param>
		public void SetIV(byte[] iv)
		{
			if(iv == null)
				throw new ArgumentNullException("iv");
			if(iv.Length == 0)
				throw new ArgumentOutOfRangeException("'iv' is zero-length.");

			// set...
			if(SymmetricAlgorithm == null)
				throw new InvalidOperationException("SymmetricAlgorithm is null.");
			this.SymmetricAlgorithm.IV = iv;
		}

		/// <summary>
		/// Sets the IV.
		/// </summary>
		/// <param name="iv"></param>
		public void SetKey(byte[] key)
		{
			if(key == null)
				throw new ArgumentNullException("key");
			if(key.Length == 0)
				throw new ArgumentOutOfRangeException("'key' is zero-length.");

			// set...
			if(SymmetricAlgorithm == null)
				throw new InvalidOperationException("SymmetricAlgorithm is null.");
			this.SymmetricAlgorithm.Key = key;
		}
	}
}
