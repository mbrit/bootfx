// BootFX - Application framework for .NET applications
// 
// File: CryptoFactory.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Security.Cryptography;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Defines the cryptograpy factory.
	/// </summary>

	public class CryptoFactory
	{
		/// <summary>
		/// Raised when a key specification needs resolving.
		/// </summary>
		public event CryptoKeySpecificationEventHandler ResolveKeySpecification;
		
		/// <summary>
		/// Private field to support <c>Keys</c> property.
		/// </summary>
		private Lookup _keys;
		
		/// <summary>
		/// Private field to hold the singleton instance.
		/// </summary>
		private static CryptoFactory _current = null;
		
		/// <summary>
		/// Private constructor.
		/// </summary>
		private CryptoFactory()
		{
			_keys = new Lookup(false);
			_keys.CreateItemValue += new CreateLookupItemEventHandler(_keys_CreateItemValue);
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static CryptoFactory()
		{
			_current = new CryptoFactory();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static CryptoFactory Current
		{
			get
			{
				return _current;
			}
		}

		/// <summary>
		/// Gets the crypto helper with the given name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		internal CryptoBase GetCrypto(string name)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");

			// get the spec...
			CryptoKeySpecification spec = (CryptoKeySpecification)Keys[name];
			if(spec == null)
				throw new InvalidOperationException("spec is null.");

			// return...
			CryptoBase crypto = spec.GetCrypto();
			if(crypto == null)
				throw new InvalidOperationException("crypto is null.");
			return crypto;
		}

		/// <summary>
		/// Gets the keys.
		/// </summary>
		private Lookup Keys
		{
			get
			{
				// returns the value...
				return _keys;
			}
		}

		private void _keys_CreateItemValue(object sender, CreateLookupItemEventArgs args)
		{
			// raise an event...
			CryptoKeySpecificationEventArgs e = new CryptoKeySpecificationEventArgs((string)args.Key);
			this.OnResolveKeySpecification(e);

			// create...
			if(e.Type == null)
				throw new InvalidOperationException("e.Type is null.");
			if(e.Key == null)
				throw new InvalidOperationException("e.Key is null.");

			// set...
			args.NewValue = new CryptoKeySpecification(e.Type, e.Key);
		}

		/// <summary>
		/// Raises the <c>ResolveKeySpecification</c> event.
		/// </summary>
		protected virtual void OnResolveKeySpecification(CryptoKeySpecificationEventArgs e)
		{
			// raise...
			if(ResolveKeySpecification != null)
				ResolveKeySpecification(this, e);
			else
				throw new InvalidOperationException("Cannot resolve key specification as no subscribers are registered against 'ResolveKeySpecification'.");
		}
	}
}
