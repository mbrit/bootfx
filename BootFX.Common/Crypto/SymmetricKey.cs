// BootFX - Application framework for .NET applications
// 
// File: SymmetricKey.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Security.Cryptography;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Defines a crypto key.
	/// </summary>
	public class SymmetricKey : ICryptoKey
	{
		/// <summary>
		/// Private field to support <c>IV</c> property.
		/// </summary>
		private byte[] _iv;

		/// <summary>
		/// Private field to support <c>Key</c> property.
		/// </summary>
		private byte[] _key;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public SymmetricKey()
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public SymmetricKey(byte[] iv, byte[] key)
		{
			if(iv == null)
				throw new ArgumentNullException("iv");
			if(iv.Length == 0)
				throw new ArgumentOutOfRangeException("'iv' is zero-length.");
			if(key == null)
				throw new ArgumentNullException("key");
			if(key.Length == 0)
				throw new ArgumentOutOfRangeException("'key' is zero-length.");
			
			_iv = iv;
			_key = key;
		}

		/// <summary>
		/// Gets the key.
		/// </summary>
		internal byte[] GetKey()
		{
			// returns the value...
			return _key;
		}
		
		/// <summary>
		/// Gets the iv.
		/// </summary>
		internal byte[] GetIV()
		{
			// returns the value...
			return _iv;
		}

		/// <summary>
		/// Gets the MD5 hash of the given string.
		/// </summary>
		/// <param name="buf"></param>
		/// <returns></returns>
		public static byte[] GetMD5Hash(string buf)
		{
			if(buf == null)
				throw new ArgumentNullException("buf");
			return new MD5CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(buf));
		}
	}
}
