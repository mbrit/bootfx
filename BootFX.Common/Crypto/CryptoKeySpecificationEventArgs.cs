// BootFX - Application framework for .NET applications
// 
// File: CryptoKeySpecificationEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Event handler for <see cref="CryptoKeySpecificationEventArgs"></see>.
	/// </summary>
	public delegate void CryptoKeySpecificationEventHandler(object sender, CryptoKeySpecificationEventArgs e);

	/// <summary>
	/// Summary description for CryptoKeySpecificationEventArgs.
	/// </summary>
	public class CryptoKeySpecificationEventArgs
	{
		/// <summary>
		/// Private field to support <c>Type</c> property.
		/// </summary>
		private Type _type;

		/// <summary>
		/// Private field to support <c>Key</c> property.
		/// </summary>
		private ICryptoKey _key;
		
		/// <summary>
		/// Private field to support <see cref="Name"/> property.
		/// </summary>
		private string _name;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public CryptoKeySpecificationEventArgs(string name)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			
			_name = name;
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}

		/// <summary>
		/// Gets or sets the key
		/// </summary>
		public ICryptoKey Key
		{
			get
			{
				return _key;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _key)
				{
					// set the value...
					_key = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the type
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _type)
				{
					// set the value...
					_type = value;
				}
			}
		}
	}
}
