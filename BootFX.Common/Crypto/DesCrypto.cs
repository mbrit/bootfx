// BootFX - Application framework for .NET applications
// 
// File: DesCrypto.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Security.Cryptography;

namespace BootFX.Common.Crypto
{
	/// <summary>
	/// Crypto provider that implements Des.
	/// </summary>
	public class DesCrypto : SymmetricCrypto
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public DesCrypto() : base(new DESCryptoServiceProvider())
		{
		}
	}
}
