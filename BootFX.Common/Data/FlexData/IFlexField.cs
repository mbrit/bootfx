// BootFX - Application framework for .NET applications
// 
// File: IFlexField.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;
using BootFX.Common.Entities;

namespace BootFX.Common.Data.FlexData
{
	/// <summary>
	/// Defines an instance of <c>IFlexField</c>.
	/// </summary>
	public interface IFlexField
	{
		string Name
		{
			get;
		}

		Type Type
		{
			get;
		}

		DbType DBType
		{
			get;
		}

		long Size
		{
			get;
		}

		string NativeName
		{
			get;
		}

		bool IsLookupProperty
		{
			get;
		}

		bool IsMultiValue
		{
			get;
		}

		bool IsLarge
		{
			get;
		}

		object GetValue(object entity);

		void SetValue(object entity, object value, SetValueReason reason);

		EntityType EntityType
		{
			get;
		}

		bool IsRequired
		{
			get;
		}
	}
}
