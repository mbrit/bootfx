// BootFX - Application framework for .NET applications
// 
// File: FlexField.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;
using BootFX.Common.Entities;

namespace BootFX.Common.Data.FlexData
{
	/// <summary>
	/// Defines an instance of <c>FlexField</c>.
	/// </summary>
	public abstract class FlexField : IFlexField
	{
		/// <summary>
		/// Private field to support <see cref="Flags"/> property.
		/// </summary>
		private FlexFieldFlags _flags;
		
		/// <summary>
		/// Private field to support <see cref="Size"/> property.
		/// </summary>
		private long _size;
		
		/// <summary>
		/// Private field to support <see cref="Type"/> property.
		/// </summary>
		private Type _type;
		
		/// <summary>
		/// Private field to support <see cref="DBType"/> property.
		/// </summary>
		private System.Data.DbType _dbType;
		
		/// <summary>
		/// Private field to support <see cref="Name"/> property.
		/// </summary>
		private string _name;
		
		protected FlexField(string name, Type type, FlexFieldFlags flags) : this(name, type, -1, flags)
		{
		}

		protected FlexField(string name, Type type, long size, FlexFieldFlags flags) : this(name, type, ConversionHelper.GetDBTypeForClrType(type), size, flags)
		{
		}

		protected FlexField(string name, DbType dbType, FlexFieldFlags flags) : this(name, dbType, -1, flags)
		{
		}

		protected FlexField(string name, DbType dbType, long size, FlexFieldFlags flags) : this(name, ConversionHelper.GetClrTypeForDBType(dbType), dbType, size, flags)
		{
		}

		protected FlexField(string name, Type type, DbType dbType, long size, FlexFieldFlags flags)
		{
			if(name == null)
				throw new ArgumentNullException("name");
			if(name.Length == 0)
				throw new ArgumentOutOfRangeException("'name' is zero-length.");
			if(type == null)
				throw new ArgumentNullException("type");
			
			// set...
			_name = name;
			_type = type;
			_dbType = dbType;
			_size = size;
			_flags = flags;
		}

		/// <summary>
		/// Gets the flags.
		/// </summary>
		internal FlexFieldFlags Flags
		{
			get
			{
				return _flags;
			}
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}

		public virtual string NativeName
		{
			get
			{
				return this.Name;
			}
		}

		/// <summary>
		/// Gets the type.
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
		}

		/// <summary>
		/// Gets the dbtype.
		/// </summary>
		public System.Data.DbType DBType
		{
			get
			{
				return _dbType;
			}
		}

		/// <summary>
		/// Gets the size.
		/// </summary>
		public long Size
		{
			get
			{
				return _size;
			}
		}

		public bool IsLookupProperty
		{
			get
			{
				return false;
			}
		}

		public bool IsMultiValue
		{
			get
			{
				return false;
			}
		}

		public bool IsLarge
		{
			get
			{
				return false;
			}
		}

		public object GetValue(object entity)
		{
			return this.DoGetValue(entity);
		}

		protected abstract object DoGetValue(object entity);

		public EntityType EntityType
		{
			get
			{
				return null;
			}
		}

		public void SetValue(object entity, object value, SetValueReason reason)
		{
			this.DoSetValue(entity, value, reason);
		}

		protected abstract void DoSetValue(object entity, object value, SetValueReason reason);

		public bool IsRequired
		{
			get
			{
				return GetFlagValue(FlexFieldFlags.Required);
			}
		}

		private bool GetFlagValue(FlexFieldFlags flags)
		{
			if((int)(this.Flags & flags) != 0)
				return true;
			else
				return false;
		}
	}
}
