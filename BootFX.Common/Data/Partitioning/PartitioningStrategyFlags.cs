// BootFX - Application framework for .NET applications
// 
// File: PartitioningStrategyFlags.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Defines flags for use with partitioning stategies.
	/// </summary>
	// mbr - 04-09-2007 - for c7 - added.	
	[Flags()]
	public enum PartitioningStrategyFlags
	{
		Normal = 0,
		ZeroLengthIdSetOk = 1,
		DoNotConstrainUpdateQuery = 2,
		DoNotConstrainDeleteQuery = 4
	}
}
