// BootFX - Application framework for .NET applications
// 
// File: IPartitionIdProvider.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Entities;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Defines an instance of <c>IPartitionIdProvider</c>.
	/// </summary>
	public interface IPartitionIdProvider
	{
		/// <summary>
		/// Gets the partition ID for writing.
		/// </summary>
		/// <returns></returns>
		// mbr - 04-09-2007 - for c7 - changed.
//		object GetPartitionIdForWriting();
		object GetPartitionIdForWriting(object entity);

		/// <summary>
		/// Gets the partition IDs for reading.
		/// </summary>
		/// <returns></returns>
		// mbr - 04-09-2007 - for c7 - changed.		
//		object[] GetPartitionIdsForReading();
		object[] GetPartitionIdsForReading(EntityType et);
	}
}
