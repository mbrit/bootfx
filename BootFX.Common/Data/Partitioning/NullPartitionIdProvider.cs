// BootFX - Application framework for .NET applications
// 
// File: NullPartitionIdProvider.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using BootFX.Common.Entities;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Summary description for NullPartitionIdProvider.
	/// </summary>
	public class NullPartitionIdProvider : IPartitionIdProvider
	{
		public NullPartitionIdProvider()
		{
		}

		public object GetPartitionIdForWriting(object entity)
		{
			// TODO:  Add NullPartitionIdProvider.GetPartitionIdForWriting implementation
			return null;
		}

		public object[] GetPartitionIdsForReading(EntityType et)
		{
			// TODO:  Add NullPartitionIdProvider.GetPartitionIdsForReading implementation
			return null;
		}
	}
}
