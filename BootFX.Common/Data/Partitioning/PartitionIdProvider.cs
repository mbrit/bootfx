// BootFX - Application framework for .NET applications
// 
// File: PartitionIdProvider.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Entities;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Defines a class that can provide IDs for partitioning.
	/// </summary>
	public abstract class PartitionIdProvider : IPartitionIdProvider
	{
		/// <summary>
		/// Private field to support <c>DefaultProvider</c> property.
		/// </summary>
		private static IPartitionIdProvider _defaultProvider;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		protected PartitionIdProvider()
		{
		}

		/// <summary>
		/// Gets the partition ID for writing.
		/// </summary>
		/// <returns></returns>
		public abstract object GetPartitionIdForWriting(object entity);

		/// <summary>
		/// Gets the partition ID or IDs for reading.
		/// </summary>
		/// <returns></returns>
		/// <remarks>By default, this method will return the result from <see cref="GetPartitionIdForWriting"></see> wrapped in an array.</remarks>
		// mbr - 04-09-2007 - for c7 - made abstract.
		public abstract object[] GetPartitionIdsForReading(EntityType et);
//		{
//			object id = this.GetPartitionIdForWriting();
//			if(id == null)
//				throw new InvalidOperationException("id is null.");
//			return new object[] { id };
//		}

		/// <summary>
		/// Gets or sets the defaultprovider
		/// </summary>
		public static IPartitionIdProvider DefaultProvider
		{
			get
			{
				return _defaultProvider;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _defaultProvider)
				{
					// set the value...
					_defaultProvider = value;
				}
			}
		}

		internal static bool PartitioningEnabled
		{
			get
			{
				if(DefaultProvider is NullPartitionIdProvider)
					return false;
				else
					return true;
			}
		}
	}
}
