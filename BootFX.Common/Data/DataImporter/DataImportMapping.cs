// BootFX - Application framework for .NET applications
// 
// File: DataImportMapping.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.Text;
using BootFX.Common;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Entities;
using BootFX.Common.Services;
using BootFX.Common.Management;

namespace BootFX.Common.Data
{
    internal class DataImportMapping
    {
        /// <summary>
        /// Private value to support the <see cref="ExternalName">ExternalName</see> property.
        /// </summary>
        private string _externalName;

        /// <summary>
        /// Private value to support the <see cref="Flags">Flags</see> property.
        /// </summary>
        private DataImportMappingFlags _flags;

        /// <summary>
        /// Private value to support the <see cref="Field">Field</see> property.
        /// </summary>
        private EntityField _field;

        internal DataImportMapping(string externalName, EntityField field, DataImportMappingFlags flags)
        {
            if (externalName == null)
                throw new ArgumentNullException("externalName");
            if (externalName.Length == 0)
                throw new ArgumentException("'externalName' is zero-length.");
            if (field == null)
                throw new ArgumentNullException("field");

            // set...
            _externalName = externalName;
            _field = field;
            _flags = flags;
        }

        /// <summary>
        /// Gets the Flags value.
        /// </summary>
        private DataImportMappingFlags Flags
        {
            get
            {
                return _flags;
            }
        }

        /// <summary>
        /// Gets the Field value.
        /// </summary>
        internal EntityField Field
        {
            get
            {
                return _field;
            }
        }

        /// <summary>
        /// Gets the ExternalName value.
        /// </summary>
        internal string ExternalName
        {
            get
            {
                return _externalName;
            }
        }

        /// <summary>
        /// Returns true if this mapping is an external key mapping.
        /// </summary>
        internal bool IsExternalKey
        {
            get
            {
                if ((int)(this.Flags & DataImportMappingFlags.ExternalKey) != 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Returns true if this mapping is required.
        /// </summary>
        internal bool IsRequired
        {
            get
            {
                // external keys are always required.
                if (this.IsExternalKey)
                    return true;
                else
                {
                    if ((int)(this.Flags & DataImportMappingFlags.Required) != 0)
                        return true;
                    else
                        return false;
                }
            }
        }

        public override string ToString()
        {
            return string.Format("'{0}' ('{1}' on '{2}'), {3}", this.ExternalName, this.Field.Name, this.Field.EntityType.FullName, 
                this.Flags);
        }
    }
}
