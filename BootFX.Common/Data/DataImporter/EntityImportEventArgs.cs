// BootFX - Application framework for .NET applications
// 
// File: EntityImportEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;
using System.Text;
using BootFX.Common;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Entities;
using BootFX.Common.Services;
using BootFX.Common.Management;

namespace BootFX.Common.Data
{
    public delegate void EntityImportEventHandler(object sender, EntityImportEventArgs e);

    public class EntityImportEventArgs : EntityEventArgs
    {
        /// <summary>
        /// Private value to support the <see cref="Reader">Reader</see> property.
        /// </summary>
        private DataReader _reader;

        internal EntityImportEventArgs(object entity, DataReader reader)
            : base(entity)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");
            _reader = reader;
        }

        /// <summary>
        /// Gets the Reader value.
        /// </summary>
        public DataReader Reader
        {
            get
            {
                return _reader;
            }
        }
    }
}
