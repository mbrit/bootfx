// BootFX - Application framework for .NET applications
// 
// File: DataImportResults.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Summary description for DataImportResults.
	/// </summary>
	public class DataImportResults
	{
		/// <summary>
		/// Private field to support <c>NumRecordsRead</c> property.
		/// </summary>
		private int _numRecordsRead;
		
		internal DataImportResults()
		{
		}

		/// <summary>
		/// Gets or sets the numrecordsread
		/// </summary>
		public int NumRecordsRead
		{
			get
			{
				return _numRecordsRead;
			}
		}

		internal void IncrementNumRecordsRead()
		{
			_numRecordsRead++;
		}
	}
}
