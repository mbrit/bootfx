// BootFX - Application framework for .NET applications
// 
// File: DataImporter.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using BootFX.Common;
using BootFX.Common.Xml;
using BootFX.Common.Data;
using BootFX.Common.Data.Text;
using BootFX.Common.Entities;
using BootFX.Common.Services;
using BootFX.Common.Management;

namespace BootFX.Common.Data
{
    public class DataImporter : Loggable, IEntityType
    {
		/// <summary>
        /// Defines the <c>ImportFinished</c> event.
        /// </summary>
        public event EventHandler ImportFinished;

        /// <summary>
        /// Defines the <c>ImportStarted</c> event.
        /// </summary>
        public event EventHandler ImportStarted;

		/// <summary>
		/// Defines the <c>EntityImported</c> event.
		/// </summary>
		public event EntityImportEventHandler EntityImportedBeforeSave;

		/// <summary>
		/// Raised when an entity is saved.
		/// </summary>
		public event EntityImportEventHandler EntityImportedAfterSave;
		
		/// <summary>
        /// Private value to support the <see cref="MainMappings">MainMappings</see> property.
        /// </summary>
        private ArrayList _mainMappings = new ArrayList();

        /// <summary>
        /// Private value to support the <see cref="Mappings">Mappings</see> property.
        /// </summary>
        private IDictionary _otherMappings = new HybridDictionary();

        /// <summary>
        /// Private value to support the <see cref="EntityType">EntityType</see> property.
        /// </summary>
        private EntityType _entityType;

        public DataImporter(Type type)
            : this(EntityType.GetEntityType(type, OnNotFound.ThrowException))
        {
        }

        public DataImporter(EntityType et)
        {
            if (et == null)
                throw new ArgumentNullException("et");

            // set...
            _entityType = et;
        }

        /// <summary>
        /// Imports the given CSV file.
        /// </summary>
        /// <param name="path"></param>
        public DataImportResults ImportCsv(string path)
        {
            if (path == null)
                throw new ArgumentNullException("path");
            if (path.Length == 0)
                throw new ArgumentException("'path' is zero-length.");

            // open...
            using (StreamReader reader = new StreamReader(path))
            {
                CsvDataReader csv = new CsvDataReader(reader, true);

                // walk...
                return this.Import(csv);
            }
        }

		/// <summary>
		/// Imports the given CSV file.
		/// </summary>
		public DataImportResults ImportCsv(Stream stream)
		{
			if(stream == null)
				throw new ArgumentNullException("stream");
			
			// return...
			using (StreamReader reader = new StreamReader(stream))
			{
				CsvDataReader csv = new CsvDataReader(reader, true);

				// walk...
				return this.Import(csv);
			}
		}

        /// <summary>
        /// Imports data from the given reader.
        /// </summary>
        /// <param name="reader"></param>
        public DataImportResults Import(DataReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");

            // mbr - 2009-11-06 - changed this so that if we are a web app, we don't create a file log...
            FileLog log = null;
            if (!(Runtime.Current.IsWebApp))
                log = LogSet.CreateFileLogger("Data Import - " + this.GetType().FullName, FileLoggerFlags.Default, new GenericFormatter());

            // txn...
            using (TransactionState txn = Database.StartTransaction())
            {
                if (log != null)
                    LogSet.BindToContext(log);
                try
                {
                    // start...
                    if (this.Log.IsInfoEnabled)
                        this.Log.Info("Starting import...");
                    this.OnImportStarted();

                    // walk each record...
                    int rowNumber = 2;
                    DataImportResults results = new DataImportResults();
                    while (reader.Read())
                    {
                        bool skip = this.ShouldSkip(reader);
                        if (!(skip))
                        {
                            try
                            {
                                // log...
                                //if (this.Log.IsDebugEnabled)
                                //    this.Log.DebugFormat("************* ROW {0} *************", rowNumber);

                                // apply...
                                this.ApplyData(reader, null, this.EntityType, this.MainMappings);

                                // ok...
                                results.IncrementNumRecordsRead();
                            }
                            catch (Exception ex)
                            {
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat(string.Format("Failed when processing row '{0}'.", rowNumber));
                                for (int index = 0; index < reader.FieldCount; index++)
                                {
                                    builder.Append("\r\n\t");
                                    builder.Append(reader.GetName(index));
                                    builder.Append(": ");
                                    builder.Append(reader[index]);
                                }

                                // ok...
                                throw new InvalidOperationException(builder.ToString(), ex);
                            }
                        }

                        // next...
                        rowNumber++;
                    }

                    // start...
                    if (this.Log.IsInfoEnabled)
                        this.Log.Info("Finishing import...");

                    // ok...
                    this.OnImportFinished();

                    // log...
                    AccurateTimer timer = null;
                    if (this.Log.IsInfoEnabled)
                    {
                        this.Log.Info("Import finished, committing transaction...");
                        timer = new AccurateTimer();
                        timer.Start();
                    }

                    // commit...
                    txn.Commit();

                    // log...
                    if (this.Log.IsInfoEnabled)
                    {
                        if (timer != null)
                        {
                            timer.Stop();
                            this.Log.InfoFormat("Commit time: {0:n5}s", timer.Duration);
                        }

                        // ok...
                        this.Log.Info("Commit complete.");
                    }

                    // return...
                    return results;
                }
                catch (Exception ex)
                {
                    // rollback...
                    txn.Rollback(ex);

                    // log...
                    string message = null;
                    if (log != null)
                        message = string.Format("The data import operation failed.  A log file can be found at: {0}", log.Path);
                    else
                        message = "The data import operation failed.  (A log file was not generated.)";
                    if (this.Log.IsErrorEnabled)
                        this.Log.Error(message, ex);
                    throw new InvalidOperationException(message, ex);
                }
                finally
                {
                    if (log != null)
                        LogSet.UnbindFromContext(log);
                }
            }
        }

		protected virtual bool ShouldSkip(DataReader reader)
		{
			if(reader == null)
				throw new ArgumentNullException("reader");
			
			return false;
		}

        private object GetEntityToImportAgainst(DataReader reader, EntityType et, ArrayList mappings)
        {
            if(reader == null)
	            throw new ArgumentNullException("reader");
            if(et == null)
	            throw new ArgumentNullException("et");       
            if (mappings == null)
                throw new ArgumentNullException("mappings");

            // walk...
            SqlFilter filter = new SqlFilter(et);
            foreach (DataImportMapping mapping in mappings)
            {
                if(mapping.IsExternalKey)
                {
                    object value = reader[mapping.ExternalName];

                    // add...
                    filter.Constraints.Add(mapping.Field, value);
                }
            }

            // fields?
            if (filter.Constraints.Count == 0)
                return et.CreateInstance();

			// timings...
			AccurateTimer timer = null;
            //if(this.Log.IsDebugEnabled)
            //{
            //    timer = new AccurateTimer();
            //    timer.Start();
            //}

			// load...
			IList existing = filter.ExecuteEntityCollection();

			// log...
			if(timer != null)
			{
				timer.Stop();
				this.Log.InfoFormat("Lookup of '{0}': {1:n5}s", et.FullName, timer.Duration);
			}

			// check...
            if (existing == null)
                throw new InvalidOperationException("'existing' is null.");

            // anything?
            if (existing.Count == 0)
                return et.CreateInstance();
            else if (existing.Count == 1)
                return existing[0];
            else
                throw new InvalidOperationException("Multiple entities were returned to import against.");
        }

        /// <summary>
        /// Applies the data in the mappings.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="entity"></param>
        /// <param name="mappings"></param>
        private object ApplyData(DataReader reader, object entity, EntityType et, ArrayList mappings)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");
            if (et == null)
                throw new ArgumentNullException("et");
            if (mappings == null)
                throw new ArgumentNullException("mappings");

			// timer...
			AccurateTimer timer = null;
            //if(this.Log.IsDebugEnabled)
            //{
            //    timer = new AccurateTimer();
            //    timer.Start();
            //}
			try
			{
				// apply...
                //if(this.Log.IsDebugEnabled)
                //{
                //    this.Log.DebugFormat("Importing '{0}'...", et.FullName);
                //    foreach (DataImportMapping mapping in mappings)
                //        this.Log.DebugFormat("\t{0}: {1}", mapping, reader[mapping.ExternalName]);
                //}

				// load an entity to use...
				if (entity == null)
				{
					// log...
                    //if (this.Log.IsDebugEnabled)
                    //    this.Log.DebugFormat("No entity supplied - loading from mappings...");

					// get...
					entity = GetEntityToImportAgainst(reader, et, mappings);
					if (entity == null)
						throw new InvalidOperationException("'entity' is null.");
				}
				else
				{
					// log...
                    //if (this.Log.IsDebugEnabled)
                    //    this.Log.DebugFormat("Entity was supplied.");
				}

				// record...
                //if (this.Log.IsDebugEnabled)
                //    this.Log.DebugFormat("Importing against '{0}' ({1})...", entity, entity.GetType().FullName);

				// walk...
				foreach (DataImportMapping mapping in mappings)
				{
					// get the value...
					object value = reader[mapping.ExternalName];

					// do a set?  if the record is new, always set.  if it's not new, ignore external fields (as this 
					// is representative of bad data).
					if (et.Storage.IsNew(entity) || !(mapping.IsExternalKey))
					{
						// required...
						if (mapping.IsRequired)
						{
							// do we have data?
							if (value == null || value is DBNull || (value is string && ((string)value).Length == 0))
								throw new InvalidOperationException(string.Format("A value for '{0}' is required and was not supplied.", mapping));
						}

						// set...
						et.Storage.SetValue(entity, mapping.Field, value, SetValueReason.UserSet);
					}
					else
					{
						if (this.Log.IsWarnEnabled)
						{
							// if the value doesn't match, flag a warning...
							object existing = et.Storage.GetValue(entity, mapping.Field);
							if (!(object.Equals(value, existing)))
							{
								this.Log.Warn(string.Format("The imported data wanted to set an external key for '{0}' to be '{1}' against field '{2}', and an existing value of '{3}' was found.",
									entity, value, mapping.Field, existing));
							}
						}
					}
				}

				// walk the other types and find ones related to this...
				foreach (EntityType otherEt in this.OtherMappings.Keys)
				{
					foreach (ChildToParentEntityLink link in otherEt.ChildLinks)
					{
						if (link.EntityType == et)
						{
							// do we already have that data...
							EntityField[] linkFields = link.GetLinkFields();
							if (linkFields == null)
								throw new InvalidOperationException("'linkFields' is null.");
							if (linkFields.Length != 1)
								throw new InvalidOperationException(string.Format("Automatic linking can only be done with single column link fields, not ones with '{0}' links.", linkFields.Length));

							// get it...
							object existingForeignId = et.Storage.GetValue(entity, linkFields[0]);
							if (existingForeignId == null)
								throw new InvalidOperationException("'existingForeignId' is null.");

							// log...
                            //if (this.Log.IsDebugEnabled)
                            //    this.Log.DebugFormat("Foreign key ID for '{0}' is '{1}'.", linkFields[0], existingForeignId);

							// get...
							object foreignEntity = null;
							bool saveAndLinkAfterImport = false;
							if (existingForeignId == null || existingForeignId is DBNull || (existingForeignId is int && (int)existingForeignId == 0))
							{
                                //if (this.Log.IsDebugEnabled)
                                //    this.Log.DebugFormat("No existing foreign key link specified - creating new record.");

								// set...
								saveAndLinkAfterImport = true;
							}
							else
							{
                                //if (this.Log.IsDebugEnabled)
                                //    this.Log.DebugFormat("Loading '{0}' with ID #{1}...", otherEt.FullName, existingForeignId);

								// load...
								foreignEntity = otherEt.Persistence.GetById(new object[] { existingForeignId }, OnNotFound.ThrowException);
								if (foreignEntity == null)
									throw new InvalidOperationException("'foreignEntity' is null.");
							}

							// import that...
							foreignEntity = this.ApplyData(reader, foreignEntity, otherEt, (ArrayList)this.OtherMappings[otherEt]);
							if (foreignEntity == null)
								throw new InvalidOperationException("'foreignEntity' is null.");

							// save...
							if (saveAndLinkAfterImport)
							{
								// save...
								otherEt.Persistence.SaveChanges(foreignEntity);

								// get the id...
								object[] foreignId = otherEt.Storage.GetKeyValues(foreignEntity);
								if (foreignId == null)
									throw new InvalidOperationException("'foreignId' is null.");
								if (foreignId.Length != 1)
									throw new InvalidOperationException(string.Format("Foreign ID is an invalid length of '{0}'.", foreignId.Length));

								// set...
								et.Storage.SetValue(entity, linkFields[0], foreignId[0], SetValueReason.UserSet);
							}
						}
					}
				}

				// ok...
				this.OnEntityImportedBeforeSave(new EntityImportEventArgs(entity, reader));

				// save the entity...
                //if (this.Log.IsDebugEnabled)
                //    this.Log.DebugFormat("Saving '{0}' ({1})...", entity, entity.GetType().FullName);
				et.Persistence.SaveChanges(entity);

				// mbr - 16-05-2008 - event...
				this.OnEntityImportedAfterSave(new EntityImportEventArgs(entity, reader));

				// return...
				return entity;
			}
			finally
			{
				if(timer != null)
				{
					timer.Stop();
					this.Log.InfoFormat("Time to import '{0}': {1:n5}s", et.FullName, timer.Duration);
				}
			}
        }

        /// <summary>
        /// Gets the MainMappings value.
        /// </summary>
        private ArrayList MainMappings
        {
            get
            {
                return _mainMappings;
            }
        }

        /// <summary>
        /// Gets the EntityType value.
        /// </summary>
        public EntityType EntityType
        {
            get
            {
                return _entityType;
            }
        }

        /// <summary>
        /// Adds a mapping to the main entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string sharedName, DataImportMappingFlags flags)
        {
            this.AddMapping(sharedName, sharedName, flags);
        }

        /// <summary>
        /// Adds a mapping to the main entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string externalName, string fieldName, DataImportMappingFlags flags)
        {
            this.AddMappingInternal(externalName, fieldName, this.EntityType, flags, true);
        }

        /// <summary>
        /// Adds a mapping to another entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string sharedName, Type type, DataImportMappingFlags flags)
        {
            this.AddMapping(sharedName, sharedName, EntityType.GetEntityType(type, OnNotFound.ThrowException), flags);
        }

        /// <summary>
        /// Adds a mapping to another entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string sharedName, EntityType et, DataImportMappingFlags flags)
        {
            this.AddMapping(sharedName, sharedName, et, flags);
        }

        /// <summary>
        /// Adds a mapping to another entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string externalName, string fieldName, Type type, DataImportMappingFlags flags)
        {
            this.AddMapping(externalName, fieldName, EntityType.GetEntityType(type, OnNotFound.ThrowException), flags);
        }

        /// <summary>
        /// Adds a mapping to another entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        public void AddMapping(string externalName, string fieldName, EntityType et, DataImportMappingFlags flags)
        {
            this.AddMappingInternal(externalName, fieldName, et, flags, false);
        }

        /// <summary>
        /// Adds a mapping to any entity.
        /// </summary>
        /// <param name="externalName"></param>
        /// <param name="fieldName"></param>
        /// <param name="et"></param>
        /// <param name="useMainEntity"></param>
        private void AddMappingInternal(string externalName, string fieldName, EntityType et, DataImportMappingFlags flags, bool useMainEntity)
        {
            if (externalName == null)
                throw new ArgumentNullException("externalName");
            if (externalName.Length == 0)
                throw new ArgumentException("'externalName' is zero-length.");
            if (fieldName == null)
                throw new ArgumentNullException("fieldName");
            if (fieldName.Length == 0)
                throw new ArgumentException("'fieldName' is zero-length.");
            if (et == null)
                throw new ArgumentNullException("et");

            // what field...
            EntityField field = et.Fields.GetField(fieldName, OnNotFound.ThrowException);
            if (field == null)
                throw new InvalidOperationException("'field' is null.");

            // create a mapping...
            DataImportMapping mapping = new DataImportMapping(externalName, field, flags);

            // get the mappings...
            ArrayList mappings = null;
            if (useMainEntity)
                mappings = this.MainMappings;
            else
            {
                // add a mapping...
                mappings = (ArrayList)this.OtherMappings[et];
                if (mappings == null)
                {
                    mappings = new ArrayList();
                    this.OtherMappings[et] = mappings;
                }
            }

            // add...
            if (mappings == null)
                throw new InvalidOperationException("'mappings' is null.");
            mappings.Add(mapping);
        }

        /// <summary>
        /// Gets the Mappings value.
        /// </summary>
        private IDictionary OtherMappings
        {
            get
            {
                return _otherMappings;
            }
        }

		/// <summary>
		/// Raises the <c>EntityImported</c> event.
		/// </summary>
		protected virtual void OnEntityImportedBeforeSave(EntityImportEventArgs e)
		{
			// raise...
			if (EntityImportedBeforeSave != null)
				EntityImportedBeforeSave(this, e);
		}

		/// <summary>
		/// Raises the <c>EntityImported</c> event.
		/// </summary>
		protected virtual void OnEntityImportedAfterSave(EntityImportEventArgs e)
		{
			// raise...
			if (EntityImportedAfterSave != null)
				EntityImportedAfterSave(this, e);
		}

		/// <summary>
        /// Raises the <c>ImportStarted</c> event.
        /// </summary>
        private void OnImportStarted()
        {
            OnImportStarted(EventArgs.Empty);
        }

        /// <summary>
        /// Raises the <c>ImportStarted</c> event.
        /// </summary>
        protected virtual void OnImportStarted(EventArgs e)
        {
            // raise...
            if (ImportStarted != null)
                ImportStarted(this, e);
        }

        /// <summary>
        /// Raises the <c>ImportFinished</c> event.
        /// </summary>
        private void OnImportFinished()
        {
            OnImportFinished(EventArgs.Empty);
        }

        /// <summary>
        /// Raises the <c>ImportFinished</c> event.
        /// </summary>
        protected virtual void OnImportFinished(EventArgs e)
        {
            // raise...
            if (ImportFinished != null)
                ImportFinished(this, e);
        }

    }
}
