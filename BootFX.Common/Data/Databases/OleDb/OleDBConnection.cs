// BootFX - Application framework for .NET applications
// 
// File: OleDBConnection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Base class for OLE DB connections.
	/// </summary>
	/// <remarks>Normally you would create strong providers, like <see cref="JetConnection"></see></remarks>
	public abstract class OleDBConnection : Connection
	{
		private DataTable _schema = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="connectionString"></param>
		protected OleDBConnection(string connectionString) : base(connectionString)
		{
		}

		protected override IDbConnection CreateNativeConnection()
		{
			// create...
			return new System.Data.OleDb.OleDbConnection(this.ConnectionString);
		}

		protected internal override ApplicationException CreateCommandException(string message, IDbCommand command, Exception originalException,
            SqlStatement sql = null)
		{
			if(command == null)
				throw new ArgumentNullException("command");
			if(originalException == null)
				throw new ArgumentNullException("originalException");

			// create...
			return new OleDBException(string.Format("{0}\r\nCommand: {1}", message, command.CommandText), originalException);
		}

		protected override IDataAdapter CreateDataAdapter(IDbCommand command)
		{
			if(command == null)
				throw new ArgumentNullException("command");
			if(!(command is OleDbCommand))
				throw new ArgumentException(string.Format(Cultures.Exceptions, "Command is of type '{0}', not OleDbCommand.", command.GetType()));

			// create...
			return new OleDbDataAdapter((OleDbCommand)command);
		}

		protected override bool DoDoesTableExist(NativeName nativeName)
		{
			if(_schema == null)
			{
				try
				{
					NativeConnection.Open();
					_schema = ((OleDbConnection) NativeConnection).GetOleDbSchemaTable(OleDbSchemaGuid.Tables,new object[] {null, null, null, "TABLE"});
				}
                finally
				{
					if(NativeConnection.State == ConnectionState.Open)
						NativeConnection.Close();
				}
			}

			DataRow[] rows = _schema.Select(string.Format("TABLE_NAME = '{0}'",nativeName));
			return rows.Length > 0;
		}

	}
}
