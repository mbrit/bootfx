// BootFX - Application framework for .NET applications
// 
// File: JetConnection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Data.OleDb;
using System.ComponentModel;
using BootFX.Common.Data.Formatters;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Provides a connection to a Jet database.
	/// </summary>
	[Connection()]
	[Description("Access/Jet (OLE DB)")]
	public class JetConnection : OleDBConnection
	{
		private static JetDialect _dialect = new JetDialect();

		/// <summary>
		/// Constructor,
		/// </summary>
		public JetConnection(string connectionString) : base(connectionString)
		{
		}

		public override BootFX.Common.Data.Schema.FieldSpecification GetFieldSpecificationForNativeTypeName(string nativeTypeName, bool throwIfNotFound)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		protected override IsolationLevel DefaultIsolutionLevel
		{
			get
			{
				return IsolationLevel.ReadCommitted;
			}
		}

		public override bool SupportsMultiStatementQueries
		{
			get
			{
				return false;
			}
		}

		protected override void FinalizeParameterConfiguration(IDbCommand command, SqlStatementParameter parameter, IDataParameter dataParameter, 
			SqlDialect dialect)
		{
			if(dataParameter == null)
				throw new ArgumentNullException("dataParameter");
			if(parameter == null)
				throw new ArgumentNullException("parameter");			

			// do the base...
			base.FinalizeParameterConfiguration (command, parameter, dataParameter, dialect);

			// INFO: OleDbType Enumeration vs. Microsoft Access Data Types
			// http://support.microsoft.com/?kbid=320435

			// old...
			OleDbParameter oleParameter = (OleDbParameter)dataParameter;

			// flip date/times...  this is a bit bad... basically, the parameter doesn't twist itself around properly...
			if(parameter.DBType == DbType.DateTime)
				oleParameter.OleDbType = OleDbType.Date;
		}

		public override BootFX.Common.Data.Formatters.SqlDialect Dialect
		{
			get
			{
				return _dialect;
			}
		}
	}
}
