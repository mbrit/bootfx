// BootFX - Application framework for .NET applications
// 
// File: MySqlConnection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.ComponentModel;
using BootFX.Common.Data.Formatters;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Summary description for <see cref="MySqlConnection"/>.
	/// </summary>
	[Description("MySQL (ODBC)"), Connection(), ConnectionStringBuilder(typeof(MySqlConnectionStringBuilder))]
	public class MySqlConnection : OdbcConnection
	{
		private static MySqlDialect _dialect = new MySqlDialect();

		/// <summary>
		/// Creates a new instance of <see cref="MySqlConnection"/>.
		/// </summary>
		public MySqlConnection(string connectionString) : base(connectionString)
		{
		}

		public override BootFX.Common.Data.Schema.FieldSpecification GetFieldSpecificationForNativeTypeName(string nativeTypeName, bool throwIfNotFound)
		{
			// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
			throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
		}

		public override BootFX.Common.Data.Formatters.SqlDialect Dialect
		{
			get
			{
				return _dialect;
			}
		}
	}
}
