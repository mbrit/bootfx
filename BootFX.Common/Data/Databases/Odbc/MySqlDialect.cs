// BootFX - Application framework for .NET applications
// 
// File: MySqlDialect.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using System.Text;
using BootFX.Common.Data.Schema;

namespace BootFX.Common.Data.Formatters
{
	/// <summary>
	/// Summary description for <see cref="MySqlDialect"/>.
	/// </summary>
	public class MySqlDialect : SqlDialect
	{
		/// <summary>
		/// Creates a new instance of <see cref="MySqlDialect"/>.
		/// </summary>
		public MySqlDialect()
		{
		}

		public override string IdentifierPrefix
		{
			get
			{
				return string.Empty;
			}
		}

		public override string IdentifierSuffix
		{
			get
			{
				return string.Empty;
			}
		}

		public override string StatementSeparator
		{
			get
			{
				return ";";
			}
		}

		public override string VariablePrefix
		{
			get
			{
				return "@";
			}
		}

        public override string FormatVariableNameForQueryText(string variableName)
		{
			return "?";
		}

		public override string LastInsertedIdVariableName
		{
			get
			{
				return "@@identity";
			}
		}

		public override string[] GetDropTableStatement(BootFX.Common.Data.Schema.SqlTable table)
		{
			if(table == null)
				throw new ArgumentNullException("table");

			return new string[] {string.Format("DROP TABLE IF EXISTS {0}{1}", this.FormatTableName(table), this.StatementSeparator)};										 
		}

		public override string[] GetCreateDefaultConstraintStatement(SqlColumn column, SqlDatabaseDefault databaseDefault)
		{
			return new string[]{};
		}

		public override string[] GetDropConstraintStatement(SqlColumn column, SqlDatabaseDefault databaseDefault)
		{
			return new string[]{};
		}

		public override string[] GetAddColumnStatement(SqlColumn column, bool add)
		{
			return new string[]{};
		}

		public override string[] GetCreateTableStatement(SqlTable table)
		{
			return new string[]{};
		}

		public override string[] GetDropIndexStatement(SqlTable table, SqlIndex index)
		{
			return null;
		}


		public override string[] GetCreateIndexStatement(SqlTable table, SqlIndex index)
		{
			return null;
		}

		public override string[] GetCreateForeignKeyStatement(SqlChildToParentLink foreignKey)
		{
			return null;
		}

		public override string[] GetDropForeignKeyStatement(SqlChildToParentLink foreignKey)
		{
			return null;
		}

		//		/// <summary>
		//		/// Gets the statement that can create the given table.
		//		/// </summary>
		//		/// <param name="table"></param>
		//		/// <returns></returns>
		//		protected override string[] GetCreateTableStatement(SqlTable table)
		//		{
		//			if(table == null)
		//				throw new ArgumentNullException("table");
		//			
		//			StringBuilder builder = new StringBuilder();
		//			builder.Append(this.CreateTableKeyword);
		//			builder.Append(" ");
		//			builder.Append(this.FormatTableName(table.NativeName));
		//			builder.Append(" (");
		//			for(int index = 0; index < table.Columns.Count; index++)
		//			{
		//				if(index > 0)
		//					builder.Append(", ");
		//				builder.Append("\r\n\t");
		//
		//				// column...
		//				SqlColumn column = table.Columns[index];
		//				builder.Append(GetColumnCreateSnippet(column));
		//			}
		//
		//			// pk...
		//			SqlColumn[] keyColumns = table.GetKeyColumns();
		//			if(keyColumns == null)
		//				throw new InvalidOperationException("keyColumns is null.");
		//			if(keyColumns.Length > 0)
		//			{
		//				builder.Append(",\r\n\tPRIMARY KEY(");
		//				for(int index = 0; index < keyColumns.Length; index++)
		//				{
		//					if(index > 0)
		//						builder.Append(", ");
		//					builder.Append(this.FormatColumnName(keyColumns[index]));
		//				}
		//				builder.Append(")");
		//			}
		//
		//			// end...
		//			builder.Append("\r\n\t)");
		//			builder.Append(this.StatementSeparator);
		//
		//			// return...
		//			return new string[]{builder.ToString()};
		//		}
	}
}
