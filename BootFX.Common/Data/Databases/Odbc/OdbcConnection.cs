// BootFX - Application framework for .NET applications
// 
// File: OdbcConnection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using BootFX.Common.Data.Formatters;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Summary description for <see cref="OdbcConnection"/>.
	/// </summary>
	public class OdbcConnection : Connection
	{
        private SqlDialect _dialect = new MySqlDialect();

		/// <summary>
		/// Creates a new instance of <see cref="OdbcConnection"/>.
		/// </summary>
		public OdbcConnection(string connectionString) : base(connectionString)
		{
		}

		protected internal override ApplicationException CreateCommandException(string message, System.Data.IDbCommand command, Exception originalException,
            SqlStatement sql = null)
		{
			if(command == null)
				throw new ArgumentNullException("command");
			if(originalException == null)
				throw new ArgumentNullException("originalException");	
			return new OdbcException(message, command, originalException);
		}

		protected override System.Data.IDataAdapter CreateDataAdapter(System.Data.IDbCommand command)
		{
			if(command == null)
				throw new ArgumentNullException("command");
			if(!(command is System.Data.Odbc.OdbcCommand))
				throw new InvalidOperationException(string.Format("Command is of type '{0}'. not OdbcCommand.", command.GetType()));
			return new System.Data.Odbc.OdbcDataAdapter((System.Data.Odbc.OdbcCommand)command);
		}

		protected override System.Data.IDbConnection CreateNativeConnection()
		{
			if(ConnectionString == null)
				throw new InvalidOperationException("'ConnectionString' is null.");
			if(ConnectionString.Length == 0)
				throw new InvalidOperationException("'ConnectionString' is zero-length.");

			// create...
			return new System.Data.Odbc.OdbcConnection(this.ConnectionString);
		}

        public override BootFX.Common.Data.Formatters.SqlDialect Dialect
        {
            get
            {
                return _dialect;
            }
        }

        public override BootFX.Common.Data.Schema.FieldSpecification GetFieldSpecificationForNativeTypeName(string nativeTypeName, bool throwIfNotFound)
        {
            throw new Exception("The method or operation is not implemented.");
        }


	}
}
