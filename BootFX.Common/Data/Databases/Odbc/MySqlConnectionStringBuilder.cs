// BootFX - Application framework for .NET applications
// 
// File: MySqlConnectionStringBuilder.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Summary description for <see cref="MySqlConnectionStringBuilder"/>.
	/// </summary>
	public class MySqlConnectionStringBuilder : ConnectionStringBuilder
	{
		/// <summary>
		/// Creates a new instance of <see cref="MySqlConnectionStringBuilder"/>.
		/// </summary>
		public MySqlConnectionStringBuilder()
		{
		}

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <param name="serverName"></param>
		/// <param name="databaseName"></param>
		/// <param name="useIntegratedSecurity"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public override string GetConnectionString(string serverName, string databaseName, bool useIntegratedSecurity, string username, string password)
		{
			if(serverName == null)
				throw new ArgumentNullException("serverName");
			if(serverName.Length == 0)
				throw new ArgumentOutOfRangeException("'serverName' is zero-length.");
			if(databaseName == null)
				throw new ArgumentNullException("databaseName");
			if(databaseName.Length == 0)
				throw new ArgumentOutOfRangeException("'databaseName' is zero-length.");

			// Driver={0}MySQL ODBC 3.51 Driver{1};Server={2};database={3};uid={4};pwd={5}
			if(useIntegratedSecurity)
				throw new NotSupportedException("Integrated security is not supported.");

            // mbr - 2008-08-30 - changed this to use the 5.1 name...
            return string.Format("Driver={0}MySQL ODBC 3.51 Driver{1};Server={2};database={3};uid={4};pwd={5}",
                "{", "}", serverName, databaseName, username, password);
        }
    }
}
