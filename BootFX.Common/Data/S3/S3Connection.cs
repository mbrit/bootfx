// BootFX - Application framework for .NET applications
// 
// File: S3Connection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Data.S3
{
	/// <summary>
	/// Defines a class that can connect to Amazon S3.
	/// </summary>
	public class S3Connection
	{
		public S3Connection()
		{
		}
	}
}
