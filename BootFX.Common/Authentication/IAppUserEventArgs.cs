// BootFX - Application framework for .NET applications
// 
// File: IAppUserEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Authentication
{
	/// <summary>
	/// Event handler for use with <see cref="IAppUserEventArgs"></see>.
	/// </summary>
	public delegate void IAppUserEventHandler(object sender, IAppUserEventArgs e);

	/// <summary>
	/// Summary description for IAppUserEventArgs.
	/// </summary>
	public class IAppUserEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <see cref="Id"/> property.
		/// </summary>
		private object[] _id;
		
		/// <summary>
		/// Private field to support <c>AppUser</c> property.
		/// </summary>
		private IAppUser _appUser;

		/// <summary>
		/// Constructor,
		/// </summary>
		public IAppUserEventArgs(object[] id)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			_id = id;
		}

		/// <summary>
		/// Gets or sets the appuser
		/// </summary>
		public IAppUser AppUser
		{
			get
			{
				return _appUser;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _appUser)
				{
					// set the value...
					_appUser = value;
				}
			}
		}

		/// <summary>
		/// Gets the id.
		/// </summary>
		public object[] Id
		{
			get
			{
				return _id;
			}
		}
	}
}
