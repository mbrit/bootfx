// BootFX - Application framework for .NET applications
// 
// File: IAppUser2.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Authentication
{
	/// <summary>
	/// Defines an interface that can return a user ID and username.
	/// </summary>
	public interface IAppUser2 : IAppUser
	{
		/// <summary>
		/// Gets the username.
		/// </summary>
		string Username
		{
			get;
		}
	}
}
