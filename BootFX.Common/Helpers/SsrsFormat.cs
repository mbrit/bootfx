// BootFX - Application framework for .NET applications
// 
// File: SsrsFormat.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Defined output formats for SSRS.
	/// </summary>
	public enum SsrsFormat
	{
		Pdf = 0,
		Html = 1,
		Excel = 2,
	}
}
