// BootFX - Application framework for .NET applications
// 
// File: SsrsHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Web;
using System.Text;
using System.Collections;
using BootFX.Common.Management;

namespace BootFX.Common.Data
{
	/// <summary>
	/// Defines a class that helps access SQL Server Reporting Services.
	/// </summary>
	public class SsrsHelper : Loggable
	{
		private const string RsFormatKey = "rs:Format";

		/// <summary>
		/// Constructor.
		/// </summary>
		public SsrsHelper()
		{
		}

		/// <summary>
		/// Gets the URL to make a GET request to.
		/// </summary>
		/// <param name="reportServer"></param>
		/// <param name="parameterValues"></param>
		/// <returns></returns>
		public string GetHttpGetUrl(SsrsReportId id, SsrsFormat format, IDictionary parameterValues)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(parameterValues == null)
				throw new ArgumentNullException("parameterValues");
			
			// http://localhost/ReportServer/?/Forest.Symphony.Reports/DeliveryNotes&rs:Format=PDF
			StringBuilder builder = new StringBuilder();
			builder.Append(id.ToString());

			// format...
			builder.Append("&");
			builder.Append(RsFormatKey);
			builder.Append("=");
			builder.Append(this.FormatToString(format));

			// data...
			string url = builder.ToString();
			if(parameterValues.Count > 0)
				url = HttpHelper.AppendVariablesToQueryString(url, parameterValues, new QueryStringDataFormatCallback(FormatCallback));

			// return...
			return url;
		}

        private string FormatCallback(object value)
        {
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                return dt.ToString("yyyy-MMM-dd HH:mm:ss");
            }
            else
                return ConversionHelper.ToString(value, Cultures.System);
        }

		/// <summary>
		/// Gets the format as a string.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		private string FormatToString(SsrsFormat format)
		{
			switch(format)
			{
				case SsrsFormat.Pdf:
					return "PDF";

				case SsrsFormat.Html:
					return "HTML4.0";
	
				case SsrsFormat.Excel:
					return "Excel";
				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", format, format.GetType()));
			}
		}

		/// <summary>
		/// Downloads the given report to a temporary file.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="?"></param>
		/// <param name="parameterValues"></param>
		/// <returns></returns>
		public string DownloadReportToFile(SsrsReportId id, SsrsFormat format, IDictionary parameterValues, HttpDownloadSettings settings)
		{
			string path = Runtime.Current.GetTempFilePath(GetFileExtension(format));
			if(path == null)
				throw new InvalidOperationException("'path' is null.");
			if(path.Length == 0)
				throw new InvalidOperationException("'path' is zero-length.");

			// download...
			this.DownloadReportToFile(id, format, parameterValues, path, settings);

			// return...
			return path;
		}

		/// <summary>
		/// Downloads the given report to the named file.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="?"></param>
		/// <param name="parameterValues"></param>
		/// <param name="filePath"></param>
		public void DownloadReportToFile(SsrsReportId id, SsrsFormat format, IDictionary parameterValues, string filePath, HttpDownloadSettings settings)
		{
			// mbr - 04-07-2008 - added a flag to say if we want to use a post or get.  post won't work for some reason...			
			this.DownloadReportToFile(id, format, parameterValues, filePath, settings, false);
		}

		/// <summary>
		/// Downloads the given report to the named file.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="?"></param>
		/// <param name="parameterValues"></param>
		/// <param name="filePath"></param>
		private void DownloadReportToFile(SsrsReportId id, SsrsFormat format, IDictionary parameterValues, string filePath, HttpDownloadSettings settings, bool post)
		{
			if(id == null)
				throw new ArgumentNullException("id");
			if(parameterValues == null)
				throw new ArgumentNullException("parameterValues");
			if(filePath == null)
				throw new ArgumentNullException("filePath");
			if(filePath.Length == 0)
				throw new ArgumentOutOfRangeException("'filePath' is zero-length.");
			
			// post?
			string url = null;
			if(!(post))
				url = this.GetHttpGetUrl(id, format, parameterValues);
			else
				url = id.ToString();

			// check...
			if(url == null)
				throw new InvalidOperationException("'url' is null.");
			if(url.Length == 0)
				throw new InvalidOperationException("'url' is zero-length.");

			// log...
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Downloading '{0}' format report from '{1}'...", format, url));

			// get...
			string tempPath = null;
			if(!(post))
				tempPath = HttpHelper.DownloadToTempFile(url, settings);
			else
			{
				// create a set of arguments...
				IDictionary values = new Hashtable();

				// setup...
				values["rs:Command"] = "Render";
				values["rc:LinkTarget"] = "main";
				if(parameterValues.Count > 0)
					values["rs:Parameters"] = "true";
				else
					values["rs:Parameters"] = "false";
				values[RsFormatKey] = this.FormatToString(format);

				// copy the user args...
				foreach(DictionaryEntry entry in parameterValues)
					values[entry.Key] = entry.Value;

				// get...
				tempPath = HttpHelper.MakePostRequestAndDownloadToTempFile(url, values, settings);
			}

			// check...
			if(tempPath == null)
				throw new InvalidOperationException("'tempPath' is null.");
			if(tempPath.Length == 0)
				throw new InvalidOperationException("'tempPath' is zero-length.");

			// get...
			Runtime.Current.SafeDelete(filePath);
			File.Move(tempPath, filePath);

			// log...
			if(this.Log.IsInfoEnabled)
				this.Log.Info("Download complete.");
		}

		/// <summary>
		/// Gets the extension for the given file.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public string GetFileExtension(SsrsFormat format)
		{
			switch(format)
			{
				case SsrsFormat.Pdf:
					return ".pdf";

				case SsrsFormat.Html:
					return ".html";

				case SsrsFormat.Excel:
					return ".xls";
				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", format, format.GetType()));
			}
		}

		/// <summary>
		/// Gets the mime type for the given file.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public string GetMimeType(SsrsFormat format)
		{
			switch(format)
			{
				case SsrsFormat.Pdf:
					return "application/pdf";

				case SsrsFormat.Html:
					return "text/html";

				case SsrsFormat.Excel:
					return "application/excel";
				default:
					throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", format, format.GetType()));
			}
		}
	}
}
