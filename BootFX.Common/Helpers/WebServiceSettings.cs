// BootFX - Application framework for .NET applications
// 
// File: WebServiceSettings.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web.Services.Protocols;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace BootFX.Common
{
	/// <summary>
	/// Defines an instance of <c>WebServiceSettings</c>.
	/// </summary>
	public class WebServiceSettings
	{
		private const string IsSecureKey = "DefaultWebServiceIsSecure";
		private const string HostKey = "DefaultWebServiceHost";
		private const string PortKey = "DefaultWebServicePort";
		private const string AppNameKey = "DefaultWebServiceAppName";

		/// <summary>
		/// Private field to support <see cref="IsDefault"/> property.
		/// </summary>
		private bool _isDefault;
		
		/// <summary>
		/// Private field to support <c>IsSecure</c> property.
		/// </summary>
		private bool _isSecure;

		/// <summary>
		/// Private field to support <c>Host</c> property.
		/// </summary>
		private string _host;

		/// <summary>
		/// Private field to support <c>Host</c> property.
		/// </summary>
		private int _port;

		/// <summary>
		/// Private field to support <c>AppName</c> property.
		/// </summary>
		private string _appName;
		
		/// <summary>
		/// Private field to support <see cref="DefaultSettings"/> property.
		/// </summary>
		private static WebServiceSettings _default;

		/// <summary>
		/// Constructor.
		/// </summary>
		public WebServiceSettings()
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		private WebServiceSettings(bool isDefault)
		{
			// default...
			_isDefault = isDefault;
			if(isDefault)
			{
				// try...
				InstallationSettings settings = Runtime.Current.InstallationSettings;
				if(settings == null)
					throw new InvalidOperationException("settings is null.");

				// get...
				string host = settings.GetStringValue(HostKey, null, Cultures.System, OnNotFound.ReturnNull);
				if(host != null && host.Length > 0)
				{
					this.Host = host;
					this.IsSecure = settings.GetBooleanValue(IsSecureKey, false, Cultures.System, OnNotFound.ThrowException);
					this.Port = settings.GetInt32Value(PortKey, 0, Cultures.System, OnNotFound.ThrowException);
					this.AppName = settings.GetStringValue(AppNameKey, null, Cultures.System, OnNotFound.ThrowException);
				}
			}
		}

		/// <summary>
		/// Gets the defaultsettings.
		/// </summary>
		public static WebServiceSettings Default
		{
			get
			{
				if(_default == null)
					_default = new WebServiceSettings(true);
				return _default;
			}
		}

		/// <summary>
		/// Gets the isdefault.
		/// </summary>
		private bool IsDefault
		{
			get
			{
				return _isDefault;
			}
		}

		/// <summary>
		/// Gets or sets the appname
		/// </summary>
		public string AppName
		{
			get
			{
				return _appName;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _appName)
				{
					// set the value...
					_appName = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the host
		/// </summary>
		public int Port
		{
			get
			{
				return _port;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _port)
				{
					// set the value...
					_port = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the host
		/// </summary>
		public string Host
		{
			get
			{
				return _host;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _host)
				{
					// set the value...
					_host = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the issecure
		/// </summary>
		public bool IsSecure
		{
			get
			{
				return _isSecure;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _isSecure)
				{
					// set the value...
					_isSecure = value;
				}
			}
		}

		public bool IsConfigured
		{
			get
			{
				if(this.Host != null && this.Host.Length > 0)
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Patches a URL to be compatible with these settings.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public void PatchUrl(SoapHttpClientProtocol ws)
		{
			if(ws == null)
				throw new ArgumentNullException("ws");
			
			// patch...
			ws.Url = this.PatchUrl(ws.Url);
			if(ws.Url == null)
				throw new InvalidOperationException("'ws.Url' is null.");
			if(ws.Url.Length == 0)
				throw new InvalidOperationException("'ws.Url' is zero-length.");
		}

		/// <summary>
		/// Patches a URL to be compatible with these settings.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string PatchUrl(string url)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");

			// check...
			if(!(this.IsConfigured))
				throw new InvalidOperationException("Web service settings are not configured.");

			// return...
			url = HttpHelper.PatchUrl(url, this.IsSecure, this.Host, this.Port, this.AppName);
			if(url == null)
				throw new InvalidOperationException("'url' is null.");
			if(url.Length == 0)
				throw new InvalidOperationException("'url' is zero-length.");
			return url;
		}

		/// <summary>
		/// Saves these settings to installation settings.
		/// </summary>
		public void Save()
		{
			if(!(this.IsDefault))
				throw new InvalidOperationException("This operation can only be called on the default settings.");

			// set...
			InstallationSettings settings = Runtime.Current.InstallationSettings;
			if(settings == null)
				throw new InvalidOperationException("settings is null.");
			settings.SetValue(IsSecureKey, this.IsSecure);
			settings.SetValue(HostKey, this.Host);
			settings.SetValue(PortKey, this.Port);
			settings.SetValue(AppNameKey, this.AppName);

			// save...
			settings.Save();
		}
	}
}
