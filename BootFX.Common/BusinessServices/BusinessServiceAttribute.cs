// BootFX - Application framework for .NET applications
// 
// File: BusinessServiceAttribute.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Runtime.Remoting;

namespace BootFX.Common.BusinessServices
{
	/// <summary>
	/// Summary description for BusinessServiceAttribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class BusinessServiceAttribute : Attribute
	{
		/// <summary>
		/// Private field to support <c>Mode</c> property.
		/// </summary>
		private WellKnownObjectMode _mode = WellKnownObjectMode.Singleton;
		
		/// <summary>
		/// Private field to support <see cref="InterfaceType"/> property.
		/// </summary>
		private Type _interfaceType;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="interfaceType"></param>
		public BusinessServiceAttribute(Type interfaceType)
		{
			if(interfaceType == null)
				throw new ArgumentNullException("interfaceType");
			_interfaceType = interfaceType;
		}

		/// <summary>
		/// Gets the interfacetype.
		/// </summary>
		public Type InterfaceType
		{
			get
			{
				return _interfaceType;
			}
		}

		/// <summary>
		/// Gets or sets the mode
		/// </summary>
		public WellKnownObjectMode Mode
		{
			get
			{
				return _mode;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _mode)
				{
					// set the value...
					_mode = value;
				}
			}
		}
	}
}
