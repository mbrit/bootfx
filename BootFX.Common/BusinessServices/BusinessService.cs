// BootFX - Application framework for .NET applications
// 
// File: BusinessService.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Runtime.Remoting;
using System.Collections;
using BootFX.Common.Entities;
using BootFX.Common.Management;

namespace BootFX.Common.BusinessServices
{
	/// <summary>
	/// Base class for business services.
	/// </summary>
	public abstract class BusinessService : LoggableMarshalByRefObject
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		protected BusinessService()
		{
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Created '{0}' service.", this.GetType()));
		}
	}
}
