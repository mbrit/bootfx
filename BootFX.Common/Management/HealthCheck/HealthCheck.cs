// BootFX - Application framework for .NET applications
// 
// File: HealthCheck.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Threading;
using BootFX.Common.Data;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Describes a class that can be used to check application health.
	/// </summary>
	public class HealthCheck : Loggable
	{
		private const string FileLogName = "Health Check";

		/// <summary>
		/// Private field to support <c>FailIfNoDefaultDatabase</c> property.
		/// </summary>
		private bool _failIfNoDefaultDatabase = true;
		
		/// <summary>
		/// Raised when the check succeeds.
		/// </summary>
		public event HealthCheckResultEventHandler Finished;
		
		/// <summary>
		/// Runs the checks that you require.
		/// </summary>
		public event EventHandler DoCheck;
		
		public HealthCheck()
		{
		}

		/// <summary>
		/// Checks the primary database connection.
		/// </summary>
		private void CheckDefaultDatabase()
		{
			try
			{
				// do we have a database?
				if(Database.HasDefaultDatabaseSettings())
				{
					if(this.Log.IsInfoEnabled)
						this.Log.Info(string.Format("Checking primary: {0} ({1})" , 
							Database.DefaultConnectionString, Database.DefaultConnectionType));

					// create...
					using(IConnection conn = Database.CreateConnection())
						conn.EnsureOpen();
				}
				else
				{
					if(this.FailIfNoDefaultDatabase)
						throw new InvalidOperationException("\tA primary database is not configured.");
					else
					{
						if(this.Log.IsInfoEnabled)
							this.Log.Info("\tPrimary database not configured.");
					}
				}
			}
			catch(Exception ex)
			{
				throw new HealthCheckException("Primary database connection failure.", ex);
			}
		}

		/// <summary>
		/// Gets or sets the failifnodefaultdatabase
		/// </summary>
		public bool FailIfNoDefaultDatabase
		{
			get
			{
				return _failIfNoDefaultDatabase;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _failIfNoDefaultDatabase)
				{
					// set the value...
					_failIfNoDefaultDatabase = value;
				}
			}
		}

		/// <summary>
		/// Checks the named databases.
		/// </summary>
		private void CheckNamedDatabases()
		{
			if(this.Log.IsInfoEnabled)
				this.Log.Info(string.Format("Checking '{0}' secondary database(s)...", Database.Databases.Count));

			// walk...
			foreach(NamedDatabase db in Database.Databases)
				CheckNamedDatabase(db);
		}

		/// <summary>
		/// Checks the given named database.
		/// </summary>
		/// <param name="db"></param>
		private void CheckNamedDatabase(NamedDatabase db)
		{
			if(db == null)
				throw new ArgumentNullException("db");
			
			try
			{
				if(this.Log.IsInfoEnabled)
				{
					this.Log.Info(string.Format("\tChecking secondary: {0} ({1})", 
						db.Settings.ConnectionString, db.Settings.ConnectionType));
				}

				// run,...
				using(IConnection conn = Database.CreateConnection(db.Name))
					conn.EnsureOpen();
			}
			catch(Exception ex)
			{
				throw new HealthCheckException(string.Format("Secondary database ({0}) connection failure.", db.Name), ex);
			}
		}

		/// <summary>
		/// Creates a file log.
		/// </summary>
		/// <returns></returns>
		private FileLog CreateFileLogger()
		{
			try
			{
				FileLog log = LogSet.CreateFileLogger(FileLogName, FileLoggerFlags.Default);
				if(log == null)
					throw new InvalidOperationException("log is null.");

				// dump some stuff...
				if(log.IsInfoEnabled)
				{
					log.Info(string.Format("Running health check '{0}'...", this.GetType().FullName));
					log.Info("\tModule: " + Runtime.Current.Application.ProductModule);
					log.Info("\tVersion: " + Runtime.Current.Application.ProductVersion);
		
				}

				// return...
				return log;
			}
			catch(Exception ex)
			{
				throw new HealthCheckException(string.Format("Failed to create log file ({0}).", 
					Runtime.Current.LogsFolderPath), ex);
			}
		}

		/// <summary>
		/// Checks the health of the application.
		/// </summary>
		/// <returns>Returns null if the checks passed, or the exception that occurred during checking.</returns>
		public HealthCheckResult Check()
		{
			HealthCheckResult result = null;

			// log...
			FileLog log = null;
			try
			{
				// create...  note this is an unusual usage pattern - don't replicate this.
				log = CreateFileLogger();
				if(log == null)
					throw new InvalidOperationException("log is null.");

				// bind...
				LogSet.BindToContext(log);

				// anything else?
				this.RunChecks();

				// all ok...
				result = new HealthCheckResult(log, null);
			}
			catch(Exception ex)
			{
				// log...
				if(this.Log.IsErrorEnabled)
					this.Log.Error("The health check failed.", ex);

				// return...
				result = new HealthCheckResult(log, ex);
			}
			finally
			{
				if(log != null)
					LogSet.UnbindFromContext(log);
			}

			// return...
			this.OnFinished(new HealthCheckResultEventArgs(result));

			// return...
			return result;
		}

		/// <summary>
		/// Runs the other checks.
		/// </summary>
		protected virtual void RunChecks()
		{
			// checks the primary database.
			this.CheckDefaultDatabase();

			// checks the other databases...
			this.CheckNamedDatabases();
			
			// do the user checks...
			this.OnDoCheck();
		}

		/// <summary>
		/// Raises the <c>DoCheck</c> event.
		/// </summary>
		private void OnDoCheck()
		{
			OnDoCheck(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DoCheck</c> event.
		/// </summary>
		protected virtual void OnDoCheck(EventArgs e)
		{
			// raise...
			if(DoCheck != null)
			{
				if(this.Log.IsInfoEnabled)
					this.Log.Info("Calling custom event...");

				// run...
				DoCheck(this, e);

				// stop...
				if(this.Log.IsInfoEnabled)
					this.Log.Info("Returned from custom event.");
			}
			else
			{
				if(this.Log.IsInfoEnabled)
					this.Log.Info("No custom check subscribers.");
			}
		}

		/// <summary>
		/// Raises the <c>CheckFailed</c> event.
		/// </summary>
		protected virtual void OnFinished(HealthCheckResultEventArgs e)
		{
			// raise...
			if(Finished != null)
				Finished(this, e);
		}
	}
}
