// BootFX - Application framework for .NET applications
// 
// File: HealthCheckResultEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Management
{
	public delegate void HealthCheckResultEventHandler(object sender, HealthCheckResultEventArgs e);

	/// <summary>
	/// Summary description for HealthCheckResultEventArgs.
	/// </summary>
	public class HealthCheckResultEventArgs : EventArgs
	{
		/// <summary>
		/// Private field to support <see cref="result"/> property.
		/// </summary>
		private HealthCheckResult _result;
		
		internal HealthCheckResultEventArgs(HealthCheckResult result)
		{
			if(result == null)
				throw new ArgumentNullException("result");
			_result = result;
		}

		/// <summary>
		/// Gets the result.
		/// </summary>
		public HealthCheckResult Result
		{
			get
			{
				return _result;
			}
		}
	}
}
