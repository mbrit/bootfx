// BootFX - Application framework for .NET applications
// 
// File: HealthCheckResult.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Text;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Summary description for HealthCheckResult.
	/// </summary>
	public class HealthCheckResult
	{
		private const string OkMessage = "[OK]";
		private const string FailedMessage = "[Failed]";

		/// <summary>
		/// Private field to support <see cref="Log"/> property.
		/// </summary>
		private FileLog _log;

		/// <summary>
		/// Private field to support <see cref="Exception"/> property.
		/// </summary>
		private Exception _exception;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="ex"></param>
		internal HealthCheckResult(FileLog log, Exception ex)
		{
			if(log == null)
				throw new ArgumentNullException("log");
			
			// set...
			_log = log;
			_exception = ex;
		}

		/// <summary>
		/// Gets the exception.
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _exception;
			}
		}
		
		/// <summary>
		/// Gets the log.
		/// </summary>
		public FileLog Log
		{
			get
			{
				return _log;
			}
		}

		public bool HasError
		{
			get
			{
				if(this.Exception != null)
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Gets the message to display to the user.
		/// </summary>
		public string Message
		{
			get
			{
				if(!(this.HasError))
					return OkMessage;
				else
				{
					// what do we use?
					StringBuilder builder = new StringBuilder();
					builder.Append(FailedMessage);
					builder.Append(" --> ");

					// next...
					if(this.Exception is HealthCheckException)
						builder.Append(this.Exception.Message);
					else
						builder.Append(this.Exception.GetType());

					// file...
					if(Log == null)
						throw new InvalidOperationException("Log is null.");
					builder.Append(" (");
					builder.Append(Path.GetFileName(this.Log.Path));
					builder.Append(")");

					// use...
					string useMessage = builder.ToString();
					while(true)
					{
						int index = useMessage.ToLower().IndexOf(OkMessage.ToLower());
						if(index == -1)
							break;

						// replace...
						string newMessage = useMessage.Substring(0, index);
						newMessage += useMessage.Substring(index + OkMessage.Length);
						useMessage = newMessage;
					}

					//' return...
					return useMessage;
				}
			}
		}
	}
}
