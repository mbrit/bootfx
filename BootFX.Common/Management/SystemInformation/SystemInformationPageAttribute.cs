// BootFX - Application framework for .NET applications
// 
// File: SystemInformationPageAttribute.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.Management
{
	/// <summary>
	///	 Attribute that indicates which classes that extend <see cref="SystemInformationPage"></see> should be included in the system information page set.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class SystemInformationPageAttribute : Attribute
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public SystemInformationPageAttribute()
		{
		}
	}
}
