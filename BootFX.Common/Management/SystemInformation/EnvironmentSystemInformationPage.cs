// BootFX - Application framework for .NET applications
// 
// File: EnvironmentSystemInformationPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Security;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;

namespace BootFX.Common.Management
{
	/// <summary>
	///	 Defines a system information page that contains environmental information, e.g. machine name and OS version.
	/// </summary>
	[SystemInformationPage()]
	public class EnvironmentSystemInformationPage : SystemInformationPage
	{
		public EnvironmentSystemInformationPage()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override string ToString()
		{
			return "Environment";
		}

		protected override object GetData()
		{
			// list...
			IDictionary data = new HybridDictionary();

			// basic environmental data...
			data.Add("MachineName", Environment.MachineName);
			data.Add("CommandLine", Environment.CommandLine);
			data.Add("OSPlatform", Environment.OSVersion.Platform.ToString());
			data.Add("OSVersion", Environment.OSVersion.Version.ToString());

			// runtime data...
			if(Runtime.IsStarted == true)
			{
				data.Add("RuntimeStarted", true.ToString());
				data.Add("Application", Runtime.Current.Application.ToString());
				data.Add("ApplicationDataFolderPath", Runtime.Current.ApplicationDataFolderPath);
				data.Add("LocalApplicationDataFolderPath", Runtime.Current.LocalApplicationDataFolderPath);
			}
			else
				data.Add("RuntimeStarted", false.ToString());

			// app domain...
			AppDomain appDomain = AppDomain.CurrentDomain;
			if(appDomain == null)
				throw new ArgumentNullException("appDomain");
			data.Add("BaseDirectory", appDomain.BaseDirectory);
			data.Add("DynamicDirectory", appDomain.DynamicDirectory);
			data.Add("AppDomain", appDomain.FriendlyName);
			data.Add("RelativeSearchPath", appDomain.RelativeSearchPath);
			data.Add("ShadowCopyFiles", appDomain.ShadowCopyFiles.ToString());
			data.Add("ApplicationBase", appDomain.SetupInformation.ApplicationBase);
			data.Add("ApplicationName", appDomain.SetupInformation.ApplicationName);
			data.Add("CachePath", appDomain.SetupInformation.CachePath);
			data.Add("ConfigurationFile", appDomain.SetupInformation.ConfigurationFile);
			data.Add("PrivateBinPath", appDomain.SetupInformation.PrivateBinPath);
			data.Add("PrivateBinPathProbe", appDomain.SetupInformation.PrivateBinPathProbe);

			// thread...
			Thread thread = Thread.CurrentThread;
			if(thread == null)
				throw new ArgumentNullException("thread");
			data.Add("ThreadName", thread.Name);
			data.Add("ThreadID", Runtime.GetCurrentThreadId());
			data.Add("ThreadCulture", thread.CurrentCulture);
			data.Add("ThreadUICulture", thread.CurrentUICulture);
			data.Add("ThreadPriority", thread.Priority);
			data.Add("ThreadState", thread.ThreadState);
			data.Add("ThreadApartmentState", thread.GetApartmentState());

			// return...
			return data;
		}
	}
}
