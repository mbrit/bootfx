// BootFX - Application framework for .NET applications
// 
// File: WebSystemInformationPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines a system information page that outputs the contents of <see cref="HttpRequest.ServerVariables"></see>.
	/// </summary>
	[SystemInformationPage()]
	public class WebSystemInformationPage : SystemInformationPage
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public WebSystemInformationPage()
		{
		}

		/// <summary>
		/// Gets the title for the page.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return "Web";
		}

		/// <summary>
		/// Gets the data for the page.
		/// </summary>
		/// <returns></returns>
		protected override object GetData()
		{
			// get the context...
			HttpContext context = HttpContext.Current;
			if(context == null)
				return new string[] { "HttpContext not available." };

			// return...
			return context.Request.ServerVariables;
		}
	}
}
