// BootFX - Application framework for .NET applications
// 
// File: SystemInformationPageCollection.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Holds a collection of <c ref="SystemInformationPage">SystemInformationPage</c> instances.
	/// </summary>
	public class SystemInformationPageCollection : CollectionBase
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public SystemInformationPageCollection()
		{
		}
		
		/// <summary>
		/// Adds a SystemInformationPage instance to the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public void Add(SystemInformationPage item)
		{
			if(item == null)
				throw new ArgumentNullException("item");
			List.Add(item);
		}  

		/// <summary>
		/// Adds a set of SystemInformationPage instances to the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public void AddRange(SystemInformationPage[] items)
		{
			if(items == null)
				throw new ArgumentNullException("items");
			for(int index = 0; index < items.Length; index++)
				Add(items[index]);
		}  
	
		/// <summary>
		/// Adds a set of SystemInformationPage instances to the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public void AddRange(SystemInformationPageCollection items)
		{
			if(items == null)
				throw new ArgumentNullException("items");
			for(int index = 0; index < items.Count; index++)
				Add(items[index]);
		}  
		
		/// <summary>
		/// Inserts a SystemInformationPage instance into the collection.
		/// </summary>
		/// <param name="item">The item to add.</param>
		public void Insert(int index, SystemInformationPage item)
		{
			if(item == null)
				throw new ArgumentNullException("item");
			List.Insert(index, item);
		}  
	
		/// <summary>
		/// Removes a SystemInformationPage item to the collection.
		/// </summary>
		/// <param name="item">The item to remove.</param>
		public void Remove(SystemInformationPage item)
		{
			if(item == null)
				throw new ArgumentNullException("item");
			List.Remove(item);
		}  
		
		/// <summary>
		/// Gets or sets an item.
		/// </summary>
		/// <param name="index">The index in the collection.</param>
		public SystemInformationPage this[int index]
		{
			get
			{
				return (SystemInformationPage)List[index];
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				List[index] = value;
			}
		}
		
		/// <summary>
		/// Returns the index of the item in the collection.
		/// </summary>
		/// <param name="item">The item to find.</param>
		/// <returns>The index of the item, or -1 if it is not found.</returns>
		public int IndexOf(SystemInformationPage item)
		{
			return List.IndexOf(item);
		}
		
		/// <summary>
		/// Discovers if the given item is in the collection.
		/// </summary>
		/// <param name="item">The item to find.</param>
		/// <returns>Returns true if the given item is in the collection.</returns>
		public bool Contains(SystemInformationPage item)
		{
			if(IndexOf(item) == -1)
				return false;
			else
				return true;
		}
		
		/// <summary>
		/// Copies the entire collection to an array.
		/// </summary>
		/// <returns>Returns the array of items.</returns>
		public SystemInformationPage[] ToArray()
		{
			return (SystemInformationPage[])InnerList.ToArray(typeof(SystemInformationPage));
		}

		/// <summary>
		/// Copies the entire collection to an array.
		/// </summary>
		/// <returns>Returns the array of items.</returns>
		public void CopyTo(SystemInformationPage[] items, int index)
		{
			if(items == null)
				throw new ArgumentNullException("items");
			InnerList.CopyTo(items, index);
		}
	}
}
