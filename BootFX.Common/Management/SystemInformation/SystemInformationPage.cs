// BootFX - Application framework for .NET applications
// 
// File: SystemInformationPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Data;
using BootFX.Common.Data;

namespace BootFX.Common.Management
{
	/// <summary>
	///	 Base class for system information pages.
	/// </summary>
	public abstract class SystemInformationPage
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		protected SystemInformationPage()
		{
		}

		/// <summary>
		/// Creates an instance of Data.
		/// </summary>
		/// <remarks>This does not assign the instance to the _data field</remarks>
		public DataTable GetDataTable()
		{
			// get...
			object data = null;
			try
			{
				data = this.GetData();
				if(data == null)
					throw new InvalidOperationException("data is null.");
			}
			catch(Exception ex)
			{
				data = new object[] { ex };
			}

			// transformer...
			DataTableTransformer transformer = new DataTableTransformer();
			DataTable table = transformer.Transform(data);
			table.TableName = this.ToString();

			// return...
			return table;
		}

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <returns></returns>
		protected abstract object GetData();
	}
}
