// BootFX - Application framework for .NET applications
// 
// File: SecuritySystemInformationPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;

namespace BootFX.Common.Management
{
	/// <summary>
	///	 Defines a system information page that contains security information, e.g. Windows Principal and app domain evidence.
	/// </summary>
	[SystemInformationPage()]
	public class SecuritySystemInformationPage : SystemInformationPage
	{
		public SecuritySystemInformationPage()
		{
		}

		protected override object GetData()
		{
			// create...
			IDictionary dictionary = new HybridDictionary();

			// prinicpal...
			IPrincipal principal = Thread.CurrentPrincipal;
			if(principal != null)
			{
				dictionary.Add("Princpal type", ((object)principal).GetType());
				IIdentity identity = principal.Identity;
				if(identity != null)
				{
					dictionary.Add("Identity type", ((object)identity).GetType());
					dictionary.Add("Identity", identity.Name);
					dictionary.Add("Authenticated", identity.IsAuthenticated);
					dictionary.Add("Authentication type", identity.AuthenticationType);
				}
				else
					dictionary.Add("Null identity", true);
			}
			else
				dictionary.Add("Null principal", true);

			// evidence...
			foreach(object evidence in AppDomain.CurrentDomain.Evidence)
			{
				string name = evidence.GetType().FullName;
				object value = null;
				if(evidence is System.Security.Policy.Url)
				{
					name = "Policy URL";
					value = ((System.Security.Policy.Url)evidence).Value;
				}
				else if(evidence is System.Security.Policy.Zone)
				{
					name = "Policy zone";
					value = ((System.Security.Policy.Zone)evidence).SecurityZone;
				}

				// add?
				if(value != null)
					dictionary.Add(name, value);
			}

			// return...
			return dictionary;
		}

		public override string ToString()
		{
			return "Security";
		}
	}
}
