// BootFX - Application framework for .NET applications
// 
// File: AssembliesSystemInformationPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Data;
using System.Reflection;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Outputs detailed information about the loaded assemblies.
	/// </summary>
	[SystemInformationPage()]
	public class AssembliesSystemInformationPage : SystemInformationPage
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public AssembliesSystemInformationPage()
		{
		}

		/// <summary>
		/// Gets the data for the page.
		/// </summary>
		/// <returns></returns>
		protected override object GetData()
		{
			// create a table of data...
			DataTable table = new DataTable("Assemblies");
			table.Locale = Cultures.System;

			// columns...
			table.Columns.Add("Name");
			table.Columns.Add("Path");
			table.Columns.Add("Date");
			table.Columns.Add("Length");

			// loop...
			foreach(Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
			{
				// file...
				FileInfo info = new FileInfo(new Uri(asm.CodeBase).LocalPath);

				// add...
				table.Rows.Add(new object[] { asm.FullName, info.FullName, info.LastWriteTime, info.Length });
			}

			// return...
			return table;
		}

		public override string ToString()
		{
			return "Assemblies";
		}
	}
}
