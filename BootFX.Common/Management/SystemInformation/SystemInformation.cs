// BootFX - Application framework for .NET applications
// 
// File: SystemInformation.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Data;
using BootFX.Common.Xml;

namespace BootFX.Common.Management
{
	/// <summary>
	///	 Describes a class that contains system information.
	/// </summary>
	/// <remarks>You can add your own pages to this class by extending <see cref="SystemInformationPage"></see> and decorating with 
	/// <see cref="SystemInformationPageAttribute"></see>.</remarks>
	public class SystemInformation : ToXmlBase
	{
		/// <summary>
		/// Private field to support <c>Pages</c> property.
		/// </summary>
		private SystemInformationPageCollection _pages = new SystemInformationPageCollection();
		
		/// <summary>
		/// Constructor.
		/// </summary>
		private SystemInformation(SystemInformationPage[] pages)
		{
			if(pages == null)
				throw new ArgumentNullException("pages");
			
			// set...
			this.Pages.AddRange(pages);
		}

		/// <summary>
		/// Gets the system information.
		/// </summary>
		/// <returns></returns>
		public static SystemInformation GetSystemInformation()
		{
			// get the pages...
			TypeFinder finder = new TypeFinder(typeof(SystemInformationPage));
			finder.AddAttributeSpecification(typeof(SystemInformationPageAttribute), false);

			// return...
			SystemInformationPage[] pages = (SystemInformationPage[])finder.CreateInstances();
			return new SystemInformation(pages);
		}

		/// <summary>
		/// Gets a collection of SystemInformationPage objects.
		/// </summary>
		public SystemInformationPageCollection Pages
		{
			get
			{
				return _pages;
			}
		}

		/// <summary>
		/// Gets the data set.
		/// </summary>
		/// <param name="dataSetName"></param>
		/// <returns></returns>
		public DataSet GetDataSet()
		{
			return this.GetDataSet(this.DefaultElementName);
		}

		/// <summary>
		/// Gets all of the tables in the 
		/// </summary>
		/// <returns></returns>
		public DataSet GetDataSet(string dataSetName)
		{
			// creates and fills a dataset...
			DataSet dataSet = new DataSet(dataSetName);
			dataSet.Locale = Cultures.System;

			// walk...
			foreach(SystemInformationPage page in this.Pages)
				dataSet.Tables.Add(page.GetDataTable());

			// return...
			return dataSet;
		}

		protected override void WriteXml(XmlWriter xml, WriteXmlContext context)
		{
			if(xml == null)
				throw new ArgumentNullException("xml");
			if(context == null)
				throw new ArgumentNullException("context");
			
			// get the data set...
			DataSet dataSet = this.GetDataSet();
			if(dataSet == null)
				throw new ArgumentNullException("dataSet");

			// save...
			dataSet.WriteXml(xml, XmlWriteMode.WriteSchema);
		}
	}
}
