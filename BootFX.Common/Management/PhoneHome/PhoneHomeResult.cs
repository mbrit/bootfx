// BootFX - Application framework for .NET applications
// 
// File: PhoneHomeResult.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Summary description for PhoneHomeResult.
	/// </summary>
	public class PhoneHomeResult
	{
		/// <summary>
		/// Private field to support <see cref="HttpResult"/> property.
		/// </summary>
		private HttpResult _httpResult;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="result"></param>
		internal PhoneHomeResult(HttpResult result)
		{
			if(result == null)
				throw new ArgumentNullException("result");
			_httpResult = result;
		}

		/// <summary>
		/// Gets the httpresult.
		/// </summary>
		public HttpResult HttpResult
		{
			get
			{
				return _httpResult;
			}
		}
		
		/// <summary>
		/// Gets the support ID.
		/// </summary>
		public string SupportId
		{
			get
			{
				if(HttpResult == null)
					throw new InvalidOperationException("HttpResult is null.");

				// check...
				if(HttpResult.ResultAsXml == null)
					throw new InvalidOperationException("HttpResult.ResultAsXml is null.");

				// get...
				XmlElement okElement = (XmlElement)HttpResult.ResultAsXml.SelectSingleNode("Result/Ok");
				if(okElement == null)
					throw new InvalidOperationException("okElement is null.");

				// return...
				return okElement.InnerText;
			}
		}
	}
}
