// BootFX - Application framework for .NET applications
// 
// File: PhoneHomeHelper.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Xml;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using BootFX.Common.Xml;
using BootFX.Common.Data;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Summary description for PhoneHomeHelper.
	/// </summary>
	public class PhoneHomeHelper : Loggable
	{
        ///// <summary>
        ///// Raised before the request is physically sent to the server.
        ///// </summary>
        //public event EventHandler BeforeRequestSent;

        ///// <summary>
        ///// Raised when the 'phone home' operation succeedes.
        ///// </summary>
        //public event PhoneHomeEventHandler PhoneHomeSucceeded;

        ///// <summary>
        ///// Raised when the 'phone home' operation fails.
        ///// </summary>
        //public event PhoneHomeEventHandler PhoneHomeFailed;

        ///// <summary>
        ///// Raised before a report is 'phoned home'.
        ///// </summary>
        //public event PhoneHomeEventHandler BeforePhoneHome;

        /// <summary>
        /// Private field to hold the singleton instance.
        /// </summary>
        private static PhoneHomeHelper _current = null;

        /// <summary>
        /// Private constructor.
        /// </summary>
        private PhoneHomeHelper()
		{
		}
		
		/// <summary>
		/// Static constructor.
		/// </summary>
		static PhoneHomeHelper()
		{
			_current = new PhoneHomeHelper();
		}
		
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static PhoneHomeHelper Current
		{
			get
			{
				return _current;
			}
		}

        //private class UploadContext : HttpDownloadContext 
        //{
        //	/// <summary>
        //	/// Private field to support <see cref="Result"/> property.
        //	/// </summary>
        //	private PhoneHomeEventArgs _innerArgs;

        //	internal UploadContext(PhoneHomeEventArgs innerArgs)
        //	{
        //		if(innerArgs == null)
        //			throw new ArgumentNullException("result");
        //		_innerArgs = innerArgs;
        //	}

        //	/// <summary>
        //	/// Gets the result.
        //	/// </summary>
        //	private PhoneHomeEventArgs InnerArgs
        //	{
        //		get
        //		{
        //			return _innerArgs;
        //		}
        //	}

        //	protected internal override void OnBeforeRequestSent(EventArgs e)
        //	{
        //		base.OnBeforeRequestSent (e);

        //		// set...
        //		this.InnerArgs.SetRequest(this.Request);

        //		// raise...
        //		PhoneHomeHelper.Current.OnBeforeRequestSent(this.InnerArgs);
        //	}
        //}

        /// <summary>
        /// Creates an error report.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public XmlDocument CreateErrorReport(string message, Exception ex, IDictionary extraData)
        {
			LogData data = new LogData(LogLevel.Error, new LogArgs(message, ex), message);
			return CreateErrorReport(data, extraData);
		}

		/// <summary>
		/// Creates an error report.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public XmlDocument CreateErrorReport(LogData data, IDictionary extraData)
		{
			if(data == null)
				throw new ArgumentNullException("data");

			// build the XML up and send it to the web service...  use this.Url and this.Key to get the details.
			XmlDocument doc = new XmlDocument();
			//XmlNamespaceManagerEx manager = XmlHelper.GetNamespaceManagerEx(doc);
			//if(manager == null)
			//	throw new InvalidOperationException("manager is null.");

			//// create...
			//XmlElement reportElement = doc.CreateElement("ErrorReport");
			//doc.AppendChild(reportElement);

			//// add...
			//AppendApplicationElement(reportElement);
			//AppendEnvironmentElement(reportElement);
			//AppendHttpElement(reportElement);
			//AppendErrorElement(reportElement, data);

			//// extra...
			//if(extraData != null)
			//{
			//	XmlElement element = doc.CreateElement("AdditionalData");
			//	reportElement.AppendChild(element);
			//	foreach(DictionaryEntry entry in extraData)
			//	{
			//		try
			//		{
			//			string fullName = ConversionHelper.ToString(entry.Key, Cultures.System);
			//			string prefix = null;
			//			string ns = null;
			//			string localName = null;
			//			manager.SplitFullName(fullName, ref prefix, ref ns, ref localName);

			//			// create...
			//			XmlElement nameElement = doc.CreateElement(prefix, localName, ns);
			//			element.AppendChild(nameElement);
			//			XmlHelper.SetElementValue(nameElement, entry.Value);
			//		}
			//		catch
			//		{
			//			// no-op...
			//		}
			//	}
			//}

			// return...
			return doc;
		}

        //private void AppendApplicationElement(XmlElement parent)
        //{
        //	if(parent == null)
        //		throw new ArgumentNullException("parent");

        //	// create...
        //	XmlElement element = parent.OwnerDocument.CreateElement("Application");
        //	parent.AppendChild(element);

        //	// basics...
        //	XmlHelper.AddElement(element, "VendorName", Runtime.Current.Application.ProductCompany);
        //	XmlHelper.AddElement(element, "ProductName", Runtime.Current.Application.ProductName);
        //	XmlHelper.AddElement(element, "ModuleName", Runtime.Current.Application.ProductModule);
        //	XmlHelper.AddElement(element, "Version", Runtime.Current.Application.ProductVersion.ToString());
        //	XmlHelper.AddElement(element, "BfxVersion", typeof(Runtime).Assembly.GetName().Version.ToString());
        //	XmlHelper.AddElement(element, "FxVersion", typeof(string).Assembly.GetName().Version.ToString());

        //	// mbr - 10-05-2007 - assembly generation takes too long			
        //	// app domain...
        //	XmlElement assembliesElement = parent.OwnerDocument.CreateElement("Assemblies");
        //	element.AppendChild(assembliesElement);

        //	// machine...
        //	foreach(Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
        //		AppendAssembly(assembliesElement, asm);
        //}

        //private void AppendAssembly(XmlElement element, Assembly asm)
        //{
        //	if(element == null)
        //		throw new ArgumentNullException("element");
        //	if(asm == null)
        //		throw new ArgumentNullException("asm");

        //	// add...
        //	XmlElement asmElement = element.OwnerDocument.CreateElement("Assembly");
        //	element.AppendChild(asmElement);
        //	XmlHelper.AddElement(asmElement, "FullName", asm.FullName);
        //}

        //private void AppendEnvironmentElement(XmlElement parent)
        //{
        //	if(parent == null)
        //		throw new ArgumentNullException("parent");

        //	// create...
        //	XmlElement element = parent.OwnerDocument.CreateElement("Environment");
        //	parent.AppendChild(element);

        //	// app domain...
        //	XmlElement appDomainElement = parent.OwnerDocument.CreateElement("AppDomain");
        //	element.AppendChild(appDomainElement);

        //	// machine and stuff...
        //	XmlHelper.AddElement(element, "MachineName", Environment.MachineName);
        //	XmlHelper.AddElement(element, "ThreadName", LogData.GetThreadName());

        //	// entry...
        //	Assembly asm = Assembly.GetEntryAssembly();
        //	if(asm != null)
        //	{
        //		XmlHelper.AddElement(element, "EntryAssemblyName", asm.FullName);
        //		XmlHelper.AddElement(element, "EntryAssemblyVersion", asm.GetName().Version.ToString());
        //	}
        //	else
        //	{
        //		XmlHelper.AddElement(element, "EntryAssemblyName", string.Empty);
        //		XmlHelper.AddElement(element, "EntryAssemblyVersion", "0.0.0.0");
        //	}

        //	// user...
        //	IPrincipal principal = Thread.CurrentPrincipal;
        //	if(principal != null)
        //	{
        //		if(principal.Identity == null)
        //			throw new InvalidOperationException("principal.Identity is null.");

        //		// auth...
        //		XmlHelper.AddElement(element, "IsAuthenticated", principal.Identity.IsAuthenticated);
        //		XmlHelper.AddElement(element, "AuthenticationType", principal.Identity.AuthenticationType);
        //		XmlHelper.AddElement(element, "WindowsUsername", principal.Identity.Name);
        //	}
        //}

        //private void AppendHttpElement(XmlElement parent)
        //{
        //	if(parent == null)
        //		throw new ArgumentNullException("parent");

        //	// create...
        //	XmlElement element = parent.OwnerDocument.CreateElement("Http");
        //	parent.AppendChild(element);

        //	// get...
        //	HttpContext context = HttpContext.Current;
        //	if(context == null)
        //	{
        //		XmlHelper.SetAttributeValue(element, "isHttp", false);
        //		return;
        //	}

        //	// information...
        //	XmlHelper.AddElement(element, "Url", context.Request.Url.ToString());
        //	XmlHelper.AddElement(element, "QueryString", context.Request.QueryString.ToString());
        //}

        //private void AppendErrorElement(XmlElement parent, LogData data)
        //{
        //	if(parent == null)
        //		throw new ArgumentNullException("parent");
        //	if(data == null)
        //		throw new ArgumentNullException("data");

        //	// check...
        //	if(data.Exception == null)
        //		throw new InvalidOperationException("data.Exception is null.");

        //	// don't forget to hash based on application version and module.
        //	string toHash = string.Format("{0}|{1}|{2}", Runtime.Current.Application.ProductModule, Runtime.Current.Application.ProductVersion, 
        //		data.Exception.ToString());
        //	string asString = HashHelper.GetMd5HashOfStringAsBase64(toHash);

        //	// create...
        //	XmlElement element = parent.OwnerDocument.CreateElement("Error");
        //	parent.AppendChild(element);
        //	XmlHelper.SetAttributeValue(element, "hash", asString);
        //	XmlHelper.AddElement(element, "Note", data.UnformattedBuf);
        //	XmlHelper.AddElement(element, "Error", data.Exception.ToString());
        //}

        ///// <summary>
        ///// Sends an error report.
        ///// </summary>
        ///// <returns>A <c>PhoneHomeResult</c> instance on success, or null if the operation is cancelled or fails.</returns>
        ///// <remarks>This operation will consume exception messages.  If you require error information, subscribe to <c>PhoneHomeFailed</c>.</remarks>
        public PhoneHomeResult SendErrorReport(string url, XmlDocument report)
        {
            /*            if (url == null)
                            throw new ArgumentNullException("url");
                        if (url.Length == 0)
                            throw new ArgumentOutOfRangeException("'url' is zero-length.");
                        if (report == null)
                            throw new ArgumentNullException("report");

                        // event...
                        PhoneHomeEventArgs e = new PhoneHomeEventArgs(url, report, HttpDownloadSettings.PhoneHomeDefault);
                        this.OnBeforePhoneHome(e);

                        // cancel?
                        if (e.Cancel)
                            return null;

                        // run it...
                        try
                        {
                            // mbr - 08-10-2007 - log...
                            if (this.Log.IsInfoEnabled)
                                this.Log.Info("Sending error report to: " + e.Url);

                            // context...
                            UploadContext context = new UploadContext(e);
                            context.Settings = e.Settings;
                            context.ExpectedReturnType = HttpResultType.Xml;

                            // pass the items in the URL back as the event may have changed them.
                            HttpResult httpResult = HttpHelper.PostXmlDocument(e.Url, e.Report, context);
                            if (httpResult == null)
                                throw new InvalidOperationException("result is null.");

                            // get...
                            if (this.Log.IsInfoEnabled)
                                this.Log.Info(string.Format("\tResult: {0} --> {1}", httpResult.ResponseStatus, httpResult.ResultAsString));

                            // reset the request...
                            e.SetRequest(null);

                            // problem?
                            if (httpResult.Status != HttpStatusCode.OK)
                                throw new InvalidOperationException(string.Format("An invalid HTTP status code of '{0}' was returned.", httpResult.Status));

                            // prepare the result...
                            PhoneHomeResult result = new PhoneHomeResult(httpResult);
                            e.SetResult(result);

                            // raise the event...
                            this.OnPhoneHomeSucceeded(e);

                            // return...
                            return result;
                        }
                        catch (Exception ex)
                        {
                            if (this.Log.IsErrorEnabled)
                                this.Log.Error("An exception occurred when sending the error report.", ex);

                            // set...
                            e.SetError(ex);
                            this.OnPhoneHomeFailed(e);

                            // return...
                            return null;
                        }*/
            return null;
        }

        ///// <summary>
        ///// Raises the <c>BeforePhoneHome</c> event.
        ///// </summary>
        //protected virtual void OnBeforePhoneHome(PhoneHomeEventArgs e)
        //{
        //	// raise...
        //	if(BeforePhoneHome != null)
        //		BeforePhoneHome(this, e);
        //}

        ///// <summary>
        ///// Raises the <c>OnPhoneHomeFailed</c> event.
        ///// </summary>
        //protected virtual void OnPhoneHomeFailed(PhoneHomeEventArgs e)
        //{
        //	// raise...
        //	if(PhoneHomeFailed != null)
        //		PhoneHomeFailed(this, e);
        //}

        ///// <summary>
        ///// Raises the <c>OnPhoneHomeSucceeded</c> event.
        ///// </summary>
        //protected virtual void OnPhoneHomeSucceeded(PhoneHomeEventArgs e)
        //{
        //	// raise...
        //	if(PhoneHomeSucceeded != null)
        //		PhoneHomeSucceeded(this, e);
        //}

        ///// <summary>
        ///// Raises the <c>BeforeRequestSent</c> event.
        ///// </summary>
        //protected virtual void OnBeforeRequestSent(PhoneHomeEventArgs e)
        //{
        //	// raise...
        //	if(BeforeRequestSent != null)
        //		BeforeRequestSent(this, e);
        //}

        ///// <summary>
        ///// Sends an exception report to the 
        ///// </summary>
        ///// <param name="ex"></param>
        ///// <returns></returns>
        //public PhoneHomeResult SendException(string message, Exception ex)
        //{
        //	return SendException(message, ex, null);
        //}

        ///// <summary>
        ///// Sends an exception report to the 
        ///// </summary>
        ///// <param name="ex"></param>
        ///// <returns></returns>
        //public PhoneHomeResult SendException(string message, Exception ex, IDictionary extraData)
        //{
        //	if(ex == null)
        //		throw new ArgumentNullException("ex");

        //	// create...
        //	XmlDocument report = this.CreateErrorReport(message, ex, extraData);
        //	if(report == null)
        //		throw new InvalidOperationException("report is null.");

        //	// send it now...
        //	return this.SendErrorReport(Runtime.Current.PhoneHomeUrl, report);
        //}
    }
}
