// BootFX - Application framework for .NET applications
// 
// File: PhoneHomeEventArgs.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Xml;
using System.ComponentModel;

namespace BootFX.Common.Management
{
	public delegate void PhoneHomeEventHandler(object sender, PhoneHomeEventArgs e);

	/// <summary>
	/// Summary description for PhoneHomeResultEventArgs.
	/// </summary>
	public class PhoneHomeEventArgs : CancelEventArgs
	{
		/// <summary>
		/// Private field to support <see cref="Request"/> property.
		/// </summary>
		private HttpWebRequest _request;
		
		/// <summary>
		/// Private field to support <see cref="Error"/> property.
		/// </summary>
		private Exception _error;
		
		/// <summary>
		/// Private field to support <c>Settings</c> property.
		/// </summary>
		private HttpDownloadSettings _settings;
		
		/// <summary>
		/// Private field to support <c>Url</c> property.
		/// </summary>
		private string _url;

		/// <summary>
		/// Private field to support <c>Report</c> property.
		/// </summary>
		private XmlDocument _report;
		
		/// <summary>
		/// Private field to support <see cref="HttpResult"/> property.
		/// </summary>
		private PhoneHomeResult _result;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="result"></param>
		internal PhoneHomeEventArgs(string url, XmlDocument report, HttpDownloadSettings settings)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			if(report == null)
				throw new ArgumentNullException("report");
			if(settings == null)
				throw new ArgumentNullException("settings");			
			
			// set...
			_url = url;
			_report = report;
			_settings = settings;
		}

		/// <summary>
		/// Gets or sets the report
		/// </summary>
		public XmlDocument Report
		{
			get
			{
				return _report;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");				

				// check to see if the value has changed...
				if(value != _report)
				{
					// set the value...
					_report = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the url
		/// </summary>
		public string Url
		{
			get
			{
				return _url;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				if(value.Length == 0)
					throw new ArgumentOutOfRangeException("'value' is zero-length.");
				
				// check to see if the value has changed...
				if(value != _url)
				{
					// set the value...
					_url = value;
				}
			}
		}

		/// <summary>
		/// Gets the httpresult.
		/// </summary>
		public PhoneHomeResult Result
		{
			get
			{
				return _result;
			}
		}

		internal void SetResult(PhoneHomeResult result)
		{
			if(result == null)
				throw new ArgumentNullException("result");
			
			// set...
			_result = result;
		}

		/// <summary>
		/// Gets or sets the settings
		/// </summary>
		public HttpDownloadSettings Settings
		{
			get
			{
				return _settings;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _settings)
				{
					// set the value...
					_settings = value;
				}
			}
		}

		/// <summary>
		/// Gets the error.
		/// </summary>
		public Exception Error
		{
			get
			{
				return _error;
			}
		}

		internal void SetError(Exception ex)
		{
			if(ex == null)
				throw new ArgumentNullException("ex");
			_error = ex;
		}

		/// <summary>
		/// Gets the request.
		/// </summary>
		public HttpWebRequest Request
		{
			get
			{
				return _request;
			}
		}

		internal void SetRequest(HttpWebRequest request)
		{
			_request = request;
		}
	}
}
