// BootFX - Application framework for .NET applications
// 
// File: NewVersionCheckEngine.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Services;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines an instance of <c>NewVersionCheckEngine</c>.
	/// </summary>
	internal class NewVersionCheckEngine : ThreadedServiceEngine
	{
		/// <summary>
		/// Private field to support <c>Enabled</c> property.
		/// </summary>
		private bool _enabled;
		
		/// <summary>
		/// Raised when a new version is available.
		/// </summary>
		public event EventHandler NewVersionAvailable;
		
		/// <summary>
		/// Private field to support <c>LastNotifiedVersion</c> property.
		/// </summary>
		private Version _lastNotifiedVersion = new Version(0,0,0,0);
		
		/// <summary>
		/// Private field to support <c>NextCheckAt</c> property.
		/// </summary>
		private DateTime _nextCheckAt = DateTime.MinValue;
		
		internal NewVersionCheckEngine() : base(new TimeSpan(0,0,10))
		{
		}

		/// <summary>
		/// Gets the nextcheckat.
		/// </summary>
		private DateTime NextCheckAt
		{
			get
			{
				// returns the value...
				return _nextCheckAt;
			}
		}

		/// <summary>
		/// Gets the engine.
		/// </summary>
		private new AppUpdateServiceHost Host
		{
			get
			{
				return (AppUpdateServiceHost)base.Host;
			}
		}

		protected override bool DoWork()
		{
			// process stopped?
			if(Host == null)
				throw new InvalidOperationException("Host is null.");
			if(this.Host.Process != null)
			{
				// have we stopped?
				if(this.Host.Process.HasExited)
				{
					this.Enabled = false;
					this.Host.OnRunFinished();
				}
			}

			// check...
			if(this.NextCheckAt != DateTime.MinValue && DateTime.UtcNow > this.NextCheckAt)
			{
				try
				{
					// ok, so try and get it...  
					AppUpdateStatus status = this.Host.GetStatus(true);
					switch(status)
					{
							// something happened...
						case AppUpdateStatus.MustUpdate:
						case AppUpdateStatus.CouldUpdate:
							this.OnNewVersionAvailable();
							break;

							// do nothing...
						case AppUpdateStatus.UpToDate:
						case AppUpdateStatus.CouldNotDetermine:
							break;

						default:
							throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", status, status.GetType()));
					}
				}
				finally
				{
					this.SetNextCheckAt();
				}
			}

			// ok...
			return this.Enabled;
		}

		/// <summary>
		/// Gets the lastnotifiedversion.
		/// </summary>
		private Version LastNotifiedVersion
		{
			get
			{
				// returns the value...
				return _lastNotifiedVersion;
			}
		}

		/// <summary>
		/// Raises the <c>NewVersionAvailable</c> event.
		/// </summary>
		private void OnNewVersionAvailable()
		{
			OnNewVersionAvailable(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>NewVersionAvailable</c> event.
		/// </summary>
		protected virtual void OnNewVersionAvailable(EventArgs e)
		{
			// raise...
			if(NewVersionAvailable != null)
				NewVersionAvailable(this, e);
		}

		/// <summary>
		/// Gets or sets the enabled
		/// </summary>
		internal bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _enabled)
				{
					// set the value...
					_enabled = value;
					
					// set...
					if(_enabled)
					{
						this.EnsureThreadInitialized();
						this.SetNextCheckAt();
					}
					else
						_nextCheckAt = DateTime.MinValue;
				}
			}
		}

		private void SetNextCheckAt()
		{
			_nextCheckAt = DateTime.UtcNow.AddMinutes(15);
		}

		protected override void OnProcessingError(System.Threading.ThreadExceptionEventArgs e)
		{
			// stop...
			this.Enabled = false;

			// raise...
			base.OnProcessingError (e);
		}
	}
}
