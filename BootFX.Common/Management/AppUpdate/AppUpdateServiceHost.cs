// BootFX - Application framework for .NET applications
// 
// File: AppUpdateServiceHost.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using BootFX.Common.Services;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines an instance of <c>AppUpdateEngine</c>.
	/// </summary>
	public class AppUpdateServiceHost : ServiceHost
	{
		internal static string BfxCommandLineKey = "bfxupdate";

		/// <summary>
		/// Raised when a new version is available.
		/// </summary>
		public event EventHandler NewVersionAvailable;
		
		/// <summary>
		/// Raised when the app has finished.
		/// </summary>
		public event EventHandler RunFinished;
		
		/// <summary>
		/// Raised when the application starts.
		/// </summary>
		public event EventHandler RunStarted;
		
		/// <summary>
		/// Private field to support <c>Process</c> property.
		/// </summary>
		private Process _process;
		
		/// <summary>
		/// Private field to support <see cref="Catalog"/> property.
		/// </summary>
		private DeployedApp _catalog;
		
		/// <summary>
		/// Raised when the application has been downloaded.
		/// </summary>
		public event EventHandler DownloadFinished;
		
		/// <summary>
		/// Private field to support <c>Operation</c> property.
		/// </summary>
		private IOperationItem _operation = new OperationItem();
		
		/// <summary>
		/// Raised when there is a problem downloading the file.
		/// </summary>
		public event ThreadExceptionEventHandler DownloadError;
		
		/// <summary>
		/// Raised when there is a problem downloading the file.
		/// </summary>
		public event ThreadExceptionEventHandler VersionCheckError;
		
		/// <summary>
		/// Raised when there is a problem downloading the file.
		/// </summary>
		public event ThreadExceptionEventHandler RunError;
		
		/// <summary>
		/// Private field to support <c>CurrentVersion</c> property.
		/// </summary>
		private Version _currentVersion;
		
		/// <summary>
		/// Private field to support <see cref="CatalogUrl"/> property.
		/// </summary>
		private string _catalogUrl;

		/// <summary>
		/// Private field to support <see cref="DeployPath"/> property.
		/// </summary>
		private string _deployRoot;
		
		public AppUpdateServiceHost(string catalogUrl)
		{
			if(catalogUrl == null)
				throw new ArgumentNullException("catalogUrl");
			if(catalogUrl.Length == 0)
				throw new ArgumentOutOfRangeException("'catalogUrl' is zero-length.");
		
			// create engines...
			AppUpdateDownloadEngine downloader = new AppUpdateDownloadEngine();
			downloader.ProcessingError += new ThreadExceptionEventHandler(downloader_ProcessingError);
			downloader.DownloadFinished += new EventHandler(downloader_DownloadFinished);
			this.Engines.Add(downloader);

			// again...
			NewVersionCheckEngine checker = new NewVersionCheckEngine();
			checker.ProcessingError += new ThreadExceptionEventHandler(checker_ProcessingError);
			checker.NewVersionAvailable += new EventHandler(checker_NewVersionAvailable);
			this.Engines.Add(checker);

			// set...
			_catalogUrl = catalogUrl;

			// what...
			_deployRoot = Path.GetDirectoryName(new Uri(Assembly.GetEntryAssembly().CodeBase).LocalPath);

			// update...
			this.RefreshCurrentVersion();
		}

		private void RefreshCurrentVersion()
		{
			// find the latest installed version...
			Regex regex = new Regex(@"^v(?<version>[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			_currentVersion = new Version(0,0,0,0);
			foreach(string path in Directory.GetDirectories(this.DeployRoot))
			{
				string name = Path.GetFileName(path);
				Match match = regex.Match(name);
				if(match.Success)
				{
					Version check = new Version(match.Groups["version"].Value);
					if(check > _currentVersion)
						_currentVersion = check;
				}
			}
		}

		/// <summary>
		/// Gets the application status.
		/// </summary>
		/// <returns></returns>
		public AppUpdateStatus GetStatus(bool reset)
		{
			if(reset)
			{
				this.RefreshCurrentVersion();
				_catalog = null;
			}

			// get the latest installed version...
			if(this.CurrentVersion == new Version(0,0,0,0))
				return AppUpdateStatus.MustUpdate;
			else
			{
				try
				{
					// get the status...
					DeployedApp catalog = this.Catalog;
					if(catalog == null)
						throw new InvalidOperationException("catalog is null.");

					// are we greater than or equal to the 
					if(this.CurrentVersion >= catalog.LatestVersion)
						return AppUpdateStatus.UpToDate;
					else
					{
						if(this.CurrentVersion >= catalog.LastSupportedVersion)
							return AppUpdateStatus.CouldUpdate;
						else
							return AppUpdateStatus.MustUpdate;
					}
				}
				catch(Exception ex)
				{
					if(this.Log.IsErrorEnabled)
						this.Log.Error("Could not determine the latest available version number.", ex);
					return AppUpdateStatus.CouldNotDetermine;
				}
			}
		}

		/// <summary>
		/// Gets the catalog.
		/// </summary>
		internal DeployedApp Catalog
		{
			get
			{
				if(_catalog == null)
				{
					XmlDocument doc = this.GetCatalogXml();
					if(doc == null)
						throw new InvalidOperationException("doc is null.");

					// get...
					_catalog = DeployedApp.FromXml(doc);
					if(_catalog == null)
						throw new InvalidOperationException("_catalog is null.");
				}
				return _catalog;
			}
		}

		/// <summary>
		/// Downloads the catalog.
		/// </summary>
		/// <returns></returns>
		private XmlDocument GetCatalogXml()
		{
			if(CatalogUrl == null)
				throw new InvalidOperationException("'CatalogUrl' is null.");
			if(CatalogUrl.Length == 0)
				throw new InvalidOperationException("'CatalogUrl' is zero-length.");

			if(Operation == null)
				throw new InvalidOperationException("Operation is null.");
			this.Operation.Status = "Downloading application catalog...";

			// get...
			XmlDocument doc = HttpHelper.DownloadToXmlDocument(this.CatalogUrl, this.DownloadSettings);
			if(doc == null)
				throw new InvalidOperationException("doc is null.");
			return doc;
		}

		/// <summary>
		/// Gets the deploypath.
		/// </summary>
		internal string DeployRoot
		{
			get
			{
				return _deployRoot;
			}
		}
		
		/// <summary>
		/// Gets the catalogurl.
		/// </summary>
		internal string CatalogUrl
		{
			get
			{
				return _catalogUrl;
			}
		}

		internal HttpDownloadSettings DownloadSettings
		{
			get
			{
				return HttpDownloadSettings.Default;
			}
		}

		/// <summary>
		/// Gets the currentversion.
		/// </summary>
		private Version CurrentVersion
		{
			get
			{
				// returns the value...
				return _currentVersion;
			}
		}

		/// <summary>
		/// Downloads the latest version of the application.
		/// </summary>
		public void DownloadLatestVersion()
		{
			// run...
			if(Operation == null)
				throw new InvalidOperationException("Operation is null.");
			this.Operation.Status = "Downloading latest version of the application...";

			// set...
			if(DownloadEngine == null)
				throw new InvalidOperationException("DownloadEngine is null.");
			this.DownloadEngine.EnsureThreadInitialized();
		}

		/// <summary>
		/// Gets the downloadworker.
		/// </summary>
		private NewVersionCheckEngine CheckEngine
		{
			get
			{
				return (NewVersionCheckEngine)this.Engines.GetEngine(typeof(NewVersionCheckEngine), true);
			}
		}

		/// <summary>
		/// Gets the downloadworker.
		/// </summary>
		private AppUpdateDownloadEngine DownloadEngine
		{
			get
			{
				return (AppUpdateDownloadEngine)this.Engines.GetEngine(typeof(AppUpdateDownloadEngine), true);
			}
		}

		private void downloader_VersionCheckError(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			this.OnDownloadError(e);
		}

		/// <summary>
		/// Raises the <c>DownloadError</c> event.
		/// </summary>
		protected virtual void OnDownloadError(ThreadExceptionEventArgs e)
		{
			// raise...
			if(DownloadError != null)
				DownloadError(this, e);
		}

		/// <summary>
		/// Raises the <c>VersionCheckError</c> event.
		/// </summary>
		protected virtual void OnVersionCheckError(ThreadExceptionEventArgs e)
		{
			// raise...
			if(VersionCheckError != null)
				VersionCheckError(this, e);
		}

		/// <summary>
		/// Raises the <c>RunError</c> event.
		/// </summary>
		protected virtual void OnRunError(ThreadExceptionEventArgs e)
		{
			// raise...
			if(RunError != null)
				RunError(this, e);
		}

		/// <summary>
		/// Gets or sets the operation
		/// </summary>
		public IOperationItem Operation
		{
			get
			{
				return _operation;
			}
			set
			{
				if(value == null)
					value = new OperationItem();

				// check to see if the value has changed...
				if(value != _operation)
				{
					// set the value...
					_operation = value;
				}
			}
		}

		internal string RootUrl
		{
			get
			{
				string url = this.CatalogUrl.Replace("\\", "/");
				while(url.EndsWith("/"))
					url = url.Substring(0, url.Length - 1);

				// index...
				int index = url.LastIndexOf("/");
				if(index == -1)
					throw new InvalidOperationException(string.Format("The URL '{0}' is invalid.", url));

				// set...
				url = url.Substring(0, index + 1);
				return url;
			}
		}

		private void downloader_DownloadFinished(object sender, EventArgs e)
		{
			// refresh...
			this.RefreshCurrentVersion();

			// raise...
			this.OnDownloadFinished();
		}

		/// <summary>
		/// Raises the <c>DownloadFinished</c> event.
		/// </summary>
		private void OnDownloadFinished()
		{
			OnDownloadFinished(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DownloadFinished</c> event.
		/// </summary>
		protected virtual void OnDownloadFinished(EventArgs e)
		{
			// raise...
			if(DownloadFinished != null)
				DownloadFinished(this, e);
		}

		/// <summary>
		/// Runs the application.
		/// </summary>
		public void Run()
		{
			if(CheckEngine == null)
				throw new InvalidOperationException("CheckEngine is null.");
			this.CheckEngine.Enabled = true;

			// get the catalog...
			try
			{
				if(this.Process != null)
					throw new InvalidOperationException("The application is already running.");

				// ok...
				if(Operation == null)
					throw new InvalidOperationException("Operation is null.");
				this.Operation.Status = "Starting application...";

				// get...
				string path = this.ExePath;
				if(path == null)
					throw new InvalidOperationException("'path' is null.");
				if(path.Length == 0)
					throw new InvalidOperationException("'path' is zero-length.");

				// mbr - 09-05-2006 - added bfx passthrough...			
				IDictionary args = CollectionsUtil.CreateCaseInsensitiveHashtable();
				args[BfxCommandLineKey] = new Uri(Assembly.GetEntryAssembly().CodeBase).LocalPath;

				// build...
				StringBuilder builder = new StringBuilder();
				foreach(DictionaryEntry arg in args)
				{
					builder.Append("\"-");
					builder.Append(arg.Key);
					builder.Append(":");
					builder.Append(arg.Value);
					builder.Append("\"");
				}

				// 2007-12-20 - lpm - ensure the new process starts in the correct directory
				ProcessStartInfo info = new ProcessStartInfo(path, builder.ToString());
				info.WorkingDirectory = Path.GetDirectoryName(path);

				// run...
				try
				{
					_process = Process.Start(info);
				}
				catch(Exception ex)
				{
					throw new InvalidOperationException(string.Format("Failed to start application.\r\nPath: {0}\r\nArgs: {1}", path, builder), ex);
				}

				// ok!
				this.Operation.Status = "Application is running.";
				this.OnRunStarted();
			}
			catch(Exception ex)
			{
				this.OnRunError(new ThreadExceptionEventArgs(ex));
			}
		}

		private void downloader_ProcessingError(object sender, ThreadExceptionEventArgs e)
		{
			this.OnDownloadError(e);
		}

		private void checker_ProcessingError(object sender, ThreadExceptionEventArgs e)
		{
			// raise...
			this.OnVersionCheckError(e);
		}

		private string CurrentVersionFolderPath
		{
			get
			{
				return Path.Combine(this.DeployRoot, "v" + this.CurrentVersion.ToString());
			}
		}

		private string ExePath
		{
			get
			{
				if(Catalog == null)
					throw new InvalidOperationException("Catalog is null.");

				// walk...
				foreach(DeployedAppFile file in this.Catalog.Files)
				{
					if(file.IsExe)
					{
						// return...
						string path = this.CurrentVersionFolderPath;
						if(path == null)
							throw new InvalidOperationException("'path' is null.");
						if(path.Length == 0)
							throw new InvalidOperationException("'path' is zero-length.");

						// add...
						path = Path.Combine(path, file.Name);
						return path;
					}
				}

				// throw...
				throw new InvalidOperationException("The downloaded application did not have an associated executable.");
			}
		}

		/// <summary>
		/// Gets the process.
		/// </summary>
		internal Process Process
		{
			get
			{
				// returns the value...
				return _process;
			}
		}

		/// <summary>
		/// Raises the <c>RunStarted</c> event.
		/// </summary>
		private void OnRunStarted()
		{
			OnRunStarted(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>RunStarted</c> event.
		/// </summary>
		protected virtual void OnRunStarted(EventArgs e)
		{
			// raise...
			if(RunStarted != null)
				RunStarted(this, e);
		}

		/// <summary>
		/// Raises the <c>RunFinished</c> event.
		/// </summary>
		internal void OnRunFinished()
		{
			// set...
			if(Process == null)
				throw new InvalidOperationException("Process is null.");
			Environment.ExitCode = this.Process.ExitCode;
			_process = null;

			// raise...
			OnRunFinished(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>RunFinished</c> event.
		/// </summary>
		protected virtual void OnRunFinished(EventArgs e)
		{
			// raise...
			if(RunFinished != null)
				RunFinished(this, e);
		}

		private void checker_NewVersionAvailable(object sender, EventArgs e)
		{
			this.OnNewVersionAvailable();
		}

		/// <summary>
		/// Raises the <c>NewVersionAvailable</c> event.
		/// </summary>
		private void OnNewVersionAvailable()
		{
			OnNewVersionAvailable(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>NewVersionAvailable</c> event.
		/// </summary>
		protected virtual void OnNewVersionAvailable(EventArgs e)
		{
			// raise...
			if(NewVersionAvailable != null)
				NewVersionAvailable(this, e);
		}
	}
}
