// BootFX - Application framework for .NET applications
// 
// File: DeployedAppFile.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Xml;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines an instance of <c>DeployedAppFile</c>.
	/// </summary>
	public class DeployedAppFile
	{
		/// <summary>
		/// Private field to support <see cref="Url"/> property.
		/// </summary>
		private string _url = null;
		
		/// <summary>
		/// Private field to support <see cref="Hash"/> property.
		/// </summary>
		private string _hash = null;
		
		/// <summary>
		/// Private field to support <see cref="Guid"/> property.
		/// </summary>
		private Guid _guid = Guid.NewGuid();
		
		/// <summary>
		/// Private field to support <see cref="DeployName"/> property.
		/// </summary>
		private string _deployName;
		private string _name;
		
		/// <summary>
		/// Private field to support <c>IsExe</c> property.
		/// </summary>
		private bool _isExe;
		
		/// <summary>
		/// Private field to support <see cref="Action"/> property.
		/// </summary>
		private DeployedAppFileAction _action = DeployedAppFileAction.Copy;

		private DeployedAppFile()
		{
		}

		public DeployedAppFile(string path, string deployName)
		{
			if(path == null)
				throw new ArgumentNullException("path");
			if(path.Length == 0)
				throw new ArgumentOutOfRangeException("'path' is zero-length.");
			if(deployName == null)
				throw new ArgumentNullException("deployName");
			if(deployName.Length == 0)
				throw new ArgumentOutOfRangeException("'deployName' is zero-length.");
			
			// set...
			_name = Path.GetFileName(path);
			_deployName = deployName;
			_hash = HashHelper.GetMd5HashAsBase64(path);
		}

		internal XmlElement GetXmlElement(XmlDocument doc)
		{
			if(doc == null)
				throw new ArgumentNullException("doc");
			
			// create...
			XmlElement element = doc.CreateElement("File");
			XmlHelper.AddElement(element, "Guid", this.Guid.ToString());
			XmlHelper.AddElement(element, "Name", this.Name);
			XmlHelper.AddElement(element, "Hash", this.Hash);
			XmlHelper.AddElement(element, "Url", "~/" + this.DeployName);
			XmlHelper.AddElement(element, "Action", this.Action.ToString());
			
			// return...
			return element;
		}

		internal static DeployedAppFile FromXml(XmlElement element)
		{
			if(element == null)
				throw new ArgumentNullException("element");

			// get...
			DeployedAppFile file = new DeployedAppFile();
			file._guid = XmlHelper.GetElementGuid(element, "Guid", OnNotFound.ThrowException);
			file._name = XmlHelper.GetElementString(element, "Name", OnNotFound.ThrowException);
			file._hash = XmlHelper.GetElementString(element, "Hash", OnNotFound.ThrowException);
			file._url = XmlHelper.GetElementString(element, "Url", OnNotFound.ThrowException);
			file._action = (DeployedAppFileAction)XmlHelper.GetElementEnumerationValue(element, "Action", typeof(DeployedAppFileAction), OnNotFound.ThrowException);

			// return...
			return file;
		}

		/// <summary>
		/// Gets the filepath.
		/// </summary>
		internal string Name
		{
			get
			{
				return _name;
			}
		}

		/// <summary>
		/// Gets the action.
		/// </summary>
		internal DeployedAppFileAction Action
		{
			get
			{
				return _action;
			}
		}

		/// <summary>
		/// Gets or sets the isexe
		/// </summary>
		public bool IsExe
		{
			get
			{
				return _isExe;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _isExe)
				{
					// set the value...
					_isExe = value;
				}
			}
		}

		/// <summary>
		/// Gets the deployname.
		/// </summary>
		internal string DeployName
		{
			get
			{
				return _deployName;
			}
		}

		/// <summary>
		/// Gets the guid.
		/// </summary>
		internal Guid Guid
		{
			get
			{
				return _guid;
			}
		}

		/// <summary>
		/// Gets the hash.
		/// </summary>
		internal string Hash
		{
			get
			{
				return _hash;
			}
		}

		/// <summary>
		/// Gets the url.
		/// </summary>
		internal string Url
		{
			get
			{
				return _url;
			}
		}
	}
}
