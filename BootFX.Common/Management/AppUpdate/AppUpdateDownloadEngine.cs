// BootFX - Application framework for .NET applications
// 
// File: AppUpdateDownloadEngine.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Services;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines an instance of <c>AppDownloadWorker</c>.
	/// </summary>
	internal class AppUpdateDownloadEngine : ThreadedServiceEngine
	{
		/// <summary>
		/// Raised when the application has been downloaded.
		/// </summary>
		public event EventHandler DownloadFinished;
		
		internal AppUpdateDownloadEngine()
		{
		}

		/// <summary>
		/// Gets the engine.
		/// </summary>
		private new AppUpdateServiceHost Host
		{
			get
			{
				return (AppUpdateServiceHost)base.Host;
			}
		}

		private IOperationItem Operation
		{
			get
			{
				if(Host == null)
					throw new InvalidOperationException("Host is null.");
				return this.Host.Operation;
			}
		}

		protected override bool DoWork()
		{
			if(Host == null)
				throw new InvalidOperationException("Host is null.");

			if(Operation == null)
				throw new InvalidOperationException("Operation is null.");

			// get...
			DeployedApp catalog = this.Host.Catalog;
			if(catalog == null)
				throw new InvalidOperationException("catalog is null.");

			// check...
			if(catalog.Files.Count == 0)
				throw new InvalidOperationException("The application does not have any files registered.");

			// create a temp folder...
			string tempFolder = Path.GetTempPath() + "\\" + Guid.NewGuid().ToString();
			Directory.CreateDirectory(tempFolder);
			try
			{
				// download...
				this.Operation.ProgressMaximum = catalog.Files.Count;
				foreach(DeployedAppFile file in catalog.Files)
				{
					// set...
					this.Operation.Status = string.Format("Downloading '{0}'...", file.Name);

					// get...
					string url = this.ResolveUrl(file.Url);
					if(url == null)
						throw new InvalidOperationException("'url' is null.");
					if(url.Length == 0)
						throw new InvalidOperationException("'url' is zero-length.");

					// get...
					string targetPath = Path.Combine(tempFolder, file.Name);
					HttpHelper.Download(url, targetPath, file.Hash, this.Host.DownloadSettings);

					// next...
					this.Operation.IncrementProgress();
				}

				// set...
				this.Operation.Status = "Creating application folder...";

				// move...
				string folder = string.Format(@"{0}\v{1}", this.Host.DeployRoot, catalog.LatestVersion);
				if(Directory.Exists(folder))
					throw new InvalidOperationException(string.Format("The folder '{0}' already exists.", folder));
				Directory.Move(tempFolder, folder);

				// ok!
				this.Operation.Status = "Download complete.";
				this.OnDownloadFinished();

				// ok...
				return false;
			}
			finally
			{
				if(Directory.Exists(tempFolder))
				{
					try
					{
						Directory.Delete(tempFolder, true);
					}
					catch
					{
					}
				}
			}
		}

		private string ResolveUrl(string url)
		{
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			
			// if...
			if(url.StartsWith("~"))
			{
				url = url.Substring(1);

				// get...
				string newUrl = this.Host.RootUrl;
				if(newUrl == null)
					throw new InvalidOperationException("'newUrl' is null.");
				if(newUrl.Length == 0)
					throw new InvalidOperationException("'newUrl' is zero-length.");

				// check...
				if(!(newUrl.EndsWith("/")) && !(url.StartsWith("/")))
					newUrl += "/";
				newUrl += url;

				// return...
				return newUrl;
			}
			else
			{
				// TODO: This method has not been implemented.  Check whether it is a valid not implementated, or a code path that was not completed.
				throw new NotImplementedException(string.Format("Not implemented for '{0}'.", this.GetType()));
			}
		}

		/// <summary>
		/// Raises the <c>DownloadFinished</c> event.
		/// </summary>
		private void OnDownloadFinished()
		{
			OnDownloadFinished(EventArgs.Empty);
		}
		
		/// <summary>
		/// Raises the <c>DownloadFinished</c> event.
		/// </summary>
		protected virtual void OnDownloadFinished(EventArgs e)
		{
			// raise...
			if(DownloadFinished != null)
				DownloadFinished(this, e);
		}
	}
}
