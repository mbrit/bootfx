// BootFX - Application framework for .NET applications
// 
// File: DeployedApp.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using BootFX.Common.Xml;

namespace BootFX.Common.Management
{
	/// <summary>
	/// Defines an instance of <c>DeployedApp</c>.
	/// </summary>
	public class DeployedApp
	{
		/// <summary>
		/// Private field to support <c>LatestVersion</c> property.
		/// </summary>
		private Version _latestVersion;

		/// <summary>
		/// Private field to support <c>LastSupportedVersion</c> property.
		/// </summary>
		private Version _lastSupportedVersion;
		
		/// <summary>
		/// Private field to support <c>Files</c> property.
		/// </summary>
		private DeployedAppFileCollection _files = new DeployedAppFileCollection();
		
		public DeployedApp()
		{
		}

		/// <summary>
		/// Gets a collection of DeployedAppFile objects.
		/// </summary>
		public DeployedAppFileCollection Files
		{
			get
			{
				return _files;
			}
		}

		/// <summary>
		/// Gets or sets the lastsupportedversion
		/// </summary>
		public Version LastSupportedVersion
		{
			get
			{
				return _lastSupportedVersion;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _lastSupportedVersion)
				{
					// set the value...
					_lastSupportedVersion = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the latestversion
		/// </summary>
		public Version LatestVersion
		{
			get
			{
				return _latestVersion;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _latestVersion)
				{
					// set the value...
					_latestVersion = value;
				}
			}
		}

		/// <summary>
		/// Gets the manifest as an XML document.
		/// </summary>
		/// <returns></returns>
		public XmlDocument ToXmlDocument()
		{
			// create...
			XmlDocument doc = new XmlDocument();

			// root...
			XmlElement rootElement = doc.CreateElement("Deploy");
			doc.AppendChild(rootElement);

			// version...
			XmlElement versionElement = doc.CreateElement("Version");
			rootElement.AppendChild(versionElement);
			XmlHelper.AddElement(versionElement, "Latest", this.LatestVersion.ToString());
			XmlHelper.AddElement(versionElement, "LastSupported", this.LastSupportedVersion.ToString());

			// get...
			bool foundExe = false;
			foreach(DeployedAppFile file in this.Files)
			{
				if(file.IsExe)
				{
					// add...
					XmlHelper.AddElement(versionElement, "Run", file.Guid.ToString());

					// stop...
					foundExe = true;
					break;
				}
			}

			// check...
			if(!(foundExe))
				throw new InvalidOperationException("No executable is registered for the deployment package.");

			// files...
			XmlElement filesElement = doc.CreateElement("Files");
			rootElement.AppendChild(filesElement);
			foreach(DeployedAppFile file in this.Files)
				filesElement.AppendChild(file.GetXmlElement(doc));

			// return...
			return doc;
		}

		public string ToXml()
		{
			XmlDocument doc = this.ToXmlDocument();
			if(doc == null)
				throw new InvalidOperationException("doc is null.");
			return doc.OuterXml;
		}

		public void Save(string path)
		{
			if(path == null)
				throw new ArgumentNullException("path");
			if(path.Length == 0)
				throw new ArgumentOutOfRangeException("'path' is zero-length.");
			
			// save...
			XmlDocument doc = this.ToXmlDocument();
			if(doc == null)
				throw new InvalidOperationException("doc is null.");
			doc.Save(path);
		}

		public static DeployedApp FromXml(XmlDocument doc)
		{
			if(doc == null)
				throw new ArgumentNullException("doc");
			
			// create...
			DeployedApp app = new DeployedApp();

			// find...
			XmlElement versionElement = (XmlElement)doc.SelectSingleNode("Deploy/Version");
			if(versionElement == null)
				throw new InvalidOperationException("versionElement is null.");
			app.LatestVersion = ParseVersion(XmlHelper.GetElementString(versionElement, "Latest", OnNotFound.ThrowException));
			app.LastSupportedVersion = ParseVersion(XmlHelper.GetElementString(versionElement, "LastSupported", OnNotFound.ThrowException));

			// exe...
			Guid exeGuid = XmlHelper.GetElementGuid(versionElement, "Run", OnNotFound.ThrowException);

			// files...
			foreach(XmlElement fileElement in doc.SelectNodes("Deploy/Files/File"))
			{
				// get...
				DeployedAppFile file = DeployedAppFile.FromXml(fileElement);
				if(file == null)
					throw new InvalidOperationException("file is null.");
				app.Files.Add(file);

				// set...
				if(file.Guid == exeGuid)
					file.IsExe = true;
			}

			// return...
			return app;
		}

		private static Version ParseVersion(string buf)
		{
			if(buf == null)
				throw new ArgumentNullException("buf");
			if(buf.Length == 0)
				throw new ArgumentOutOfRangeException("'buf' is zero-length.");
			
			// return...
			try
			{
				return new Version(buf);
			}
			catch(Exception ex)
			{
				throw new InvalidOperationException(string.Format("Failed to convert '{0}' to a version.", buf), ex);
			}
		}
	}
}
