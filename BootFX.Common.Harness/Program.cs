// BootFX - Application framework for .NET applications
// 
// File: Program.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using BootFX.Common.Email;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using SNM = System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;

namespace BootFX.Common.Harness
{
    class Program
    {
        private const string SmtpHostname = "email-smtp.eu-west-1.amazonaws.com";
        private const string SmtpUsername = "AKIAJF4N5UXUOQOCBUXA";
        private const string SmtpPassword = "AvhT+QPVGgqNkcgMmEJ1Nes6DJiQlQfD49Oqk+ccckg3";
        private const int SmtpPort = 587;

        static void Main(string[] args)
        {
            try
            {
                var asm = typeof(Program).Assembly;
                Runtime.Start("BootFX", "Harness", "Console", asm.GetName().Version);

                TestBfx(asm);
                //TestVanilla(asm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                if(Debugger.IsAttached)
                    Console.ReadLine();
            }
        }

        private static void TestVanilla(Assembly asm)
        {
            var filePath = ResourceHelper.CopyResourceToTempFile(asm, "BootFX.Common.Harness.Resources.MailFooterChas.png");
            string body = "blah blah blah... body goes here with the image tag: <div style=\"background-color:pink;padding:10px;border:1px solid black\"><img src=\"cid:companyLogo\" /></div><h3>and then some following stuff</h3>";

            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.From = new SNM.MailAddress("matt@amxmobile.com");
            message.To.Add(new SNM.MailAddress("matt@amxmobile.com"));
            message.Subject = "foo " + DateTime.UtcNow;

            var ct = new ContentType("application/png")
            {
                Name = Path.GetFileName(filePath)
            };
            var linked = new SNM.LinkedResource(filePath, ct)
            {
                ContentId = "companyLogo"
            };

            var av = SNM.AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            av.LinkedResources.Add(linked);
            message.AlternateViews.Add(av);

            var client = new SNM.SmtpClient();
            client.Host = SmtpHostname;
            client.Credentials = new NetworkCredential(SmtpUsername, SmtpPassword);
            client.Port = SmtpPort;
            client.EnableSsl = true;
            client.Send(message);
        }

        private static void TestBfx(System.Reflection.Assembly asm)
        {
            var message = new MailMessage()
            {
                ToAsString = "matt@amxmobile.com",
                FromAsString = "matt@amxmobile.com",
                Subject = "test bfx -- " + DateTime.UtcNow,
                MailFormat = MailFormat.Html
            };

            var embed = MailEmbeddedImage.GetImageForResource(asm, "BootFX.Common.Harness.Resources.MailFooterChas.png", "application/png");
            message.EmbeddedImages.Add(embed);

            StringBuilder builder = new StringBuilder();
            builder.Append("<html><head></head><body>");
            builder.Append("<h1>Hello, world</h1>");
            builder.Append("<div>quite</div>");
            builder.Append("<div>");
            embed.WriteImageReference(builder);
            builder.Append("</div>");
            builder.Append("</body></html>");

            message.Body = builder.ToString();

            var xml = message.ToXmlDocument();
            var toSend = MailMessage.FromXmlDocument(xml);

            var settings = new SmtpConnectionSettings(SmtpHostname, SmtpUsername, SmtpPassword, 587, true);
            var smtp = new NetSmtpMailProvider(settings);
            smtp.Send(toSend);

            Console.WriteLine("Done.");
        }
    }
}
