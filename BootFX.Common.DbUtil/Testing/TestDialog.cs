// BootFX - Application framework for .NET applications
// 
// File: TestDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using BootFX.Common.UI.Desktop;
using BootFX.Common.Data;
using BootFX.Common.Entities;

namespace BootFX.Common.DbUtil
{
	/// <summary>
	/// Summary description for TestDialog.
	/// </summary>
	public class TestDialog : System.Windows.Forms.Form
	{
		private BootFX.Common.UI.Desktop.EntityListView listEntities;
		private BootFX.Common.UI.Desktop.EntityListView listData;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TestDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listData = new BootFX.Common.UI.Desktop.EntityListView();
			this.listEntities = new BootFX.Common.UI.Desktop.EntityListView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// listData
			// 
			this.listData.DataSource = null;
			this.listData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listData.FullRowSelect = true;
			this.listData.HideSelection = false;
			this.listData.Location = new System.Drawing.Point(0, 4);
			this.listData.Name = "listData";
			this.listData.Size = new System.Drawing.Size(616, 622);
			this.listData.TabIndex = 0;
			this.listData.View = System.Windows.Forms.View.Details;
			// 
			// listEntities
			// 
			this.listEntities.DataSource = null;
			this.listEntities.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listEntities.HideSelection = false;
			this.listEntities.Location = new System.Drawing.Point(4, 4);
			this.listEntities.Name = "listEntities";
			this.listEntities.Size = new System.Drawing.Size(388, 622);
			this.listEntities.TabIndex = 3;
			this.listEntities.Text = "button1";
			this.listEntities.View = System.Windows.Forms.View.Details;
			this.listEntities.SelectedIndexChanged += new System.EventHandler(this.listEntities_SelectedIndexChanged);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.listEntities);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.DockPadding.Bottom = 4;
			this.panel2.DockPadding.Left = 4;
			this.panel2.DockPadding.Top = 4;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(392, 630);
			this.panel2.TabIndex = 5;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(392, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(8, 630);
			this.splitter1.TabIndex = 6;
			this.splitter1.TabStop = false;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.listData);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.DockPadding.Bottom = 4;
			this.panel3.DockPadding.Right = 4;
			this.panel3.DockPadding.Top = 4;
			this.panel3.Location = new System.Drawing.Point(400, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(620, 630);
			this.panel3.TabIndex = 7;
			// 
			// TestDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1020, 630);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel2);
			this.Name = "TestDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Test Dialog";
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Gets or sets the entitytype
		/// </summary>
		private EntityType SelectedEntityType
		{
			get
			{
				return this.listEntities.SelectedEntity as EntityType;
			}
		}

		/// <summary>
		/// Updates the view.
		/// </summary>
		private void RefreshView()
		{
			if(this.SelectedEntityType != null)
			{
				try
				{
					SqlFilter filter = new SqlFilter(this.SelectedEntityType);
					filter.Fields.AddRange(this.SelectedEntityType.Fields.ToArray());
					IList list = filter.ExecuteEntityCollection();
					if(list.Count > 1000)
					{
						// clip it...
						IList newList = this.SelectedEntityType.CreateCollectionInstance();
						for(int index = 0; index < 1000; index++)
							newList.Add(list[index]);

						// flip...
						list = newList;
					}

					if(list is EntityCollection)
						list = ((EntityCollection)list).GetDefaultFieldView();

					// show...
					this.listData.DataSource = list;
				}
				catch(Exception ex)
				{
					Alert.ShowWarning(this, string.Format("Failed to get entities for '{0}'.", this.SelectedEntityType), ex);
				}
			}
			else
				this.listData.DataSource = null;
		}

		/// <summary>
		/// Sets the entity types.
		/// </summary>
		/// <param name="entityTypes"></param>
		internal void SetEntityTypes(EntityType[] entityTypes)
		{
			if(entityTypes == null)
				throw new ArgumentNullException("entityTypes");
			
			// show...
			this.listEntities.DataSource = entityTypes;
		}

		private void listEntities_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.RefreshView();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if(this.DesignMode)
				return;

			// setup...
			this.Text = string.Format("Two47 DBUtil Entity Test [{0}]", AppDomain.CurrentDomain.FriendlyName);
		}
	}
}
