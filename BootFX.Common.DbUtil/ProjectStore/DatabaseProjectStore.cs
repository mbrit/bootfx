// BootFX - Application framework for .NET applications
// 
// File: DatabaseProjectStore.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using BootFX.Common.Data;

namespace BootFX.Common.DbUtil
{
	/// <summary>
	/// Defines an instance of <c>DatabaseProjectStore</c>.
	/// </summary>
	internal class DatabaseProjectStore : ProjectStore
	{		
		internal DatabaseProjectStore()
		{
		}

		internal override string BrowseForProject(System.Windows.Forms.Control owner, bool forSave)
		{
			this.ConfigureConnection();
			return "Bs.SpServices.xml";
		}

		/// <summary>
		/// Configures the database connection.
		/// </summary>
		private void ConfigureConnection()
		{
			Database.Databases.Add(new NamedDatabase("BfxDBUtil", new ConnectionSettings("BfxDBUtil", typeof(SqlServerConnection), 
				"integrated security=sspi;data source=(local);initial catalog=dbutil")));
		}

		internal override void Save(string filePath, string xml)
		{
			if(filePath == null)
				throw new ArgumentNullException("filePath");
			if(filePath.Length == 0)
				throw new ArgumentOutOfRangeException("'filePath' is zero-length.");
			if(xml == null)
				throw new ArgumentNullException("xml");
			if(xml.Length == 0)
				throw new ArgumentOutOfRangeException("'xml' is zero-length.");

			// configure...
			this.ConfigureConnection();

			// load...
			ProjectItem item = ProjectItem.GetByName(filePath);
			if(item == null)
			{
				item = new ProjectItem();
				item.Name = filePath;
				item.Version = 1;
			}
			else
				item.Version = item.Version + 1;

			// set...
			item.DateTime = DateTime.UtcNow;
			item.MachineName = Environment.MachineName;
			item.TrimToFit("machinename");
			item.Username = Thread.CurrentPrincipal.Identity.Name;
			item.TrimToFit("username");
			item.Data = xml;

			// save...
			item.SaveChanges();
		}

		internal override bool IsReadOnly(string path)
		{
			return false;
		}

		internal override System.Xml.XmlDocument Load(string path)
		{
			if(path == null)
				throw new ArgumentNullException("path");
			if(path.Length == 0)
				throw new ArgumentOutOfRangeException("'path' is zero-length.");

			// setup...
			this.ConfigureConnection();
			
			// get...
			ProjectItem item = ProjectItem.GetByName(path);
			if(item != null)
			{
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(item.Data);

				// return...
				return doc;
			}
			else
				throw new InvalidOperationException(string.Format("A project with name '{0}' was not found.", path));
		}
	}
}
