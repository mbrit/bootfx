// BootFX - Application framework for .NET applications
// 
// File: ProjectItemBase.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.DbUtil
{
    using System;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Data;
    using System.Collections;
    using System.Collections.Specialized;
    using BootFX.Common;
    using BootFX.Common.Data;
    using BootFX.Common.Entities;
    using BootFX.Common.Entities.Attributes;
    using BootFX.Common.BusinessServices;
    
    
    /// <summary>
    /// Defines the base entity type for 'DbUtilProjects'.
    /// </summary>
    [Serializable()]
    [Index("DbUtilProjectsName", "DbUtilProjectsName", true, "Name")]
    public abstract class ProjectItemBase : BootFX.Common.Entities.Entity
    {
        
        /// <summary>
        /// Constructor.
        /// </summary>
        protected ProjectItemBase()
        {
        }
        
        /// <summary>
        /// Deserialization constructor.
        /// </summary>
        protected ProjectItemBase(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : 
                base(info, context)
        {
        }
        
        /// <summary>
        /// Gets or sets the value for 'ProjectId'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'ProjectId' column.
        /// </remarks>
        [EntityField("ProjectId", System.Data.DbType.Int32, ((BootFX.Common.Entities.EntityFieldFlags.Key | BootFX.Common.Entities.EntityFieldFlags.Common) 
                    | BootFX.Common.Entities.EntityFieldFlags.AutoIncrement))]
        public int ProjectId
        {
            get
            {
                return ((int)(this["ProjectId"]));
            }
            set
            {
                this["ProjectId"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'Name'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'Name' column.
        /// </remarks>
        [EntityField("Name", System.Data.DbType.String, BootFX.Common.Entities.EntityFieldFlags.Common, 128)]
        public string Name
        {
            get
            {
                return ((string)(this["Name"]));
            }
            set
            {
                this["Name"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'Data'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'Data' column.
        /// </remarks>
        [EntityField("Data", System.Data.DbType.String, BootFX.Common.Entities.EntityFieldFlags.Large)]
        public string Data
        {
            get
            {
                return ((string)(this["Data"]));
            }
            set
            {
                this["Data"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'DateTime'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'DateTime' column.
        /// </remarks>
        [EntityField("DateTime", System.Data.DbType.DateTime, BootFX.Common.Entities.EntityFieldFlags.Common)]
        public System.DateTime DateTime
        {
            get
            {
                return ((System.DateTime)(this["DateTime"]));
            }
            set
            {
                this["DateTime"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'Version'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'Version' column.
        /// </remarks>
        [EntityField("Version", System.Data.DbType.Int32, BootFX.Common.Entities.EntityFieldFlags.Common)]
        public int Version
        {
            get
            {
                return ((int)(this["Version"]));
            }
            set
            {
                this["Version"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'MachineName'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'MachineName' column.
        /// </remarks>
        [EntityField("MachineName", System.Data.DbType.String, BootFX.Common.Entities.EntityFieldFlags.Common, 64)]
        public string MachineName
        {
            get
            {
                return ((string)(this["MachineName"]));
            }
            set
            {
                this["MachineName"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the value for 'Username'.
        /// </summary>
        /// <remarks>
        /// This property maps to the 'Username' column.
        /// </remarks>
        [EntityField("Username", System.Data.DbType.String, BootFX.Common.Entities.EntityFieldFlags.Common, 128)]
        public string Username
        {
            get
            {
                return ((string)(this["Username"]));
            }
            set
            {
                this["Username"] = value;
            }
        }
        
        /// <summary>
        /// Creates an SqlFilter for an instance of 'ProjectItem'.
        /// </summary>
        public static BootFX.Common.Data.SqlFilter CreateFilter()
        {
            return new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
        }
        
        /// <summary>
        /// Sets properties on an instance of 'ProjectItem'.
        /// </summary>
        public static void SetProperties(int projectId, string[] propertyNames, object[] propertyValues)
        {
            ProjectItem entity = BootFX.Common.DbUtil.ProjectItem.GetById(projectId);
            entity.SetProperties(entity, propertyNames, propertyValues);
            entity.SaveChanges();
        }
        
        /// <summary>
        /// Get all <see cref="ProjectItem"/> entities.
        /// </summary>
        public static ProjectItemCollection GetAll()
        {
            BootFX.Common.Data.SqlFilter filter = BootFX.Common.Data.SqlFilter.CreateGetAllFilter(typeof(ProjectItem));
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets the <see cref="ProjectItem"/> entity with the given ID.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        public static ProjectItem GetById(int projectId)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetById(projectId, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets the <see cref="ProjectItem"/> entity where the ID matches the given specification.
        /// </summary>
        public static ProjectItem GetById(int projectId, BootFX.Common.Data.SqlOperator projectIdOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("ProjectId", projectIdOperator, projectId);
            return ((ProjectItem)(filter.ExecuteEntity()));
        }
        
        /// <summary>
        /// Gets the <see cref="ProjectItem"/> entity with the given ID.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        public static ProjectItem GetById(int projectId, BootFX.Common.OnNotFound onNotFound)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetById(projectId, BootFX.Common.Data.SqlOperator.EqualTo, onNotFound);
        }
        
        /// <summary>
        /// Gets the <see cref="ProjectItem"/> entity where the ID matches the given specification.
        /// </summary>
        public static ProjectItem GetById(int projectId, BootFX.Common.Data.SqlOperator projectIdOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("ProjectId", projectIdOperator, projectId);
            ProjectItem results = ((ProjectItem)(filter.ExecuteEntity()));
            return results;
        }
        
        /// <summary>
        /// Gets an entity where Name is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for index <c>DbUtilProjectsName</c>.
        /// </remarks>
        public static ProjectItem GetByName(string name)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByName(name, BootFX.Common.OnNotFound.ReturnNull);
        }
        
        /// <summary>
        /// Gets an entity where Name is equal to the given value.
        /// </summary>
        /// <remarks>
        /// Created for index <c>DbUtilProjectsName</c>.
        /// </remarks>
        public static ProjectItem GetByName(string name, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Name", BootFX.Common.Data.SqlOperator.EqualTo, name);
            ProjectItem results = ((ProjectItem)(filter.ExecuteEntity()));
            return results;
        }
        
        /// <summary>
        /// Gets entities where Data is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for column <c>Data</c>
        /// </remarks>
        public static ProjectItemCollection GetByData(string data)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByData(data, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets entities where Data matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Data</c>
        /// </remarks>
        public static ProjectItemCollection GetByData(string data, BootFX.Common.Data.SqlOperator dataOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Data", dataOperator, data);
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets entities where Data matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Data</c>
        /// </remarks>
        public static ProjectItemCollection GetByData(string data, BootFX.Common.Data.SqlOperator dataOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Data", dataOperator, data);
            ProjectItemCollection results = ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
            return results;
        }
        
        /// <summary>
        /// Gets entities where DateTime is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for column <c>DateTime</c>
        /// </remarks>
        public static ProjectItemCollection GetByDateTime(System.DateTime dateTime)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByDateTime(dateTime, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets entities where DateTime matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>DateTime</c>
        /// </remarks>
        public static ProjectItemCollection GetByDateTime(System.DateTime dateTime, BootFX.Common.Data.SqlOperator dateTimeOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("DateTime", dateTimeOperator, dateTime);
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets entities where DateTime matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>DateTime</c>
        /// </remarks>
        public static ProjectItemCollection GetByDateTime(System.DateTime dateTime, BootFX.Common.Data.SqlOperator dateTimeOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("DateTime", dateTimeOperator, dateTime);
            ProjectItemCollection results = ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
            return results;
        }
        
        /// <summary>
        /// Gets entities where Version is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for column <c>Version</c>
        /// </remarks>
        public static ProjectItemCollection GetByVersion(int version)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByVersion(version, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets entities where Version matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Version</c>
        /// </remarks>
        public static ProjectItemCollection GetByVersion(int version, BootFX.Common.Data.SqlOperator versionOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Version", versionOperator, version);
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets entities where Version matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Version</c>
        /// </remarks>
        public static ProjectItemCollection GetByVersion(int version, BootFX.Common.Data.SqlOperator versionOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Version", versionOperator, version);
            ProjectItemCollection results = ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
            return results;
        }
        
        /// <summary>
        /// Gets entities where MachineName is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for column <c>MachineName</c>
        /// </remarks>
        public static ProjectItemCollection GetByMachineName(string machineName)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByMachineName(machineName, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets entities where MachineName matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>MachineName</c>
        /// </remarks>
        public static ProjectItemCollection GetByMachineName(string machineName, BootFX.Common.Data.SqlOperator machineNameOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("MachineName", machineNameOperator, machineName);
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets entities where MachineName matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>MachineName</c>
        /// </remarks>
        public static ProjectItemCollection GetByMachineName(string machineName, BootFX.Common.Data.SqlOperator machineNameOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("MachineName", machineNameOperator, machineName);
            ProjectItemCollection results = ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
            return results;
        }
        
        /// <summary>
        /// Gets entities where Username is equal to the given value.
        /// </summary>
        /// <bootfx>
        /// CreateEntityFilterEqualToMethod - ProjectItem
        /// </bootfx>
        /// <remarks>
        /// Created for column <c>Username</c>
        /// </remarks>
        public static ProjectItemCollection GetByUsername(string username)
        {
            return BootFX.Common.DbUtil.ProjectItem.GetByUsername(username, BootFX.Common.Data.SqlOperator.EqualTo);
        }
        
        /// <summary>
        /// Gets entities where Username matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Username</c>
        /// </remarks>
        public static ProjectItemCollection GetByUsername(string username, BootFX.Common.Data.SqlOperator usernameOperator)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Username", usernameOperator, username);
            return ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Gets entities where Username matches the given specification.
        /// </summary>
        /// <remarks>
        /// Created for column <c>Username</c>
        /// </remarks>
        public static ProjectItemCollection GetByUsername(string username, BootFX.Common.Data.SqlOperator usernameOperator, BootFX.Common.OnNotFound onNotFound)
        {
            BootFX.Common.Data.SqlFilter filter = new BootFX.Common.Data.SqlFilter(typeof(ProjectItem));
            filter.Constraints.Add("Username", usernameOperator, username);
            ProjectItemCollection results = ((ProjectItemCollection)(filter.ExecuteEntityCollection()));
            return results;
        }
        
        /// <summary>
        /// Searches for <see cref="ProjectItem"/> items with the given terms.
        /// </summary>
        public static ProjectItemCollection Search(string terms)
        {
            BootFX.Common.Data.SqlSearcher searcher = new BootFX.Common.Data.SqlSearcher(typeof(ProjectItem), terms);
            return ((ProjectItemCollection)(searcher.ExecuteEntityCollection()));
        }
        
        /// <summary>
        /// Returns the value in the 'Name' property.
        /// </summary>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
