// BootFX - Application framework for .NET applications
// 
// File: ProjectItemCollectionBase.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;

namespace BootFX.Common.DbUtil
{
    using System;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Data;
    using System.Collections;
    using System.Collections.Specialized;
    using BootFX.Common;
    using BootFX.Common.Data;
    using BootFX.Common.Entities;
    using BootFX.Common.Entities.Attributes;
    using BootFX.Common.BusinessServices;
    
    
    /// <summary>
    /// Defines the base collection for entities of type <see cref="ProjectItem"/>.
    /// </summary>
    [Serializable()]
    public abstract class ProjectItemCollectionBase : BootFX.Common.Entities.EntityCollection
    {
        
        /// <summary>
        /// Constructor.
        /// </summary>
        protected ProjectItemCollectionBase() : 
                base(typeof(ProjectItem))
        {
        }
        
        /// <summary>
        /// Deserialization constructor.
        /// </summary>
        protected ProjectItemCollectionBase(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : 
                base(info, context)
        {
        }
        
        /// <summary>
        /// Gets or sets the item with the given index.
        /// </summary>
        public ProjectItem this[int index]
        {
            get
            {
                return ((ProjectItem)(this.GetItem(index)));
            }
            set
            {
                this.SetItem(index, value);
            }
        }
        
        /// <summary>
        /// Adds a <see cref="ProjectItem"/> instance to the collection.
        /// </summary>
        public int Add(ProjectItem item)
        {
            return base.Add(item);
        }
        
        /// <summary>
        /// Adds a range of <see cref="ProjectItem"/> instances to the collection.
        /// </summary>
        public void AddRange(ProjectItem[] items)
        {
            base.AddRange(items);
        }
        
        /// <summary>
        /// Adds a range of <see cref="ProjectItem"/> instances to the collection.
        /// </summary>
        public void AddRange(ProjectItemCollection items)
        {
            base.AddRange(items);
        }
    }
}
