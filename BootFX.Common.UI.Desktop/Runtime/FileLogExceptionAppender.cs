// BootFX - Application framework for .NET applications
// 
// File: FileLogExceptionAppender.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Summary description for FileLogAppender.
	/// </summary>
	internal class FileLogExceptionAppender : Appender
	{
		private const string LogName = "Application Exception";

		internal FileLogExceptionAppender(ILog log, ILogFormatter formatter, LogLevel minLevel, LogLevel maxLevel)
			: base(log, formatter, minLevel, maxLevel)
		{
		}

		protected override void DoAppend(LogData data)
		{
			if(data == null)
				throw new ArgumentNullException("data");
			
			// create a file log...
			FileLog log = LogSet.CreateFileLogger(LogName, FileLoggerFlags.EnsureNewFile | FileLoggerFlags.OwnFolder |
				FileLoggerFlags.AddDateToFileName);
			if(log == null)
				throw new InvalidOperationException("log is null.");

			// output the xml...
			XmlDocument report = PhoneHomeHelper.Current.CreateErrorReport(data, null);
			if(report == null)
				throw new InvalidOperationException("report is null.");

			// write it...
			log.Error(report.OuterXml, null);
		}
	}
}
