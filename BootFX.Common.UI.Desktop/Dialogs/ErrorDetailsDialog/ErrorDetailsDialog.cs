// BootFX - Application framework for .NET applications
// 
// File: ErrorDetailsDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Defines the built-in error details dialog and allows the user to drill into the original exception.
	/// </summary>
	/// <remarks>Special debug information is available if you hold down the Shift key when this dialog is shown.</remarks>
	public class ErrorDetailsDialog : BaseForm
	{	
		/// <summary>
		/// Private field to support <c>State</c> property.
		/// </summary>
		private object _state;
		
		/// <summary>
		/// Private field to support <c>Exception</c> property.
		/// </summary>
		private Exception _exception;

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.TextBox textError;
		private System.Windows.Forms.Button buttonSystemInformation;
		private System.Windows.Forms.Button buttonCopy;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ErrorDetailsDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textError = new System.Windows.Forms.TextBox();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonSystemInformation = new System.Windows.Forms.Button();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.textError);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(568, 372);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "&Details:";
			// 
			// textError
			// 
			this.textError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textError.Location = new System.Drawing.Point(12, 20);
			this.textError.Multiline = true;
			this.textError.Name = "textError";
			this.textError.ReadOnly = true;
			this.textError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textError.Size = new System.Drawing.Size(548, 340);
			this.textError.TabIndex = 0;
			this.textError.Text = "";
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonClose.Location = new System.Drawing.Point(500, 388);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(76, 23);
			this.buttonClose.TabIndex = 0;
			this.buttonClose.Text = "Close";
			// 
			// buttonSystemInformation
			// 
			this.buttonSystemInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonSystemInformation.Location = new System.Drawing.Point(12, 388);
			this.buttonSystemInformation.Name = "buttonSystemInformation";
			this.buttonSystemInformation.Size = new System.Drawing.Size(144, 23);
			this.buttonSystemInformation.TabIndex = 1;
			this.buttonSystemInformation.Text = "&System Information...";
			this.buttonSystemInformation.Click += new System.EventHandler(this.buttonSystemInformation_Click);
			// 
			// buttonCopy
			// 
			this.buttonCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonCopy.Location = new System.Drawing.Point(164, 388);
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.Size = new System.Drawing.Size(76, 23);
			this.buttonCopy.TabIndex = 2;
			this.buttonCopy.Text = "&Copy";
			this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
			// 
			// ErrorDetailsDialog
			// 
			this.AcceptButton = this.buttonClose;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonClose;
			this.ClientSize = new System.Drawing.Size(584, 418);
			this.Controls.Add(this.buttonCopy);
			this.Controls.Add(this.buttonSystemInformation);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.groupBox1);
			this.MaximizeBox = false;
			this.Name = "ErrorDetailsDialog";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Error Details";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Gets or sets the state
		/// </summary>
		public object State
		{
			get
			{
				return _state;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _state)
				{
					// set the value...
					_state = value;
				}
			}
		}

		private void buttonSystemInformation_Click(object sender, System.EventArgs e)
		{
			if(DesktopRuntime.IsStarted == true)
				DesktopRuntime.Current.ShowSystemInformation(this);	
			else
			{
				// show it manually - we dupe code here because it's legitimate to have this dialog
				// appear as part of the startup process...
				SystemInformationDialog dialog = new SystemInformationDialog();
				dialog.ShowDialog(this);
			}
		}

		private void buttonCopy_Click(object sender, System.EventArgs e)
		{
			this.CopyAsText();
		}

		/// <summary>
		/// Copies the details.
		/// </summary>
		private void Save()
		{
			EventHandler xmlHandler = null;
			if(this.Exception != null)
				xmlHandler = new EventHandler(SaveAsXmlClick);
		}

		/// <summary>
		/// Copies the details.
		/// </summary>
		private void Copy()
		{
			EventHandler xmlHandler = null;
			if(this.Exception != null)
				xmlHandler = new EventHandler(CopyAsXmlClick);
			this.ShowDataFormatPopup(this.buttonCopy, "Copy", new EventHandler(CopyAsTextClick), xmlHandler);
		}

		/// <summary>
		/// Shows the data format popup.
		/// </summary>
		/// <param name="control"></param>
		/// <param name="prefix"></param>
		/// <param name="textHandler"></param>
		/// <param name="xmlHandler"></param>
		private void ShowDataFormatPopup(Control control, string prefix, EventHandler textHandler, EventHandler xmlHandler)
		{
			if(control == null)
				throw new ArgumentNullException("control");
            if (prefix == null)
                throw new ArgumentNullException("prefix");
            if (prefix.Length == 0)
                throw new ArgumentException("'prefix' is zero-length.");
			if(textHandler == null)
				throw new ArgumentNullException("textHandler");
			
			// show...
			ContextMenu menu = new ContextMenu();
			menu.MenuItems.Add(prefix + " as Text", textHandler);
			if(xmlHandler != null)
				menu.MenuItems.Add(prefix + " as XML", xmlHandler);

			// show...
			Point point = new Point(control.Width, 0);
			menu.Show(control, point);
		}

		private void SaveAsTextClick(object sender, EventArgs e)
		{
			// filename...
			string filename = Alert.ShowSaveTextFileDialog(this, "Errors Details.txt");
			if(filename == null)
				return;

			// save...
			using(StreamWriter writer = new StreamWriter(filename))
			{
				writer.Write(this.Message);
			}

			// tell...
			Alert.ShowInformation(this, string.Format(Cultures.User, "The details have been saved to '{0}'.", filename));
		}

		private void SaveAsXmlClick(object sender, EventArgs e)
		{
//			// filename...
//			string filename = Alert.ShowSaveXmlFileDialog(this, StringHelper.GetString("ExceptionReportXml", OnStringNotFound.ReturnName));
//					// ExceptionReportXml = 'Exception Report.xml'
//			if(filename == null)
//				return;
//
//			// report...
//			Cursor oldCursor = this.Cursor;
//			this.Cursor = Cursors.WaitCursor;
//			try
//			{
//				// get the report...
//				if(Exception == null)
//					throw new ArgumentNullException("Exception");
//				ExceptionReport report = new ExceptionReport(this.Exception, this.State, true);
//
//				// set...
//				report.SaveXml(filename);
//			}
//			finally
//			{
//				this.Cursor = oldCursor;
//			}
//
//			// set...
//			Alert.ShowInformation(this, string.Format(Cultures.User, StringHelper.GetString("TheDetailsHaveBeenSavedToP0", OnStringNotFound.ReturnName), filename));
//					// TheDetailsHaveBeenSavedToP0 = 'The details have been saved to '{0}'.'
			Alert.ShowWarning(this, "Not implemented.");
		}

		private void CopyAsTextClick(object sender, EventArgs e)
		{
			this.CopyAsText();
		}

		private void CopyAsText()
		{
			Clipboard.SetDataObject(this.Message, true);
			Alert.ShowInformation(this, "The details have been copied to the clipboard.\r\n\r\nYou can now paste the information into an e-mail message and send it to your support department.");
		}

		private void CopyAsXmlClick(object sender, EventArgs e)
		{
//			// report...
//			Cursor oldCursor = this.Cursor;
//			this.Cursor = Cursors.WaitCursor;
//			try
//			{
//				// get the report...
//				if(Exception == null)
//					throw new ArgumentNullException("Exception");
//				ExceptionReport report = new ExceptionReport(this.Exception, this.State, true);
//
//				// set...
//				Clipboard.SetDataObject(report.ToXml(), true);
//			}
//			finally
//			{
//				this.Cursor = oldCursor;
//			}
//
//			// set...
//			Alert.ShowInformation(this, StringHelper.GetString("TheDetailsHaveBeenCopiedToTheClipboard", OnStringNotFound.ReturnName));
//					// TheDetailsHaveBeenCopiedToTheClipboard = 'The details have been copied to the clipboard.'
			Alert.ShowWarning(this, "Not implemented.");
		}

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			Save();	
		}

		private void buttonDebug_Click(object sender, System.EventArgs e)
		{
			if(Debugger.IsAttached == true)
				Debugger.Break();
			else
				Debugger.Launch();
		}

		private void buttonPop_Click(object sender, System.EventArgs e)
		{
			if(Exception == null)
				throw new ArgumentNullException("Exception");

			// get a temp file...
			string tempFilePath = null;
			if(Runtime.IsStarted == true)
				tempFilePath = Runtime.Current.GetTempTextFilePath();
			else
				tempFilePath = Path.GetTempFileName() + ".txt";

			// dump...
			using(StreamWriter writer = new StreamWriter(tempFilePath))
			{
				writer.WriteLine(this.Exception.ToString());
			}

			// show...
			System.Diagnostics.Process.Start(tempFilePath);
		}

		private void linkShowReport_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			ShowReport();	
		}

		/// <summary>
		/// Displays the error report.
		/// </summary>
		private void ShowReport()
		{
			if(Exception == null)
				throw new ArgumentNullException("Exception");

			Alert.ShowWarning(this, "Not implemented.");
		}

		/// <summary>
		/// Gets or sets the exception
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _exception;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _exception)
				{
					// set the value...
					_exception = value;
					if(this.Exception != null)
						this.Message = this.Exception.ToString();
					else
						this.Message = null;
				}
			}
		}

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		public string Message
		{
			get
			{
				return this.textError.Text;
			}
			set
			{
				this.textError.Text = value;
			}
		}
	}
}
