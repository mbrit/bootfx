// BootFX - Application framework for .NET applications
// 
// File: ErrorDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Xml;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Defines a built dialog that display an error, warning or exception message.
	/// </summary>
	public class ErrorDialog : BaseForm
	{
        private string AutoSendKey = "BfxAutoSendErrors";

        /// <summary>
        /// Private value to support the <see cref="SendPanelDelta">SendPanelDelta</see> property.
        /// </summary>
        private int _sendPanelDelta;

        private bool _suppressErrorReport = false;

		/// <summary>
		/// Private field to support <see cref="ReportSent"/> property.
		/// </summary>
		private bool _reportSent;
		
		/// <summary>
		/// Private field to support <c>State</c> property.
		/// </summary>
		private object _state;
		
		/// <summary>
		/// Private field to support <c>IconType</c> property.
		/// </summary>
		private IconType _iconType;
		
		/// <summary>
		/// Private field to support <c>Exception</c> property.
		/// </summary>
		private Exception _exception;
		
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textError;
		private System.Windows.Forms.Button buttonSend;
		private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkDetails;
        private System.Windows.Forms.Panel panelMain;
        private CheckBox checkAuto;
        private Button buttonDoNotSend;
        private Panel panelSend;
        private TextBox textSupportId;
        private ProgressBar progressBar1;
        private Label labelStatus;
        private Button buttonClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ErrorDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.Text = Alert.Caption;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textError = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkDetails = new System.Windows.Forms.LinkLabel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.checkAuto = new System.Windows.Forms.CheckBox();
            this.buttonDoNotSend = new System.Windows.Forms.Button();
            this.panelSend = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.textSupportId = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelStatus = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMain.SuspendLayout();
            this.panelSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textError);
            this.groupBox1.Location = new System.Drawing.Point(60, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 117);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "An &error has occurred:";
            // 
            // textError
            // 
            this.textError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textError.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textError.Location = new System.Drawing.Point(12, 20);
            this.textError.Multiline = true;
            this.textError.Name = "textError";
            this.textError.ReadOnly = true;
            this.textError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textError.Size = new System.Drawing.Size(284, 91);
            this.textError.TabIndex = 0;
            // 
            // buttonSend
            // 
            this.buttonSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSend.Location = new System.Drawing.Point(137, 132);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(227, 23);
            this.buttonSend.TabIndex = 0;
            this.buttonSend.Text = "&Send an Error Report (Recommended)";
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(16, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // linkDetails
            // 
            this.linkDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkDetails.Location = new System.Drawing.Point(13, 191);
            this.linkDetails.Name = "linkDetails";
            this.linkDetails.Size = new System.Drawing.Size(108, 16);
            this.linkDetails.TabIndex = 1;
            this.linkDetails.TabStop = true;
            this.linkDetails.Text = "&More information...";
            this.linkDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkDetails_LinkClicked);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.checkAuto);
            this.panelMain.Controls.Add(this.buttonDoNotSend);
            this.panelMain.Controls.Add(this.buttonSend);
            this.panelMain.Controls.Add(this.pictureBox1);
            this.panelMain.Controls.Add(this.linkDetails);
            this.panelMain.Controls.Add(this.groupBox1);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(372, 220);
            this.panelMain.TabIndex = 4;
            // 
            // checkAuto
            // 
            this.checkAuto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkAuto.Location = new System.Drawing.Point(137, 190);
            this.checkAuto.Name = "checkAuto";
            this.checkAuto.Size = new System.Drawing.Size(227, 17);
            this.checkAuto.TabIndex = 4;
            this.checkAuto.Text = "&Automatically send reports in future";
            this.checkAuto.UseVisualStyleBackColor = true;
            this.checkAuto.CheckedChanged += new System.EventHandler(this.checkAuto_CheckedChanged);
            // 
            // buttonDoNotSend
            // 
            this.buttonDoNotSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDoNotSend.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonDoNotSend.Location = new System.Drawing.Point(137, 161);
            this.buttonDoNotSend.Name = "buttonDoNotSend";
            this.buttonDoNotSend.Size = new System.Drawing.Size(227, 23);
            this.buttonDoNotSend.TabIndex = 3;
            this.buttonDoNotSend.Text = "&Do Not Send an Error Report";
            // 
            // panelSend
            // 
            this.panelSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelSend.Controls.Add(this.buttonClose);
            this.panelSend.Controls.Add(this.textSupportId);
            this.panelSend.Controls.Add(this.progressBar1);
            this.panelSend.Controls.Add(this.labelStatus);
            this.panelSend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSend.Location = new System.Drawing.Point(0, 220);
            this.panelSend.Name = "panelSend";
            this.panelSend.Size = new System.Drawing.Size(372, 66);
            this.panelSend.TabIndex = 5;
            this.panelSend.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSend_Paint);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Enabled = false;
            this.buttonClose.Location = new System.Drawing.Point(283, 35);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(81, 23);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            // 
            // textSupportId
            // 
            this.textSupportId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textSupportId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textSupportId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textSupportId.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.textSupportId.Location = new System.Drawing.Point(283, 10);
            this.textSupportId.Name = "textSupportId";
            this.textSupportId.Size = new System.Drawing.Size(81, 20);
            this.textSupportId.TabIndex = 2;
            this.textSupportId.Text = "9999999";
            this.textSupportId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(15, 42);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(256, 11);
            this.progressBar1.TabIndex = 1;
            // 
            // labelStatus
            // 
            this.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatus.Location = new System.Drawing.Point(13, 10);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(258, 29);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "Sending error report, please wait...";
            // 
            // ErrorDialog
            // 
            this.AcceptButton = this.buttonSend;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.buttonDoNotSend;
            this.ClientSize = new System.Drawing.Size(372, 286);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelSend);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorDialog";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ErrorDialog";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelSend.ResumeLayout(false);
            this.panelSend.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Icon icon = this.GetIcon();
			e.Graphics.DrawIconUnstretched(icon, this.pictureBox1.ClientRectangle);
		}

		/// <summary>
		/// Gets the icon to use.
		/// </summary>
		/// <returns></returns>
		private Icon GetIcon()
		{
			switch(IconType)
			{
				case IconType.Error:
					return SystemIcons.Error;
				case IconType.Warning:
					return SystemIcons.Warning;

				default:
                    throw new NotSupportedException(string.Format("Cannot handle '{0}'.", IconType));

			}
		}

		private void linkDetails_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			ShowDetails();	
		}

		/// <summary>
		/// Shows the details.
		/// </summary>
		private void ShowDetails()
		{
			// display more information in a bigger dialog.
			// mbr - 10-05-2007 - added using.			
			using(ErrorDetailsDialog details = new ErrorDetailsDialog())
			{
				if(this.Exception != null)
					details.Exception = this.Exception;
				else
					details.Message = this.Message;

				// set the state...
				details.State = this.State;

				// show...
				details.ShowDialog(this);
			}
		}

		/// <summary>
		/// Gets or sets the state
		/// </summary>
		public object State
		{
			get
			{
				return _state;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _state)
				{
					// set the value...
					_state = value;
				}
			}
		}

		private void linkBreak_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Debugger.Break();
		}

		/// <summary>
		/// Gets or sets the exception
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _exception;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _exception)
				{
					// set the value...
					_exception = value;
					if(this.Exception != null)
						this.Message = this.Exception.Message;
					else
						this.Message = null;
				}
			}
		}

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		public string Message
		{
			get
			{
				return this.textError.Text;
			}
			set
			{
				this.textError.Text = value;
			}
		}

		/// <summary>
		/// Gets or sets the icontype
		/// </summary>
		public IconType IconType
		{
			get
			{
				return _iconType;
			}
			set
			{
				// check to see if the value has changed...
				if(value != _iconType)
				{
					// set the value...
					_iconType = value;
				}
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// design...
			if(this.DesignMode)
				return;

			// play...
			Sound.PlaySound(this.IconType);

            // delta...
            this.SuspendLayout();
            try
            {
                _sendPanelDelta = this.panelSend.Height;
                this.Height -= _sendPanelDelta;
                this.panelSend.Visible = false;
            }
            finally
            {
                this.ResumeLayout();
            }

            // get...
            if (Runtime.IsStarted)
            {
                this.checkAuto.Enabled = true;

                // get...
                if(Runtime.Current.UserSettings.Contains(AutoSendKey))
                {
                    this.checkAuto.Checked = Runtime.Current.UserSettings.GetBooleanValue(AutoSendKey, false, Cultures.System, OnNotFound.ReturnNull);
                    if(this.checkAuto.Checked)
                        this.HandleSend();
                }
                else
                    this.checkAuto.Checked = true;
            }
            else
                this.checkAuto.Enabled = false;
		}

		private void buttonSendErrorReport_Click(object sender, System.EventArgs e)
		{
			this.SendReport();
		}

		private string SendReport()
		{
			_reportSent = true;
			try
			{
                if (Runtime.Current.HasPhoneHomeUrl)
                {
                    // create a report...
                    XmlDocument doc = PhoneHomeHelper.Current.CreateErrorReport(this.Message, this.Exception, null);
                    if (doc == null)
                        throw new InvalidOperationException("doc is null.");

                    // send...
                    OperationContext context = new OperationContext(null, null);
                    SendErrorReportProcess process = new SendErrorReportProcess(context,
                        Runtime.Current.PhoneHomeUrl, doc);

                    // run..
                    using (OperationDialog dialog = new OperationDialog())
                    {
                        // caption...
                        dialog.Caption = "Sending Error Report to Server";

                        // set...
                        Uri uri = new Uri(Runtime.Current.PhoneHomeUrl);
                        dialog.StatusBarText = uri.Host;
                        if (string.Compare(uri.Scheme, "https", true, Cultures.System) == 0)
                            dialog.StatusBarText += " (secure)";

                        // sub...
                        dialog.Failed += new System.Threading.ThreadExceptionEventHandler(dialog_Failed);

                        // run...
                        dialog.Run(process);
                        dialog.ShowDialog(this);
                    }

                    // what happened?
                    if (process.RunResult is PhoneHomeResult)
                    {
                        // mbr - 2009-06-04 - changed...
                        //Alert.ShowInformation(this, string.Format("The error report has been sent.  Your support ID is: {0}", 
                        //    ((PhoneHomeResult)process.RunResult).SupportId));
                        return ((PhoneHomeResult)process.RunResult).SupportId;
                    }
                    else
                    {
                        Alert.ShowWarning(this, "The error report was not sent.");
                        return null;
                    }
                }
                else
                {
                    Alert.ShowInformation(this, "This application does not support automatic error reporting.\r\n\r\nTo assist us, please follow these steps:\r\n\r\n   - Click OK to dismiss this message.\r\n   - Click the 'More information' link to show more error information.\r\n   - A new window will appear.  Click the 'Copy' to copy the message contents to your clipboard.\r\n   - Paste the error information into an e-mail message and send it to your support department.\r\n   - Dismiss both error windows to continue your work.");
                    return null;
                }
			}
			catch(Exception ex)
			{
				const string message = "The error report could not be sent.";
				if(this.Log.IsErrorEnabled)
					this.Log.Error(message, ex);

				// show...
				Alert.ShowWarning(this, message);
                return null;

			}
		}

		/// <summary>
		/// Gets the reportsent.
		/// </summary>
		private bool ReportSent
		{
			get
			{
				return _reportSent;
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing (e);
			if(this.DesignMode)
				return;

            // mbr - 2009-06-04 - this feature was a total pain, so I have removed it...
//            try
//            {
//                // if we haven't sent a report, and we don't have a debugger attached, nag the user.
//                if(!(this.ReportSent) && !(System.Diagnostics.Debugger.IsAttached) && !(this.SuppressErrorReport))
//                {
//                    // mbr - 28-06-2007 - changed this to use an option dialog...
//                    using(OptionDialog dialog = new OptionDialog("You have not sent an error report.  Are you sure you do not want to send an error report?"))
//                    {
//                        dialog.AddButton("&Send Error Report", DialogResult.Yes);
//                        dialog.AddButton("Do &Not Send Error Report", DialogResult.No);
//                        dialog.AddButton("Cancel", DialogResult.Cancel);

//                        // show...
//                        DialogResult result = dialog.ShowDialog(this);
//                        switch(result)
//                        {
//                            case DialogResult.Yes:
//                                this.SendReport();
//                                break;

//                            case DialogResult.No:
//                                break;

//                            case DialogResult.Cancel:
//                                e.Cancel = true;
//                                break;

//                            default:
//                                throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", result, result.GetType()));
//                        }
//                    }

////					if(Alert.AskYesNoQuestion(this, "You have not sent an error report.  Are you sure you do not want to send an error report?") 
////						== DialogResult.No)
////					{
////						e.Cancel = true;
////					}
//                }
//            }
//            catch
//            {
//                // no-op on the exception (otherwise we'll get recursive).
//                Alert.ShowWarning(this, "An error occurred when closing the form.");
//            }

            // set...
            try
            {
                if (Runtime.IsStarted)
                    Runtime.Current.UserSettings.SetValue(AutoSendKey, this.checkAuto.Checked);
            }
            catch (Exception ex)
            {
                const string message = "An error occurred when closing the form.";
                if (this.Log.IsErrorEnabled)
                    this.Log.Error(message, ex);
                Alert.ShowWarning(this, message);
            }
        }

		internal bool SuppressErrorReport
		{
			get
			{
				return _suppressErrorReport;
			}
			set
			{
				if(_suppressErrorReport != value)
				{
                    // mbr - 2009-06-04 - changed the error reporting, not sure what to do with this now...
				}
			}
		}

		private void dialog_Failed(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			Alert.ShowWarning(this, "The error report could not be sent.", e.Exception, AlertFlags.SuppressErrorReport);
		}

        private void checkAuto_CheckedChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Gets the SendPanelDelta value.
        /// </summary>
        private int SendPanelDelta
        {
            get
            {
                return _sendPanelDelta;
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            this.HandleSend();
        }

        private void HandleSend()
        {
            try
            {
                this.buttonSend.Enabled = false;
                this.buttonDoNotSend.Enabled = false;

                // run...
                string supportId = this.SendReport();
                if (!(string.IsNullOrEmpty(supportId)))
                {
                    this.ShowPanel();
                    this.labelStatus.Text = "Your error report has been sent.\r\nPlease note this number:";
                    this.textSupportId.Text = supportId;
                    this.progressBar1.Value = this.progressBar1.Maximum;
                }
            }
            catch (Exception ex)
            {
                // mbr - 2009-06-04 - this intentionally displays nothing - the handling is in SendReport.  anything
                // here is super-weird.
                if (this.Log.IsErrorEnabled)
                    this.Log.Error("An error occurred handling the report.", ex);
            }
            finally
            {
                // close...
                this.buttonClose.Enabled = true;
                this.buttonClose.DialogResult = DialogResult.Cancel;
                this.AcceptButton = this.buttonClose;
                this.CancelButton = this.buttonClose;
                this.buttonClose.Focus();
            }
        }

        private void ShowPanel()
        {
            if (!(this.panelSend.Visible))
            {
                this.panelSend.Visible = true;
                this.Height += this.panelSend.Height;
            }
        }

        private void panelSend_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Black, this.panelSend.Left, 0, this.panelSend.Right, 0);
        }
	}
}
