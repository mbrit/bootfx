// BootFX - Application framework for .NET applications
// 
// File: SendErrorReportProcess.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Summary description for SendErrorReportProces.
	/// </summary>
	public class SendErrorReportProcess : OperationDialogProcess
	{
		/// <summary>
		/// Private field to support <see cref="Url"/> property.
		/// </summary>
		private string _url;
		
		/// <summary>
		/// Private field to support <see cref="Report"/> property.
		/// </summary>
		private XmlDocument _report;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="report"></param>
		/// <param name="url"></param>
		public SendErrorReportProcess(OperationContext context, string url, XmlDocument report)
			: base(context)
		{
			if(report == null)
				throw new ArgumentNullException("report");
			if(url == null)
				throw new ArgumentNullException("url");
			if(url.Length == 0)
				throw new ArgumentOutOfRangeException("'url' is zero-length.");
			
			// send...
			_url = url;
			_report = report;
		}

		/// <summary>
		/// Gets the report.
		/// </summary>
		private XmlDocument Report
		{
			get
			{
				return _report;
			}
		}

		/// <summary>
		/// Gets the url.
		/// </summary>
		private string Url
		{
			get
			{
				return _url;
			}
		}

		protected override object DoRun()
		{
			if(Url == null)
				throw new ArgumentNullException("Url");
			if(Url.Length == 0)
				throw new ArgumentOutOfRangeException("'Url' is zero-length.");
			if(Report == null)
				throw new ArgumentNullException("Report");

			// send...
			PhoneHomeResult result = null;
			while(true)
			{
				this.Context.Status = "Sending error report...";

				// try it...
				try
				{
					result = PhoneHomeHelper.Current.SendErrorReport(this.Url, this.Report);

					// we did it!
					break;
				}
				catch(WebException ex)
				{
					this.Context.Status = "An error occurred.";

					// log...
					if(this.Log.IsErrorEnabled)
						this.Log.Error("An error occurred when sending the error report.", ex);

					// show...
					if(ThreadUIHelper.Current.AskYesNoQuestion("An error occurred when sending the error report.  Do you want to try again?") 
						== DialogResult.No)
					{
						// quit...
						return false;
					}
				}
			}

			// ok...
			this.Context.Status = "Finished.";
			
			// output...
			if(this.Log.IsInfoEnabled)
			{
				if(result != null)
					this.Log.Info("\tPOST result: " + result.HttpResult);
				else
					this.Log.Info("\tPOST result: (null)");
			}

			// nope...
			return result;
		}
	}
}
