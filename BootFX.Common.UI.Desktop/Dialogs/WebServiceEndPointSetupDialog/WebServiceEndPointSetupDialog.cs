// BootFX - Application framework for .NET applications
// 
// File: WebServiceEndPointSetupDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Summary description for WebServiceEndPointSetupDialog.
	/// </summary>
	public class WebServiceEndPointSetupDialog : BaseForm
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonOK;
		private System.Windows.Forms.TextBox textPort;
		private System.Windows.Forms.CheckBox checkSecure;
		private System.Windows.Forms.Label labelHost;
		private System.Windows.Forms.RadioButton radioDefaultPort;
		private System.Windows.Forms.TextBox textHost;
		private System.Windows.Forms.TextBox textAppName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.RadioButton radioSpecificPort;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected WebServiceSettings Settings
		{
			get
			{
				WebServiceSettings settings = WebServiceSettings.Default;
				if(settings == null)
					throw new InvalidOperationException("settings is null.");

				return settings;
			}
		}

		public WebServiceEndPointSetupDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textAppName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.checkSecure = new System.Windows.Forms.CheckBox();
			this.textPort = new System.Windows.Forms.TextBox();
			this.radioSpecificPort = new System.Windows.Forms.RadioButton();
			this.radioDefaultPort = new System.Windows.Forms.RadioButton();
			this.textHost = new System.Windows.Forms.TextBox();
			this.labelHost = new System.Windows.Forms.Label();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonOK = new System.Windows.Forms.Button();
			this.buttonReset = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textAppName);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.checkSecure);
			this.groupBox1.Controls.Add(this.textPort);
			this.groupBox1.Controls.Add(this.radioSpecificPort);
			this.groupBox1.Controls.Add(this.radioDefaultPort);
			this.groupBox1.Controls.Add(this.textHost);
			this.groupBox1.Controls.Add(this.labelHost);
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(340, 164);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Web Sevice End Point";
			// 
			// textAppName
			// 
			this.textAppName.Location = new System.Drawing.Point(112, 108);
			this.textAppName.Name = "textAppName";
			this.textAppName.Size = new System.Drawing.Size(220, 20);
			this.textAppName.TabIndex = 10;
			this.textAppName.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 108);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 20);
			this.label1.TabIndex = 9;
			this.label1.Text = "Application name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// checkSecure
			// 
			this.checkSecure.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.checkSecure.Location = new System.Drawing.Point(8, 136);
			this.checkSecure.Name = "checkSecure";
			this.checkSecure.Size = new System.Drawing.Size(220, 20);
			this.checkSecure.TabIndex = 8;
			this.checkSecure.Text = "Use secure (HTTPS) connection";
			// 
			// textPort
			// 
			this.textPort.Location = new System.Drawing.Point(112, 80);
			this.textPort.Name = "textPort";
			this.textPort.Size = new System.Drawing.Size(88, 20);
			this.textPort.TabIndex = 7;
			this.textPort.Text = "";
			// 
			// radioSpecificPort
			// 
			this.radioSpecificPort.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioSpecificPort.Location = new System.Drawing.Point(8, 80);
			this.radioSpecificPort.Name = "radioSpecificPort";
			this.radioSpecificPort.Size = new System.Drawing.Size(100, 24);
			this.radioSpecificPort.TabIndex = 2;
			this.radioSpecificPort.Text = "Specific Port:";
			this.radioSpecificPort.CheckedChanged += new System.EventHandler(this.RadioButtonCheckChanged);
			// 
			// radioDefaultPort
			// 
			this.radioDefaultPort.Checked = true;
			this.radioDefaultPort.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioDefaultPort.Location = new System.Drawing.Point(8, 52);
			this.radioDefaultPort.Name = "radioDefaultPort";
			this.radioDefaultPort.Size = new System.Drawing.Size(100, 24);
			this.radioDefaultPort.TabIndex = 1;
			this.radioDefaultPort.TabStop = true;
			this.radioDefaultPort.Text = "Default Port";
			this.radioDefaultPort.CheckedChanged += new System.EventHandler(this.RadioButtonCheckChanged);
			// 
			// textHost
			// 
			this.textHost.Location = new System.Drawing.Point(112, 24);
			this.textHost.Name = "textHost";
			this.textHost.Size = new System.Drawing.Size(220, 20);
			this.textHost.TabIndex = 5;
			this.textHost.Text = "";
			// 
			// labelHost
			// 
			this.labelHost.Location = new System.Drawing.Point(8, 24);
			this.labelHost.Name = "labelHost";
			this.labelHost.Size = new System.Drawing.Size(100, 20);
			this.labelHost.TabIndex = 3;
			this.labelHost.Text = "Host:";
			this.labelHost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonCancel.Location = new System.Drawing.Point(272, 180);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(76, 23);
			this.buttonCancel.TabIndex = 10;
			this.buttonCancel.Text = "&Cancel";
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// buttonOK
			// 
			this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonOK.Location = new System.Drawing.Point(192, 180);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new System.Drawing.Size(76, 23);
			this.buttonOK.TabIndex = 9;
			this.buttonOK.Text = "&OK";
			this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
			// 
			// buttonReset
			// 
			this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonReset.Location = new System.Drawing.Point(8, 180);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(112, 23);
			this.buttonReset.TabIndex = 11;
			this.buttonReset.Text = "&Reset To Default";
			this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
			// 
			// WebServiceEndPointSetupDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(354, 208);
			this.Controls.Add(this.buttonReset);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOK);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "WebServiceEndPointSetupDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Web Service End Point Setup";
			this.Load += new System.EventHandler(this.WebServiceEndPointSetupDialog_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void WebServiceEndPointSetupDialog_Load(object sender, System.EventArgs e)
		{
			if(Settings == null)
				throw new InvalidOperationException("Settings is null.");

			// get values and populate...
			if(this.Settings.IsConfigured)
			{
				if(this.Settings.Port > 0)
				{
					this.radioSpecificPort.Checked = true;
					this.textPort.Text = this.Settings.Port.ToString();
				}
				else
				{
					this.radioDefaultPort.Checked = true;
				}
			
				this.textAppName.Text = this.Settings.AppName;
				this.textHost.Text = this.Settings.Host;
				this.checkSecure.Checked = this.Settings.IsSecure;
			}
			this.RefreshEnableState();
		}

		private void RefreshEnableState()
		{
			if(this.radioDefaultPort.Checked)
				this.textPort.Enabled = false;
			else
				this.textPort.Enabled = true;
		}

		private bool DoApply()
		{
			FormChecker checker = new FormChecker();
			// get...
			string appName = checker.GetStringValue(new ControlReference(this.textAppName, "Application name"), false);
			string host = checker.GetStringValue(new ControlReference(this.textHost, "Host"), false);
			
			int port = 0;
			if(this.radioSpecificPort.Checked)
				port = checker.GetInt32Value(new ControlReference(this.textPort, "Port"), true);
			
			// set...
			this.Settings.AppName = appName;
			this.Settings.Host = host;
			this.Settings.Port = port;
			this.Settings.IsSecure = this.checkSecure.Checked;
			
			// check...		
			if(checker.HasErrors)
			{
				Alert.ShowWarning(this, checker.GetAllErrorsSeparatedByCrLf());
				return false;
			}

			// save...
			this.Settings.Save();
			return true;
		}

		private void RadioButtonCheckChanged(object sender, System.EventArgs e)
		{
			this.RefreshEnableState();
		}

		private void buttonCancel_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void buttonOK_Click(object sender, System.EventArgs e)
		{
			if(this.DoApply())
				this.DialogResult = DialogResult.OK;
		}

		private void buttonReset_Click(object sender, System.EventArgs e)
		{
			this.Settings.Host = null;
			this.Settings.Save();
			this.DialogResult = DialogResult.OK;
		}

		protected override bool ProcessDialogKey(Keys keyData)
		{
			switch(keyData)
			{
				case Keys.Escape:
					this.DialogResult = DialogResult.Cancel;
					break;
				case Keys.Enter:
					if(this.DoApply())
						this.DialogResult = DialogResult.OK;
					break;
				default:
					// no-op
					break;
			}

			return base.ProcessDialogKey (keyData);
		}

	}
}
