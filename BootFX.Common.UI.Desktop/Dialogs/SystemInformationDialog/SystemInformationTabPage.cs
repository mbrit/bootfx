// BootFX - Application framework for .NET applications
// 
// File: SystemInformationTabPage.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Windows.Forms;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Defines a page used to hold an <see cref="EntityListView"></see> control on <see cref="SystemInformationDialog"></see>.
	/// </summary>
	internal class SystemInformationTabPage : TabPage
	{
		/// <summary>
		/// Private field to support <c>Grid</c> property.
		/// </summary>
		private EntityListView _grid = new EntityListView();
		
		/// <summary>
		/// Private field to support <c>Page</c> property.
		/// </summary>
		private SystemInformationPage _page;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="page"></param>
		public SystemInformationTabPage(SystemInformationPage page)
		{
			if(page == null)
				throw new ArgumentNullException("page");
			
			_page = page;
			this.Text = page.ToString();

			// controls...
			this.DockPadding.All = 4;
			this.Grid.Dock = DockStyle.Fill;
			this.Controls.Add(this.Grid);
		}

		/// <summary>
		/// Gets the grid.
		/// </summary>
		private EntityListView Grid
		{
			get
			{
				// returns the value...
				return _grid;
			}
		}

		/// <summary>
		/// Gets the page.
		/// </summary>
		public SystemInformationPage Page
		{
			get
			{
				return _page;
			}
		}

		/// <summary>
		/// Refreshes the view.
		/// </summary>
		public void RefreshView()
		{
			this.Grid.DataSource = this.Page.GetDataTable();
		}
	}
}
