// BootFX - Application framework for .NET applications
// 
// File: SystemInformationDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using BootFX.Common.Xml;
using BootFX.Common.Management;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Defines the built-in system information dialog.
	/// </summary>
	public class SystemInformationDialog : BaseForm
	{
		/// <summary>
		/// Private field to support <c>SystemInformation</c> property.
		/// </summary>
		private BootFX.Common.Management.SystemInformation _systemInformation;
		
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.TabControl tabPages;
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonMoreSystemInformation;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SystemInformationDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonClose = new System.Windows.Forms.Button();
			this.tabPages = new System.Windows.Forms.TabControl();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonMoreSystemInformation = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonClose.Location = new System.Drawing.Point(584, 384);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(76, 23);
			this.buttonClose.TabIndex = 0;
			this.buttonClose.Text = "Close";
			// 
			// tabPages
			// 
			this.tabPages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabPages.Location = new System.Drawing.Point(8, 8);
			this.tabPages.Name = "tabPages";
			this.tabPages.SelectedIndex = 0;
			this.tabPages.Size = new System.Drawing.Size(652, 368);
			this.tabPages.TabIndex = 1;
			this.tabPages.SelectedIndexChanged += new System.EventHandler(this.tabPages_SelectedIndexChanged);
			// 
			// buttonCopy
			// 
			this.buttonCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonCopy.Location = new System.Drawing.Point(208, 384);
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.TabIndex = 2;
			this.buttonCopy.Text = "&Copy";
			this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonSave.Location = new System.Drawing.Point(292, 384);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.TabIndex = 3;
			this.buttonSave.Text = "&Save...";
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonMoreSystemInformation
			// 
			this.buttonMoreSystemInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonMoreSystemInformation.Location = new System.Drawing.Point(8, 384);
			this.buttonMoreSystemInformation.Name = "buttonMoreSystemInformation";
			this.buttonMoreSystemInformation.Size = new System.Drawing.Size(192, 23);
			this.buttonMoreSystemInformation.TabIndex = 4;
			this.buttonMoreSystemInformation.Text = "Microsoft System Information...";
			this.buttonMoreSystemInformation.Click += new System.EventHandler(this.buttonMoreSystemInformation_Click);
			// 
			// SystemInformationDialog
			// 
			this.AcceptButton = this.buttonClose;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonClose;
			this.ClientSize = new System.Drawing.Size(668, 414);
			this.Controls.Add(this.buttonMoreSystemInformation);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.buttonCopy);
			this.Controls.Add(this.tabPages);
			this.Controls.Add(this.buttonClose);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SystemInformationDialog";
			this.ShowInTaskbar = false;
			this.Text = "System Information";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnLoad(EventArgs e)
		{
			// base...
			base.OnLoad (e);

			// design...
			if(this.DesignMode == true)
				return;

			// refresh...
			RefreshSystemInformation();
		}

		/// <summary>
		/// Updates the system information.
		/// </summary>
		private void RefreshSystemInformation()
		{
			this.tabPages.TabPages.Clear();

			// get the sys info...
			_systemInformation = BootFX.Common.Management.SystemInformation.GetSystemInformation();

			// loop the pages...
			foreach(SystemInformationPage page in this.SystemInformation.Pages)
				this.tabPages.TabPages.Add(new SystemInformationTabPage(page));

			// tab...
			this.RefreshCurrentTab();
		}

		private void tabPages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.RefreshCurrentTab();
		}

		/// <summary>
		/// Refreshes the current tab.
		/// </summary>
		private void RefreshCurrentTab()
		{
			// get the selected page...
			TabPage page = this.tabPages.SelectedTab;
			if(page != null && page is SystemInformationTabPage)
				((SystemInformationTabPage)page).RefreshView();
		}

		private void buttonCopy_Click(object sender, System.EventArgs e)
		{
			Copy();	
		}

		/// <summary>
		/// Copies the system information to the clipboard.
		/// </summary>
		private void Copy()
		{
			if(this.SystemInformation != null)
			{
				Clipboard.SetDataObject(this.SystemInformation.ToXml(), true);
				Alert.ShowInformation(this, "The system information has been copied to the clipboard.");
			}
		}

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			Save();	
		}

		/// <summary>
		/// Saves the system information to xml.
		/// </summary>
		public void Save()
		{
			if(this.SystemInformation == null)
				return;

			string filename = Alert.ShowSaveXmlFileDialog(this, "System Information.xml");
			if(filename != null)
			{
				// save it...
				this.SystemInformation.SaveXml(filename);
				Alert.ShowInformation(this, string.Format(Cultures.Exceptions, "System information has been saved to '{0}'.", filename));
			}
		}

		private void buttonMoreSystemInformation_Click(object sender, System.EventArgs e)
		{
			LaunchMsInfoDialog dialog = new LaunchMsInfoDialog();
			dialog.ShowDialog(this);
		}

		/// <summary>
		/// Gets the systeminformation.
		/// </summary>
		public BootFX.Common.Management.SystemInformation SystemInformation
		{
			get
			{
				return _systemInformation;
			}
		}
	}
}
