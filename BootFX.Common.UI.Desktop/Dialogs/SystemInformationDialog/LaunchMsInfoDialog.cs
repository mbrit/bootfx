// BootFX - Application framework for .NET applications
// 
// File: LaunchMsInfoDialog.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BootFX.Common.UI.Desktop
{
	/// <summary>
	/// Defines a dialog that provides access ot the Microsoft System Information utility.
	/// </summary>
	internal class LaunchMsInfoDialog : BaseForm
	{
		private System.Windows.Forms.Button buttonLaunch;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LaunchMsInfoDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonLaunch = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonLaunch
			// 
			this.buttonLaunch.Location = new System.Drawing.Point(12, 24);
			this.buttonLaunch.Name = "buttonLaunch";
			this.buttonLaunch.Size = new System.Drawing.Size(320, 23);
			this.buttonLaunch.TabIndex = 1;
			this.buttonLaunch.Text = "&Launch Microsoft System Info utility interactively...";
			this.buttonLaunch.Click += new System.EventHandler(this.buttonLaunch_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Location = new System.Drawing.Point(12, 56);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(320, 23);
			this.buttonSave.TabIndex = 2;
			this.buttonSave.Text = "&Save Microsoft System Info utility information to disk...";
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(264, 112);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(88, 23);
			this.buttonCancel.TabIndex = 3;
			this.buttonCancel.Text = "Cancel";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.buttonLaunch);
			this.groupBox1.Controls.Add(this.buttonSave);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(344, 92);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Do you want to?";
			// 
			// LaunchMsInfoDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(358, 144);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.buttonCancel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "LaunchMsInfoDialog";
			this.Text = "Microsoft System Information";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonLaunch_Click(object sender, System.EventArgs e)
		{
			DesktopRuntime.Current.LaunchMicrosoftSystemInformation(this);
			DialogResult = DialogResult.OK;
		}

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			DesktopRuntime.Current.SaveMicrosoftSystemInformation(this);
			DialogResult = DialogResult.OK;
		}
	}
}
