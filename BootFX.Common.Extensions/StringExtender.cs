// BootFX - Application framework for .NET applications
// 
// File: StringExtender.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Text;

namespace BootFX.Common.Data
{
    public static class StringExtender
    {
        public static int ToInt32(this string buf)
        {
            return ConversionHelper.ToInt32(buf, Cultures.System);
        }

        public static long ToInt64(this string buf)
        {
            return ConversionHelper.ToInt64(buf, Cultures.System);
        }

        public static decimal ToDecimal(this string buf)
        {
            return ConversionHelper.ToDecimal(buf, Cultures.System);
        }

        public static double ToDouble(this string buf)
        {
            return ConversionHelper.ToDouble(buf, Cultures.System);
        }

        public static float ToSingle(this string buf)
        {
            return ConversionHelper.ToSingle(buf, Cultures.System);
        }

        public static bool ToBoolean(this string buf)
        {
            return ConversionHelper.ToBoolean(buf, Cultures.System);
        }
    }
}
