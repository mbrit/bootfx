// BootFX - Application framework for .NET applications
// 
// File: LayoutHint.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;
using System.Globalization;
using BootFX.Common.Data.FlexData;
using BootFX.Common.Entities;

namespace BootFX.Common.UI
{
	/// <summary>
	/// Defines an instance of <c>LayoutHint</c>.
	/// </summary>
	[Serializable()]
	public class LayoutHint
	{
		/// <summary>
		/// Private field to support <see cref="DateTimeFormat"/> property.
		/// </summary>
		private string _dateTimeFormat;
		
		/// <summary>
		/// Private field to support <see cref="FormatHint"/> property.
		/// </summary>
		private string _formatHint;
		
		/// <summary>
		/// Private field to support <see cref="Multiline"/> property.
		/// </summary>
		private bool _multiline = false;
		
		/// <summary>
		/// Private field to support <see cref="MaxLength"/> property.
		/// </summary>
		private int _maxLength = -1;
		
		public const int SinglelineLimit = 128;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="field"></param>
		public LayoutHint(IFlexField field)
		{
			if(field == null)
				throw new ArgumentNullException("field");
			
			// is it a string?
			if(field.DBType == DbType.String || field.DBType == DbType.StringFixedLength || 
				field.DBType == DbType.AnsiString || field.DBType == DbType.AnsiStringFixedLength)
			{
				if(!(field.IsLarge))
				{
					if(field.Size <= int.MaxValue)
						_maxLength = (int)field.Size;
					else
						_maxLength = -1;

					// big?
					if(_maxLength > 128 || _maxLength == -1)
						_multiline = true;
				}
				else
				{
					_maxLength = -1;
					_multiline = true;
				}
			}
			else
			{
				if(!(field.IsLarge))
					_maxLength = 64;
				else
					_maxLength = -1;
			}

			// mbr - 19-03-2006 - date format...
			// mbr - 11-09-2006 - date/time...			
//			if(field.DBType == DbType.DateTime)
			if(field.DBType == DbType.DateTime || field.DBType == DbType.Date || field.DBType == DbType.Time)
			{
				CultureInfo user = Cultures.User;
				if(user == null)
					throw new InvalidOperationException("user is null.");

				// switch...
				switch(field.DBType)
				{
					case DbType.DateTime:
						_formatHint = string.Format("'{0}' or '{0} {1}'", user.DateTimeFormat.ShortDatePattern.ToLower(), user.DateTimeFormat.ShortTimePattern.ToLower());
						_dateTimeFormat = string.Format("{0} {1}", user.DateTimeFormat.ShortDatePattern, user.DateTimeFormat.ShortTimePattern);
						break;

					case DbType.Date:
						_formatHint = string.Format("'{0}'", user.DateTimeFormat.ShortDatePattern.ToLower());
						_dateTimeFormat = user.DateTimeFormat.ShortDatePattern;
						break;

					case DbType.Time:
						_formatHint = string.Format("'{0}'", user.DateTimeFormat.ShortTimePattern.ToLower());
						_dateTimeFormat = user.DateTimeFormat.ShortTimePattern;
						break;

					default:
						throw new NotSupportedException(string.Format("Cannot handle '{0}' ({1}).", field.DBType, field.DBType.GetType()));
				}				
			}
		}

		/// <summary>
		/// Gets the multiline.
		/// </summary>
		public bool Multiline
		{
			get
			{
				return _multiline;
			}
		}

		/// <summary>
		/// Gets the maxlength.
		/// </summary>
		public int MaxLength
		{
			get
			{
				return _maxLength;
			}
		}

		/// <summary>
		/// Gets the formathint.
		/// </summary>
		public string FormatHint
		{
			get
			{
				return _formatHint;
			}
		}

		/// <summary>
		/// Gets the datetimeformat.
		/// </summary>
		public string DateTimeFormat
		{
			get
			{
				return _dateTimeFormat;
			}
		}
	}
}
