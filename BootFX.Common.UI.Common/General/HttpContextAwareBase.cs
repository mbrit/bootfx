// BootFX - Application framework for .NET applications
// 
// File: HttpContextAwareBase.cs
// Build: 5.0.61009.900
// 
// An open source project by Matthew Reynolds (@mbrit).  
// Copyright 2001-2018 Matthew Reynolds, Red Piranha Labs Limited,
// Elixia Solutions Limited.  All Rights Reserved.
//
// Licensed under the MIT license.

using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.Collections;
using BootFX.Common.Management;

namespace BootFX.Common.UI
{
	/// <summary>
	/// Describes a utility class that's context aware.
	/// </summary>
	public abstract class HttpContextAwareBase : Loggable
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		protected HttpContextAwareBase()
		{
		}

		/// <summary>
		/// Gets the context.
		/// </summary>
		protected HttpContext Context
		{
			get
			{
				if(HttpContext.Current == null)
					throw new InvalidOperationException("Context is null.");
				return HttpContext.Current;
			}
		}

		/// <summary>
		/// Gets the current handler.
		/// </summary>
		// mbr - 24-01-2008 - added.
		protected IHttpHandler Handler
		{
			get
			{
				if(Context == null)
					throw new InvalidOperationException("Context is null.");
				return this.Context.Handler;
			}
		}

		/// <summary>
		/// Gets the current page.
		/// </summary>
		// mbr - 24-01-2008 - added.
		protected Page Page
		{
			get
			{
				return this.Handler as Page;
			}
		}

		/// <summary>
		/// Gets the request.
		/// </summary>
		protected HttpRequest Request
		{
			get
			{
				if(Context == null)
					throw new InvalidOperationException("Context is null.");
				HttpRequest request = Context.Request;
				if(request == null)
					throw new InvalidOperationException("request is null.");
				return request;
			}
		}

		/// <summary>
		/// Gets the response.
		/// </summary>
		protected HttpResponse Response
		{
			get
			{
				if(Context == null)
					throw new InvalidOperationException("Context is null.");
				HttpResponse response = Context.Response;
				if(response == null)
					throw new InvalidOperationException("response is null.");
				return response;
			}
		}

		/// <summary>
		/// Gets the response.
		/// </summary>
		protected HttpServerUtility Server
		{
			get
			{
				if(Context == null)
					throw new InvalidOperationException("Context is null.");
				HttpServerUtility server = Context.Server;
				if(server == null)
					throw new InvalidOperationException("server is null.");
				return server;
			}
		}
	
		/// <summary>
		/// Gets the response.
		/// </summary>
		protected HttpSessionState Session
		{
			get
			{
				if(Context == null)
					throw new InvalidOperationException("Context is null.");
				HttpSessionState state = Context.Session;
				if(state == null)
					throw new InvalidOperationException("state is null.");
				return state;
			}
		}

		/// <summary>
		/// Gets the host name.
		/// </summary>
		protected string Host
		{
			get
			{
				if(Request == null)
					throw new InvalidOperationException("Request is null.");
				if(Request.Url == null)
					throw new InvalidOperationException("Request.Url is null.");
				return this.Request.Url.Host;
			}
        }

        // mbr - 2010-04-16 - added...
        protected IDictionary Items
        {
            get
            {
                if (Context == null)
                    throw new InvalidOperationException("Context is null.");
                return Context.Items;
            }
        }

        public bool HasContext
        {
            get
            {
                if (HttpContext.Current != null)
                    return true;
                else
                    return false;
            }
        }
	}
}
